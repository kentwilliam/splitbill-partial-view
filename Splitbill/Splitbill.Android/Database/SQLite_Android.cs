﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Splitbill.Database;
using SplitBill.Droid.Database;
using Xamarin.Forms;

[assembly:Dependency(typeof(SQLite_Android))]

namespace SplitBill.Droid.Database
{
    class SQLite_Android : ISQLite
    {
        public SQLite_Android() { }
        public SQLite.SQLiteConnection GetConnection() {
            var sqlitefilename = "DB.db3";
            String documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentPath,sqlitefilename);
            var conn = new SQLite.SQLiteConnection(path);
            return conn;
        }
    }
}