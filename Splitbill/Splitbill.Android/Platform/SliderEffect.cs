﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Splitbill.Droid.Platform;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("EffectsSample")]
[assembly: Xamarin.Forms.ExportEffect(typeof(SliderEffect), "BlueSliderEffect")]
namespace Splitbill.Droid.Platform
{
    public class SliderEffect:PlatformEffect
    {
        public SliderEffect()
        {
        }

        protected override void OnAttached()
        {
            var seekBar = (SeekBar)Control;
            
            seekBar.ProgressDrawable.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Blue.ToAndroid(), PorterDuff.Mode.Multiply));
            seekBar.Thumb.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Blue.ToAndroid(), PorterDuff.Mode.SrcIn));
        }

        protected override void OnDetached()
        {
            throw new NotImplementedException();
        }
    }
}