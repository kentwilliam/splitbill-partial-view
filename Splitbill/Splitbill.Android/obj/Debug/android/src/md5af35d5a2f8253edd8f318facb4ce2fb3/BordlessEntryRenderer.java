package md5af35d5a2f8253edd8f318facb4ce2fb3;


public class BordlessEntryRenderer
	extends md51558244f76c53b6aeda52c8a337f2c37.EntryRenderer
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Bill.Droid.Renderers.BordlessEntryRenderer, Splitbill.Android", BordlessEntryRenderer.class, __md_methods);
	}


	public BordlessEntryRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == BordlessEntryRenderer.class)
			mono.android.TypeManager.Activate ("Bill.Droid.Renderers.BordlessEntryRenderer, Splitbill.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public BordlessEntryRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == BordlessEntryRenderer.class)
			mono.android.TypeManager.Activate ("Bill.Droid.Renderers.BordlessEntryRenderer, Splitbill.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public BordlessEntryRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == BordlessEntryRenderer.class)
			mono.android.TypeManager.Activate ("Bill.Droid.Renderers.BordlessEntryRenderer, Splitbill.Android", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
