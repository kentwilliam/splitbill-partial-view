package md5bd503e40ce6105a992b09a5ea69db03d;


public class AlerDialogShowListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.content.DialogInterface.OnShowListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onShow:(Landroid/content/DialogInterface;)V:GetOnShow_Landroid_content_DialogInterface_Handler:Android.Content.IDialogInterfaceOnShowListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("Syncfusion.SfImageEditor.Android.AlerDialogShowListener, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", AlerDialogShowListener.class, __md_methods);
	}


	public AlerDialogShowListener ()
	{
		super ();
		if (getClass () == AlerDialogShowListener.class)
			mono.android.TypeManager.Activate ("Syncfusion.SfImageEditor.Android.AlerDialogShowListener, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public AlerDialogShowListener (android.app.AlertDialog p0)
	{
		super ();
		if (getClass () == AlerDialogShowListener.class)
			mono.android.TypeManager.Activate ("Syncfusion.SfImageEditor.Android.AlerDialogShowListener, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", "Android.App.AlertDialog, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public void onShow (android.content.DialogInterface p0)
	{
		n_onShow (p0);
	}

	private native void n_onShow (android.content.DialogInterface p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
