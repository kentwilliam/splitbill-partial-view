package md5bd503e40ce6105a992b09a5ea69db03d;


public class CustomAdapter
	extends android.widget.BaseAdapter
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_getCount:()I:GetGetCountHandler\n" +
			"n_getView:(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;:GetGetView_ILandroid_view_View_Landroid_view_ViewGroup_Handler\n" +
			"n_getItem:(I)Ljava/lang/Object;:GetGetItem_IHandler\n" +
			"n_getItemId:(I)J:GetGetItemId_IHandler\n" +
			"";
		mono.android.Runtime.register ("Syncfusion.SfImageEditor.Android.CustomAdapter, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", CustomAdapter.class, __md_methods);
	}


	public CustomAdapter ()
	{
		super ();
		if (getClass () == CustomAdapter.class)
			mono.android.TypeManager.Activate ("Syncfusion.SfImageEditor.Android.CustomAdapter, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public CustomAdapter (android.content.Context p0, int p1, int[] p2, java.lang.String[] p3)
	{
		super ();
		if (getClass () == CustomAdapter.class)
			mono.android.TypeManager.Activate ("Syncfusion.SfImageEditor.Android.CustomAdapter, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.Int32[], mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.String[], mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2, p3 });
	}

	public CustomAdapter (android.content.Context p0, int p1, int[] p2)
	{
		super ();
		if (getClass () == CustomAdapter.class)
			mono.android.TypeManager.Activate ("Syncfusion.SfImageEditor.Android.CustomAdapter, Syncfusion.SfImageEditor.XForms.Android, Version=16.1451.0.26, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.Int32[], mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public int getCount ()
	{
		return n_getCount ();
	}

	private native int n_getCount ();


	public android.view.View getView (int p0, android.view.View p1, android.view.ViewGroup p2)
	{
		return n_getView (p0, p1, p2);
	}

	private native android.view.View n_getView (int p0, android.view.View p1, android.view.ViewGroup p2);


	public java.lang.Object getItem (int p0)
	{
		return n_getItem (p0);
	}

	private native java.lang.Object n_getItem (int p0);


	public long getItemId (int p0)
	{
		return n_getItemId (p0);
	}

	private native long n_getItemId (int p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
