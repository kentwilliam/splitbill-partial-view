﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Rivets;
using Android.Content.PM;
using ImageCircle.Forms.Plugin.Droid;
using SplitBill.Droid;
using SplitBill;

namespace Splitbill.Droid
{
    [Activity(Label = "Bill", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    [IntentFilter(new[] { Android.Content.Intent.ActionView },
    DataScheme = "Urunan",
    DataHost = "products",
    Categories = new[] { Android.Content.Intent.CategoryDefault, Android.Content.Intent.CategoryBrowsable })]
    public class ProductActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(savedInstanceState);

            //SetContentView(Resource.Layout.ProductLayout);
            String Username1 = "";
            String Token1 = "";
            try
            {
                var hasil = Intent.DataString;
                int t1 = hasil.IndexOf('=');
                int t2 = hasil.IndexOf('&');
                var tempToken = hasil.Substring(t1+1, (t2-t1)-1);
                var tempUsername = hasil.Substring(hasil.LastIndexOf("=")+1);
                Token1 = tempToken;
                Username1 = tempUsername;
            }
            catch (Exception ex)
            { }
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            ImageCircleRenderer.Init();
            App APP = new App();
            APP.StartForgetPassword(Token1, Username1);
            LoadApplication(APP);
        }
        void callXamarinForm()
        {

        }
    }
}