﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Bill;
using Xamarin.Forms.Platform.Android;
using Bill.Droid.Renderers;
using SplitBill;
using Android.Graphics;

[assembly: ExportRenderer(typeof(Entry), typeof(SoftEntryRenderer))]

namespace Bill.Droid.Renderers
{
    public class SoftEntryRenderer:EntryRenderer
    {
        public SoftEntryRenderer(Context context) : base(context)
        {
            AutoPackage = false;
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
        }
    }
}