﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Bill;
using Xamarin.Forms.Platform.Android;
using Bill.Droid.Renderers;
using Splitbill;

[assembly: ExportRenderer(typeof(Editor), typeof(BordlessEditorRenderer))]

namespace Bill.Droid.Renderers
{
    public class BordlessEditorRenderer:EditorRenderer
    {
        public BordlessEditorRenderer(Context context) : base(context)
        {
            AutoPackage = false;
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            Control.TextAlignment = Android.Views.TextAlignment.Center;
            if (e.OldElement == null)
            {
                Control.Background = null;
            }
        }
    }
}