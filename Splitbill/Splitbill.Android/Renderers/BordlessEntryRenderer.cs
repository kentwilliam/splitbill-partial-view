﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Bill;
using Xamarin.Forms.Platform.Android;
using Bill.Droid.Renderers;
using Splitbill;

[assembly: ExportRenderer(typeof(ExtendedEntry), typeof(BordlessEntryRenderer))]

namespace Bill.Droid.Renderers
{
    public class BordlessEntryRenderer:EntryRenderer
    {
        public BordlessEntryRenderer(Context context) : base(context)
        {
            AutoPackage = false;
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            Control.TextAlignment = Android.Views.TextAlignment.Gravity;
            
            if (e.OldElement == null)
            {
                Control.Background = null;
            }
        }
    }
}