﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SplitBill.Droid.Renderers.BacktoNormal;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(Xamarin.Forms.Button), typeof(NormalButtonRenderer))]
namespace SplitBill.Droid.Renderers.BacktoNormal
{
    class NormalButtonRenderer:Xamarin.Forms.Platform.Android.ButtonRenderer
    {
        public NormalButtonRenderer(Context context) : base(context)
        {
            AutoPackage = false;
        }
    }
}