﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Bill.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using System.ComponentModel;
using Bill;
using Splitbill;

[assembly: ExportRenderer(typeof(NavExtendedViewcell), typeof(ViewCellItemSelectedCustomRenderer))]
namespace Bill.Droid.Renderers
{
    public class ViewCellItemSelectedCustomRenderer:ViewCellRenderer
    {
        private Android.Views.View _cellCore;
        private Drawable _unselectedBackground;
        private bool _selected;

        protected override Android.Views.View GetCellCore(Cell item,
                                                             Android.Views.View convertView,
                                                             ViewGroup parent,
                                                             Context context)
        {
            _cellCore = base.GetCellCore(item, convertView, parent, context);

            _selected = false;
            _unselectedBackground = _cellCore.Background;

            return _cellCore;
        }

        protected override void OnCellPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            base.OnCellPropertyChanged(sender, args);
            if (args.PropertyName == "IsSelected")
            {
                _selected = !_selected;
                if (_selected)
                {
                    _cellCore.SetBackgroundColor(Android.Graphics.Color.Rgb(49, 56, 72));
                }
                else
                {
                    _cellCore.SetBackgroundColor(Android.Graphics.Color.Transparent);
                }
            }
            else
            {
                _cellCore.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }
    }
}