﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Splitbill;
using Splitbill.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MySlider), typeof(MySliderRenderer))]
namespace Splitbill.Droid.Renderers
{
    public class MySliderRenderer:SliderRenderer
    {
        public MySliderRenderer()
        {

        }
        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            SeekBar skb = Control;
            Drawable thumb = skb.Thumb;
            int thumbtop = (skb.Height / 2 - thumb.IntrinsicHeight / 2);
            thumb.SetBounds(thumb.Bounds.Left-12,thumbtop-12,thumb.Bounds.Left+thumb.IntrinsicWidth+12,thumbtop+thumb.IntrinsicHeight+12);
            skb.SetThumb(thumb);
            
            Control.SetThumb(thumb);
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Slider> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                var progress = new PaintDrawable(Android.Graphics.Color.Rgb(57,169,251));
                progress.SetCornerRadius(10);
                progress.SetIntrinsicHeight(15);
                var progressClip =
                    new ClipDrawable(progress, GravityFlags.Left,
                    ClipDrawableOrientation.Horizontal);

                // secondary progress
                var secondary = new PaintDrawable(Android.Graphics.Color.Gray);
                secondary.SetCornerRadius(10);
                secondary.SetIntrinsicHeight(15);
                var secondaryProgressClip =
                    new ClipDrawable(secondary, GravityFlags.Left,
                    ClipDrawableOrientation.Horizontal);

                // background
                PaintDrawable background = new
                             PaintDrawable(Android.Graphics.Color.LightGray);
                background.SetCornerRadius(10);
                background.SetIntrinsicHeight(15);

                // retrieve LayerDrawable reference of the SeekBar control
                LayerDrawable layeredDrawableReference
                    = (LayerDrawable)Control.ProgressDrawable;

                // apply our custom drawable objects to the 
                // given sub-views through their IDs
                layeredDrawableReference.
                    SetDrawableByLayerId(
                    Android.Resource.Id.Background, background);
                layeredDrawableReference.
                    SetDrawableByLayerId(
                    Android.Resource.Id.Progress, progressClip);
                layeredDrawableReference.
                    SetDrawableByLayerId(
                    Android.Resource.Id.SecondaryProgress,
                              secondaryProgressClip);

            }
        }
    }
}