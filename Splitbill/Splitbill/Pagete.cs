﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill
{
    public class Pagete : ContentPage
    {
        public EventHandler Eve;
        public bool isConnected;
        public Pagete(String url)
        {
            WebView browser = new WebView();
            
            browser.Source = new UrlWebViewSource
            {
                Url = url
            };
            browser.VerticalOptions = LayoutOptions.FillAndExpand;
            browser.Navigated += Browser_Navigated;
            Content = new StackLayout
            {
                Children = { browser }
            };
        }

        private async void Browser_Navigated(object sender, WebNavigatedEventArgs e)
        {
            var res = e.Result;
            var r1 = e.Source;
            var r2 = e.Url;
            var r3 = e.NavigationEvent;
            if (e.Url.ToLower().Contains("callback"))
            {
                isConnected = true;
                Eve(this,EventArgs.Empty);
            }
            //throw new NotImplementedException();
        }
    }
}