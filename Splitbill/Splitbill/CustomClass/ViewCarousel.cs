﻿using Xamarin.Forms;

namespace SplitBill.Views
{
    public class ViewCarousel : ContentView
    {
        public ViewCarousel()
        {
            Grid WrapperGrid = new Grid();
            WrapperGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            WrapperGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Image img = new Image();
            img.HorizontalOptions = LayoutOptions.FillAndExpand;
            img.VerticalOptions = LayoutOptions.FillAndExpand;
            img.Aspect = Aspect.AspectFill;
            img.SetBinding(Image.SourceProperty, "ImageSource");

            WrapperGrid.Children.Add(img);
            Content = WrapperGrid;
        }
    }
}