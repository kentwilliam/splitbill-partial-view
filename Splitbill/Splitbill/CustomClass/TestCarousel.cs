﻿
using Syncfusion.XForms.TabView;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill
{
    public class TestCarousel:ContentPage
    {
        public TestCarousel()
        {
            Label but1 = new Label()
            {
                BackgroundColor = Color.Transparent,
                HorizontalTextAlignment = TextAlignment.End,
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium,typeof(Label)),
                Text = "Grid1",
                TextColor = Color.Black,
                VerticalTextAlignment = TextAlignment.Center
            };
            
            SfTabView tabv = new SfTabView();
            but1.GestureRecognizers.Add(new TapGestureRecognizer((v, e) => { tabv.SelectedIndex = 0; }));
            tabv.Margin = new Thickness(0, 150, 0, 0);
            var Grid1 = new Grid() { BackgroundColor = Color.Red};
            var Grid2 = new Grid() { BackgroundColor = Color.Green };
            var Grid3 = new Grid() { BackgroundColor = Color.Blue };

            ListView lv = new ListView();
            lv.ItemsSource = new List<String>() { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "23", "44", "54", "aa", "46", "s", "d", "f", "g", "h", "j" };

            ListView lv1 = new ListView();
            lv1.ItemsSource = new List<String>() { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10","23","44", "54","aa", "46","s","d","f","g","h","j" };

       
            Grid1.Children.Add(lv);
            Grid2.Children.Add(lv1);
            var tabItems = new TabItemCollection()
            {
                new SfTabItem(){
                    Title = "Grid1",
                    Content = Grid1,
                    HeaderContent = but1
                },
                new SfTabItem(){
                    Title = "Grid2",
                    Content = Grid2
                },
            };
            tabv.Items = tabItems;
            tabv.VisibleHeaderCount = 2;
            Content = tabv;
        }
    }
}
