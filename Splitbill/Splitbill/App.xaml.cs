﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Splitbill.Model.MVVM;
using Splitbill.Model.User;
using Splitbill.Pages;
using Splitbill.Pages.Login;
using Splitbill.Popup.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill
{
    public partial class App : Application
    {
        public static int ctr;
        public static HomePage MyHomePage;
        public static Splitbill.Database.UserDatabase MyUserDatabase;
        public static Api.Core.CoreApi _Api;
        public static UserData GlobalDataUser { get; set; }

        public static float DpiFontSize {get;set;}
        public static Double Resolution { get; set; }
        public static double Width { get; set; }
        public static double Height { get; set; }
        public static GlobalLabel GlobalabelProp { get; set; }
        public static double Multiplier { get; set; }
        public static Double NormalText { get; set; }
        public static Double LargeText { get; set; }
        public static Double SmallText { get; set; }
        public static Double IconSize { get; set; }
        public static double SMallIconSize { get; set; }
        public static double XXLargeText { get; set; }
        public static List<String> PathGambar;


        public static bool RequestFromGroup { get; set; }
        public static string GroupId { get; set; }
        public App ()
		{
			InitializeComponent();
            MyUserDatabase = new Database.UserDatabase();
            
            _Api = new Api.Core.CoreApi();

            Com.OneSignal.Abstractions.NotificationReceived exampleNotificationReceivedDelegate = delegate (OSNotification result)
            {
                var m = result.payload.body;
                Dictionary<string, object> additionalData = result.payload.additionalData;
                try
                {
                    App.GlobalDataUser.BALANCE = "Rp. " + (Convert.ToDouble(additionalData["balance"] + "").ToString("n0"));
                }
                catch (System.Exception e) { }

                try
                {
                    App.GlobalDataUser.NOTIFICATIONCOUNTER = additionalData["notification"] + "";
                }
                catch (Exception ex) { }
                try
                {
                    if (Convert.ToInt32(additionalData["notification"] + "") > 0)
                    {
                        App.GlobalDataUser.NOTIFICATIONSHOW = true;
                    }
                    else
                    {
                        App.GlobalDataUser.NOTIFICATIONSHOW = false;
                    }
                }
                catch (Exception ex) { }
            };
            Com.OneSignal.Abstractions.NotificationOpened exampleNotificationOpenedDelegate = delegate (OSNotificationOpenedResult result)
            {
                var m = result.notification.payload.body;
                Dictionary<String, object> additionalData = result.notification.payload.additionalData;

                try
                {
                    var value1 = additionalData["type"];
                    var value2 = additionalData["id_ori"];
                    if (value1.ToString().ToLower() == "req")
                    {
                        App.MyHomePage.ToNotificationOpenRequest(value2.ToString(), "req");
                    }
                    else if (value1.ToString().ToLower() == "send")
                    {
                        App.MyHomePage.ToNotificationOpenSend(value2.ToString(), "snd");
                    }
                    else if (value1.ToString().ToLower() == "reqf")
                    {
                        App.MyHomePage.ToNotificationOpenRequestPaid(value2.ToString(), "req");
                    }
                }
                catch (Exception ex)
                {

                }
                // App.globalMainPage.OpenTransaction(value1.ToString(), value2.ToString());
            };

            OneSignal.Current.StartInit("da3c12f9-0295-4d85-9778-712e6c2da674").HandleNotificationOpened(exampleNotificationOpenedDelegate).HandleNotificationReceived(exampleNotificationReceivedDelegate).InFocusDisplaying(Com.OneSignal.Abstractions.OSInFocusDisplayOption.InAppAlert).EndInit();
            App.Resolution = 150;
            //MainPage = new LoginPage();
        }

        public void StartForgetPassword(String token, String username)
        {
            MainPage = new Splitbill.Pages.ForgotPasswordAppLink.ForgotPassword(token, username);
        }
        public void StartMainPage()
        {
            IntroPage intro = new IntroPage();
            LoginPage login = new LoginPage();
            var data = MyUserDatabase.GetUser();
            if (data != null)
            {
                //login.SetUserNamePassword(data.Username, data.Password);
                MainPage = login;
            }
            else
            {
                MainPage = intro;
            }
            
        }
        
        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
            // Handle when your app sleeps
            String xxx = "";
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
