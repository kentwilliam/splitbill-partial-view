﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using Splitbill.Model.Notification;
using Splitbill.Model.RequestModel;
using Splitbill.Popup.Notification;
using Splitbill.Popup;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace Splitbill.Popup.TrackingBill
{
    public class TrackingBillDone:PopupPage
    {
        Label totalText;
        Label totalValue;
        SfListView sflist;
        SKShader GradientShaderTest;
        NotificationOpenScheduleModel modelForSchedule;
        List<NotificationOpenSfListModel> req;
        CheckBox check;
        String IDDetail;
        Button payment;
        Image PreviewImage;
        String ImageString;
        SendNotificationDetailJson nota;
        Label datetimeLabel = new Label();
        Splitbill.Views.Loading.Loading loadingBar;
        public void InitData()
        {
            Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await PopupNavigation.PushAsync(new Splitbill.Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });

                req.Clear();
                var resp = App._Api.GetDataResponseFromServer("transaction/transaction_detail", JsonConvert.SerializeObject(nota));
                String result = resp.Result;
                if (result.ToLower() == "no internet")
                {
                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        await DisplayAlert("","No Internet Connection","Ok");
                    });
                }
                else
                {
                    var resu = JsonConvert.DeserializeObject<TransactionDetailJsonResponse_request>(resp.Result);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (!String.IsNullOrEmpty(resu.message))
                        {
                            if (resu.message.ToLower().Contains("expired"))
                            {
                                await Task.Delay(200);
                                App.MyHomePage.LogOut();
                            }
                        }
                        else
                        {
                            IDDetail = resu.request.idMoney_request_detail;
                            #region masukan Data Schedule
                            modelForSchedule = new NotificationOpenScheduleModel();
                            modelForSchedule.token = App.GlobalDataUser.TOKEN;
                            modelForSchedule.username = App.GlobalDataUser.USERNAME;
                            modelForSchedule.clicked = false;
                            modelForSchedule.blueColor = Color.Gray;
                            modelForSchedule.id_Detail = resu.request.idMoney_request_detail;
                            if (resu.request.scheduled_paid_date == "" || resu.request.scheduled_paid_date == "0000-00-00" || String.IsNullOrEmpty(resu.request.scheduled_paid_date))
                            {
                                modelForSchedule.dateSetting = DateTime.Now;
                            }
                            else
                            {
                                modelForSchedule.dateSetting = DateTime.Parse(resu.request.scheduled_paid_date);
                                modelForSchedule.blueColor = Color.FromRgb(57, 169, 251);
                                modelForSchedule.clicked = true;
                            }
                            modelForSchedule.description = resu.request.scheduled_desc;
                            #endregion
                            #region Masukan Data Listview
                            var penerima = resu.request.reciever_id;
                            var dataMenuAssign = resu.menu_assign;
                            var daftarMakanan = dataMenuAssign.Select(x => new { x.name, x.photo_menu }).Distinct();
                            NotificationOpenSfListModel m;
                            foreach (var i in daftarMakanan)
                            {
                                m = new NotificationOpenSfListModel();
                                m.NAMAMAKANAN = i.name;
                                m.TOTALMENU = "Rp " + (dataMenuAssign.Where(x => x.name == i.name).FirstOrDefault().amount).ToString("n0");
                                m.photo = i.photo_menu;
                                FormattedString str = new FormattedString();
                                var dataTeman = dataMenuAssign.Where(x => x.name == i.name);
                                foreach (var nama in dataTeman)
                                {
                                    if (nama.reciever_id.ToLower() == penerima.ToLower())
                                    {
                                        str.Spans.Add(new Span()
                                        {
                                            Text = nama.reciever_id + ",",
                                            FontSize = App.DpiFontSize *6
                                            ,
                                            FontAttributes = FontAttributes.Bold,
                                            ForegroundColor = Color.Black
                                        });
                                    }
                                    else
                                    {
                                        str.Spans.Add(new Span()
                                        {
                                            Text = nama.reciever_id + ",",
                                            FontSize = App.DpiFontSize * 6,
                                            ForegroundColor = Color.LightGray
                                        });
                                    }
                                }
                                m.FRIENDS = str;
                                var amountUser = (dataMenuAssign.Where(x => x.name == i.name && x.reciever_id.ToLower() ==penerima.ToLower()).FirstOrDefault());
                                m.MONEYTOPAY = amountUser == null ? "" : "Rp " + amountUser.amount_user.ToString("n0");
                                req.Add(m);
                            }
                            #endregion

                            totalText.Text = "Total Rp: ";
                            totalValue.Text = resu.request.amount_after.ToString("n0");
                            sflist.ItemsSource = null;
                            sflist.ItemsSource = req;
                            

                            resp.Dispose();
                            datetimeLabel.Text = !String.IsNullOrEmpty(resu.request.scheduled_paid_date) ? DateTime.Parse(resu.request.scheduled_paid_date).ToString("dd/MM/yyyy") : "";
                            if (!String.IsNullOrEmpty(resu.request.bill_photo))
                            {
                                ImageString = "http://www.urunan.id/splitbill/" + resu.request.bill_photo;
                                PreviewImage.Source = "PhotoSign.png";
                                PreviewImage.IsEnabled = true;
                                PreviewImage.GestureRecognizers.Add(new TapGestureRecognizer(PhotoClick));
                            }
                            else
                            {
                                PreviewImage.IsVisible = false;
                                PreviewImage.IsEnabled = false;
                            }
                        }
                    });
                }
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Task.Delay(300);
                    try
                    { await PopupNavigation.PopAsync(); }
                    catch (Exception ex) { }
                    
                });
            });
        }

        private async void PhotoClick(View arg1, object arg2)
        {
            ImageSource src = ImageString;
            Splitbill.Popup.Request.PopupPreviewImage previmg = new Request.PopupPreviewImage(src);
            await PopupNavigation.PushAsync(previmg);
        }

        public TrackingBillDone(SendNotificationDetailJson nota)
        {
            loadingBar = new Views.Loading.Loading() { TranslationX = 1000 };
            req = new List<NotificationOpenSfListModel>();
            this.nota = nota;
            Grid mygrid = new Grid();
            mygrid.RowSpacing = 0;
            mygrid.BackgroundColor = Color.FromRgb(234, 237, 244);
            for (int i = 0; i < 16; i++)
            { mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }); }

            #region Buat Header
            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;

            mygrid.Children.Add(viewMelengkung, 0, 0);
            Grid.SetRowSpan(viewMelengkung, 5);

            CachedImage ciImg = new CachedImage()
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = new Thickness(0, 10, 0, 0),
                WidthRequest = 90,
                HeightRequest = 90,
                LoadingPlaceholder = "GetImage",
                DownsampleToViewSize = true,
            };
            ciImg.Transformations.Add(new CircleTransformation());
            ciImg.SetBinding(CachedImage.SourceProperty, "PHOTO");
            ciImg.BindingContext = App.GlobalDataUser;

            mygrid.Children.Add(ciImg, 0, 0);
            Grid.SetRowSpan(ciImg, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize =App.DpiFontSize * 8, FontAttributes = FontAttributes.Bold, TextColor = Color.White };
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;
            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize * 8, VerticalTextAlignment = TextAlignment.Center, TextColor = Color.White };
            jumlah.SetBinding(Label.TextProperty, "BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);
            #endregion

            StackLayout DetailInfo = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                Spacing = 1,
                Margin = new Thickness(10)
            };
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal};
            StackLayout tab3_4 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab4 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab5 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            #region tab1
            StackLayout LeftSubTab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,HorizontalOptions = LayoutOptions.StartAndExpand};
            StackLayout RightSubTab1 = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.EndAndExpand };
            Image gambarnota = new Image() {
                Source = "MyTransaction.png",
                Aspect = Aspect.AspectFit,
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
            };
            Label descriptionLabel = new Label()
            {
                Text="Description",
                FontSize = App.DpiFontSize * 8,
                VerticalOptions = LayoutOptions.Center
            };
            LeftSubTab1.Children.Add(gambarnota);
            LeftSubTab1.Children.Add(descriptionLabel);
            tab1.Children.Add(LeftSubTab1);

            PreviewImage = new Image()
            {
                Aspect = Aspect.AspectFit,
                Source = "TakePhoto.png",
                IsVisible = true,
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15
            };
            datetimeLabel = new Label() {
                FontSize = App.DpiFontSize * 6,
                VerticalOptions = LayoutOptions.Center
            };
            RightSubTab1.Children.Add(datetimeLabel);
            RightSubTab1.Children.Add(PreviewImage);
            tab1.Children.Add(RightSubTab1);

            #endregion
            #region Tab3
            DataTemplate MenuDataTemplate = new DataTemplate(() =>
            {
                ImageCircle.Forms.Plugin.Abstractions.CircleImage img = new ImageCircle.Forms.Plugin.Abstractions.CircleImage();
                Label Nama = new Label();
                Label Friends = new Label() {
                    FontSize = App.DpiFontSize * 6,
                };
                Label Inputan = new Label() {
                    FontSize = App.DpiFontSize * 6
                };
                Label moneyToPay = new Label() {
                    FontSize = App.DpiFontSize * 6
                };

                Inputan.VerticalOptions = LayoutOptions.FillAndExpand;
                Inputan.HorizontalOptions = LayoutOptions.Fill;
                Inputan.FontSize = App.DpiFontSize * 6;
                Inputan.TextColor = Color.LightGray;
                Inputan.Margin = new Thickness(0, 0, 5, 0);
                Grid HorizontalGrid = new Grid();
                StackLayout WrapperStack = new StackLayout();

                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.3, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.2, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.2, GridUnitType.Star) });

                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5, GridUnitType.Star) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5, GridUnitType.Star) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });

                BoxView bb = new BoxView() { BackgroundColor = Color.LightGray };
                HorizontalGrid.Children.Add(bb, 0, 0);
                Grid.SetColumnSpan(bb, 4);

                BoxView bb1 = new BoxView() { BackgroundColor = Color.LightGray };
                HorizontalGrid.Children.Add(bb1, 0, 3);
                Grid.SetColumnSpan(bb1, 4);

                img.Source = "DC.png";
                img.BorderColor = Color.Gray;
                img.BorderThickness = 1;
                img.HeightRequest = 50;
                img.WidthRequest = 50;
                img.Aspect = Aspect.AspectFill;
                img.HorizontalOptions = LayoutOptions.Center;
                img.VerticalOptions = LayoutOptions.Center;

                Nama.HorizontalOptions = LayoutOptions.Fill;
                Nama.VerticalOptions = LayoutOptions.FillAndExpand;
                Nama.FontSize = App.DpiFontSize * 6;
                Nama.HeightRequest = 100;
                Nama.TextColor = Color.Black;
                Nama.VerticalTextAlignment = TextAlignment.End;

                Nama.TextColor = Color.Gray;
                img.SetBinding(Image.SourceProperty, "photo");
                Nama.SetBinding(Label.TextProperty, "NAMAMAKANAN");
                Friends.SetBinding(Label.FormattedTextProperty, "FRIENDS");
                Inputan.SetBinding(Label.TextProperty, "TOTALMENU");
                moneyToPay.SetBinding(Label.TextProperty, "MONEYTOPAY");

                HorizontalGrid.Children.Add(img, 0, 1);
                Grid.SetRowSpan(img, 2);

                HorizontalGrid.Children.Add(Nama, 1, 1);

                StackLayout l = new StackLayout();
                l.Margin = new Thickness(0, 3, 0, 0);
                l.Orientation = StackOrientation.Vertical;
                Friends.TextColor = Color.LightGray;
                Friends.VerticalTextAlignment = TextAlignment.End;
                Friends.FontSize = 11;
                l.Children.Add(Friends);

                HorizontalGrid.Children.Add(l, 1, 2);
                Grid.SetColumnSpan(l, 2);

                StackLayout l1 = new StackLayout();
                l1.Margin = new Thickness(0, 5, 10, 0);
                moneyToPay.FontAttributes = FontAttributes.Bold;
                moneyToPay.TextColor = Color.Black;
                moneyToPay.VerticalOptions = LayoutOptions.Center;
                moneyToPay.FontSize = App.DpiFontSize * 6;
                l1.Children.Add(moneyToPay);

                HorizontalGrid.Children.Add(l1, 3, 2);

                Inputan.VerticalTextAlignment = TextAlignment.End;
                HorizontalGrid.Children.Add(Inputan, 2, 1);

                WrapperStack.Children.Add(HorizontalGrid);
                return new ViewCell { View = WrapperStack };
            });
            sflist = new SfListView();
            sflist.ItemTemplate = MenuDataTemplate;
            sflist.SelectionMode = SelectionMode.None;
            sflist.ItemSize = 85;
            tab3.Children.Add(sflist);
            #endregion

            #region tab3_4
            StackLayout subTab3_4 = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.EndAndExpand };
            totalText = new Label()
            {
                Text = "Total : ",
                FontSize = App.DpiFontSize * 6,
                VerticalOptions = LayoutOptions.Center
            };
            totalValue = new Label() {
                Text = "00000000",
                FontSize = App.DpiFontSize * 6,
                VerticalOptions = LayoutOptions.Center,
                WidthRequest = 80,
            };
            subTab3_4.Children.Add(totalText);
            subTab3_4.Children.Add(totalValue);
            tab3_4.Children.Add(subTab3_4);
            #endregion

            DetailInfo.Children.Add(tab1);
            DetailInfo.Children.Add(sflist);
            DetailInfo.Children.Add(tab3_4);

            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                BorderWidth = 1,
                CornerRadius = 15,
                ShapeType = XFShapeView.ShapeType.Box,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                BorderColor = Color.Transparent,
                Color = Color.White,
                Margin = new Thickness(10, 10, 10, 0)
            };
            wrapper.Content = DetailInfo;
            mygrid.Children.Add(wrapper, 0, 5);
            Grid.SetRowSpan(wrapper, 11);

            mygrid.Children.Add(loadingBar);
            Grid.SetRowSpan(loadingBar,16);

            Content = mygrid;
        }

        protected override Task OnAppearingAnimationEnd()
        {
            InitData();
            return base.OnAppearingAnimationEnd();
        }
        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
        }
    }
}
