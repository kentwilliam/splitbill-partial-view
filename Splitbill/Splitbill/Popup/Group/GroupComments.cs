﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using Splitbill.Converter;
using Splitbill.Model.Group;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Group
{
    public class GroupComments:PopupPage
    {
        int indexItem = 0;
        GroupCommentsModel datatoEdit;
        PhotoFile data;
        SfListView sflist;
        String ThisUserName = App.GlobalDataUser.USERNAME;
        CachedImage leftImage;
        CachedImage friendEdit;
        Entry CommentEntry;
        bool EditMode;
        List<GroupCommentsModel> modelSfList = new List<GroupCommentsModel>();
        void InitData()
        {
            EditMode = false;
            sflist.ItemsSource = null;
            modelSfList = new List<GroupCommentsModel>();
            GroupCommentGetComment js = new GroupCommentGetComment()
            {
                username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN,
                photo_id = data.PhotoId
            };
            Task.Run(() =>
            {
                var resp = App._Api.GetDataResponseFromServer("group/view_group_photo_comment", JsonConvert.SerializeObject(js));
                var resu = resp.Result;
                if (resu.ToLower() != "no internet")
                {
                    var parse = JsonConvert.DeserializeObject<HeadGroupCommentResponse>(resp.Result);
                    foreach (var d in parse.comment)
                    {
                        modelSfList.Add(new GroupCommentsModel()
                        {
                            Id = d.id,
                            Comment = d.user_id+"#"+d.comment,
                            Time = d.time,
                            UserName = d.user_id,
                            UserPhoto = "http://www.urunan.id/splitbill/" + d.commenter_photo
                        });
                    }
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        sflist.BatchBegin();
                        sflist.ItemsSource = modelSfList;
                        sflist.BatchCommit();
                    });
                }
            });


        }
        public GroupComments(PhotoFile data)
        {
            this.data = data;
            InitView();
        }
        void InitView()
        {
            Grid MainLayout = new Grid() { RowSpacing = 0, ColumnSpacing = 0, BackgroundColor = Color.White };
            MainLayout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(65) });
            MainLayout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1) });
            MainLayout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            MainLayout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1) });
            MainLayout.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });

            StackLayout Header = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                BackgroundColor = Color.White,
            };
            CachedImage Arrow = new CachedImage()
            {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start,
                Margin = new Thickness(10, 0, 0, 0),
                Source = "Left.png"
            };
            Arrow.GestureRecognizers.Add(new TapGestureRecognizer(async (v, e) => {
                await PopupNavigation.PopAsync();
            }));
            Label TitlePage = new Label()
            {
                TextColor = Color.Black,
                FontSize = App.DpiFontSize * 10,
                Margin = new Thickness(20, 0, 0, 0),
                Text = "Comments",
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            BoxView separator1 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 };
            sflist = new SfListView()
            {
                ItemTemplate = ListDataTemplate(),
                RowSpacing = 5,
                AutoFitMode = AutoFitMode.Height
            };
            sflist.RightSwipeTemplate = GetRightSwipeTemplate();
            sflist.AllowSwiping = true;
            sflist.SwipeStarted += Sflist_SwipeStarted;
            sflist.SwipeEnded += Sflist_SwipeEnded;
            BoxView separator2 = new BoxView()
            {
                BackgroundColor = Color.LightGray,
                HeightRequest = 1,
            };

            StackLayout CommentGrid = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 0,
            };
            CachedImage ProfileIcon = new CachedImage()
            {
                WidthRequest = App.DpiFontSize * 25,
                HeightRequest = App.DpiFontSize * 25,
                Transformations = new List<FFImageLoading.Work.ITransformation>() { new CircleTransformation() },
                Source = App.GlobalDataUser.PHOTO,
                HorizontalOptions = LayoutOptions.Start,
                Margin = new Thickness(10, 0, 0, 0)
            };
            CommentEntry = new Entry()
            {
                FontSize = App.DpiFontSize * 7,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            Label CommentPost = new Label()
            {
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 7,
                HorizontalTextAlignment = TextAlignment.Center,
                Text = "POST",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Margin = new Thickness(0, 0, 10, 0)
            };
            CommentEntry.Focused += (async (v, e) => {
                await Task.Delay(400);
                (sflist.LayoutManager as LinearLayout).ScrollToRowIndex(modelSfList.Count);
            });
            CommentPost.GestureRecognizers.Add(new TapGestureRecognizer((v,e)=> {
                if (EditMode)
                {
                    
                    EditComment();
                }
                else
                {
                    
                    PostComment();
                }
            }));
            Header.Children.Add(Arrow);
            Header.Children.Add(TitlePage);
            CommentGrid.Children.Add(ProfileIcon);
            CommentGrid.Children.Add(CommentEntry);
            CommentGrid.Children.Add(CommentPost);

            MainLayout.Children.Add(Header, 0, 0);
            MainLayout.Children.Add(separator1, 0, 1);
            MainLayout.Children.Add(sflist, 0, 2);
            MainLayout.Children.Add(separator2, 0, 3);
            MainLayout.Children.Add(CommentGrid, 0, 4);
            MainLayout.BackgroundColor = Color.White;
            Content = MainLayout;
        }
        private void Sflist_SwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            indexItem = -1;
            if (((GroupCommentsModel)e.ItemData).UserName.ToLower() != ThisUserName.ToLower())
            {
                e.Cancel = true;
            }
        }

        private void Sflist_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {
            indexItem = e.ItemIndex;
            if ((sender as SfListView).SwipeOffset > 40)
            {
                datatoEdit = (GroupCommentsModel)e.ItemData;
                (sender as SfListView).ResetSwipe();
            }
        }

        private void RightImage_BindingContextChanged(object sender, EventArgs e)
        {
            if (leftImage == null)
            {
                leftImage = sender as CachedImage;
                (leftImage.Parent as Xamarin.Forms.View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(DeleteComment) });
            }
        }

        private void Editfriend_bindingContextChanged(object sender, EventArgs e)
        {
            
            if (friendEdit == null)
            {
                var friendEdit = sender as CachedImage;
                (friendEdit.Parent as Xamarin.Forms.View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(EditCommentList) });
            }
        }

        private void EditCommentList()
        {
            EditMode = true;
            var index = indexItem;
            sflist.ResetSwipe();
            CommentEntry.Text = modelSfList[index].Comment.Split('#')[1];
        }

        private void PostComment()
        {
            if (!String.IsNullOrEmpty(CommentEntry.Text))
            {
                Api.Encode.GroupCommentPostJson commentModel = new Api.Encode.GroupCommentPostJson()
                {
                    token = App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME,
                    comment = CommentEntry.Text,
                    photo_id = data.PhotoId
                };
                var resp = App._Api.GetDataResponseFromServer("group/add_group_photo_comment", JsonConvert.SerializeObject(commentModel));
                var resu = resp.Result;
                if (resu.ToLower() != "no internet")
                {
                    var hasil = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    DisplayAlert("", hasil.message, "Ok");
                    InitData();
                }
                EditMode = false;
                CommentEntry.Text = "";
            }
        }
        private void DeleteComment()
        {
            Api.Encode.GroupCommentPostJson commentModel = new Api.Encode.GroupCommentPostJson()
            {
                token = App.GlobalDataUser.TOKEN,
                username = App.GlobalDataUser.USERNAME,
                comment_id = datatoEdit.Id,
            };
            var resp = App._Api.GetDataResponseFromServer("group/delete_group_photo_comment", JsonConvert.SerializeObject(commentModel));
            var resu = resp.Result;
            if (resu.ToLower() != "no internet")
            {
                var hasil = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                DisplayAlert("", hasil.message, "Ok");
                InitData();
            }
            EditMode = false;
            CommentEntry.Text = "";
        }
        private void EditComment()
        {
            Api.Encode.GroupCommentPostJson commentModel = new Api.Encode.GroupCommentPostJson()
            {
                token = App.GlobalDataUser.TOKEN,
                username = App.GlobalDataUser.USERNAME,
                comment_id = datatoEdit.Id,
                comment = CommentEntry.Text
            };
            var resp = App._Api.GetDataResponseFromServer("group/edit_group_photo_comment", JsonConvert.SerializeObject(commentModel));
            var resu = resp.Result;
            if (resu.ToLower() != "no internet")
            {
                var hasil = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                DisplayAlert("", hasil.message, "Ok");
                InitData();
            }
            EditMode = false;
            CommentEntry.Text = "";
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            InitData();
        }
        DataTemplate GetRightSwipeTemplate()
        {
            DataTemplate dt = new DataTemplate(() => {
                Grid Main = new Grid()
                {
                    ColumnSpacing = 0,
                    RowSpacing = 0
                };
                Main.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                Main.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                Grid GTrash = new Grid() { BackgroundColor = Color.FromRgb(216, 13, 13) };
                Grid GEdit = new Grid() { BackgroundColor = Color.FromRgb(0, 162, 232) };

                CachedImage Trash = new CachedImage()
                {
                    WidthRequest = App.DpiFontSize * 15,
                    HeightRequest = App.DpiFontSize * 15,
                    Source = "Delete.png",
                    Aspect = Aspect.AspectFit,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                };
                CachedImage Edit = new CachedImage()
                {
                    WidthRequest = App.DpiFontSize * 15,
                    HeightRequest = App.DpiFontSize * 15,
                    Source = "edit.png",
                    Aspect = Aspect.AspectFit,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                };
                Trash.BindingContextChanged += RightImage_BindingContextChanged;
                Edit.BindingContextChanged += Editfriend_bindingContextChanged;
                GEdit.Children.Add(Edit);
                GTrash.Children.Add(Trash);
                Main.Children.Add(GEdit, 0, 0);
                Main.Children.Add(GTrash, 1, 0);
                return Main;
            });
            return dt;
        }
        DataTemplate ListDataTemplate()
        {
            DataTemplate template = new DataTemplate(() =>
            {
                StackLayout UpperGrid = new StackLayout() { Spacing = 0 };
                StackLayout Main = new StackLayout() { Orientation = StackOrientation.Horizontal };
                CachedImage circle = new CachedImage()
                {
                    WidthRequest = App.DpiFontSize * 20,
                    HeightRequest = App.DpiFontSize * 20,
                    Transformations = new List<FFImageLoading.Work.ITransformation>() { new CircleTransformation() },
                    Source = "user72.png",
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Start,
                    Margin = new Thickness(10, 3, 0, 0)
                };
                circle.SetBinding(CachedImage.SourceProperty, "UserPhoto");
                Label Comments = new Label()
                {
                    FontSize = App.DpiFontSize * 9,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    WidthRequest = 260,
                    Margin = new Thickness(0,5,0,0)
                };
                Binding bind = new Binding();
                bind.Converter = new CommentConverter();
                bind.Path = "Comment";
                Comments.SetBinding(Label.FormattedTextProperty, bind);

                Label TimeLabel = new Label()
                {
                    FontSize = App.DpiFontSize * 5,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    TextColor = Color.LightGray,
                    Margin = new Thickness(0,0,10,0)
                };
                TimeLabel.SetBinding(Label.TextProperty, "Time");
                BoxView bx = new BoxView()
                {
                    BackgroundColor = Color.LightGray,
                    HeightRequest = 1
                };
                Main.Children.Add(circle);
                Main.Children.Add(Comments);
                Main.Children.Add(TimeLabel);
                UpperGrid.Children.Add(new BoxView() { HeightRequest = 7, BackgroundColor = Color.Transparent });
                UpperGrid.Children.Add(Main);
                UpperGrid.Children.Add(new BoxView() { HeightRequest = 7, BackgroundColor = Color.Transparent });
                UpperGrid.Children.Add(bx);

                return UpperGrid;
            });
            return template;
        }
    }
}
