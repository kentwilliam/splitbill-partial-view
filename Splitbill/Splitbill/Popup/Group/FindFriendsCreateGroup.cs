﻿using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Model.Send;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Group
{
    public class FindFriendsCreateGroup : PopupPage
    {
        public EventHandler Eve;
        String SelectedFriend;
        Grid backGrid = new Grid();
        SfListView sflist;
        Label Judul;
        List<FindFriendSendModel> ListModel = new List<FindFriendSendModel>();
        public List<String> selectedModel = new List<string>();
        //dibuat untuk mengambil Data ketika modal close
        public String nama { get; set; }
        public String noHp { get; set; }

        public void initData()
        {
            Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async() => {
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });
                
                try
                {
                    ListModel = new List<FindFriendSendModel>();
                    var resp = App._Api.GetDataResponseFromServer("user/friends_data", JsonConvert.SerializeObject(new Username_Token()
                    {
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME
                    }));

                    var resu = JsonConvert.DeserializeObject<HeadFriendsJsonResponse>(resp.Result);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (App._Api.ProcessDataOrNot(resu.message, false))
                        {
                            foreach (var r in resu.friends)
                            {
                                if (!SelectedFriend.Contains(r.id_teman))
                                {
                                    FindFriendSendModel model = new FindFriendSendModel();
                                    model.nama = r.id_teman;
                                    model.noHp = r.phone_number;
                                    model.photo = String.IsNullOrEmpty(r.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + r.photo;
                                    ListModel.Add(model);
                                }
                            }

                            sflist.ItemsSource = null;
                            sflist.ItemsSource = ListModel;
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(async() =>
                            {
                                await Task.Delay(200);
                                App.MyHomePage.LogOut();
                            });
                        }
                    });
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        await DisplayAlert("", "No Internet", "ok");
                    });
                    
                }

                Device.BeginInvokeOnMainThread(async() => {
                    await Task.Delay(300);
                    await PopupNavigation.PopAsync();
                });
            });
            
        }
        public FindFriendsCreateGroup(String selectedfriends = "")
        {
            if (selectedfriends == null)
            {SelectedFriend = "";}
            else
            {SelectedFriend = selectedfriends;}
            
            Judul = new Label()
            {
                Text = "Select Friend",
                FontSize = App.DpiFontSize * 10,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White
            };
            XFShapeView.ShapeView shape = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 15,
                Margin = new Thickness(15, 0, 15, 0),
                Color = Color.White
            };

            StackLayout search = new StackLayout() { Orientation = StackOrientation.Vertical};
            SearchBar bar = new SearchBar() { Margin = new Thickness(0, 0, 40, 0) };
            search.Children.Add(bar);

            sflist = new SfListView()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.Transparent,
                SelectionMode = SelectionMode.Multiple,
                SelectionGesture = TouchGesture.Tap,
                Margin = new Thickness(5, 0, 5, 0),
                ItemTemplate = new DataTemplate(()=> 
                {
                    StackLayout wrapper = new StackLayout() { Orientation = StackOrientation.Horizontal };

                    CircleImage ciImg = new CircleImage()
                    {
                        Margin = new Thickness(10,0,0,0),
                        WidthRequest  = 50,
                        HeightRequest = 50,
                        Aspect = Aspect.AspectFill,
                        BorderColor = Color.LightGray,
                        BorderThickness = 1,
                    };
                    ciImg.SetBinding(CircleImage.SourceProperty,"photo");

                    StackLayout mainStack = new StackLayout() { Orientation = StackOrientation.Vertical};
                    Label Nama = new Label() {TextColor = Color.Black};
                    Nama.SetBinding(Label.TextProperty,"nama");

                    Label nomorTelpon = new Label() { TextColor = Color.Black};
                    nomorTelpon.SetBinding(Label.TextProperty,"noHp");

                    mainStack.Children.Add(Nama); mainStack.Children.Add(nomorTelpon);


                    wrapper.Children.Add(ciImg);
                    wrapper.Children.Add(mainStack);
                    StackLayout OuterWrapper = new StackLayout();
                    OuterWrapper.Children.Add(wrapper);
                    OuterWrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 });
                    return new ViewCell() { View = OuterWrapper };
                }),
                ItemSize = 65
            };

            Button SelectFriendButton = new Button()
            {
                VerticalOptions = LayoutOptions.End,
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10),
                FontSize = App.DpiFontSize * 8,
            Text = "Add Friend",
            };
            SelectFriendButton.Clicked += SelectFriendButton_Clicked;
            search.Children.Add(sflist);
            search.Children.Add(SelectFriendButton);
            shape.Content = search;

            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.BackgroundColor = Color.Black;
            Image backImage = new Image()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Source = "background.jpg",
                Aspect = Aspect.AspectFill,
            };

            backGrid.Children.Add(backImage, 0, 0);
            Grid.SetRowSpan(backImage, 4);

            backGrid.Children.Add(Judul, 0, 0);
            backGrid.Children.Add(shape, 0, 1);
            Grid.SetRowSpan(shape, 3);
            Content = backGrid;
        }

        private void SelectFriendButton_Clicked(object sender, EventArgs e)
        {
            var hasil = sflist.SelectedItems;
            foreach (var v in hasil)
            {
                selectedModel.Add((v as FindFriendSendModel).nama);
            }
            Eve(this, EventArgs.Empty);
        }
        protected override Task OnAppearingAnimationEnd()
        {
            return base.OnAppearingAnimationEnd();
        }
        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }
    }
}
