﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using Splitbill.Converter;
using Splitbill.Model.Group;
using Splitbill.Views.Loading;
using Syncfusion.ListView.XForms;
using Syncfusion.SfCarousel.XForms;
using Syncfusion.XForms.TabView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace Splitbill.Popup.Group
{
    public class GroupPhotoReview:PopupPage
    {
        StackLayout LikeByOtherStack;
        Label LikeByOtherLabel;
        PhotoFile photoFileSelected;
        int index = 0;
        CachedImage LikeCached;
        Image likeIcon;
        CachedImage CommentCached;
        SfCarousel sfrousel;
        List<Model.Group.GroupCommentsModel> modelSfList = new List<Model.Group.GroupCommentsModel>();
        SfListView sflist;
        public GroupPhotoReview(List<PhotoFile> data)
        {
            StackLayout MainStack = new StackLayout();
            
            sfrousel = new SfCarousel() { };
            sfrousel.ItemHeight = 400;
            sfrousel.ItemTemplate = new DataTemplate(() => {

                Image img = new Image()
                {
                    Aspect = Aspect.AspectFit,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };
                img.SetBinding(Image.SourceProperty, "PhotoName");

                return img;
            });

            sfrousel.BatchBegin();
            sfrousel.ItemsSource = data;
            sfrousel.BatchCommit();
            
            sfrousel.SelectionChanged += Sfrousel_SelectionChanged;
            //sflistview
            sflist = new SfListView()
            {
                ItemTemplate = ListDataTemplate(),
                RowSpacing = 5,
                ItemsSource = new List<String>() { "1", "2", "3", "$", "a", "s", "d", "f", "g" },
                AutoFitMode = AutoFitMode.Height
            };
            sflist.AllowSwiping = true;
            
            modelSfList.Add(new Model.Group.GroupCommentsModel() { Comment = "qwepoiasdl;kzxc,mxcvuioewnrmsdfjklsdfm,.asdflkjqweroiuxcvlknasdf", UserName = "kent", UserPhoto = "user72.png" });
            modelSfList.Add(new Model.Group.GroupCommentsModel() { Comment = "WASAW ASLDKASJDLKAJDSOIQWOEIUQWEIASJZCLKJASDASLDKJQWEOIQWE123123123123", UserName = "Betty", UserPhoto = "user72.png" });
            modelSfList.Add(new Model.Group.GroupCommentsModel() { Comment = "WASAW ASLDKASJDLKAJDSOIQWOEIUQWEIASJZCLKJASDASLDKJQWEOIQWE", UserName = "niko", UserPhoto = "user72.png" });
            sflist.ItemsSource = modelSfList;

            //Stack Icon
            StackLayout icons = new StackLayout() { Orientation = StackOrientation.Horizontal, Spacing = 0, Margin = new Thickness(10, 0, 0, 0) };
            likeIcon = new Image()
            {
                Source = "LikeGray.png",
                WidthRequest = 15 * App.DpiFontSize,
                HeightRequest = 15 * App.DpiFontSize,
                Aspect = Aspect.AspectFit
            };
            Image CommentIcon = new Image()
            {
                Source = "CommentGray.png",
                WidthRequest = 15 * App.DpiFontSize,
                HeightRequest = 15 * App.DpiFontSize,
                Aspect = Aspect.AspectFit,
                Margin = new Thickness(10, 0, 0, 0)
            };
            likeIcon.GestureRecognizers.Add(new TapGestureRecognizer(likedCachedClicked));
            CommentIcon.GestureRecognizers.Add(new TapGestureRecognizer(async (v, e) => {
                await PopupNavigation.PushAsync(new GroupComments(photoFileSelected));
            }));

             LikeByOtherLabel = new Label() {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10,0,0,0)
            };
            LikeByOtherStack= new StackLayout()
            {
                HeightRequest = 0,
                Spacing = 0,
            };
            LikeByOtherStack.Children.Add(LikeByOtherLabel);
            LikeByOtherStack.Children.Add(new BoxView() { HeightRequest = 1, BackgroundColor = Color.LightGray,VerticalOptions = LayoutOptions.CenterAndExpand });
            icons.Children.Add(likeIcon);
            icons.Children.Add(CommentIcon);

            MainStack.Children.Add(sfrousel);
            MainStack.Children.Add(icons);
            MainStack.Children.Add(new BoxView() { HeightRequest = 1,BackgroundColor = Color.LightGray });
            MainStack.Children.Add(LikeByOtherStack);

            MainStack.Children.Add(sflist);
            MainStack.BackgroundColor = Color.White;
            Content = MainStack;
        }

        private void Sfrousel_SelectionChanged(object sender, Syncfusion.SfCarousel.XForms.SelectionChangedEventArgs e)
        {
            photoFileSelected = e.SelectedItem as PhotoFile;
            initComment(photoFileSelected);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            sfrousel.SelectedIndex = 0;
        }
        void likedCachedClicked(View v, Object obj)
        {
            String ImgName = (v as Image).Source.ToString();
            bool IsLike = false;
            String ApiString = "";

            if (ImgName.ToLower().Contains("likegray"))
            {
                IsLike = false;
                ApiString = "group/like_group_photo";
                (v as Image).Source = "LikeBlue.png";
            }
            else
            {
                IsLike = true;
                ApiString = "group/unlike_group_photo";
                (v as Image).Source = "LikeGray.png";
            }
            GroupCommentPostJson js = new GroupCommentPostJson();
            js.username = App.GlobalDataUser.USERNAME;
            js.token = App.GlobalDataUser.TOKEN;
            js.photo_id = photoFileSelected.PhotoId;

            var resp = App._Api.GetDataResponseFromServer(ApiString,JsonConvert.SerializeObject(js));
            var resu = resp.Result;

            if (resu.ToLower() != "no internet")
            {
                var result = JsonConvert.DeserializeObject<ErrorMessage>(resu);
                if (result.status == "true")
                {
                    initComment(photoFileSelected);
                }
                else
                {
                    DisplayAlert("",result.message,"Ok");
                }
            }
        }
        void initComment(PhotoFile data)
        {
            likeIcon.Source = "LikeGray.png";
            sflist.ItemsSource = null;
            modelSfList = new List<GroupCommentsModel>();
            GroupCommentGetComment js = new GroupCommentGetComment() {
                username = App.GlobalDataUser.USERNAME,
                token  = App.GlobalDataUser.TOKEN,
                photo_id = data.PhotoId
            };
            Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await PopupNavigation.PushAsync(new LoadingPopup());
                    await Task.Delay(250);
                });
                var resp = App._Api.GetDataResponseFromServer("group/view_group_photo_comment", JsonConvert.SerializeObject(js));
                var resu = resp.Result;
                try
                {
                    if (resu.ToLower() != "no internet")
                    {
                        var parse = JsonConvert.DeserializeObject<HeadGroupCommentResponse>(resp.Result);
                        foreach (var d in parse.comment)
                        {
                            modelSfList.Add(new GroupCommentsModel()
                            {
                                Comment = d.user_id+"#"+d.comment,
                                Time = d.time,
                                UserName = d.user_id,
                                UserPhoto = "http://www.urunan.id/splitbill/" + d.commenter_photo
                            });
                        }
                        FormattedString frm = new FormattedString();
                        if (parse.like.Count > 0)
                        {
                            if (parse.like.Distinct().ToList().Count > 1)
                            {
                                frm.Spans.Add(new Span() { Text = "Liked By ", FontSize = 6 * App.DpiFontSize });
                                frm.Spans.Add(new Span() { Text = parse.like[0], FontSize = 6 * App.DpiFontSize, FontAttributes = FontAttributes.Bold });
                                frm.Spans.Add(new Span() { Text = " and Others", FontSize = 6 * App.DpiFontSize });
                            }
                            else
                            {
                                frm.Spans.Add(new Span() { Text = "Liked By ", FontSize = 6 * App.DpiFontSize });
                                frm.Spans.Add(new Span() { Text = parse.like[0], FontSize = 6 * App.DpiFontSize, FontAttributes = FontAttributes.Bold });
                            }
                           
                        }
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            sflist.BatchBegin();
                            sflist.ItemsSource = modelSfList;
                            sflist.BatchCommit();

                            if (frm.Spans.Count > 0)
                            {
                                LikeByOtherStack.IsVisible = true;
                                LikeByOtherStack.HeightRequest = 35;
                                LikeByOtherLabel.FormattedText = frm;
                            }
                            else
                            {
                                LikeByOtherStack.IsVisible = false;
                            }
                            var Ownlike = parse.like.Contains(App.GlobalDataUser.USERNAME);
                            if (Ownlike)
                                likeIcon.Source = "LikeBlue.png";
                            else
                                likeIcon.Source = "LikeGray.png";
                        });
                    }
                }
                catch (Exception ex)
                {

                }
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Task.Delay(250);
                    await PopupNavigation.PopAsync();
                    
                });
            });

                
        }
        DataTemplate ListDataTemplate()
        {
            DataTemplate template = new DataTemplate(() =>
            {
                StackLayout UpperGrid = new StackLayout() { Spacing = 0 };
                StackLayout Main = new StackLayout() { Orientation = StackOrientation.Horizontal };
                CachedImage circle = new CachedImage()
                {
                    WidthRequest = App.DpiFontSize * 15,
                    HeightRequest = App.DpiFontSize * 15,
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Start,
                    Margin = new Thickness(10, 3, 0, 0),
                    DownsampleToViewSize = true,
                    Transformations = new List<FFImageLoading.Work.ITransformation>() { new CircleTransformation()}
                };
                circle.SetBinding(CachedImage.SourceProperty, "UserPhoto");
                Label Comments = new Label()
                {
                    FontSize = App.DpiFontSize * 6,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    LineBreakMode = LineBreakMode.WordWrap,
                    Margin = new Thickness(0, 0, 15, 0),
                    VerticalOptions = LayoutOptions.CenterAndExpand
                };
                Binding bind = new Binding();
                bind.Converter = new CommentConverter();
                bind.Path = "Comment";
                Comments.SetBinding(Label.FormattedTextProperty, bind);
                BoxView bx = new BoxView()
                {
                    BackgroundColor = Color.LightGray,
                    HeightRequest = 1
                };
                Main.Children.Add(circle);
                Main.Children.Add(Comments);
                UpperGrid.Children.Add(Main);
                UpperGrid.Children.Add(new BoxView() { HeightRequest = 3, BackgroundColor = Color.Transparent });
                UpperGrid.Children.Add(bx);

                return UpperGrid;
            });
            return template;
        }
    }
}
