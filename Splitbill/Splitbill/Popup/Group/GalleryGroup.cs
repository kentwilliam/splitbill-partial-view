﻿using Android.Graphics;
using FFImageLoading.Forms;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Group
{
	public class GalleryGroup : PopupPage
	{
        int totalItems;
        int indexItem;
        ObservableCollection<Imageclass> imgList;
        String PreviewImageSource;
        CachedImage PreviewImage;
        Imageclass prevSelected;
        bool MultipleSelect;
        String GroupId;
        public GalleryGroup(String groupID)
		{
            GroupId = groupID;
            totalItems = App.PathGambar.Count;
            Grid myGrid = new Grid();
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(45)});
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star)});
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            PreviewImage = new CachedImage();
            PreviewImage.HorizontalOptions = LayoutOptions.Center;
            PreviewImage.VerticalOptions = LayoutOptions.Center;
            PreviewImage.HeightRequest = 400;
            PreviewImage.WidthRequest = 350;
            PreviewImage.BackgroundColor = Xamarin.Forms.Color.LightGray;
            PreviewImage.Margin = new Thickness(10);
            PreviewImage.DownsampleToViewSize = true;
            PreviewImage.CacheType = FFImageLoading.Cache.CacheType.All;
            PreviewImage.DownsampleUseDipUnits = true;
            PreviewImage.BitmapOptimizations = true;
            PreviewImage.Aspect = Aspect.AspectFit;

            Button SendPhoto = new Button() {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Upload",
                WidthRequest = 100,
                FontSize = Device.GetNamedSize(NamedSize.Small,typeof(Button)),
                Margin = new Thickness(0,0,15,0)
            };
            Button CancelPhoto = new Button()
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Cancel",
                WidthRequest = 100,
                FontSize = Device.GetNamedSize(NamedSize.Small,typeof(Label)),
                Margin = new Thickness(15,0,0,0)
            };
            CancelPhoto.Clicked += (async(v,e) => { await PopupNavigation.PopAsync(); });
            SendPhoto.Clicked += SendPhoto_Clicked;
            Grid buttonMultiWrapper = new Grid() {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand,
                HeightRequest = 30,
                WidthRequest = 30,
                Margin = new Thickness(0, 0, 15, 10),
            };
            CachedImage multi = new CachedImage() {
                Source = "MultipleIcon.png",
            };
            XFShapeView.ShapeView tintColorBlue = new XFShapeView.ShapeView() {
                ShapeType = XFShapeView.ShapeType.Circle,
                Color =Xamarin.Forms.Color.CornflowerBlue,
                Opacity = 0.4f,
                IsVisible = false
            };
            buttonMultiWrapper.Children.Add(multi,0,0);
            buttonMultiWrapper.Children.Add(tintColorBlue,0,0);
            buttonMultiWrapper.GestureRecognizers.Add(new TapGestureRecognizer((v,e) =>
            {

                MultipleSelect = !MultipleSelect;
                tintColorBlue.IsVisible = MultipleSelect;
                foreach (var r in imgList)
                {
                    r.SELECTED = false;
                }
            }));
            imgList = new ObservableCollection<Imageclass>();

            foreach (var x in App.PathGambar.Take(5))
            {
                imgList.Add(new Imageclass() { SRC = x});
            }

            SfListView sflist = new SfListView();
            sflist.BackgroundColor = Xamarin.Forms.Color.White;
            sflist.ItemSize = 75;
            sflist.Margin = new Thickness(10, 0, 10, 0);
            sflist.ItemSpacing = 5;
            sflist.ItemsSource = imgList;
            sflist.Orientation = Orientation.Horizontal;
            sflist.ItemTapped += Sflist_ItemTapped;
            sflist.ItemTemplate = new DataTemplate(() => {
                Grid ImgTemplate = new Grid();
                ImgTemplate.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star)});
                ImgTemplate.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                ImgTemplate.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                ImgTemplate.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                CachedImage img = new CachedImage();
                img.HorizontalOptions = LayoutOptions.FillAndExpand;
                img.VerticalOptions = LayoutOptions.FillAndExpand;
                img.BackgroundColor =Xamarin.Forms.Color.AliceBlue;
                img.HeightRequest = 75;
                img.WidthRequest = 75;
                img.Aspect = Aspect.AspectFill;
                img.DownsampleToViewSize = true;
                img.SetBinding(CachedImage.SourceProperty,"SRC");

                XFShapeView.ShapeView box = new XFShapeView.ShapeView() {
                    ShapeType= XFShapeView.ShapeType.Box,
                    BackgroundColor =Xamarin.Forms.Color.Black,
                    Opacity = 0.5,
                    IsVisible = false,
                };
                box.SetBinding(XFShapeView.ShapeView.IsVisibleProperty, "SELECTED");
                XFShapeView.ShapeView cawang = new XFShapeView.ShapeView() {
                    ShapeType = XFShapeView.ShapeType.Circle,
                    Margin = new Thickness(10),
                    Color = Xamarin.Forms.Color.LimeGreen,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    BorderColor = Xamarin.Forms.Color.SlateGray,
                    HeightRequest =35,
                    WidthRequest = 35,
                    IsVisible = false
                };
                cawang.SetBinding(XFShapeView.ShapeView.IsVisibleProperty, "SELECTED");
                ImgTemplate.Children.Add(img,0,0);
                ImgTemplate.Children.Add(box,0,0);
                ImgTemplate.Children.Add(cawang,1,1);
                Grid.SetRowSpan(img, 2);
                Grid.SetColumnSpan(img, 2);
                Grid.SetRowSpan(box, 2);
                Grid.SetColumnSpan(box, 2);
                return ImgTemplate;
            });
            sflist.LayoutManager = new GridLayout() { SpanCount = 2};
            sflist.LoadMoreCommand = new Command<object>(LoadMoreItems, CanLoadMoreItems);
            sflist.LoadMoreOption = LoadMoreOption.Manual;
            sflist.SetBinding(SfListView.IsBusyProperty,"IsBusy");

            myGrid.Children.Add(PreviewImage,0,1);
            myGrid.Children.Add(SendPhoto,0,0);
            myGrid.Children.Add(CancelPhoto,0,0);
            myGrid.Children.Add(sflist,0,5);
            myGrid.Children.Add(buttonMultiWrapper, 0,4);
            Grid.SetRowSpan(PreviewImage,4);
            Grid.SetRowSpan(sflist,2);
            this.BackgroundColor = Xamarin.Forms.Color.White;
            Content = myGrid;
		}

        private async void SendPhoto_Clicked(object sender, EventArgs e)
        {
            (sender as Button).IsEnabled = false;
            sendPhotoGroup spg = new sendPhotoGroup() {username = App.GlobalDataUser.USERNAME,token = App.GlobalDataUser.TOKEN, group_id = GroupId };
            
            foreach (var x in imgList)
            {
                if (x.SELECTED)
                {
                    var sdk = GetImageAsByteArray(x.SRC);
                    String hasil = Convert.ToBase64String(ResizeImageAndroid(sdk));
                    spg.string64.Add(hasil);
                }
            }
            await Task.Run(() => {
                Device.BeginInvokeOnMainThread(async() =>
                {
                    await PopupNavigation.PushAsync(new Splitbill.Views.Loading.LoadingPopup("Uploading"));
                    await Task.Delay(300);
                });

                var resp = App._Api.GetDataResponseFromServer("group/upload_group_pic", JsonConvert.SerializeObject(spg));
                String result = resp.Result;
                if (result.ToLower() == "no internet")
                {
                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        await DisplayAlert("","No Internet Connection","Ok");
                    });
                }
                else
                {
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(result);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (resu.status == "true")
                        {
                            await DisplayAlert("", "Upload Complete", "Ok");
                            MessagingCenter.Send<String>("", "reload");
                        }

                        await Task.Delay(300);
                        await PopupNavigation.PopAsync();
                    });
                }
                

                
            });
            await PopupNavigation.PopAsync();
            (sender as Button).IsEnabled = true;
        }

        public static byte[] ResizeImageAndroid(byte[] imageData)
        {
            int scaleSize = 1;

            // Load the bitmap
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
            float maximal = Math.Max(originalImage.Width,originalImage.Height);
            while (maximal / scaleSize > 700)
            {
                scaleSize += 1;
            }
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, originalImage.Width/ scaleSize, originalImage.Height/ scaleSize, false);

   
            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);

                originalImage.Recycle();
                originalImage.Dispose();
                originalImage = null;

                resizedImage.Recycle();
                resizedImage.Dispose();
                resizedImage = null;
                return ms.ToArray();
            }

        }

        private void Sflist_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (prevSelected != null)
            {
                if (MultipleSelect == false)
                { prevSelected.SELECTED = false; }
                
            }
            (sender as SfListView).IsEnabled = false;
            var item = e.ItemData as Imageclass;
            PreviewImage.Source = item.SRC;

            item.SELECTED = !item.SELECTED;
            prevSelected = item;
            (sender as SfListView).IsEnabled = true;
        }

        private bool CanLoadMoreItems(object obj)
        {
            if (imgList.Count >= totalItems)
                return false;
            return true;
        }
        private async void LoadMoreItems(object obj)
        {
            var listView = obj as Syncfusion.ListView.XForms.SfListView;
            IsBusy = true;
            await Task.Delay(2500);
            var index = imgList.Count;
            var count = index + 5 >= totalItems ? totalItems - index : 5;
            AddProducts(index, count);
            IsBusy = false;
        }
        private void AddProducts(int index, int count)
        {
            for (int i = index; i < index + count; i++)
            {
                imgList.Add(new Imageclass() { SRC = App.PathGambar[i] });
            }
        }
        private byte[] GetImageAsByteArray(string imageFilePath)
        {
            FileStream fileStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            return binaryReader.ReadBytes((int)fileStream.Length);
        }
    }
    class Imageclass:INotifyPropertyChanged
    {
        private String src { get; set; }
        private bool Selected { get; set; }
        public bool SELECTED { get { return Selected; } set { Selected = value; OnPropertyChanged("SELECTED"); } }
        public String SRC { get { return src; } set { src = value;OnPropertyChanged("SRC"); } }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
    public class sendPhotoGroup
    {
        public sendPhotoGroup()
        {
            string64 = new List<string>();
        }
        public String username { get; set; }
        public String token { get; set; }
        public String group_id { get; set; }
        public List<String> string64 { get; set; }
    }
}