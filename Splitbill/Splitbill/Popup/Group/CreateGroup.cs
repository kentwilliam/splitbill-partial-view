﻿using Splitbill.Model;
using Splitbill.Model.MVVM;
using Splitbill.Model.RequestModel;
using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Splitbill.Pages;
using System.Collections.ObjectModel;
using SkiaSharp.Views.Forms;
using SkiaSharp;
using FFImageLoading.Forms;
using Splitbill.Popup.Send;
using Splitbill.Model.Send;
using Rg.Plugins.Popup.Pages;
using Splitbill.Api.Encode;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using System.Threading.Tasks;

namespace Splitbill.Popup.Group
{
    public class CreateGroup : PopupPage
    {
        SKShader GradientShaderTest;
        MainPage MP;
        Editor Description;
        Entry Money;
        SfListView listItem;
        ExtendedEntry MyFriends;
        ExtendedEntry MyMoney;
        Image previewImage;
        String namaFoto;
        Editor MyFriends2;
        //public ObservableCollection<RequestFriend> ListTerdaftar;

        CreateGroupJson model = new CreateGroupJson();
        public void resetForm()
        {
            Money.Text = "0";
            Description.Text = "";
        }
        
        public CreateGroup()
        {
            Grid mygrid = new Grid();
            mygrid.BackgroundColor = Color.FromRgb(234, 237, 244);
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });


            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;
            mygrid.Children.Add(viewMelengkung,0,0);
            Grid.SetRowSpan(viewMelengkung,5);

            CircleImage ciImg1 = new CircleImage()
            {
                BorderColor = Color.Gray,
                BorderThickness = 1,
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 100,
                HeightRequest = 100,
                Margin = new Thickness(0, 10, 0, 0),
                Aspect = Aspect.AspectFill
            };
            ciImg1.SetBinding(CircleImage.SourceProperty,"PHOTO");
            ciImg1.BindingContext = App.GlobalDataUser;
            mygrid.Children.Add(ciImg1, 0, 0);
            Grid.SetRowSpan(ciImg1, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize * 8, FontAttributes = FontAttributes.Bold,TextColor = Color.White };
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;

            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize * 8, VerticalTextAlignment = TextAlignment.Center,TextColor = Color.White };
            jumlah.SetBinding(Label.TextProperty, "BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);


            StackLayout DetailInfo = new StackLayout() { Orientation = StackOrientation.Vertical,BackgroundColor = Color.White,
            Spacing = 1,Margin = new Thickness(10)};
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };

            StackLayout wrapperTab3 = new StackLayout() { VerticalOptions = LayoutOptions.FillAndExpand,Spacing = 0};
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.FillAndExpand };

            #region tab1
            Label To = new Label()
            {
                Text = " Group Name :",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center,
                FontSize = App.DpiFontSize * 8,
            };
             MyFriends= new ExtendedEntry() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Group Name",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 8,
             };
            MyFriends.Completed += (v, e) =>
            {
                MyFriends.Text = MyFriends.Text.First().ToString().ToUpper() + MyFriends.Text.Substring(1);
            };
            tab1.Children.Add(To);
            tab1.Children.Add(MyFriends);
            DetailInfo.Children.Add(tab1);

            #endregion
            BoxView separator1 = new BoxView() { BackgroundColor = Color.LightGray ,HeightRequest = 1f};
            DetailInfo.Children.Add(separator1);
            #region tab2

            CachedImage FriendsLogo = new CachedImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "FriendsGray.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                DownsampleToViewSize = true
            };
            MyMoney = new ExtendedEntry()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Invite Friends",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 8,
            };
            CachedImage plusFriend = new CachedImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "AddFriend.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                DownsampleToViewSize = true
            };

            plusFriend.GestureRecognizers.Add(new TapGestureRecognizer(OpenFriend));
            tab2.Children.Add(FriendsLogo);
            tab2.Children.Add(MyMoney);
            tab2.Children.Add(plusFriend);
            DetailInfo.Children.Add(tab2);

            #endregion
            BoxView separator2 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f };
            DetailInfo.Children.Add(separator2);
            #region tab3
            Label To2 = new Label()
            {
                Text = " Description:",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Start,
                Margin = new Thickness(0,10,0,0),
                FontSize = App.DpiFontSize * 8,
            };
            MyFriends2 = new Editor()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 160,
                Text = "",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 8,
                
            };
            MyFriends2.Completed += (v,e)=>
            {
                if (!String.IsNullOrEmpty(MyFriends2.Text))
                { MyFriends2.Text = MyFriends2.Text.First().ToString().ToUpper() + MyFriends2.Text.Substring(1); }
                
            };
            tab3.Children.Add(To2);
            tab3.Children.Add(MyFriends2);
            wrapperTab3.Children.Add(tab3);

            StackLayout tab4 = new StackLayout() {Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.EndAndExpand};
            CachedImage photo = new CachedImage()
            {
                Source = "TakePhoto.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                DownsampleToViewSize = true
            };

#pragma warning disable CS0618 // Type or member is obsolete
            photo.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                (v as CachedImage).IsEnabled = false;
                Popup.Request.PopupPhotoRequest phot = new Popup.Request.PopupPhotoRequest(namaFoto);
                phot.Eve += async (sender, args) => {
                    namaFoto = (sender as Popup.Request.PopupPhotoRequest).PhotoName;
                    if (!String.IsNullOrEmpty(namaFoto))
                    {
                        photo.Source = "PhotoSign.png";
                    }
                    await PopupNavigation.PopAsync();
                };
                (v as CachedImage).IsEnabled = true;
                await PopupNavigation.PushAsync(phot); }));
#pragma warning restore CS0618 // Type or member is obsolete
            tab4.Children.Add(photo);
            DetailInfo.Children.Add(wrapperTab3);
            DetailInfo.Children.Add(tab4);
            #endregion
            BoxView separator3 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f, VerticalOptions = LayoutOptions.End,Margin = new Thickness(0,0,0,5) };
            DetailInfo.Children.Add(separator3);

            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Color = Color.White,
                CornerRadius = 15,
                Margin = new Thickness(15,10,15,0),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button buttonSendMoney = new Button()
            {
                BackgroundColor = Color.CornflowerBlue,
                Text = "Send",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10,5,10,0),
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.End,
                FontSize = App.DpiFontSize * 8,
            };
            buttonSendMoney.Clicked += B_Clicked;
            DetailInfo.Children.Add(buttonSendMoney);

            wrapper.Content = DetailInfo;
            mygrid.Children.Add(wrapper, 0, 5);
            Grid.SetRowSpan(wrapper, 13);
            Content = mygrid;
        }
        private void CreateSendButton(Grid mygrid)
        {
            Button b = new Button();
            b.Text = "Send";
            b.FontSize = App.DpiFontSize * 8;
            b.BackgroundColor = Color.FromRgb(57, 169, 251);
            b.TextColor = Color.White;
            b.Clicked += B_Clicked;
            b.BorderRadius = 0;
            b.HorizontalOptions = LayoutOptions.FillAndExpand;
            mygrid.Children.Add(b, 0, 11);
        }

        private async void B_Clicked(object sender, EventArgs e)
        {
            model.username = App.GlobalDataUser.USERNAME;
            model.token = App.GlobalDataUser.TOKEN;
            model.name = MyFriends.Text;
            model.lat = "";
            model.lng = "";
            model.place_id = "";
            model.photo = namaFoto;
            model.description = MyFriends2.Text;
            model.friends = MyMoney.Text.Split(',').ToList<String>().Where(x=>!String.IsNullOrEmpty(x)).ToList();
            try
            {
                var resp = App._Api.GetDataResponseFromServer("group/create_group", JsonConvert.SerializeObject(model));
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                if (App._Api.ProcessDataOrNot(resu.message, false))
                {
                    await DisplayAlert("", resu.message, "ok");

                    if (resu.status == "true")
                    { await PopupNavigation.PopAsync(); App.MyHomePage.BackToHome(); }
                }
                else
                {
                    await Task.Delay(200);
                    App.MyHomePage.LogOut();
                }
                
            }
            catch (Exception ex)
            {
                await DisplayAlert("","No Internet","ok");
            }
            
        }
        private async void OpenFriend(Xamarin.Forms.View arg1, object arg2)
        {
            Splitbill.Popup.Group.FindFriendsCreateGroup friend = new Popup.Group.FindFriendsCreateGroup(MyMoney.Text);
            friend.initData();
            friend.Eve += ModalClose;
            await PopupNavigation.PushAsync(friend);
            
        }
        private async void ModalClose(Object sender,  EventArgs e)
        {
            model.friends = (sender as FindFriendsCreateGroup).selectedModel;
            foreach (String t in model.friends)
            {
                MyMoney.Text += t + ",";
            }
            await PopupNavigation.PopAsync();
            
        }
        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            //path.LineTo(0, e.Info.Height-100);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
            //canvas.DrawPath(path, new SKPaint { Shader = GambarShaderTest });
        }
    }
}