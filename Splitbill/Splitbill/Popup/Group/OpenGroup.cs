﻿using Splitbill.Model;
using Splitbill.Model.MVVM;
using Splitbill.Model.RequestModel;
using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Splitbill.Pages;
using System.Collections.ObjectModel;
using SkiaSharp.Views.Forms;
using SkiaSharp;
using FFImageLoading.Forms;
using Splitbill.Popup.Send;
using Splitbill.Model.Send;
using Rg.Plugins.Popup.Pages;
using Splitbill.Api.Encode;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using System.Threading.Tasks;
using FFImageLoading.Transformations;
using Splitbill.Api.Decode;
using Splitbill.Model.Group;
using System.IO;
using SplitBill.DependencyServiceMedia;

namespace Splitbill.Popup.Group
{
    public class OpenGroup : PopupPage
    {
        Button openGallery;
        Button buttonToClick;
        StackLayout stackButton;
        Button buttonSendMoney;
        SKShader GradientShaderTest;
        MainPage MP;
        Editor Description;
        Entry Money;
        SfListView listItem;
        ExtendedEntry MyFriends;
        ExtendedEntry MyMoney;
        Image previewImage;
        String namaFoto;
        //public ObservableCollection<RequestFriend> ListTerdaftar;
        String thisGroupId = "";
        CreateGroupJson model = new CreateGroupJson();
        ImageSource imgsrc;
        String Creator_id;
        DatePicker dt = new DatePicker();
        CachedImage plusFriend;
        StackLayout wrappertab5;
        OpenGroupModel thismodel = new OpenGroupModel();
        String SourceTarget = "TakePhoto.png";
        Grid mygrid;
        List<string> _images = new List<string>();
        public void InitData()
        {
            thismodel.group.Clear();
            Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async()=> {
                    DependencyService.Get<IMediaService>().ClearFiles(_images);
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });

                Api.Encode.GetGrousJson js = new Api.Encode.GetGrousJson();
                js.username = App.GlobalDataUser.USERNAME;
                js.token = App.GlobalDataUser.TOKEN;
                js.id = thisGroupId;
                var resp = App._Api.GetDataResponseFromServer("group/groups", JsonConvert.SerializeObject(js));
                string result = resp.Result;
                if (result.ToLower() != "no internet")
                {
                    var resu = JsonConvert.DeserializeObject<Api.Decode.HeadViewJsonResponse>(resp.Result);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (App._Api.ProcessDataOrNot(resu.message, resu.group == null ? false : true) == true)
                        {
                            if (resu.status == "true")
                            {
                                MyMoney.Text = resu.group.member_username;
                                MyFriends.Text = resu.group.name;
                                Description.Text = String.IsNullOrEmpty(resu.group.description) ? "" : resu.group.description;
                                Creator_id = resu.group.creator_id;
                                dt.Date = DateTime.Parse(resu.group.date);//Ini Error
                                if (!String.IsNullOrEmpty(resu.group.cover_photo))
                                {
                                    SourceTarget = "PhotoSignGray.png";
                                    previewImage.Source = SourceTarget;
                                    imgsrc = resu.group.cover_photo;
                                    namaFoto = resu.group.cover_photo;
                                }
                                else
                                {
                                    SourceTarget = "TakePhoto.png";
                                    previewImage.Source = SourceTarget;
                                }

                                #region Proses List Photo
                                wrappertab5.Children.Clear();
                                for (int i = 0; i < resu.group.photo.Count; i++)
                                {
                                    photoDetail temp = resu.group.photo[i];
                                    List<String> FilePhoto = temp.filename;
                                    ListPhoto myPhoto = new ListPhoto();
                                    myPhoto.UserUploadPhoto = "https://www.urunan.id/splitbill/" + resu.group.photo[i].uploader_photo;

                                    myPhoto.UploaderName = resu.group.photo[i].uploader;
                                    myPhoto.Time = resu.group.photo[i].elapsed_time;
                                    
                                    for (int o = 0; o < FilePhoto.Take(3).ToList<String>().Count; o++)
                                    {
                                        myPhoto.NotActualFileName.Add(new PhotoFile() { PhotoName = "https://www.urunan.id/splitbill/" + FilePhoto[o], index = i });
                                    }
                                    for (int o = 0; o < FilePhoto.Count; o++)
                                    {
                                        myPhoto.ActualFileName.Add(new PhotoFile() { PhotoName = "https://www.urunan.id/splitbill/" + FilePhoto[o], index = i,PhotoId = temp.photo_id[o] });
                                    }

                                    myPhoto.CounterActual_NotActual = myPhoto.ActualFileName.Count - myPhoto.NotActualFileName.Count;
                                    StackLayout WrapperSf1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
                                    StackLayout wrapperFotoStacklayout = new StackLayout();
                                    XFShapeView.ShapeView photoWrap = new XFShapeView.ShapeView()
                                    {
                                        WidthRequest = 35,
                                        HeightRequest = 35,
                                        ShapeType = XFShapeView.ShapeType.Circle,
                                        BorderColor = Color.Black,
                                        BorderWidth = 1,
                                    };
                                    CachedImage cimg = new CachedImage()
                                    {
                                        Source = myPhoto.UserUploadPhoto,
                                        CacheType = FFImageLoading.Cache.CacheType.Memory,
                                        DownsampleToViewSize = true,
                                    };
                                    cimg.Transformations.Add(new CircleTransformation());
                                    photoWrap.Content = cimg;
                                    Label UploaderName = new Label()
                                    {
                                        FontSize = App.DpiFontSize * 6,
                                        HorizontalOptions = LayoutOptions.Center,
                                        WidthRequest = 50,
                                        HorizontalTextAlignment = TextAlignment.Center
                                    };
                                    UploaderName.SetBinding(Label.TextProperty, "UploaderName");
                                    UploaderName.BindingContext = myPhoto;
                                    wrapperFotoStacklayout.Children.Add(photoWrap);
                                    wrapperFotoStacklayout.Children.Add(UploaderName);

                                    XFShapeView.ShapeView counterWrap = new XFShapeView.ShapeView()
                                    {
                                        WidthRequest = 25,
                                        HeightRequest = 25,
                                        ShapeType = XFShapeView.ShapeType.Circle,
                                        BorderColor = Color.Black,
                                        BorderWidth = 1,
                                        HorizontalOptions = LayoutOptions.End
                                    };
                                    counterWrap.SetBinding(XFShapeView.ShapeView.IsVisibleProperty, "ShowCounter");
                                    counterWrap.BindingContext = myPhoto;

                                    Label CounterLabel = new Label()
                                    {
                                        HorizontalOptions = LayoutOptions.Center,
                                        VerticalOptions = LayoutOptions.Center,
                                        FontSize = App.DpiFontSize * 6,
                                        TextColor = Color.CornflowerBlue,
                                    };
                                    CounterLabel.SetBinding(Label.TextProperty, "CounterActual_NotActualString");
                                    CounterLabel.BindingContext = myPhoto;

                                    counterWrap.Content = CounterLabel;
                                    SfListView sf1 = new SfListView()
                                    {
                                        HeightRequest = 51,
                                        Orientation = Orientation.Horizontal,
                                        ItemTemplate = new DataTemplate(() =>
                                        {
                                            CachedImage bg = new CachedImage()
                                            {
                                                Margin = new Thickness(5),
                                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                                VerticalOptions = LayoutOptions.FillAndExpand,
                                                DownsampleToViewSize = true,
                                                CacheType = FFImageLoading.Cache.CacheType.Memory,
                                                BackgroundColor = Color.Gray
                                            };
                                            bg.SetBinding(CachedImage.SourceProperty, "PhotoName");
                                            return bg;
                                        }),
                                        ItemsSource = new List<int>() { 1, 2, 3 },
                                        ItemSize = 60,
                                        SelectionBackgroundColor = Color.LightGray,
                                    };
                                    sf1.ItemTapped += (async (v, e) =>
                                    {
                                        (v as SfListView).IsEnabled = false;
                                        var r = (e.ItemData as PhotoFile).index;
                                        var re = thismodel.group[r].ActualFileName;
                                        GroupPhotoReview Review = new GroupPhotoReview(re);
                                        await PopupNavigation.PushAsync(Review);
                                        (v as SfListView).IsEnabled = true;

                                    });
                                    sf1.ItemsSource = myPhoto.NotActualFileName;
                                    Label sf1Label = new Label()
                                    {
                                        WidthRequest = 30,
                                        FontSize = App.DpiFontSize * 6,
                                        VerticalOptions = LayoutOptions.Center,
                                        HorizontalTextAlignment = TextAlignment.End,
                                    };
                                    sf1Label.SetBinding(Label.TextProperty, "Time");
                                    sf1Label.BindingContext = myPhoto;

                                    WrapperSf1.Children.Add(wrapperFotoStacklayout);
                                    WrapperSf1.Children.Add(sf1);
                                    WrapperSf1.Children.Add(counterWrap);
                                    WrapperSf1.Children.Add(sf1Label);
                                    wrappertab5.Children.Add(WrapperSf1);
                                    wrappertab5.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 });
                                    thismodel.group.Add(myPhoto);
                                }
                                #endregion

                            }
                        }
                        else
                        {
                            App.MyHomePage.LogOut();
                        }
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        App.MyHomePage.ShowAlert("No Internet Connection");
                    });
                }
                Device.BeginInvokeOnMainThread(async () => {
                    await Task.Delay(300);
                    if (Creator_id.ToLower().Trim() != App.GlobalDataUser.USERNAME.ToLower().Trim())
                    { plusFriend.IsVisible = false; }
                    try
                    {
                       
                        await PopupNavigation.PopAsync();
                    }
                    catch (Exception ex)
                    {

                    }
                    
                });
            });
        }
        bool processing = false;
        public OpenGroup(String grupId)
        {
            
            MessagingCenter.Subscribe<App, List<string>>((App)Xamarin.Forms.Application.Current, "ImagesSelected", (s, images) =>
            {
                bool Reload = false;
                if (processing == false)
                {
                    processing = true;
                    _images = images;
                    sendPhotoGroup spg = new sendPhotoGroup() { username = App.GlobalDataUser.USERNAME, token = App.GlobalDataUser.TOKEN, group_id = grupId };

                    foreach (var str in _images)
                    {
                        byte[] tempFotoByte = GetImageAsByteArray(str);
                        String hasil = Convert.ToBase64String(tempFotoByte);
                        spg.string64.Add(hasil);
                    }
                     Task.Run(() => {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await PopupNavigation.PushAsync(new Splitbill.Views.Loading.LoadingPopup("Uploading"));
                            await Task.Delay(300);
                        });

                        var resp = App._Api.GetDataResponseFromServer("group/upload_group_pic", JsonConvert.SerializeObject(spg));
                        String result = resp.Result;
                        if (result.ToLower() == "no internet")
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                await DisplayAlert("", "No Internet Connection", "Ok");
                            });
                        }
                        else
                        {
                            var resu = JsonConvert.DeserializeObject<ErrorMessage>(result);
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                if (resu.status == "true")
                                {
                                    await DisplayAlert("", "Upload Complete", "Ok");
                                    Reload = true;
                                }
                                await Task.Delay(300);
                                await PopupNavigation.PopAsync();
                                MessagingCenter.Send<String>("","reload");
                            });
                        }

                    });
                }
                processing = false;
            });
            dt.IsEnabled = false;
            stackButton = new StackLayout() { Orientation = StackOrientation.Horizontal };
            thisGroupId = grupId;
            mygrid = new Grid();
            mygrid.BackgroundColor = Color.FromRgb(234, 237, 244);
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });


            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;
            mygrid.Children.Add(viewMelengkung, 0, 0);
            Grid.SetRowSpan(viewMelengkung, 5);

            CircleImage ciImg1 = new CircleImage()
            {
                BorderColor = Color.Gray,
                BorderThickness = 1,
                HorizontalOptions = LayoutOptions.Center,
                HeightRequest = 100,
                WidthRequest = 100,
                Margin = new Thickness(0, 10, 0, 0),
                Aspect = Aspect.AspectFill
            };
            ciImg1.SetBinding(CircleImage.SourceProperty, "PHOTO");
            ciImg1.BindingContext = App.GlobalDataUser;
            mygrid.Children.Add(ciImg1, 0, 0);
            Grid.SetRowSpan(ciImg1, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize*8, FontAttributes = FontAttributes.Bold, TextColor = Color.White };
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;

            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize *8, VerticalTextAlignment = TextAlignment.Center, TextColor = Color.White };
            jumlah.SetBinding(Label.TextProperty, "BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);


            StackLayout DetailInfo = new StackLayout() { Orientation = StackOrientation.Vertical, BackgroundColor = Color.White,
                Spacing = 1, Margin = new Thickness(10) };
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };

            StackLayout wrapperTab3 = new StackLayout() { VerticalOptions = LayoutOptions.FillAndExpand, Spacing = 0 };
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.FillAndExpand };

            #region tab1
            Label To = new Label()
            {
                Text = " Group Name :",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center,
                FontSize = App.DpiFontSize * 6,
            };
            MyFriends = new ExtendedEntry() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Group Name",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 6
            };
            MyFriends.Completed += (v, e) => {
                MyFriends.Text = MyFriends.Text.First().ToString().ToUpper() + MyFriends.Text.Substring(1);
            };
            tab1.Children.Add(To);
            tab1.Children.Add(MyFriends);
            DetailInfo.Children.Add(tab1);

            #endregion
            BoxView separator1 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f };
            DetailInfo.Children.Add(separator1);
            #region tab2

            CachedImage FriendsLogo = new CachedImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "FriendsGray.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize*15,
                DownsampleToViewSize = true
            };
            MyMoney = new ExtendedEntry()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Invite Friends",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 6,
            };
            MyMoney.TextChanged += MyMoney_TextChanged;
            plusFriend = new CachedImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "AddFriend.png",
                WidthRequest = App.DpiFontSize  * 15,
                HeightRequest = App.DpiFontSize * 15,
                DownsampleToViewSize = true,
                IsEnabled = false,
            };
            ToolbarItem t = new ToolbarItem() { Name = "Item1", Order = ToolbarItemOrder.Primary, Priority = 1, Text = "item1" };
            ToolbarItems.Add(t);
            plusFriend.GestureRecognizers.Add(new TapGestureRecognizer(OpenFriend));
            tab2.Children.Add(FriendsLogo);
            tab2.Children.Add(MyMoney);
            tab2.Children.Add(plusFriend);
            DetailInfo.Children.Add(tab2);

            #endregion
            BoxView separator2 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f };
            DetailInfo.Children.Add(separator2);
            #region tab3
            Label To2 = new Label()
            {
                Text = " Description:",
                HorizontalOptions = LayoutOptions.Start,
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Start,
                Margin = new Thickness(0, 10, 0, 0),
                FontSize = 6* App.DpiFontSize,
                WidthRequest = 75,
                
            };
            Description = new Editor()
            {
                WidthRequest = 225,
                HeightRequest = 75,
                Text = "Italiano!!!!",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize *6,
                HorizontalOptions = LayoutOptions.Start
            };
            Description.Completed += (v, e) => {
                Description.Text = Description.Text.First().ToString().ToUpper() + Description.Text.Substring(1);
            };
            tab3.Children.Add(To2);
            tab3.Children.Add(Description);
            wrapperTab3.Children.Add(tab3);

            StackLayout tab4 = new StackLayout() {Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.EndAndExpand};
            previewImage = new Image()
            {
                IsEnabled = false,
                Source = SourceTarget,
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
            };
            previewImage.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                if (App.GlobalDataUser.USERNAME != Creator_id)
                {
                    Popup.Request.PopupPreviewImage pop = new Request.PopupPreviewImage("https://www.urunan.id/splitbill/" + namaFoto);
                    await PopupNavigation.PushAsync(pop);
                }
                else
                {
                    Popup.Group.PopupPhotoCoverGroup phot = new Popup.Group.PopupPhotoCoverGroup(namaFoto);
                    phot.GroupId = thisGroupId;
                    phot.Eve += async (sender, args) => {
                        namaFoto = (sender as Popup.Group.PopupPhotoCoverGroup).PhotoName;
                        if (!String.IsNullOrEmpty(namaFoto))
                        {
                            previewImage.Source = "PhotoSign.png";
                            SourceTarget = "PhotoSign.png";
                        }
                        await PopupNavigation.PopAsync();
                    };
                    await PopupNavigation.PushAsync(phot);
                }
                 }));
            tab4.Children.Add(previewImage);
            tab1.Children.Add(tab4);
            DetailInfo.Children.Add(wrapperTab3);

            #endregion
            BoxView separator6 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f, VerticalOptions = LayoutOptions.End, Margin = new Thickness(0, 0, 0, 5) };
            DetailInfo.Children.Add(separator6);

            #region tab5
            ScrollView tab5 = new ScrollView();
            wrappertab5 = new StackLayout() { Spacing = 2.5};
            tab5.Content = wrappertab5;
            DetailInfo.Children.Add(tab5);
            #endregion

            BoxView separator3 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f, VerticalOptions = LayoutOptions.End,Margin = new Thickness(0,0,0,5) };
            DetailInfo.Children.Add(separator3);

            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Color = Color.White,
                CornerRadius = 15,
                Margin = new Thickness(15,10,15,0),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            buttonToClick = new Button() {
                BackgroundColor = Color.CornflowerBlue,
                Text = "Send Request",
                FontSize = App.DpiFontSize * 6,
                TextColor = Color.White,
                WidthRequest = 125,
                Margin = new Thickness(5)
            };
            buttonToClick.Clicked += ToRequest;

            openGallery = new Button();
            openGallery.Text = "Upload";
            openGallery.BackgroundColor = Color.CornflowerBlue;
            openGallery.TextColor = Color.White;
            openGallery.WidthRequest = 125;
            openGallery.Margin = new Thickness(5);
            openGallery.FontSize = App.DpiFontSize * 6;
            openGallery.HorizontalOptions = LayoutOptions.EndAndExpand;
            openGallery.Clicked += OpenGallery_Clicked;

            stackButton.Children.Add(buttonToClick);
            stackButton.Children.Add(openGallery);
            DetailInfo.Children.Add(stackButton);

            wrapper.Content = DetailInfo;
            mygrid.Children.Add(wrapper, 0, 5);
            Grid.SetRowSpan(wrapper, 13);


            Label titik3 = new Label();
            titik3.Text = "...";
            titik3.TextColor = Color.White;
            titik3.FontAttributes = FontAttributes.Bold;
            titik3.HorizontalTextAlignment = TextAlignment.End;
            titik3.VerticalOptions = LayoutOptions.Start;
            titik3.VerticalTextAlignment = TextAlignment.Start;
            titik3.FontSize = App.DpiFontSize * 16;
            titik3.Margin = new Thickness(0, 0,10,5);
            titik3.GestureRecognizers.Add(new TapGestureRecognizer(DotClicked));
            mygrid.Children.Add(titik3,0,0);
            Grid.SetRowSpan(titik3, 2);
            Content = mygrid;
            DisableEditFriend();
            MessagingCenter.Subscribe<String>(this, "reload", (v) => {
                InitData();
            });
        }
        bool CanAddFriend;
        private void MyMoney_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (e.OldTextValue.Count() > e.NewTextValue.Count())
                {
                    if (e.OldTextValue.LastIndexOf(',') > 0)
                    {
                        MyMoney.Text = e.OldTextValue.Substring(0, e.OldTextValue.LastIndexOf(','));
                    }
                    else
                    {
                        MyMoney.Text = "";
                    }
                }
                else if (e.NewTextValue.Count() > e.OldTextValue.Count())
                {
                    if(!CanAddFriend)
                        MyMoney.Text = e.OldTextValue;
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {

            }

            
        }

        private async void DotClicked(View arg1, object arg2)
        {
            DynamiycButtonsPopup dyna = new DynamiycButtonsPopup();
            if (Creator_id.ToLower() == App.GlobalDataUser.USERNAME.ToLower())
            {
                Button LeaveGroupButton = new Button()
                {
                    BackgroundColor = Color.IndianRed,
                    Text = "Leave Group",
                    FontSize =App.DpiFontSize * 8,
                    TextColor = Color.White,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.End,
                    Margin = new Thickness(10, 5, 10, 5),
                };
                buttonSendMoney = new Button()
                {
                    BackgroundColor = Color.CornflowerBlue,
                    Text = "Edit Group",
                    Margin = new Thickness(10, 5, 10, 5),
                    TextColor = Color.White,
                    VerticalOptions = LayoutOptions.End,
                    FontSize = App.DpiFontSize * 8,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };
                dyna.AddButton(buttonSendMoney);
                buttonSendMoney.Clicked += ((v,e) => {
                    EnableEditFriend();
                });
                dyna.AddButton(LeaveGroupButton);
                LeaveGroupButton.Clicked += ((v, e) => {
                    PopupNavigation.PopAsync();
                    LeaveGroupClick();
                });
            }
            else
            {
                Button LeaveGroupButton = new Button()
                {
                    BackgroundColor = Color.IndianRed,
                    Text = "Leave Group",
                    FontSize = App.DpiFontSize * 8,
                    TextColor = Color.White,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.End,
                    Margin = new Thickness(10, 5, 10, 5),
                };
                dyna.AddButton(LeaveGroupButton);
                LeaveGroupButton.Clicked += ((v, e) => {
                    PopupNavigation.PopAsync();
                    LeaveGroupClick();
                });
            }
            await PopupNavigation.PushAsync(dyna);
        }

        private void CreateSendButton(Grid mygrid)
        {
            StackLayout buttons = new StackLayout() { Orientation = StackOrientation.Horizontal};

            Button b = new Button();
            b.WidthRequest = 100;
            b.Text = "Send";
            b.FontSize = 8* App.DpiFontSize;
            b.BackgroundColor = Color.CornflowerBlue;
            b.TextColor = Color.White;
            b.Clicked += B_Clicked;
            b.BorderRadius = 0;

            buttons.Children.Add(b);
            mygrid.Children.Add(buttons, 0, 11);
        }

        private async void OpenGallery_Clicked(object sender, EventArgs e)
        {
            //GalleryGroup gp = new GalleryGroup(thisGroupId);
            //await PopupNavigation.PushAsync(gp);

            await DependencyService.Get<IMediaService>().OpenGallery();
        }
        private async void B_Clicked(object sender, EventArgs e)
        {
            try
            {
                model.username = App.GlobalDataUser.USERNAME;
                model.token = App.GlobalDataUser.TOKEN;
                model.name = MyFriends.Text;
                model.lat = "";
                model.lng = "";
                model.place_id = "";
                model.photo = namaFoto;
                model.friends = MyMoney.Text.Split(',').ToList();
                model.group_id = thisGroupId;
                model.description = Description.Text;
                var resp = App._Api.GetDataResponseFromServer("group/edit_group", JsonConvert.SerializeObject(model));
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);

                if (resu.message.ToLower() == "expired")
                {
                    await Task.Delay(200);
                    App.MyHomePage.LogOut();
                }
                else
                {
                    await DisplayAlert("", resu.message, "ok");
                    if (resu.status == "true")
                    { await PopupNavigation.PopAsync(); App.MyHomePage.BackToHome(); }
                    DisableEditFriend();
                }
            }
            catch (Exception ex)
            {await DisplayAlert("", "Could Not Process Request", "Ok"); }
        }
        private async void LeaveGroupClick()
        {
            var answer = await DisplayAlert("","are you sure?","Yes","no");
            if (answer)
            {
                try
                {
                    Api.Encode.LeaveGroupJsonResponse js = new LeaveGroupJsonResponse()
                    {
                        username = App.GlobalDataUser.USERNAME,
                        token = App.GlobalDataUser.TOKEN,
                        group_id = thisGroupId
                    };
                    var resp = App._Api.GetDataResponseFromServer("group/leave_group", JsonConvert.SerializeObject(js));
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    if (resu.message.ToLower() == "expired")
                    {
                        await Task.Delay(200);
                        App.MyHomePage.LogOut();
                    }
                    else
                    {
                        await DisplayAlert("", resu.message, "ok");
                        await PopupNavigation.PopAsync();
                        App.MyHomePage.BackToHome();
                    }
                }
                catch (Exception ex)
                { await DisplayAlert("", "Could Not Process Request", "Ok"); }
            }
            
        }
        private async void OpenFriend(Xamarin.Forms.View arg1, object arg2)
        {
            Splitbill.Popup.Group.FindFriendsCreateGroup friend = new Popup.Group.FindFriendsCreateGroup(MyMoney.Text);
            friend.initData();
            friend.Eve += ModalClose;
            await PopupNavigation.PushAsync(friend);
            
        }
        private async void ModalClose(Object sender,  EventArgs e)
        {
            model.friends = (sender as FindFriendsCreateGroup).selectedModel;
            CanAddFriend = true;
            if (MyMoney.Text.IndexOf(',') > 0)
            {
                foreach (String t in model.friends)
                {
                    MyMoney.Text += ","+t ;
                }
            }
            else
            {
                foreach (String t in model.friends)
                {
                    MyMoney.Text += t+",";
                }
            }
            CanAddFriend = false;
            await PopupNavigation.PopAsync();
            
        }
        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            //path.LineTo(0, e.Info.Height-100);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
            //canvas.DrawPath(path, new SKPaint { Shader = GambarShaderTest });
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            InitData();
        }
        private async void ToRequest(object sender,EventArgs e)
        {
            App.RequestFromGroup = true;
            App.GroupId = thisGroupId;

            (sender as Button).IsEnabled = false;

            App.MyHomePage.ToRequestView();

            (sender as Button).IsEnabled = true;
            await PopupNavigation.PopAsync();
        }
        public async void EnableEditFriend()
        {
            await PopupNavigation.PopAsync();
            buttonToClick.Clicked -= ToRequest;
            buttonToClick.Clicked -= B_Clicked;
            MyMoney.IsEnabled = true;
            MyFriends.IsEnabled = true;
            Description.IsEnabled = true;
            previewImage.IsEnabled = true;
            openGallery.IsVisible = false;
            openGallery.WidthRequest = 0;
            if (SourceTarget.ToLower() == "takephoto.png")
            {
                SourceTarget = "TakePhotoBlue.png";
            }
            else if (SourceTarget.ToLower() == "photosigngray.png")
            { SourceTarget = "PhotoSign.png"; }

            previewImage.Source = SourceTarget;
            plusFriend.Source = "AddFriend.png";
            buttonToClick.Text = "Save Edit";
            buttonToClick.Clicked += B_Clicked;
            buttonToClick.HorizontalOptions = LayoutOptions.FillAndExpand;
            plusFriend.IsEnabled = true;
        }
        public void DisableEditFriend()
        {
            buttonToClick.Clicked -= ToRequest;
            buttonToClick.Clicked -= B_Clicked;
            MyMoney.IsEnabled = false;
            MyFriends.IsEnabled = false;
            Description.IsEnabled = false;
            previewImage.IsEnabled = false;
            openGallery.IsVisible = true;
            openGallery.WidthRequest = 125;
            if (SourceTarget.ToLower() == "takephotoblue.png")
            { SourceTarget = "TakePhoto.png"; }
            else if (SourceTarget.ToLower() == "photosign.png")
            { SourceTarget = "PhotoSignGray.png"; }
            previewImage.Source = SourceTarget;

            plusFriend.Source = "AddFriendsGray.png";
            buttonToClick.Text = "Send Request";
            buttonToClick.Clicked += ToRequest;
            buttonToClick.HorizontalOptions = LayoutOptions.Start;
            plusFriend.IsEnabled = false;
        }
        protected override bool OnBackButtonPressed()
        {
            if (buttonToClick.Text.Equals("Save Edit"))
            {
                DisableEditFriend();
                return true;
            }
            else
            {
                mygrid.Children.Clear();
                return false;
            }
            
        }

        private byte[] GetImageAsByteArray(string imageFilePath)
        {
            FileStream fileStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            return binaryReader.ReadBytes((int)fileStream.Length);
        }
    }
}