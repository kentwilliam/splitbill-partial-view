﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Popup
{
    public class Popup2:PopupPage
    {
        public Popup2()
        {
            Grid G = new Grid() { WidthRequest = 300, HeightRequest = 350 };
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 20,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                HeightRequest = 350
            };

            Grid ContentGrid = new Grid();

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(175) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(75) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(100) });

            StackLayout TextLayout = new StackLayout() { Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center };

            Label judulDescription = new Label()
            {
                Text = "Look at your settling bills classy way!",
                FontSize = 0.75 * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White,
                Margin = new Thickness(15, 0, 15, 0)
            };
            TextLayout.Children.Add(judulDescription);

            Button button1 = new Button() { BackgroundColor = Color.Gray, Text = "Got it, THANKS", BorderRadius = 10, HorizontalOptions = LayoutOptions.Center, WidthRequest = 300, HeightRequest = 50, VerticalOptions = LayoutOptions.Center };

            Image PreviewImage = new Image()
            {
                Source = "NotificationIcon.png",
                WidthRequest = 300,
                HeightRequest = 175,
                Margin = new Thickness(0, 15, 0, 0),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            ContentGrid.Children.Add(PreviewImage, 0, 0);
            ContentGrid.Children.Add(button1, 0, 2);
            ContentGrid.Children.Add(TextLayout, 0, 1);

            box.Content = ContentGrid;
            G.Children.Add(box);

            this.BackgroundColor = Color.FromRgba(25, 25, 25, 0.8f);
            Content = G;
        }
    }
}
