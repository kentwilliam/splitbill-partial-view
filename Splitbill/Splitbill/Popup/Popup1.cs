﻿using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Rg.Plugins.Popup.Services;

namespace Splitbill.Popup
{
    public class PopupLogout:PopupPage
    {
        
        public PopupLogout()
        {
            Grid G = new Grid() { WidthRequest = 300, HeightRequest = 350 };
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 20,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 3,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                HeightRequest = 200,
            };

            Grid ContentGrid = new Grid();
            
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3f, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.8f, GridUnitType.Star) });
            StackLayout buttonLayout = new StackLayout() { Orientation = StackOrientation.Vertical, VerticalOptions = LayoutOptions.CenterAndExpand, Margin = new Thickness(0, 15, 0, 0) };

            SKCanvasView button1 = new SKCanvasView();
            button1.PaintSurface += Button1_PaintSurface;
            //Button button1 = new Button() { BackgroundColor = Color.Gray, Text = "Button 1", BorderRadius = 10, HorizontalOptions = LayoutOptions.Center, WidthRequest = 290, HeightRequest = 45 };
            Label or = new Label() {Text = "~or~",TextColor = Color.CornflowerBlue,HorizontalOptions = LayoutOptions.CenterAndExpand,FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium,typeof(Label))};
            SKCanvasView button2 = new SKCanvasView();

            Grid grid1 = new Grid() { WidthRequest = 290,HeightRequest=45,HorizontalOptions = LayoutOptions.Center};
            grid1.Children.Add(button1,0,0);
            Grid grid2 = new Grid() { WidthRequest = 290, HeightRequest = 45, HorizontalOptions = LayoutOptions.Center };
            grid2.Children.Add(button2, 0, 0);

            Label lGrid1 = new Label() {Text = "Send Money" ,TextColor = Color.White, FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalTextAlignment = TextAlignment.Center,
            VerticalTextAlignment = TextAlignment.Center};

            Label lGrid2 = new Label()
            {
                Text = "Request Money",
                TextColor = Color.White,
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            grid1.Children.Add(lGrid1,0,0);
            grid1.GestureRecognizers.Add(new TapGestureRecognizer(ToSend));
            grid2.Children.Add(lGrid2,0,0);
            grid2.GestureRecognizers.Add(new TapGestureRecognizer(ToRequest));
            button2.PaintSurface += Button2_PaintSurface;
            

            buttonLayout.Children.Add(grid1);
            buttonLayout.Children.Add(or);
            buttonLayout.Children.Add(grid2);

            ContentGrid.Children.Add(buttonLayout, 0, 1);
            box.Content = ContentGrid;


            StackLayout imageWrapper = new StackLayout() {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start
            };

            CircleImage ciImg = new CircleImage()
            {
                Source = "Send_Request.png",
                HeightRequest = 100,
                WidthRequest = 100,
                BorderColor = Color.CornflowerBlue,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BorderThickness = 1,
                Aspect = Aspect.AspectFill
            };
            imageWrapper.Children.Add(ciImg);
            Grid Wrapper = new Grid() { HeightRequest = 285, WidthRequest = 325, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.Center };
            Wrapper.Children.Add(box);
            Wrapper.Children.Add(imageWrapper);
            G.Children.Add(Wrapper, 0, 0);
            this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.65f);
            Content = G;
        }

        private async void ToRequest(View arg1, object arg2)
        {
            await PopupNavigation.PopAsync();
            App.MyHomePage.ToRequestView();
        }
        private async void ToSend(View arg1, object arg2)
        {
            await PopupNavigation.PopAsync();
            App.MyHomePage.ToSendView();
        }
        private void Button2_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            var surface = e.Surface;
            var canvas = surface.Canvas;
            SKShader GradientShaderTest = SKShader.CreateLinearGradient(new SKPoint(0, 0),
                new SKPoint(e.Info.Width, e.Info.Height),
                new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) },
                null, SKShaderTileMode.Clamp);
            SKRect kotak = new SKRect(0, 0, e.Info.Width, e.Info.Height);
            canvas.DrawRoundRect(kotak, 7, 7, new SKPaint() { Shader = GradientShaderTest });
        }

        private void Button1_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            var surface = e.Surface;
            var canvas = surface.Canvas;
            SKShader GradientShaderTest = SKShader.CreateLinearGradient(new SKPoint(0,0),
                new SKPoint(e.Info.Width , e.Info.Height ),
                new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) },
                null,SKShaderTileMode.Clamp);
            SKRect kotak = new SKRect(0,0,e.Info.Width,e.Info.Height);
            canvas.DrawRoundRect(kotak, 7, 7, new SKPaint() { Shader = GradientShaderTest });
        }
    }
}
