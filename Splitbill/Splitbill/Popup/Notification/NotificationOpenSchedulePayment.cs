﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Model.Notification;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace Splitbill.Popup
{
    public class NotificationOpenSchedulePayment : PopupPage
    {
        public event EventHandler CloseThis;
        public String Total;
        public String ToWhom;
        public Xamarin.Forms.Entry Code;
        public event EventHandler doThis;
        public bool editMode;

        public NotificationOpenScheduleModel model;
        public DatePicker cdp;
        public Xamarin.Forms.Entry shortDescription;

        Xamarin.Forms.Entry dateLabel;
        ObservableCollection<String> listTanggal;
        ObservableCollection<String> listBulan;
        ObservableCollection<String> listTahun;
        StackLayout Message;

        public NotificationOpenSchedulePayment(NotificationOpenScheduleModel mod)
        {
            model = mod;
            listTanggal = new ObservableCollection<string>();
            listBulan = new ObservableCollection<string>();
            listTahun = new ObservableCollection<string>();
            #region InitTanggal_Bulan_Tahun
            for (int i = 1; i < 32; i++)
            {
                listTanggal.Add(i + "");
            }

            for (int i = 1; i < 13; i++)
            {
                listBulan.Add(i + "");
            }

            for (int i = 1990; i < 2050; i++)
            {
                listTahun.Add(i + "");
            }
            #endregion 

            StackLayout Frame = new StackLayout();
            Frame.BackgroundColor = Color.FromRgb(0, 162, 232);

            Message = new StackLayout();
            Message.BackgroundColor = Color.White;
            Message.Margin = new Thickness(5);

            StackLayout tab1 = new StackLayout();
            tab1.Orientation = StackOrientation.Horizontal;
            Image closeIcon = new Image();
            closeIcon.WidthRequest = App.DpiFontSize * 10;
            closeIcon.HeightRequest = App.DpiFontSize * 10;
            closeIcon.Margin = new Thickness(10);
            closeIcon.HorizontalOptions = LayoutOptions.EndAndExpand;
            closeIcon.Source = "CLOSE.png";
            closeIcon.GestureRecognizers.Add(new TapGestureRecognizer((v, e) =>
            {
                PopupNavigation.PopAsync();
            }));

            Label Title = new Label();
            Title.Text = "Pay Schedule";
            Title.FontSize = App.DpiFontSize * 8;
            Title.VerticalTextAlignment = TextAlignment.Center;
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.HorizontalOptions = LayoutOptions.FillAndExpand;

            BoxView bx = new BoxView() { HorizontalOptions = LayoutOptions.StartAndExpand, WidthRequest = 30, HeightRequest = 30, Margin = new Thickness(5) };


            tab1.Children.Add(bx);
            tab1.Children.Add(Title);
            tab1.Children.Add(closeIcon);
            Message.Children.Add(tab1);

            Label subTitle = new Label();
            subTitle.Text = "Set the Date";
            subTitle.FontSize = App.DpiFontSize * 6;
            subTitle.HorizontalTextAlignment = TextAlignment.Center;
            Message.Children.Add(subTitle);

            //Buat Tab1
            cdp = new DatePicker();
            cdp.MinimumDate = DateTime.Now;
            cdp.DateSelected += Cdp_DateSelected;
            cdp.WidthRequest = 0;
            cdp.IsVisible = false;
            dateLabel = new Xamarin.Forms.Entry()
            {
                Text = DateTime.Today.Date.ToString("dd/MM/yyyy"),
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = App.DpiFontSize * 7,
                WidthRequest = 100,
                HorizontalOptions = LayoutOptions.Center
            };

            dateLabel.Focused += ((v,e) => {
                dateLabel.Unfocus();
                cdp.Focus();
            });
            Message.Children.Add(cdp);
            Message.Children.Add(dateLabel);
            //End
            shortDescription = new Xamarin.Forms.Entry();
            shortDescription.Placeholder = "Add Short Description";
            shortDescription.FontSize = App.DpiFontSize * 6;
            shortDescription.WidthRequest = 200;
            shortDescription.HorizontalTextAlignment = TextAlignment.Center;
            shortDescription.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Message.Children.Add(shortDescription);

            StackLayout tab3 = new StackLayout();
            tab3.Orientation = StackOrientation.Horizontal;

            Button setScheduleButton = new Button();
            setScheduleButton.Text = "Set Schedule";
            setScheduleButton.TextColor = Color.White;
            setScheduleButton.BackgroundColor = Color.FromRgb(0, 162, 232);
            setScheduleButton.Margin = new Thickness(25, 15, 25, 15);
            setScheduleButton.Clicked += setScheduleButton_Clicked;

            Button cancelButton = new Button();
            cancelButton.BackgroundColor = Color.LightGray;
            cancelButton.Text = "Delete";
            cancelButton.TextColor = Color.White;
            cancelButton.HorizontalOptions = LayoutOptions.CenterAndExpand;
            cancelButton.Margin = new Thickness(25, 15, 25, 15);
            cancelButton.Clicked += (sender, v) => { model = new NotificationOpenScheduleModel(); CloseThis(this, EventArgs.Empty); };
            Message.Children.Add(tab3);
            shortDescription.Text = model.description;
            cdp.Date = DateTime.Now.AddDays(1);
            cdp.MinimumDate = DateTime.Now.AddDays(1);

            if (model.clicked == true)
            {
                Title.Text = "Edit Schedule";
                cdp.Date = model.dateSetting;
                setScheduleButton.Text = "Edit";
                tab3.Children.Add(setScheduleButton);
                tab3.Children.Add(cancelButton);
            }
            else
            {
                setScheduleButton.HorizontalOptions = LayoutOptions.CenterAndExpand;
                tab3.Children.Add(setScheduleButton);
            }

            Frame.Children.Add(Message);
            Frame.VerticalOptions = LayoutOptions.Center;
            Frame.Margin = new Thickness(15);
            Frame.BackgroundColor = Color.FromRgb(0, 162, 232);
            Content = Frame;
            this.BackgroundColor = new Color(0, 0, 0, 0.4);
        }

        private void Cdp_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (e.NewDate != model.dateSetting.Date)
            {
                dateLabel.Text = e.NewDate.ToString("dd/MM/yyyy");
                model.dateSetting = e.NewDate;
                model.clicked = true;
            }
            else
            {
                model.clicked = true;
            }
        }

        private void setScheduleButton_Clicked(object sender, EventArgs e)
        {
            model.description = shortDescription.Text;
            model.clicked = true;
            doThis(this, EventArgs.Empty);
        }
    }
}
