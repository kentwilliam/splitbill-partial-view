﻿using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Notification
{
    public class NotificationOpenSend:PopupPage
    {
        SendNotificationDetailJson nota;
        Label MessageStackLabel;
        Label Description;
        public async void InitData()
        {
            try
            {
                var resp = App._Api.GetDataResponseFromServer("transaction/transaction_detail", JsonConvert.SerializeObject(nota));
                var resu = JsonConvert.DeserializeObject<TransactionDetailJsonResponse_send>(resp.Result);

                if (App._Api.ProcessDataOrNot(resu.message, resu.send == null ? false : true))
                {
                    App.GlobalDataUser.NOTIFICATIONCOUNTER = resu.notification;
                    App.GlobalDataUser.NOTIFICATIONSHOW = Convert.ToInt32(resu.notification) > 0 ? true : false;

                    String Message = resu.send[0].sender_id + " " + resu.send[0].bold + " " + resu.send[0].after + " " + "Rp. " + resu.send[0].amount;
                    MessageStackLabel.Text = Message;
                    Description.Text = resu.send[0].description;
                    if (String.IsNullOrEmpty(Description.Text) || String.IsNullOrWhiteSpace(Description.Text))
                    {
                        Description.Text = "---";
                    }
                }
                else
                {
                    await Task.Delay(200);
                    App.MyHomePage.LogOut();
                    
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("","No Internet","ok");
            }
            
        }
        public NotificationOpenSend(SendNotificationDetailJson Nota)
        {
            this.nota = Nota;
            StackLayout mainStack = new StackLayout() {
                Margin = new Thickness(5),
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.White,
                IsClippedToBounds = true
            };
            
            MessageStackLabel = new Label() {
                FontSize = App.DpiFontSize * 6,
                HorizontalTextAlignment = TextAlignment.Center
            };

            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,HeightRequest = 40};
            BoxView bx1 = new BoxView() {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start,
            };
            Label Judul = new Label()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
                Text = "Send Money",
                FontSize = App.DpiFontSize * 8,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            Image xicon = new Image() {
                WidthRequest = App.DpiFontSize * 10,
                HeightRequest = App.DpiFontSize * 10,
                Aspect = Aspect.AspectFit,
                Source = "CLOSE.png",
                Margin = new Thickness(5)
            };
            xicon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                App.MyHomePage.ToNotificationPage();
                await PopupNavigation.PopAsync();
            }));
            tab1.Children.Add(bx1);
            tab1.Children.Add(Judul);
            tab1.Children.Add(xicon);

            Frame wrapper = new Frame() {
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 300,
                Margin = 0,
                Padding =0,
                CornerRadius = 5,
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.Center,
                Content = mainStack,
                BackgroundColor = Color.CornflowerBlue,
                OutlineColor = Color.CornflowerBlue
            };
            Description = new Label()
            {
                FontSize = App.DpiFontSize * 8,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                HorizontalTextAlignment = TextAlignment.Center
            };

            mainStack.Children.Add(tab1);
            mainStack.Children.Add(new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1,Margin = new Thickness(15,0,15,0)});
            mainStack.Children.Add(MessageStackLabel);
            mainStack.Children.Add(Description);
            mainStack.Children.Add(new BoxView() {
                HeightRequest = 20,
                BackgroundColor = Color.Transparent
            });
            
            
            Content = wrapper;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            InitData();
        }
        protected override bool OnBackButtonPressed()
        {
            App.MyHomePage.ToNotificationPage();
            return false;
        }
    }
}
