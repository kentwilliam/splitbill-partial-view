﻿using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Encode;
using Splitbill.Model.Send;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Notification
{
    public class PopupOtpNotificationMoney : PopupPage
    {
        public EventHandler Eve;
        NotificationOpenSendMoneyJson tempModel;
        Entry OTPEntry;
        public PopupOtpNotificationMoney(NotificationOpenSendMoneyJson model)
        {
            tempModel = model;
            Grid G = new Grid() { WidthRequest = 340, HeightRequest = 400,HorizontalOptions = LayoutOptions.Center,VerticalOptions = LayoutOptions.Center };
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 15,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                HeightRequest = 300
            };

            Grid ContentGrid = new Grid();

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(150) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star) });

            OTPEntry = new Entry()
            {
                Text = "",
                FontSize = App.DpiFontSize * 8,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.Black,
                Margin = new Thickness(15, 0, 15, 0),
                Placeholder = "Insert Otp",
                Keyboard = Keyboard.Numeric
            };

            Button button1 = new Button() { BackgroundColor = Color.CornflowerBlue,
                Text = "Pay Now",
                BorderRadius = 10,
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 300,
                HeightRequest = 50,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                FontSize = App.DpiFontSize * 6,
                TextColor = Color.White
                
            };
            button1.Clicked += Button1_Clicked;
            StackLayout Message = new StackLayout()
            {
                HeightRequest = 175,
                WidthRequest = 300,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = new Thickness(0,25,0,0)
            };

            Label MessageText = new Label() {
            Text = "Do You Want to Pay money \n Rp "+model.total.ToString("n0")+" ?",
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            VerticalTextAlignment = TextAlignment.Center,
            HorizontalTextAlignment = TextAlignment.Center,
            FontSize = App.DpiFontSize * 8,
            TextColor = Color.Black
            };

            Message.Children.Add(MessageText);
            ContentGrid.Children.Add(Message, 0, 0);
            ContentGrid.Children.Add(button1, 0, 2);
            ContentGrid.Children.Add(OTPEntry, 0, 1);

            box.Content = ContentGrid;
            G.Children.Add(box);

            CircleImage ciimg = new CircleImage()
            {
                Source = "Send_Request.png",
                BorderColor = Color.CornflowerBlue,
                BorderThickness = 1,
                Aspect = Aspect.Fill,
                WidthRequest = 100,
                HeightRequest = 100,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Center
            };
            G.Children.Add(ciimg, 0, 0);

            this.BackgroundColor = Color.FromRgba(25, 25, 25, 0.8f);
            ScrollView sv = new ScrollView() { Content = G};
            Content = sv;
        }

        private async void Button1_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(OTPEntry.Text))
            {
                await DisplayAlert("","Insert OTP","Ok");
            }
            else {

                tempModel.OTP = OTPEntry.Text;
                var resp = App._Api.GetDataResponseFromServer("transaction/approve_money_request", JsonConvert.SerializeObject(tempModel));
                String result = resp.Result;
                if (result.ToLower() != "no internet")
                {
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(result);
                    if (App._Api.ProcessDataOrNot(resu.message, false))
                    {
                        await DisplayAlert("", resu.message, "ok");
                        await PopupNavigation.PopAsync();
                        await PopupNavigation.PopAsync();
                        App.MyHomePage.BackToHome();
                        App.GlobalDataUser.BALANCEDOUBLEVALUE = App.GlobalDataUser.BALANCEDOUBLEVALUE - tempModel.total;
                        App.GlobalDataUser.BALANCE = "Rp " + App.GlobalDataUser.BALANCEDOUBLEVALUE.ToString("n0");
                    }
                    else
                    {
                        await DisplayAlert("", resu.message, "Ok");
                        await PopupNavigation.PopAsync();
                        await PopupNavigation.PopAsync();
                        await Task.Delay(200);
                        App.MyHomePage.LogOut();
                    }
                }
                else
                {
                    await DisplayAlert("","No Internet Connection","Ok");
                }
            }
            
          
        }
    }
}
