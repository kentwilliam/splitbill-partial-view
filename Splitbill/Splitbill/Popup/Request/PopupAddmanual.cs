﻿using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill.Popup.Request
{
    public class PopupAddmanual:PopupPage
    {
        public EventHandler Eve;
        public String NamaMakanan;
        public String HargaTotal;
        public String Persentase;
        public String Total;
        public String Sisa;

        MySlider sliderPersentase;
        Entry entryHargaTotal;
        Entry entryTotal;
        public PopupAddmanual()
        {
            Grid G = new Grid() { WidthRequest = 300, HeightRequest = 350 };
            ScrollView sv = new ScrollView() { VerticalOptions = LayoutOptions.FillAndExpand};
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 10,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                Margin = new Thickness(5,0,5,25)
            };

            StackLayout wrapper = new StackLayout();
            wrapper.Margin = new Thickness(10, 5, 10, 5);

            StackLayout JudulStack = new StackLayout() { Orientation = StackOrientation.Horizontal};

            Image CloseIcon = new Image()
            {
                WidthRequest = App.DpiFontSize * 10,
                HeightRequest = App.DpiFontSize * 10,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                Source = "CLOSE.png"
            };
            CloseIcon.GestureRecognizers.Add(new TapGestureRecognizer(async (v,e)=> { await PopupNavigation.PopAsync(); }));
            BoxView bx = new BoxView()
            {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start
            };
            Label Judul = new Label() {Text = "Add Manual",FontSize = App.DpiFontSize * 10
            ,HorizontalOptions = LayoutOptions.CenterAndExpand};

            StackLayout NamaMenuLayout = new StackLayout() { Orientation = StackOrientation.Horizontal};
            Label labeljudulNamaMenu = new Label() { Text = "Nama Menu :", FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center};
            Entry entryJudulNamaMenu = new Entry() { HorizontalOptions = LayoutOptions.FillAndExpand,FontSize = App.DpiFontSize * 6 };

            entryJudulNamaMenu.TextChanged += ((v, e) =>{NamaMakanan = e.NewTextValue;});

            StackLayout HargaTotalLayout = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Label labeljudulHargaTotal = new Label() { Text = "Harga Total:",FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center };
            entryHargaTotal = new Entry() { HorizontalOptions = LayoutOptions.FillAndExpand, FontSize = App.DpiFontSize * 6 };
          

            StackLayout TotalLayout = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Label labelTotal = new Label() { Text = "Total     :", FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center };
            entryTotal = new Entry() { HorizontalOptions = LayoutOptions.FillAndExpand, FontSize = App.DpiFontSize * 6, IsEnabled = false };

            StackLayout PersentaseWrapper = new StackLayout(); 
            StackLayout PersentaseLayout = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Label LabelPersentaseValue = new Label() {WidthRequest = 35,Text="0"};
            Label labelPersentase = new Label() { Text = "Persentase:", FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center };

            sliderPersentase = new MySlider() {HorizontalOptions = LayoutOptions.FillAndExpand,Maximum = 100,Minimum = 0 ,HeightRequest = 30,WidthRequest = 300};
            
            sliderPersentase.ValueChanged += ((v, e) => {
                var newStep = Math.Round(e.NewValue / 1);
                sliderPersentase.Value = newStep * 1;
                LabelPersentaseValue.Text = sliderPersentase.Value +"";

                try
                {
                    double val = Convert.ToDouble(newStep);
                    var yangHarusDibayarOwner = Convert.ToDouble(entryHargaTotal.Text) * (val / 100);
                    Sisa =(Convert.ToDouble(entryHargaTotal.Text) - yangHarusDibayarOwner)+"";
                    entryTotal.Text = yangHarusDibayarOwner + "";
                }
                catch (Exception ex) { entryTotal.Text = "0"; } 
            });

            Button But = new Button() { Text = "Confirm",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,FontAttributes = FontAttributes.Bold,
                FontSize = App.DpiFontSize * 6,
                HorizontalOptions = LayoutOptions.FillAndExpand,VerticalOptions = LayoutOptions.Center,
                BorderRadius = 10,
                Margin = new Thickness(20)
            };
            But.Clicked += (async(v,e)=> {
                (v as Button).IsEnabled = false;
                Persentase = sliderPersentase.Value + "";
                Sisa = entryTotal.Text;
                HargaTotal = entryHargaTotal.Text;
                if (String.IsNullOrEmpty(NamaMakanan))
                {
                    await DisplayAlert("", "Please Insert Nama Makanan", "ok");
                }
                else if (String.IsNullOrEmpty(entryTotal.Text)) { await DisplayAlert("", "Please Add Percentage", "ok"); }
                else if (entryTotal.Text == "0") { await DisplayAlert("", "Please Add Percentage Or Add Harga Total", "ok"); }
                else
                {
                    Eve(this, EventArgs.Empty);
                }
                (v as Button).IsEnabled = true;
            });

            entryHargaTotal.TextChanged += ((v, e) =>
            {
                var newStep = sliderPersentase.Value;
                try
                {
                    double val = Convert.ToDouble(newStep);
                    var yangHarusDibayarOwner = Convert.ToDouble(entryHargaTotal.Text) * (val / 100);
                    Sisa = (Convert.ToDouble(entryHargaTotal.Text) - yangHarusDibayarOwner) + "";
                    entryTotal.Text = yangHarusDibayarOwner + "";
                }
                catch (Exception ex) { entryTotal.Text = "0"; }
            });
            NamaMenuLayout.Children.Add(labeljudulNamaMenu);
            NamaMenuLayout.Children.Add(entryJudulNamaMenu);

            HargaTotalLayout.Children.Add(labeljudulHargaTotal);
            HargaTotalLayout.Children.Add(entryHargaTotal);

            PersentaseWrapper.Children.Add(PersentaseLayout);
            PersentaseLayout.Children.Add(labelPersentase);
            PersentaseLayout.Children.Add(LabelPersentaseValue);
            PersentaseWrapper.Children.Add(sliderPersentase);

            TotalLayout.Children.Add(labelTotal);
            TotalLayout.Children.Add(entryTotal);

            JudulStack.Children.Add(bx);
            JudulStack.Children.Add(Judul);
            JudulStack.Children.Add(CloseIcon);

            wrapper.Children.Add(JudulStack);
            wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1f});
            wrapper.Children.Add(NamaMenuLayout);
            wrapper.Children.Add(HargaTotalLayout);
            wrapper.Children.Add(PersentaseWrapper);
            wrapper.Children.Add(TotalLayout);
            wrapper.Children.Add(But);
            box.Content = wrapper;


            this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.6f);
            sv.Content = box;
            Content = sv;
        }
    }
}
