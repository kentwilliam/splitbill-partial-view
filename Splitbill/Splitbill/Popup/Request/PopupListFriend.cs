﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Popup.Request
{
    public class PopupListFriend:PopupPage
    {
        public EventHandler DoPush;
        SfListView list = new SfListView();
        public List<Model.ListFriend.ListFriendModal> friend = new List<Model.ListFriend.ListFriendModal>();
        public PopupListFriend()
        {
            
        }
        public void InitView()
        {
            StackLayout head = new StackLayout() { BackgroundColor = Color.FromRgb(0, 162, 232),Orientation = StackOrientation.Horizontal};
            Label judul = new Label() { Text = "Friend", FontSize = 20,VerticalTextAlignment = TextAlignment.Center ,TextColor = Color.White};
            Image icon = new Image() { Source = "BackIcon.png",Aspect = Aspect.AspectFit,Margin = new Thickness(5) };
            icon.GestureRecognizers.Add(new TapGestureRecognizer(async (v,e)=> {
                await PopupNavigation.PopAsync();
            }));

            head.Children.Add(icon);
            head.Children.Add(judul);


            Grid Body = new Grid();
            Body.WidthRequest = 300;
            Body.HeightRequest = 300;
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star)});
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            Body.BackgroundColor = Color.FromRgb(250,252,252);
            Body.VerticalOptions = LayoutOptions.Center;
            Body.HorizontalOptions = LayoutOptions.Center;
            list.ItemTemplate = new DataTemplate(() =>
            {
                Grid sl = new Grid();
                sl.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3,GridUnitType.Star)});
                sl.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                sl.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star)});
                sl.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });

                Label nama = new Label(); 
                nama.Margin = new Thickness(15,0,0,0);
                nama.HorizontalOptions = LayoutOptions.StartAndExpand;
                nama.VerticalTextAlignment = TextAlignment.Center;
                nama.SetBinding(Label.TextProperty, "NAMA");
                Image img = new Image() { Source = "CLOSE.png",WidthRequest = App.DpiFontSize * 10,HeightRequest = App.DpiFontSize*10, HorizontalOptions = LayoutOptions.EndAndExpand, Margin = new Thickness(0, 10, 10, 10), Aspect = Aspect.AspectFit };

                img.GestureRecognizers.Add(new TapGestureRecognizer(itemClicked));


                BoxView bx = new BoxView() { BackgroundColor = Color.LightGray};

                sl.Children.Add(nama,0,0);
                sl.Children.Add(img,1,0);
                sl.Children.Add(bx,0,1);
                Grid.SetColumnSpan(bx,2);
                return sl;
            });
            list.ItemSize = 40;


            Button savebtn = new Button() {Text = "Save",TextColor = Color.White,BackgroundColor = Color.FromRgb(0, 162, 232) ,BorderRadius = 0};
            savebtn.Clicked += Savebtn_Clicked;


            Body.Children.Add(savebtn,0,11);
            Body.Children.Add(head,0,0);
            Body.Children.Add(list,0,2);
            Grid.SetRowSpan(head,2);
            Grid.SetRowSpan(list,9);
            Grid.SetRowSpan(savebtn, 2);

            Content = Body;
        }

        private void itemClicked(Xamarin.Forms.View arg1, object arg2)
        {
            friend.Remove((list.SelectedItem as Model.ListFriend.ListFriendModal));
            list.ItemsSource = null;
            list.ItemsSource = friend;
        }

        private void Savebtn_Clicked(object sender, EventArgs e)
        {
            DoPush(this, EventArgs.Empty);
        }

        public void InitList(String[] Friends)
        {
            foreach (var x in Friends)
            {
                friend.Add(new Model.ListFriend.ListFriendModal() { NAMA = x.TrimStart().TrimEnd()});
            }
            list.ItemsSource = null;
            list.ItemsSource = friend;
        }
    }
}
