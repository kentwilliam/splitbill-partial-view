﻿using FFImageLoading.Forms;
using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill.Popup.Request
{
    public class PopupLocation:PopupPage
    {
        public EventHandler Eve;
        public String Location;
        public PopupLocation(String text)
        {
            Location = text;
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 10,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                Margin = new Thickness(5)
            };

            StackLayout wrapper = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand,Spacing = 1};
            wrapper.Margin = new Thickness(10,5,10,5);

            StackLayout locationLayout = new StackLayout() { Orientation = StackOrientation.Horizontal};
            CachedImage icon = new CachedImage() {
                HeightRequest = 15 * App.DpiFontSize,
                WidthRequest = 15 * App.DpiFontSize,
                Source = "location.png",
                HorizontalOptions = LayoutOptions.Start
            };
            Entry ent = new Entry() {
                Placeholder = "Location",
                FontSize = 8*App.DpiFontSize,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Text = text
            };
            ent.TextChanged += ((v,e) =>
            {
                Location = e.NewTextValue;
            });
            Button but = new Button() {
                WidthRequest = 125,
                FontSize = 7* App.DpiFontSize,
                Text = "Set Location",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                Margin = new Thickness(10)
            };
            but.Clicked += But_Clicked;
            locationLayout.Children.Add(icon);
            locationLayout.Children.Add(ent);
            wrapper.Children.Add(locationLayout);
            wrapper.Children.Add(but);
            box.Content = wrapper;
            this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.6f);
            Content = box;
        }

        private void But_Clicked(object sender, EventArgs e)
        {
            Eve(Location, EventArgs.Empty);
        }
    }
}
