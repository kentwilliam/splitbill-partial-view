﻿using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill.Popup.Request
{
    public class PopupAdditional:PopupPage
    {
        public EventHandler Eve;
        public String Discount;
        public String Hashtag;
        public String Taxes;
        public String ServiceCharge;
        MySlider sliderHargaTotalLayout;
        MySlider SliderTotal;
        MySlider sliderPersentase;
        public PopupAdditional(int taxes,int servicecharge,int diskon)
        {
            Taxes = taxes == 0?10+"":taxes+"";
            ServiceCharge = servicecharge == 0? 5+"":servicecharge+"";
            Discount = diskon == 0 ? 0 + "" : diskon+"";

            ScrollView sv = new ScrollView() { VerticalOptions = LayoutOptions.FillAndExpand};
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 10,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                Margin = new Thickness(5)
            };

            StackLayout wrapper = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand,Spacing = 1};
            wrapper.Margin = new Thickness(10,5,10,5);

            StackLayout JudulStack = new StackLayout() { Orientation = StackOrientation.Horizontal };

            Image CloseIcon = new Image()
            {
                WidthRequest = App.DpiFontSize * 10,
                HeightRequest = App.DpiFontSize * 10,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                Source = "CLOSE.png"
            };
            CloseIcon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {await PopupNavigation.PopAsync(); }));
            BoxView bx = new BoxView()
            {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start
            };
            Label Judul = new Label() {Text = "Additional",FontSize = App.DpiFontSize * 10,
                HorizontalOptions = LayoutOptions.CenterAndExpand};

            StackLayout NamaMenuLayout = new StackLayout() { Orientation = StackOrientation.Horizontal};
            Label labeljudulNamaMenu = new Label() { Text = "Hash Tag :", FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center};
            Entry entryJudulNamaMenu = new Entry() { HorizontalOptions = LayoutOptions.FillAndExpand,FontSize = App.DpiFontSize * 6 };

            entryJudulNamaMenu.TextChanged += ((v, e) =>{ Hashtag = e.NewTextValue;});

            StackLayout wrapperHargaTotalLayout = new StackLayout();
            StackLayout HargaTotalLayout = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Label labeljudulHargaTotal = new Label() { Text = "Discount :",FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center };
            Label labelHargaTotalvalue = new Label() { Text = "0",HorizontalOptions = LayoutOptions.FillAndExpand, FontSize = App.DpiFontSize * 6 };
            sliderHargaTotalLayout = new MySlider() { HorizontalOptions = LayoutOptions.FillAndExpand, Minimum = 0,Maximum= 100 ,WidthRequest = 300,HeightRequest = 40};

            sliderHargaTotalLayout.ValueChanged += ((v, e) => {
                var newStep = Math.Round(e.NewValue / 1);
                sliderHargaTotalLayout.Value = newStep * 1;
                labelHargaTotalvalue.Text = sliderHargaTotalLayout.Value + "";
                Discount = sliderHargaTotalLayout.Value + "";
            });

            StackLayout wrapperTotalLayout = new StackLayout();
            StackLayout TotalLayout = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Label labelTotal = new Label() { Text = "Taxes     :", FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center };
            Label labeltotalValue = new Label() {Text = Taxes, FontSize = App.DpiFontSize * 6 };
            SliderTotal = new MySlider() { HorizontalOptions = LayoutOptions.FillAndExpand,Minimum = 0,Maximum =100,HeightRequest = 40,WidthRequest = 300};
                SliderTotal.ValueChanged += ((v, e) => {
                var newStep = Math.Round(e.NewValue / 1);
                SliderTotal.Value = newStep * 1;
                labeltotalValue.Text = SliderTotal.Value + "";
                Taxes = SliderTotal.Value+"";
            });

            StackLayout PersentaseWrapper = new StackLayout(); 
            StackLayout PersentaseLayout = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Label LabelPersentaseValue = new Label() {WidthRequest = 35,Text=ServiceCharge,FontSize = App.DpiFontSize * 6};
            Label labelPersentase = new Label() { Text = "Service Charge:", FontSize = App.DpiFontSize * 6, VerticalTextAlignment = TextAlignment.Center };
            sliderPersentase = new MySlider() {HorizontalOptions = LayoutOptions.FillAndExpand,Maximum = 100,Minimum = 0,HeightRequest = 40,WidthRequest = 300 };
            sliderPersentase.ValueChanged += ((v, e) => {
                var newStep = Math.Round(e.NewValue / 1);
                sliderPersentase.Value = newStep * 1;
                LabelPersentaseValue.Text = sliderPersentase.Value +"";
                ServiceCharge = sliderPersentase.Value + "";
            });
            StackLayout buttonLayout = new StackLayout() {Margin = new Thickness(20) };
            Button But = new Button() { Text = "Confirm",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,FontAttributes = FontAttributes.Bold,
                FontSize = App.DpiFontSize * 6,
                HorizontalOptions = LayoutOptions.Fill,BorderRadius = 5
            };
            But.Clicked += (async(v,e)=> {
                (v as Button).IsEnabled = false;
                Eve(this, EventArgs.Empty);
                (v as Button).IsEnabled = true;
            });
            NamaMenuLayout.Children.Add(labeljudulNamaMenu);
            NamaMenuLayout.Children.Add(entryJudulNamaMenu);

            wrapperHargaTotalLayout.Children.Add(HargaTotalLayout);
            HargaTotalLayout.Children.Add(labeljudulHargaTotal);
            HargaTotalLayout.Children.Add(labelHargaTotalvalue);
            wrapperHargaTotalLayout.Children.Add(sliderHargaTotalLayout);

            PersentaseWrapper.Children.Add(PersentaseLayout);
            PersentaseLayout.Children.Add(labelPersentase);
            PersentaseLayout.Children.Add(LabelPersentaseValue);
            PersentaseWrapper.Children.Add(sliderPersentase);

            wrapperTotalLayout.Children.Add(TotalLayout);
            TotalLayout.Children.Add(labelTotal);
            TotalLayout.Children.Add(labeltotalValue);
            wrapperTotalLayout.Children.Add(SliderTotal);

            JudulStack.Children.Add(bx);
            JudulStack.Children.Add(Judul);
            JudulStack.Children.Add(CloseIcon);

            buttonLayout.Children.Add(But);

            wrapper.Children.Add(JudulStack);
            wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1f});
            wrapper.Children.Add(NamaMenuLayout);
            wrapper.Children.Add(wrapperHargaTotalLayout);
            wrapper.Children.Add(PersentaseWrapper);
            wrapper.Children.Add(wrapperTotalLayout);
            wrapper.Children.Add(buttonLayout);
            box.Content = wrapper;


            this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.6f);
            sv.Content = box;
            Content = sv;
        }
        protected override void OnAppearing()
        { 
            base.OnAppearing();
            sliderHargaTotalLayout.Value = Convert.ToInt32(Discount);
            sliderHargaTotalLayout.Focus();
            sliderPersentase.Value = Convert.ToInt32(ServiceCharge);
            sliderPersentase.Focus();
            SliderTotal.Value = Convert.ToInt32(Taxes);
            SliderTotal.Focus();
        }
    }
}
