﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Model.RequestModel;
using Splitbill.Model.Send;
using Syncfusion.DataSource.Extensions;
using Syncfusion.ListView.XForms;
using Syncfusion.ListView.XForms.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Request
{
    public class PopupFindFriendsRequest:PopupPage
    {
        public EventHandler Eve;
        public EventHandler EveGroup;
        Grid backGrid = new Grid();
        public SfListView sflistFriend;
        public SfListView sflistGroup;
        Label Judul;
        List<FindFriendSendModel> ListModel = new List<FindFriendSendModel>();
        //dibuat untuk mengambil Data ketika modal close
        public String nama { get; set; }
        public String noHp { get; set; }
        String ShowedFriend="";
        XFShapeView.ShapeView shape;
        XFShapeView.ShapeView shapeGroup;
        PopupGroupModel modelGroup = new PopupGroupModel();
        Button PrevButton = new Button();
        public void initData()
        {
            String[] tempFriend = ShowedFriend.Split(',');

            Task.Run(() => {
                Device.BeginInvokeOnMainThread(async() => {
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });
                    ListModel.Clear();
                    #region Get Friends Data
                    var resp = App._Api.GetDataResponseFromServer("user/friends_data", JsonConvert.SerializeObject(new Username_Token()
                    {
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME
                    }));
                    String Result = resp.Result;
                    if (Result.ToLower() == "no internet")
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await DisplayAlert("", "No Internet Connection", "Ok");
                        });
                    }
                    else
                    { 
                    var resu = JsonConvert.DeserializeObject<HeadFriendsJsonResponse>(Result);
                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        if (App._Api.ProcessDataOrNot(resu.message, resu.friends == null ? false : true))
                        {
                            foreach (var r in resu.friends)
                            {
                                var ctr = tempFriend.Where(x => x.ToLower().Trim() == r.id_teman.ToLower().Trim());
                                if (ctr.Count() <= 0)
                                {
                                    FindFriendSendModel model = new FindFriendSendModel();
                                    model.nama = r.id_teman;
                                    model.noHp = r.phone_number;
                                    model.photo = String.IsNullOrEmpty(r.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + r.photo;
                                    ListModel.Add(model);
                                }
                            }

                            sflistFriend.ItemsSource = null;
                            sflistFriend.ItemsSource = ListModel;
                        }
                        else
                        {
                            await Task.Delay(200);
                            App.MyHomePage.LogOut();
                        }
                    });
                    }
                    #endregion

                    #region Get Friends Data
                    modelGroup.MEMBER.Clear();
                    var resp1 = App._Api.GetDataResponseFromServer("group/groups", JsonConvert.SerializeObject(new Api.Encode.GetGrousJson()
                    {
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME,
                        id = null
                    }));
                    Result = resp1.Result;
                    if (Result.ToLower() == "no internet")
                    {
                        Device.BeginInvokeOnMainThread(async() =>
                        {
                            await DisplayAlert("","No Internet Connection","Ok");
                        });
                    }
                    else
                    { 
                        var resu1 = JsonConvert.DeserializeObject<Api.Decode.GetGroupsJsonRespond>(Result);
                        Device.BeginInvokeOnMainThread(async() =>
                        {
                            if (App._Api.ProcessDataOrNot(resu1.message, resu1.group == null ? false : true))
                            {
                                ObservableCollection<GroupMember> temp = new ObservableCollection<GroupMember>();
                                foreach (var r in resu1.group)
                                {
                                    List<String> tempnama = new List<string>();
                                    tempnama = r.member_username.Split(',').ToList().Where(x => x != "").ToList();
                                    List<String> tempgambar = r.member_photo != null ? r.member_photo.Split(',').ToList() : new List<string>();
                                    for (int i = 0; i < tempnama.Count(); i++)
                                    {
                                        GroupMember gm = new GroupMember()
                                        {
                                            GROUPNAME = r.name,
                                            MEMBERNAME = tempnama[i],
                                            IMGSRC = tempgambar.Count() > 0 ? "http://www.urunan.id/splitbill/" + tempgambar[i] : "",
                                            SELECTED = false,
                                            GROUPID = r.idGroup
                                        };
                                        temp.Add(gm);
                                    }
                                }
                                modelGroup.MEMBER = temp.OrderByDescending(x => x.GROUPID == App.GroupId).ToObservableCollection();
                                sflistGroup.ItemsSource = null;
                                sflistGroup.ItemsSource = modelGroup.MEMBER;
                                sflistGroup.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
                                {
                                    PropertyName = "namagroup",
                                    KeySelector = (object obj1) =>
                                    {
                                        var item = obj1 as Splitbill.Model.RequestModel.GroupMember;
                                        return item.GROUPNAME;
                                    },
                                });
                                sflistGroup.CollapseAll();
                                foreach (var x in sflistGroup.DataSource.DisplayItems)
                                {
                                    if (x is GroupResult)
                                    {
                                        GroupResult gr = x as GroupResult;
                                        var asd = gr.Items.ToList<GroupMember>();
                                        if (asd.Count() > 0)
                                        {
                                            if (asd.First().GROUPID == App.GroupId)
                                            { gr.Expand(); sflistGroup.ExpandGroup(gr); sflistGroup.RefreshView(); BCompleted_Clicked(new Button(), EventArgs.Empty); return; }

                                        }
                                    }
                                }
                            }
                            else
                            {
                                await Task.Delay(200);
                                App.MyHomePage.LogOut();
                            }
                        });
                    }
                    #endregion

                Device.BeginInvokeOnMainThread(async () => {
                    await Task.Delay(300);
                    try
                    { await PopupNavigation.PopAsync(); }
                    catch (Exception ex) { }
                    
                });
            });
            
        }
        public PopupFindFriendsRequest(String Friends)
        {
            ShowedFriend = Friends;
            Judul = new Label()
            {
                Text = "Select Friend",
                FontSize = 10*App.DpiFontSize,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalTextAlignment = TextAlignment.Start,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White
            };
            for (int i = 0; i < 20; i++)
            {
                backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            #region Create Friend Page 
            shape = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 15,
                Margin = new Thickness(15, 0, 15, 0),
                Color = Color.White
            };
            StackLayout search = new StackLayout() { Orientation = StackOrientation.Vertical };
            SearchBar bar = new SearchBar() {  HeightRequest = 35,FontSize = App.DpiFontSize*7,TextColor = Color.Gray};
            bar.TextChanged += Bar_TextChanged;
            sflistFriend = new SfListView()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.Transparent,
                Margin = new Thickness(5, 0, 5, 0),
                ItemTemplate = new DataTemplate(() =>
                {
                    StackLayout wrapper = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(0, 5, 0, 5) };

                    CircleImage ciImg = new CircleImage()
                    {
                        Margin = new Thickness(10, 0, 0, 0),
                        WidthRequest = 50,
                        HeightRequest = 50,
                        Aspect = Aspect.AspectFill,
                        BorderColor = Color.LightGray,
                        BorderThickness = 1,
                    };
                    ciImg.SetBinding(CircleImage.SourceProperty, "photo");

                    StackLayout mainStack = new StackLayout() { Orientation = StackOrientation.Vertical };
                    Label Nama = new Label() { TextColor = Color.Black,FontSize = 8* App.DpiFontSize};
                    Nama.SetBinding(Label.TextProperty, "nama");

                    Label nomorTelpon = new Label() { TextColor = Color.Black ,FontSize = App.DpiFontSize * 8};
                    nomorTelpon.SetBinding(Label.TextProperty, "noHp");

                    mainStack.Children.Add(Nama); mainStack.Children.Add(nomorTelpon);


                    wrapper.Children.Add(ciImg);
                    wrapper.Children.Add(mainStack);
                    StackLayout OuterWrapper = new StackLayout();
                    OuterWrapper.Children.Add(wrapper);
                    OuterWrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 });
                    return new ViewCell() { View = OuterWrapper };
                }),
                SelectionMode = SelectionMode.Multiple,
                SelectionGesture = TouchGesture.Tap,
                ItemSize = 70
            };

            Button SelectFriendButton = new Button()
            {
                VerticalOptions = LayoutOptions.End,
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10),
                Text = "Add Friend",
                FontSize = 8*App.DpiFontSize
            };
            SelectFriendButton.IsEnabled = false;
            SelectFriendButton.Clicked += SelectFriendButton_Clicked;
            search.Children.Add(bar);
            search.Children.Add(sflistFriend);
            search.Children.Add(SelectFriendButton);
            shape.Content = search;
            sflistFriend.SelectionChanged += ((v, e) => {
                if (sflistFriend.SelectedItems.Count > 0)
                { SelectFriendButton.IsEnabled = true; }
                else
                { SelectFriendButton.IsEnabled = false; }
            });
            #endregion


            #region Create Group Page 
            StackLayout GroupStackView = new StackLayout() { Orientation = StackOrientation.Vertical };
            shapeGroup = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 15,
                Margin = new Thickness(15, 0, 15, 0),
                Color = Color.White,
                TranslationX = 500
            };
            StackLayout searchGroup = new StackLayout() { Orientation = StackOrientation.Vertical ,IsClippedToBounds = true};
            sflistGroup = new SfListView();
            sflistGroup.IsClippedToBounds = true;
            sflistGroup.SelectionGesture = TouchGesture.Tap;
            sflistGroup.SelectionMode = SelectionMode.Single;
            sflistGroup.SelectionBackgroundColor = Color.Transparent;
            sflistGroup.IsStickyHeader = true;
            sflistGroup.ItemSize = 51;
            sflistGroup.Margin = new Thickness(5);
            sflistGroup.SelectionMode = SelectionMode.Multiple;
            sflistGroup.ItemTemplate = new DataTemplate(() =>
            {
                StackLayout sl = new StackLayout() { Margin = new Thickness(5,5,0,0),Orientation = StackOrientation.Horizontal ,HeightRequest = 49};
                CachedImage cimg = new CachedImage()
                {
                    CacheType = FFImageLoading.Cache.CacheType.All,
                    DownsampleToViewSize = true,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };
                cimg.Transformations.Add(new CircleTransformation());
                cimg.SetBinding(CachedImage.SourceProperty,"IMGSRC");

                XFShapeView.ShapeView sv = new XFShapeView.ShapeView()
                {
                    ShapeType = XFShapeView.ShapeType.Circle,
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    WidthRequest = 35,
                    HeightRequest = 35,
                    BorderColor = Color.Black,
                    BorderWidth = 1,
                    Content = cimg
                };

                Label Name = new Label()
                {
                    TextColor = Color.CornflowerBlue,
                    FontSize = App.DpiFontSize * 6,
                    VerticalOptions = LayoutOptions.Center
                };
                Name.SetBinding(Label.TextProperty, "MEMBERNAME");
                Image sv1 = new Image()
                {
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    WidthRequest = 25,
                    HeightRequest = 25,
                    Margin = new Thickness(0, 0, 5, 0),
                };
                Binding bindSv1 = new Binding();
                bindSv1.Path = "SELECTED";
                bindSv1.Converter = new Converter.SelectionBoolToImageConverter();
                sv1.SetBinding(Image.SourceProperty, bindSv1);

                sl.Children.Add(sv);
                sl.Children.Add(Name);
                sl.Children.Add(sv1);
                StackLayout wrapper = new StackLayout() { BackgroundColor = Color.FromHex("#f1eeee") };

                wrapper.Children.Add(sl);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.FromHex("#bebcbc"),HeightRequest = 1 });
                return new ViewCell() { View = wrapper};
            });
            sflistGroup.GroupHeaderTemplate = new DataTemplate(() => {
                StackLayout Wrapper = new StackLayout() { Spacing = 1, BackgroundColor = Color.White };
                StackLayout sl = new StackLayout() { Orientation = StackOrientation.Horizontal,HeightRequest = 34 };
                
                Label GroupName = new Label()
                {
                    TextColor = Color.CornflowerBlue,
                    FontSize = App.DpiFontSize * 8,
                    Margin = new Thickness(5, 0, 0, 0),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Start,
                    FontAttributes = FontAttributes.Bold
                };
                GroupName.SetBinding(Label.TextProperty, new Binding("Key"));

                Image check = new Image()
                {
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 5, 0),
                    WidthRequest = 25,
                    HeightRequest = 25,
                    Aspect = Aspect.AspectFit
                };

                Binding bindingImage = new Binding(".");
                bindingImage.Converter = new Converter.GroupSelectionConverter();
                bindingImage.ConverterParameter = sflistGroup;
                check.SetBinding(Image.SourceProperty, bindingImage);

                var tapped = new TapGestureRecognizer();
                tapped.Tapped += TapGestureRecognizer_Tapped;
                check.GestureRecognizers.Add(tapped);

                sl.Children.Add(GroupName);
                sl.Children.Add(check);

                Wrapper.Children.Add(sl);
                Wrapper.Children.Add(new BoxView() {BackgroundColor = Color.Gray,HeightRequest = 1 });
                return Wrapper;
            });
            sflistGroup.GroupHeaderSize = 36;
            sflistGroup.ItemsSource = modelGroup.MEMBER;
            sflistGroup.SelectionChanged += Sflist_SelectionChanged;
            sflistGroup.IsStickyGroupHeader = true;
            sflistGroup.AllowGroupExpandCollapse = true;
            sflistGroup.GroupExpanding += Sflist_GroupExpanding;
            sflistGroup.ItemAppearing += SflistGroup_ItemAppearing;
            Button AddGroup = new Button()
            {
                VerticalOptions = LayoutOptions.End,
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10),
                Text = "Add Friend",
                FontSize = 8*App.DpiFontSize
            };
            AddGroup.Clicked += ((v,e) => {
                EveGroup(this,EventArgs.Empty);
            });
            GroupStackView.Children.Add(sflistGroup);
            GroupStackView.Children.Add(AddGroup);
            shapeGroup.Content = GroupStackView;
            #endregion


            #region create ButtonLayout
            Frame frameButtonLayout = new Frame()
            {
                HorizontalOptions = LayoutOptions.Center,
                IsClippedToBounds = true,
                CornerRadius = 5,
                Margin = new Thickness(0, 0, 0, 20),
                VerticalOptions = LayoutOptions.Center,
                Padding = 0,
                BackgroundColor = Color.Transparent
            };
            StackLayout ButtonLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                Spacing = 0,
                BackgroundColor = Color.FromRgba(255, 255, 255, 0.5),
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button bPending = new Button()
            {
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                FontSize = 5 * App.DpiFontSize,
                WidthRequest = 100,
                Text = "Friend",
                FontAttributes = FontAttributes.Bold
            };
            PrevButton = bPending;
            bPending.Clicked += (async(v,e)=> {

                PrevButton.FontAttributes = FontAttributes.None;
                (v as Button).FontAttributes = FontAttributes.Bold;
                PrevButton = (v as Button);
                (v as Button).IsEnabled = false;
                shape.TranslateTo(0,0, 250);
                shapeGroup.TranslateTo(500,0, 250);
                (v as Button).IsEnabled = true;

            });

            Button bCompleted = new Button()
            {
                WidthRequest = 100,
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                FontSize = App.DpiFontSize*5,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Group"
            };
            bCompleted.Clicked += BCompleted_Clicked;

           

            BoxView bx = new BoxView()
            {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.Fill,
                WidthRequest = 0.5f
            ,
                Margin = new Thickness(0, 2, 0, 2)
            };

            ButtonLayout.Children.Add(bPending);
            ButtonLayout.Children.Add(bx);
            ButtonLayout.Children.Add(bCompleted);
            frameButtonLayout.Content = ButtonLayout;
            #endregion

            backGrid.BackgroundColor = Color.Black;
            Image backImage = new Image()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Source = "background.jpg",
                Aspect = Aspect.AspectFill,
            };

            backGrid.Children.Add(backImage, 0, 0);
            backGrid.Children.Add(Judul, 0,1);
            backGrid.Children.Add(frameButtonLayout, 0, 5);
            backGrid.Children.Add(shape, 0, 7);
            backGrid.Children.Add(shapeGroup,0,7);
            Grid.SetRowSpan(shape, 13);
            Grid.SetRowSpan(shapeGroup,13);
            Grid.SetRowSpan(Judul, 7);
            Grid.SetRowSpan(frameButtonLayout,2);
            Grid.SetRowSpan(backImage, 20);
            Content = backGrid;

            
            
        }

        private void SflistGroup_ItemAppearing(object sender, ItemAppearingEventArgs e)
        {
            
        }

        private async void BCompleted_Clicked(object v, EventArgs e)
        {
            PrevButton.FontAttributes = FontAttributes.None;
            (v as Button).FontAttributes = FontAttributes.Bold;
            PrevButton = (v as Button);

            (v as Button).IsEnabled = false;
            shape.TranslateTo(-500, 0, 250);
            shapeGroup.TranslateTo(0, 0, 250);
            (v as Button).IsEnabled = true;
        }

        private void Bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var hasil = ListModel.Where(x => x.nama.ToLower().Contains(e.NewTextValue.ToLower()));
            sflistFriend.ItemsSource = null;
            sflistFriend.ItemsSource = hasil;
        }

        private void SelectFriendButton_Clicked(object sender, EventArgs e)
        {   
            Eve(this, EventArgs.Empty);
        }
        protected override Task OnAppearingAnimationEnd()
        {
            initData();
            return base.OnAppearingAnimationEnd();
        }
        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        #region function and callback for SFlistGroup
        GroupResult expandedGroup;
        GroupResult PreviousGroupResult;
        private void Sflist_GroupExpanding(object sender, GroupExpandCollapseChangingEventArgs e)
        {
            if (e.Groups.Count > 0)
            {
                var group = e.Groups[0];
                if (expandedGroup == null || group.Key != expandedGroup.Key)
                {
                    foreach (var otherGroup in sflistGroup.DataSource.Groups)
                    {
                        if (group.Key != otherGroup.Key)
                        {
                            sflistGroup.CollapseGroup(otherGroup);
                        }
                    }
                    expandedGroup = group;
                    sflistGroup.ExpandGroup(expandedGroup);
                }
            }
        }

        private void Sflist_SelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {

            if (sflistGroup.SelectionMode == SelectionMode.Multiple)
            {
                GroupResult gr = null;
                for (int i = 0; i < e.AddedItems.Count(); i++)
                {
                    var item = e.AddedItems[i];
                    (item as Splitbill.Model.RequestModel.GroupMember).SELECTED = true;
                    gr = GetGroup(item);
                }
                for (int i = 0; i < e.RemovedItems.Count; i++)
                {
                    var item = e.RemovedItems[i];
                    (item as Splitbill.Model.RequestModel.GroupMember).SELECTED = false;
                    gr = GetGroup(item);
                }
                if (gr != null)
                { RefreshGroupHeader(gr); }
                sflistGroup.RefreshView();
            }
        }

        private GroupResult GetGroup(object itemData)
        {
            GroupResult itemGroup = null;

            foreach (var item in this.sflistGroup.DataSource.DisplayItems)
            {
                if (item is GroupResult)
                {
                    itemGroup = item as GroupResult;
                }
                if (item == itemData)
                    break;
            }
            return itemGroup;
        }

        
        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (PreviousGroupResult != null)
            {
                foreach (var item in PreviousGroupResult.Items)
                {
                    (item as GroupMember).SELECTED = false;
                }
                this.RefreshGroupHeader(PreviousGroupResult);
            }

            var image = (sender as Image);
            GroupResult groupResult = image.BindingContext as GroupResult;
           
                if (groupResult == null)
                    return;

                var items = groupResult.Items.ToList<Splitbill.Model.RequestModel.GroupMember>().ToList();

                if ((items.All(listItem => listItem.SELECTED == true)))
                {
                    for (int i = 0; i < items.Count(); i++)
                    {
                        var item = items[i];
                        (item as Splitbill.Model.RequestModel.GroupMember).SELECTED = false;
                    }
                }
                else if ((items.All(listItem => listItem.SELECTED == false)))
                {
                    for (int i = 0; i < items.Count(); i++)
                    {
                        var item = items[i];
                        (item as Splitbill.Model.RequestModel.GroupMember).SELECTED = true;
                    }
                }
            
            this.RefreshGroupHeader(groupResult);
            PreviousGroupResult = groupResult;
            sflistGroup.RefreshView();
        }
        private void RefreshGroupHeader(GroupResult group)
        {
            foreach (var item in this.sflistGroup.GetVisualContainer().Children)
            {
                if (item.BindingContext == group)
                {
                    item.BindingContext = null;
                    (item as GroupHeaderItem).Content.BindingContext = null;
                }
            }
        }
        #endregion

    }
}
