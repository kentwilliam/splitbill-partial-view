﻿using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Decode;
using Syncfusion.SfImageEditor.XForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace Splitbill.Popup.Request
{
    public class PopupPhotoRequest : PopupPage
    {
        public String PhotoName;
        public EventHandler Eve;
        SfImageEditor editor;
        StackLayout tab3;
        MediaFile _Media;
        Button CropButton;
        StackLayout myStack;
        public PopupPhotoRequest(String img)
        {
             myStack= new StackLayout()
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                WidthRequest = 325,
                BackgroundColor = Color.White,
                Spacing = 0,
                Margin = 0
            };
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            tab3 = new StackLayout() { Orientation = StackOrientation.Vertical };

            #region Tab1
            Image closeIcon = new Image()
            {
                Source = "CLOSE.png",
                HeightRequest = App.DpiFontSize * 10,
                WidthRequest = App.DpiFontSize * 10,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.Center,
                Aspect = Aspect.AspectFit,
                Margin = new Thickness(5)
            };
            closeIcon.GestureRecognizers.Add(new TapGestureRecognizer((v, e) => {
                Eve(this, EventArgs.Empty);
            }));
            tab1.Children.Add(closeIcon);
            #endregion

            #region Tab2
            editor = new SfImageEditor()
            {
                WidthRequest = 250,
                HeightRequest = String.IsNullOrEmpty(img)?0:300,
                Source = String.IsNullOrEmpty(img)? "": "https://www.urunan.id/splitbill/" + img,
                BackgroundColor = Color.LightGray,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                EnableZooming = true,
                PanningMode = PanningMode.SingleFinger
            };
            editor.ImageSaved += Editor_ImageSaved;
            editor.ToolbarSettings.IsVisible = false;
            tab2.Children.Add(editor);
            #endregion

            #region Tab3
            CreateTakePicture_GaleryButtons();
            #endregion

            myStack.Children.Add(tab1);
            myStack.Children.Add(tab2);
            myStack.Children.Add(tab3);
            Content = myStack;
        }

        private async void Editor_ImageSaved(object sender, ImageSavedEventArgs args)
        {
            byte[] hasilFoto = GetImageAsByteArray(args.Location);
            try
            {
                String tempString64 = Convert.ToBase64String(hasilFoto);
                var resp = App._Api.GetResponsePhotoApi("transaction/upload_bill_base64", JsonConvert.SerializeObject(new Splitbill.Api.Encode.SendPhotoJson()
                {
                    string64 = tempString64,
                    token =App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME
                }));

                var resu = JsonConvert.DeserializeObject<PhotoErrorMessage>(resp.Result);
                if (App._Api.ProcessDataOrNot(resu.message, false))
                {
                    PhotoName = resu.filename;
                    await DisplayAlert("", "Upload Success", "ok");
                    Eve(this,EventArgs.Empty);
                    CreateTakePicture_GaleryButtons();
                }
                else
                {
                    await Task.Delay(200);
                    App.MyHomePage.LogOut();
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("", "No Internet", "ok");
            }
        }
        protected override bool OnBackButtonPressed()
        {
            Eve(this, EventArgs.Empty);
            return true;
        }
        bool Cropping;
        private async void TakePicture_click(object sender, EventArgs e)
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

            if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                cameraStatus = results[Permission.Camera];
                storageStatus = results[Permission.Storage];
            }

            if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
            {
                _Media = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,
                    CompressionQuality = 75,
                });
                //SendTo Server
                if (_Media != null)
                {
                    editor.Source = ImageSource.FromStream(() => { return _Media.GetStream(); });
                    editor.HeightRequest = 300;
                    CreateCrop_SendButtons();
                    //SendImageToServer(file);
                }

            }
            else
            {
                await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
            }
        }
        private async void Galery_click(object sender, EventArgs e)
        {
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 75,
            });
            //SendTo Server
            if (file != null)
            {
                editor.Source = ImageSource.FromStream(() => { return file.GetStream(); });
                CreateCrop_SendButtons();
                editor.HeightRequest = 300;
                //SendImageToServer(file);
            }
            else
            {
                await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
            }

        }
        private void CreateTakePicture_GaleryButtons()
        {
            tab3.Children.Clear();
            Button takePictureButton = new Button()
            {
                Text = "Take Picture",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                FontSize = App.DpiFontSize *6,
                Margin = new Thickness(5)
            };
            takePictureButton.Clicked += TakePicture_click;
            Button takeGalleryButton = new Button()
            {
                Text = "Gallery",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                FontSize = App.DpiFontSize * 6,
                Margin = new Thickness(5)
            };
            takeGalleryButton.Clicked += Galery_click;

            tab3.Children.Add(takePictureButton);
            tab3.Children.Add(takeGalleryButton);
        }
        private void CreateCrop_SendButtons()
        {
            tab3.Children.Clear();
            CropButton = new Button()
            {
                Text = "Crop Picture",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
                Margin = new Thickness(5)
            };
            CropButton.Clicked += CropBtn_Click;
            Button SendButton = new Button()
            {
                Text = "Send",
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
                Margin = new Thickness(5)
            };
            SendButton.Clicked += SendBtn_Click;

            tab3.Children.Add(CropButton);
            tab3.Children.Add(SendButton);
        }
        private void CropBtn_Click(object sender, EventArgs e)
        {
            if (Cropping == false)
            {
                editor.ToggleCropping();
                Cropping = true;
            }
            else
            {
                Cropping = false;
                editor.Crop();
            }

        }
        private void SendBtn_Click(object sender, EventArgs e)
        {
            editor.Save();
        }
        private byte[] GetImageAsByteArray(string imageFilePath)
        {
            FileStream fileStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            return binaryReader.ReadBytes((int)fileStream.Length);
        }

    }
}
