﻿using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace Splitbill.Popup.Request
{
    public class PopupPreviewImage : PopupPage
    {
        public Image PreviewImage;
        public MediaFile _Media;
        public event EventHandler DoThis;
        public String PhotoName;
        public PopupPreviewImage(ImageSource src)
        {
            PreviewImage = new Image() { WidthRequest = 250, HeightRequest = 400, BackgroundColor = Color.FromRgb(255, 255, 255), Margin = new Thickness(5), Source = src };

            Grid Head = new Grid();
            StackLayout Body = new StackLayout();
            StackLayout footer = new StackLayout();

            Body.Spacing = 0;
            footer.Orientation = StackOrientation.Horizontal;

            Head.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(35)});
            Head.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1,GridUnitType.Star) });
            Head.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(35) });
            Head.BackgroundColor = Color.FromRgb(0, 162, 232);

            Image Backbutton = new Image() { WidthRequest = 35, HeightRequest = 35, Aspect = Aspect.AspectFit, HorizontalOptions = LayoutOptions.Start, Margin = new Thickness(15, 10, 10, 10), Source = "BackIcon.png" };
            Backbutton.GestureRecognizers.Add(new TapGestureRecognizer(BackToRequest));

            Label DoneText = new Label();
            DoneText.Text = "Done";
            DoneText.TextColor = Color.White;
            DoneText.VerticalTextAlignment = TextAlignment.Center;
            DoneText.HorizontalOptions = LayoutOptions.EndAndExpand;
            DoneText.VerticalOptions = LayoutOptions.Center;
            DoneText.Margin = new Thickness(0, 0, 15, 0);
            DoneText.FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            DoneText.GestureRecognizers.Add(new TapGestureRecognizer(BackToRequest));


            Image Logo = new Image() { Aspect = Aspect.AspectFit, HorizontalOptions = LayoutOptions.Center, Source = "LogoUrunan.png" };

            //Head.Children.Add(DoneText);
            Head.Children.Add(Backbutton,0,0);
            Head.Children.Add(Logo,1,0);

            Body.Children.Add(Head);
            Body.Children.Add(PreviewImage);
            Body.Children.Add(footer);

            Body.VerticalOptions = LayoutOptions.Center;
            Body.Margin = new Thickness(15);
            Body.BackgroundColor = Color.FromRgb(225, 225, 225);
            Content = Body;
        }

        private void BackToRequest(Xamarin.Forms.View arg1, object arg2)
        {
            PopupNavigation.PopAsync();
        }
    }
}
