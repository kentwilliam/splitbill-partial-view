﻿using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill.Popup.Friend
{
    public class PopupAddFriend : PopupPage
    {
        public EventHandler Eve;
        public PopupAddFriend()
        {
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 30,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                HeightRequest = 150
            };

            Grid ContentGrid = new Grid();

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5f, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5f, GridUnitType.Star) });

            StackLayout TextLayout = new StackLayout() {VerticalOptions = LayoutOptions.Center, Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Center};

            Label Description = new Label()
            {
                Text = "Do You Want To Add Friend ?",
                FontSize = 10 * App.DpiFontSize,
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                Margin = new Thickness(10, 0, 10, 0)
            };
            TextLayout.Children.Add(Description);

            StackLayout buttonLayout = new StackLayout() {HorizontalOptions = LayoutOptions.Center, Orientation = StackOrientation.Horizontal, Margin = new Thickness(0,15, 0, 15) };
            Button button1 = new Button() { BackgroundColor = Color.CornflowerBlue, Text = "Add", BorderRadius = 17, HorizontalOptions = LayoutOptions.Center, WidthRequest = 145, HeightRequest = 50,FontSize = App.DpiFontSize *6};
            button1.Clicked += LogOutClick;
            Button button2 = new Button() { BackgroundColor = Color.Gray, Text = "Cancel", BorderRadius = 17, HorizontalOptions = LayoutOptions.Center, WidthRequest = 145, HeightRequest = 50 ,FontSize = 6* App.DpiFontSize};
            button2.Clicked += CancelClick;
            buttonLayout.Children.Add(button1);
            buttonLayout.Children.Add(button2);

            ContentGrid.Children.Add(buttonLayout, 0, 1);
            ContentGrid.Children.Add(TextLayout, 0, 0);
            box.Content = ContentGrid;
            
            
            this.BackgroundColor = Color.FromRgba(25, 25, 25, 0.8f);
            Content = box;
        }

        private async void CancelClick(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
        }
        private async void LogOutClick(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
            Eve(new object(),EventArgs.Empty);
        }
    }
}
