﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup
{
    public class PopupBank:PopupPage
    {
        public PopupBank(String namabank)
        {
            XFShapeView.ShapeView mainContentFrame = new XFShapeView.ShapeView { CornerRadius = 10,IsClippedToBounds = true };

            StackLayout menuStack = new StackLayout();
            menuStack.Orientation = StackOrientation.Horizontal;
            BoxView bx = new BoxView() { WidthRequest = 30,HeightRequest=30,Margin = new Thickness(5),HorizontalOptions = LayoutOptions.Start };

            Label Title = new Label() { HorizontalOptions = LayoutOptions.FillAndExpand, HorizontalTextAlignment = TextAlignment.Center };

            Image closeIcon = new Image() { WidthRequest = App.DpiFontSize * 10,HeightRequest = App.DpiFontSize * 10,Margin = new Thickness(10),HorizontalOptions = LayoutOptions.End ,Source = "CLOSE.png",Aspect = Aspect.AspectFit};

            closeIcon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> { await PopupNavigation.PopAsync(); }));
            menuStack.Children.Add(bx);
            menuStack.Children.Add(Title);
            menuStack.Children.Add(closeIcon);

            StackLayout mainContent = new StackLayout() { IsClippedToBounds = true};

            Label l1 = new Label();
            l1.Text = "1. Masukkan Kartu ATM dan PIN "+namabank+" anda";
            l1.Margin = new Thickness(15, 0, 5, 0);

            Label l2 = new Label();
            l2.Text = "2. Masukkan ke menu TRANSFER dan klik "+namabank+" Virtual Account";
            l2.Margin = new Thickness(15, 0, 5, 0);

            Label l3 = new Label();
            l3.Text = "3. Masukkan kode perusahaan untuk "+namabank+":12345 dan nomor telepon yang terdaftar pada aplikasi ";
            l3.Margin = new Thickness(15, 0, 5, 0);

            Label l4 = new Label();
            l4.Text = "4. Masukkan jumlah top up yang diinginkan";
            l4.Margin = new Thickness(15, 0, 5, 0);

            Label l5 = new Label();
            l5.Text = "5. ikuti instruksi untuk menyelesaikan transaksi";
            l5.Margin = new Thickness(15, 0, 5, 0);
            mainContent.Children.Add(menuStack);
            mainContent.Children.Add(l1);
            mainContent.Children.Add(l2);
            mainContent.Children.Add(l3);
            mainContent.Children.Add(l4);
            mainContent.Children.Add(l5);

            BoxView offset = new BoxView();
            offset.HeightRequest = 35;
            mainContent.Children.Add(offset);
            mainContent.BackgroundColor = Color.White;
            mainContent.Margin = new Thickness(5);
            mainContentFrame.Content = mainContent;
            mainContentFrame.WidthRequest = 350;
            mainContentFrame.HeightRequest = 350;

            mainContentFrame.Color = Color.White;
            mainContentFrame.Margin = new Thickness(10, 0, 10, 0);
            mainContentFrame.VerticalOptions = LayoutOptions.CenterAndExpand;
            mainContentFrame.HorizontalOptions = LayoutOptions.CenterAndExpand;
            mainContentFrame.BorderWidth = 3;
            mainContentFrame.BorderColor = Color.CornflowerBlue;

            Content = mainContentFrame;
        }
    }
}
