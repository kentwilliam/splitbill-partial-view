﻿using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode.Mandiri;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace Splitbill.Popup.AddMoney
{
    public class AddMoneyRedeem:PopupPage
    {
        Entry AmountEntry;
        public AddMoneyRedeem()
        {
            StackLayout mainStack = new StackLayout() {
                Margin = new Thickness(5),
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.White,
                IsClippedToBounds = true
            };

            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,HeightRequest = 40};
            BoxView bx1 = new BoxView() {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start,
            };
            Label Judul = new Label()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
                Text = "REDEEM",
                FontSize = (App.LargeText) * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            Image xicon = new Image() {
                WidthRequest = App.DpiFontSize*10,
                HeightRequest = App.DpiFontSize * 10,
                Aspect = Aspect.AspectFit,
                Source = "CLOSE.png",
                Margin = new Thickness(5)
            };
            xicon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                await PopupNavigation.PopAsync();
            }));

            Label MessageStack = new Label()
            {
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                Text = "Your Amount Rp " + App.GlobalDataUser.BALANCEDOUBLEVALUE.ToString("n0") + " \n \n Insert Amount to Redeem:",
                HorizontalTextAlignment = TextAlignment.Center
            };

            AmountEntry = new Entry()
            {
                Placeholder = "Rp....",
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium,typeof(Entry)),
                TextColor = Color.Black,
                WidthRequest = 125,
                HorizontalOptions = LayoutOptions.Center
            };
            #region buat checkStack
            StackLayout CheckStack = new StackLayout() {
                Orientation = StackOrientation.Horizontal,
                Margin = new Thickness(10,0,10,0)
            };
            CheckBox checkbox1 = new CheckBox();
            Label checkLabel = new Label() {
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium,typeof(Label)),
                Text = "Are You Sure ?",
                VerticalOptions = LayoutOptions.Center
            };
            CheckStack.Children.Add(checkbox1);
            CheckStack.Children.Add(checkLabel);
            #endregion

            Button Brequest = new Button() {
                Text = "REDEEM",
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                BackgroundColor = Color.CornflowerBlue,
                Margin = new Thickness(10,0,10,5),
                TextColor = Color.White
            };
            Brequest.Clicked += Brequest_Clicked;
            tab1.Children.Add(bx1);
            tab1.Children.Add(Judul);
            tab1.Children.Add(xicon);

            mainStack.Children.Add(tab1);
            mainStack.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1, Margin = new Thickness(15, 0, 15, 0) });
            mainStack.Children.Add(MessageStack);
            mainStack.Children.Add(new BoxView()
            {
                HeightRequest = 10,
                BackgroundColor = Color.Transparent
            });
            mainStack.Children.Add(AmountEntry);
            mainStack.Children.Add(CheckStack);
            mainStack.Children.Add(Brequest);
            mainStack.Children.Add(new BoxView()
            {
                HeightRequest = 10,
                BackgroundColor = Color.Transparent
            });
            Frame wrapper = new Frame() {
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 300,
                Margin = 0,
                Padding =0,
                CornerRadius = 5,
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.Center,
                Content = mainStack,
                BackgroundColor = Color.CornflowerBlue,
                OutlineColor = Color.CornflowerBlue
            };
            Content = wrapper;
        }

        private void Brequest_Clicked1(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private async void Brequest_Clicked(object sender, EventArgs e)
        {
            App.MyHomePage.ShowLoadingPage();
            Splitbill.Api.Encode.AddMoneyJson js = new Api.Encode.AddMoneyJson() {
                username = App.GlobalDataUser.USERNAME,
            token = App.GlobalDataUser.TOKEN,
            amount = AmountEntry.Text,
        };
            
            
            

            var resp = App._Api.GetDataResponseFromServer("Mandiri_API/withdraw_mandiri_ecash_balance", JsonConvert.SerializeObject(js));
            var resu = JsonConvert.DeserializeObject<MoneyMandiriJsonResponse>(resp.Result);
            if (App._Api.ProcessDataOrNot(resu.message,false))
            {
                App.MyHomePage.ShowAlert(resu.message);
                App.GlobalDataUser.BALANCEDOUBLEVALUE = resu.balance;
                App.GlobalDataUser.BALANCE = "Rp " + App.GlobalDataUser.BALANCEDOUBLEVALUE.ToString("n0");
                App.GlobalDataUser.NOTIFICATIONCOUNTER = resu.notification;

                await PopupNavigation.PopAsync();
                App.MyHomePage.HideLoadingPage();
            }
            else
            {
                await Task.Delay(200);
                App.MyHomePage.LogOut();
            }
        }
    }
}
