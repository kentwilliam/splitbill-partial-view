﻿using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using Splitbill.Popup.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.EditProfile
{
    public class PopupEditProfile:PopupPage
    {
        CircleImage ciImg;
        UpdateUserJson data;
        Entry email;
        Entry nama;
        public PopupEditProfile()
        {
            data = new UpdateUserJson();
            ScrollView sv = new ScrollView() { VerticalOptions = LayoutOptions.FillAndExpand,BackgroundColor = Color.Transparent};
            XFShapeView.ShapeView Wrapper = new XFShapeView.ShapeView() {
                WidthRequest = 325,
                Color = Color.White,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Center,
                CornerRadius = 5,
                Margin = new Thickness(25,0,25,0),
                BorderWidth = 3,
                BorderColor = Color.CornflowerBlue
            };

            StackLayout JudulStack = new StackLayout() { Orientation = StackOrientation.Horizontal,Margin = new Thickness(5,0,5,0) };
            Image CloseIcon = new Image()
            {
                WidthRequest = App.DpiFontSize * 10,
                HeightRequest = App.DpiFontSize * 10,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                Source = "CLOSE.png"
            };
            CloseIcon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> { await PopupNavigation.PopAsync(); }));
            BoxView bx = new BoxView()
            {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start
            };
            Label judul = new Label()
            {
                Text = "Edit Profile",
                FontSize = App.DpiFontSize * 12,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            JudulStack.Children.Add(bx);
            JudulStack.Children.Add(judul);
            JudulStack.Children.Add(CloseIcon);



            StackLayout wrapper = new StackLayout() {
                IsClippedToBounds = true,
            };

            #region tempate Foto
            Grid TempatFoto = new Grid();
            TempatFoto.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star)});
            TempatFoto.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            TempatFoto.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            TempatFoto.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            CircleImage iconFoto = new CircleImage() {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.Start,
                FillColor = Color.White,
                BorderThickness = 1,
                BorderColor = Color.Gray,
                Margin = new Thickness(15,0,0,0),
                Source = "TakePhoto2.png"
            };
            iconFoto.GestureRecognizers.Add(new TapGestureRecognizer(OpenPhoto));
            ciImg = new CircleImage()
            {
                FillColor = Color.DarkGreen,
                BorderColor = Color.Gray,
                BorderThickness = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                WidthRequest = 90,
                HeightRequest = 90,
                Source = "user72.png",
                Aspect = Aspect.AspectFill
            };
            ciImg.SetBinding(CircleImage.SourceProperty, "PHOTO");
            ciImg.BindingContext = App.GlobalDataUser;
            TempatFoto.Children.Add(ciImg,0,0);
            TempatFoto.Children.Add(iconFoto, 1, 1);
            Grid.SetRowSpan(ciImg,2);
            Grid.SetColumnSpan(ciImg, 2);
            #endregion

            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,Margin = new Thickness(5,0,5,0) };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal,Margin = new Thickness(5,0,5,0) };

            #region tab1
            Image icon1 = new Image()
            {
                Source = "Friends.png",
                Aspect = Aspect.AspectFit,
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.End
            };
            nama = new Entry() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill,
                Margin = new Thickness(0, 3, 0, 0),
                FontSize = App.DpiFontSize * 8,
                Text = App.GlobalDataUser.USERNAME
            };
            Image icon1_1 = new Image()
            {
                Source = "edit.png",
                Aspect = Aspect.AspectFit,
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.End
            };
            tab1.Children.Add(icon1);
            tab1.Children.Add(nama);
            tab1.Children.Add(icon1_1);
            #endregion

            #region tab2
            Image icon2 = new Image()
            {
                Source = "MailBlue.png",
                Aspect = Aspect.AspectFit,
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.End
            };
            email = new Entry()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,VerticalOptions = LayoutOptions.End,
                FontSize = App.DpiFontSize *8,
                Text = App.GlobalDataUser.EMAIL
            };
            tab2.Children.Add(icon2);
            tab2.Children.Add(email);
            #endregion

            Button confirm = new Button() {
                BackgroundColor = Color.CornflowerBlue,
                Text = "Save Changes",
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                FontSize = App.DpiFontSize * 10,
                VerticalOptions = LayoutOptions.Center,
                Margin = new Thickness(5),
                BorderRadius = 5
            };
            confirm.Clicked += Confirm_Clicked;

            wrapper.Children.Add(JudulStack);
            wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f });
            wrapper.Children.Add(TempatFoto);
            wrapper.Children.Add(tab1);
            wrapper.Children.Add(tab2);
            wrapper.Children.Add(confirm);
            wrapper.BackgroundColor = Color.Transparent;
            wrapper.Margin = new Thickness(7);
            Wrapper.Content = wrapper;

            sv.Content = Wrapper;
            Content = sv;
        }

        private async void Confirm_Clicked(object sender, EventArgs e)
        {
            try
            {
                data.new_username = nama.Text;
                data.email = email.Text;
                data.username = App.GlobalDataUser.USERNAME;
                data.token = App.GlobalDataUser.TOKEN;
                var resp = App._Api.GetDataResponseFromServer("user/update_user", JsonConvert.SerializeObject(data));
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                if (resu.status == "true")
                {
                    App.GlobalDataUser.USERNAME = nama.Text;
                    App.GlobalDataUser.EMAIL = email.Text;
                    await DisplayAlert("", resu.message, "Ok");
                    await PopupNavigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("", resu.message, "Ok");
                    await PopupNavigation.PopAsync();
                    if (App._Api.ProcessDataOrNot(resu.message, false) == false)
                    {
                        await Task.Delay(200);
                        App.MyHomePage.LogOut();
                    }
                }

            }
            catch (Exception ex)
            { await DisplayAlert("","No Internet","ok"); }
            
        }

        private async void OpenPhoto(View arg1, object arg2)
        {
            (arg1 as CircleImage).IsEnabled = false;
            await PopupNavigation.PushAsync(new PopupOpenPhoto());
            

            (arg1 as CircleImage).IsEnabled = true;
        }
        void SendImageToServer(MediaFile _Media)
        {
            byte[] filedata = System.Text.Encoding.UTF8.GetBytes(_Media.Path);
            byte[] hasilFoto;

            using (var memoryStream = new MemoryStream())
            {
                _Media.GetStream().CopyTo(memoryStream);
                _Media.Dispose();
                hasilFoto = memoryStream.ToArray();
            }
            try
            {
                String tempString64 = Convert.ToBase64String(hasilFoto);
                var resp = App._Api.GetResponsePhotoApi("user/upload_profpic", JsonConvert.SerializeObject(new Splitbill.Api.Encode.SendPhotoJson()
                {
                    string64 = tempString64,
                    token =App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME
                }));

                var resu = JsonConvert.DeserializeObject<PhotoErrorMessage>(resp.Result);
                if (!String.IsNullOrEmpty(resu.message))
                {
                    if (resu.message.ToLower() == "expired")
                    {
                        App.Current.MainPage = new Pages.Login.LoginPage();
                    }
                }
                else
                {
                    App.GlobalDataUser.PHOTO = "http://www.urunan.id/splitbill/" + resu.filename;
                    data.photo = resu.filename;
                    ciImg.Source = "http://www.urunan.id/splitbill/" + resu.filename;
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("","No Internet","ok");
            }
            
            
        }

    }
}
