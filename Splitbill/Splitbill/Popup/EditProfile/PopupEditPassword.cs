﻿using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Encode;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.EditProfile
{
    public class PopupEditPassword : PopupPage
    {
        Entry verifyNewPassword;
        Entry newPassword;
        Entry oldPassword;
        public PopupEditPassword()
        {
            ScrollView sv = new ScrollView() { VerticalOptions = LayoutOptions.FillAndExpand};
            XFShapeView.ShapeView f = new XFShapeView.ShapeView() {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(20,0,20,0),
                Padding = new Thickness(2),
                CornerRadius = 5,
                IsClippedToBounds = true,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 3,
                Color = Color.White
            };
            StackLayout wrapper = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.White,
                IsClippedToBounds = true,
                Margin = new Thickness(5)
            };

            StackLayout JudulStack = new StackLayout() { Orientation = StackOrientation.Horizontal};
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            Button submitBtn = new Button() {
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                Text = "Save Changes",
                FontSize = 8 * App.DpiFontSize,
                Margin = new Thickness(10),
                BorderRadius = 7,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 1
            };
            submitBtn.Clicked += SubmitBtn_Clicked;
            #region tab1
            Label tab1Label = new Label() {
                Text = "Current Password : ",
                VerticalTextAlignment = TextAlignment.Center,
                FontSize = App.DpiFontSize * 8
            };
            oldPassword = new Entry() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill,
                Margin = new Thickness(0, 3, 0, 0),
                FontSize = 8 * App.DpiFontSize
            };
            tab1.Children.Add(tab1Label);
            tab1.Children.Add(oldPassword);
            #endregion

            #region tab 2
            Label tab2Label = new Label()
            {
                Text = "New  Password : ",
                VerticalTextAlignment = TextAlignment.Center,
                FontSize = 8 * App.DpiFontSize
            };
            newPassword = new Entry()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill,
                Margin = new Thickness(0, 3, 0, 0),
                FontSize = 8 * App.DpiFontSize
            };
            tab2.Children.Add(tab2Label);
            tab2.Children.Add(newPassword);
            #endregion

            #region tab 3
            Label tab3Label = new Label()
            {
                Text = "Verify Password : ",
                VerticalTextAlignment = TextAlignment.Center,
                FontSize = 8 * App.DpiFontSize
            };
            verifyNewPassword = new Entry()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill,
                Margin = new Thickness(0, 3, 0, 0),
                FontSize = 8 * App.DpiFontSize
            };
            tab3.Children.Add(tab3Label);
            tab3.Children.Add(verifyNewPassword);
            #endregion

            Image CloseIcon = new Image() {
                WidthRequest = App.DpiFontSize * 10,
                HeightRequest = App.DpiFontSize *10,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                Source = "CLOSE.png"
            };
            CloseIcon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> { await PopupNavigation.PopAsync(); }));
            BoxView bx = new BoxView() {
                WidthRequest = App.DpiFontSize*15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start
            };
            Label judul = new Label() {
                Text = "Change Password",
                FontSize = 10 * App.DpiFontSize,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            JudulStack.Children.Add(bx);
            JudulStack.Children.Add(judul);
            JudulStack.Children.Add(CloseIcon);

            wrapper.Children.Add(JudulStack);
            wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1f});
            wrapper.Children.Add(tab1);
            wrapper.Children.Add(tab2);
            wrapper.Children.Add(tab3);
            wrapper.Children.Add(submitBtn);
            wrapper.Margin = new Thickness(7);
            f.Content = wrapper;
            sv.Content = f;

            Content = sv;
        }

        private async void SubmitBtn_Clicked(object sender, EventArgs e)
        {
            (sender as Button).IsEnabled = false;
            if (!string.IsNullOrWhiteSpace(newPassword.Text) && !String.IsNullOrWhiteSpace(verifyNewPassword.Text))
            {
                if (newPassword.Text.ToLower() == verifyNewPassword.Text.ToLower())
                {
                    UpdatePasswordJson passwordToUpdate = new UpdatePasswordJson()
                    {
                        old_password = oldPassword.Text,
                        new_password = newPassword.Text,
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME
                    };
                    try
                    {
                        var resp = App._Api.GetDataResponseFromServer("user/update_password", JsonConvert.SerializeObject(passwordToUpdate));
                        var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                        if (!String.IsNullOrEmpty(resu.message))
                        {
                            if (resu.message.ToLower() == "expired")
                            {
                                App.Current.MainPage = new Pages.Login.LoginPage();
                            }
                        }
                        else if (resu.status == "true")
                        {
                            await DisplayAlert("", resu.message, "ok");
                            await PopupNavigation.PopAsync();
                        }
                        else if (!String.IsNullOrEmpty(resu.message))
                        {
                            if (resu.message.ToLower() == "expired")
                            {
                                await Task.Delay(200);
                                App.MyHomePage.LogOut();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("", "No Internet", "ok");
                    }

                }
            }
            else
            {
                await DisplayAlert("","Fill Blank Fields","ok");
            }
            (sender as Button).IsEnabled = true;
        }
    }
}
