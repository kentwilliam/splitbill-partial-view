﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Popup.MyTransaction.Completed
{
    public class MyTransactionCompleted:PopupPage
    {
        Label Description;
        public MyTransactionCompleted(String Message,String description)
        {
            StackLayout mainStack = new StackLayout() {
                Margin = new Thickness(5),
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.White,
                IsClippedToBounds = true
            };

            Label MessageStack = new Label() {
                FontSize = App.DpiFontSize * 6,
                Text = Message,
                HorizontalTextAlignment = TextAlignment.Center
            };

            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,HeightRequest = 40};
            BoxView bx1 = new BoxView() {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                HorizontalOptions = LayoutOptions.Start,
            };
            Label Judul = new Label()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.Center,
                Text = "Send Money",
                FontSize = App.DpiFontSize * 10,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            Image xicon = new Image() {
                WidthRequest = App.DpiFontSize * 10,
                HeightRequest = App.DpiFontSize * 10,
                Aspect = Aspect.AspectFit,
                Source = "CLOSE.png",
                Margin = new Thickness(5)
            };
            xicon.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                await PopupNavigation.PopAsync();
            }));
            tab1.Children.Add(bx1);
            tab1.Children.Add(Judul);
            tab1.Children.Add(xicon);

            Frame wrapper = new Frame() {
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 300,
                Margin = 0,
                Padding =0,
                CornerRadius = 5,
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.Center,
                Content = mainStack,
                BackgroundColor = Color.CornflowerBlue,
                OutlineColor = Color.CornflowerBlue
            };
            Description = new Label()
            {
                FontSize = App.DpiFontSize * 4,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                Text = description
            };

            mainStack.Children.Add(tab1);
            mainStack.Children.Add(new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1,Margin = new Thickness(15,0,15,0)});
            mainStack.Children.Add(MessageStack);
            mainStack.Children.Add(Description);
            mainStack.Children.Add(new BoxView()
            {
                HeightRequest = 20,
                BackgroundColor = Color.Transparent
            });

            Content = wrapper;
        }
    }
}
