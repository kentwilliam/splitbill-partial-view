﻿using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Model.Send;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Popup.Send
{
    public class PopupOtpSendMoney : PopupPage
    {
        public EventHandler Eve;
        SendMoneyModel tempModel;
        Entry OTPEntry;
        public PopupOtpSendMoney(SendMoneyModel model)
        {
            tempModel = model;
            Grid G = new Grid() { WidthRequest = 340, HeightRequest = 400 ,VerticalOptions = LayoutOptions.CenterAndExpand,
            HorizontalOptions = LayoutOptions.CenterAndExpand};
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 20,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                HeightRequest = 300
            };

            Grid ContentGrid = new Grid();

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(150) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star) });

            OTPEntry = new Entry()
            {
                Text = "",
                FontSize = App.DpiFontSize * 6,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.Black,
                Margin = new Thickness(15, 0, 15, 0),
                Placeholder = "Insert Otp",
                Keyboard = Keyboard.Numeric
            };

            Button button1 = new Button() { BackgroundColor = Color.CornflowerBlue,
                Text = "Send Money",
                BorderRadius = 10,
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 300,
                HeightRequest = 50,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                FontSize = App.DpiFontSize * 6,
                TextColor = Color.White
            };
            button1.Clicked += Button1_Clicked;
            StackLayout Message = new StackLayout()
            {
                HeightRequest = 175,
                WidthRequest = 300,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = new Thickness(0,15,0,0)
            };

            Label MessageText = new Label() {
            Text = "You Send Money "+model.amount.ToString("n0") +" To "+ model.friend,
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            VerticalTextAlignment = TextAlignment.Center,
            HorizontalTextAlignment = TextAlignment.Center,
            FontSize = App.DpiFontSize * 8,
            TextColor = Color.Gray
            };

            Message.Children.Add(MessageText);
            ContentGrid.Children.Add(Message, 0, 0);
            ContentGrid.Children.Add(button1, 0, 2);
            ContentGrid.Children.Add(OTPEntry, 0, 1);

            box.Content = ContentGrid;
            G.Children.Add(box);

            this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.65f);

            CircleImage ciimg = new CircleImage() {
                Source= "Send_Request.png",
                BorderColor = Color.CornflowerBlue,
                BorderThickness = 1,
                Aspect = Aspect.Fill,
                WidthRequest = 100,
                HeightRequest=100,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Center
            };
            G.Children.Add(ciimg,0,0);
            Content = G;
        }

        private async void Button1_Clicked(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(OTPEntry.Text))
            {
                await DisplayAlert("", "Insert OTP", "Ok");
            }
            else
            {
                await App.MyHomePage.ShowLoadingPage();
                (sender as Button).IsEnabled = false;
                try
                {
                    tempModel.OTP = OTPEntry.Text;
                    var resp = App._Api.GetDataResponseFromServer("transaction/create_money_send", JsonConvert.SerializeObject(tempModel));
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    if (App._Api.ProcessDataOrNot(resu.message, false))
                    {
                        await DisplayAlert("", resu.message, "ok");
                        await PopupNavigation.PopAsync();
                        App.MyHomePage.BackToHome();
                        App.GlobalDataUser.BALANCEDOUBLEVALUE = App.GlobalDataUser.BALANCEDOUBLEVALUE - tempModel.amount;
                        App.GlobalDataUser.BALANCE = "Rp " + App.GlobalDataUser.BALANCEDOUBLEVALUE.ToString("n0");
                    }
                    else
                    {
                        await Task.Delay(200);
                        App.MyHomePage.LogOut();   
                    }
                }
                catch (Exception ex)
                {
                    await DisplayAlert("", "No Internet", "ok");
                }
            (sender as Button).IsEnabled = true;
                await App.MyHomePage.HideLoadingPage();
            }
        }
    }
}
