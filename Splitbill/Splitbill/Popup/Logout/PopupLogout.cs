﻿using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill.Popup.Logout
{
    public class PopupLogout : PopupPage
    {
        public EventHandler Eve;
        public PopupLogout()
        {
            Grid G = new Grid() { WidthRequest = 300, HeightRequest = 350 };
            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 30,
                Color = Color.White,
                BorderColor = Color.CornflowerBlue,
                BorderWidth = 2,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 340,
                HeightRequest = 250
            };

            Grid ContentGrid = new Grid();

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5f, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5f, GridUnitType.Star) });

            StackLayout TextLayout = new StackLayout() { Orientation = StackOrientation.Vertical, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.End };

            Label Description = new Label()
            {
                Text = "Do You Want To Sign Out ?",
                FontSize = 10 * App.DpiFontSize,
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                Margin = new Thickness(10, 0, 10, 0)
            };
            TextLayout.Children.Add(Description);

            StackLayout buttonLayout = new StackLayout() {HorizontalOptions = LayoutOptions.Center, Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.Center, Margin = new Thickness(0,15, 0, 0) };
            Button button1 = new Button() { BackgroundColor = Color.CornflowerBlue, Text = "Sign Out", BorderRadius = 17, HorizontalOptions = LayoutOptions.Center, WidthRequest = 145, HeightRequest = 50,FontSize = App.DpiFontSize *6,TextColor = Color.White};
            button1.Clicked += LogOutClick;
            Button button2 = new Button() { BackgroundColor = Color.Gray, Text = "Cancel", BorderRadius = 17, HorizontalOptions = LayoutOptions.Center, WidthRequest = 145, HeightRequest = 50 ,FontSize = 6* App.DpiFontSize,TextColor = Color.White};
            button2.Clicked += CancelClick;
            buttonLayout.Children.Add(button1);
            buttonLayout.Children.Add(button2);

            ContentGrid.Children.Add(buttonLayout, 0, 1);
            ContentGrid.Children.Add(TextLayout, 0, 0);
            box.Content = ContentGrid;

            CircleImage ciImg = new CircleImage()
            {
                Source = "LogoutCircle.png",
                HeightRequest = 100,
                WidthRequest = 100,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Center,
                BorderColor = Color.CornflowerBlue,
                BorderThickness = 2
            };
            Grid Wrapper = new Grid() { HeightRequest = 300, WidthRequest = 325, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.Center };
            Wrapper.Children.Add(box);
            Wrapper.Children.Add(ciImg);
            G.Children.Add(Wrapper, 0, 0);
            this.BackgroundColor = Color.FromRgba(25, 25, 25, 0.8f);
            Content = G;
        }

        private async void CancelClick(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
        }
        private async void LogOutClick(object sender, EventArgs e)
        {
            await PopupNavigation.PopAsync();
            Eve(new object(),EventArgs.Empty);
        }
    }
}
