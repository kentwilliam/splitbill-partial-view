﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill
{
    public class GlobalFunctionCheck
    {
        public bool CheckMessageApiForProcessData(String Message, bool DataFill)
        {
            bool process = false;
            if (!String.IsNullOrEmpty(Message))//jika ada message
            {
                if (Message.ToLower().Contains("expired"))
                {
                    process = false;
                }
                else
                {
                    process = true;
                }
            }
            else //jika tidak ada message
            {
                if (DataFill)//terdapat data
                { process = true; }
                else//tidak terdapat data
                { process = true; }
            }
            return process;
        }
    }
}
