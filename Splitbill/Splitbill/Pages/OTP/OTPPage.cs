﻿using Com.OneSignal;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using Splitbill.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;


namespace Splitbill.Pages.OTP
{
    public class OTPPage : ContentPage
    {
        public event EventHandler DoThis;
        String userName;
        String phone;
        String password;
        String email;
        SignUpJson data;   
        Entry OtpNumber;
        public OTPPage(SignUpJson json)
        {
            data = json;
            userName =json.username;
            phone = json.phone_number;
            password = json.password;
            email = json.email;
            Grid myGrid = new Grid();
            myGrid.BackgroundColor = Color.FromRgb(250, 250, 252);
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star)});
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Grid ContentGrid = new Grid();
            ContentGrid.ColumnSpacing = 0;
            ContentGrid.RowSpacing = 0;
            ContentGrid.BackgroundColor = Color.FromRgb(57, 169, 251);
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            myGrid.Children.Add(ContentGrid,0,0);
            Image iLogo1 = new Image();
            iLogo1.Source = "LogoUrunan.png";
            iLogo1.Aspect = Aspect.AspectFit;
            iLogo1.HorizontalOptions = LayoutOptions.Center;
            iLogo1.VerticalOptions = LayoutOptions.Center;
            ContentGrid.Children.Add(iLogo1,1, 0);

            Image backIcon = new Image();
            backIcon.Source = "BackIcon.png";
            backIcon.WidthRequest = 35;
            backIcon.HeightRequest = 35;
            backIcon.Margin = new Thickness(0,5,5,5);
            backIcon.Aspect = Aspect.AspectFit;
            backIcon.VerticalOptions = LayoutOptions.Center;
            backIcon.GestureRecognizers.Add(new TapGestureRecognizer(PrevPage));
            ContentGrid.Children.Add(backIcon, 0, 0);

            Label Title = new Label();
            Title.Text = "Enter OTP Code Here ";
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalTextAlignment = TextAlignment.Center;
            Title.FontSize = 22;
            Title.TextColor = Color.FromRgb(128, 128, 128);

            OtpNumber = new Entry() ;
            OtpNumber.HorizontalOptions = LayoutOptions.FillAndExpand;
            OtpNumber.VerticalOptions = LayoutOptions.FillAndExpand;
            OtpNumber.HorizontalTextAlignment = TextAlignment.Center;
            
            OtpNumber.Placeholder = "--- ---"; 
            OtpNumber.FontSize = App.XXLargeText * Device.GetNamedSize(NamedSize.Medium,typeof(Entry));
            OtpNumber.Keyboard = Keyboard.Numeric;
            OtpNumber.TextChanged += OtpNumber_TextChanged;
            myGrid.Children.Add(Title,0,1);
            myGrid.Children.Add(OtpNumber,0,2);
            Grid.SetRowSpan(OtpNumber,2);

            StackLayout sl = new StackLayout();
            Label l = new Label() { HorizontalTextAlignment = TextAlignment.Center,Text = "Enter 6-Digit Code",TextColor = Color.Gray};
            Label resend = new Label() { HorizontalTextAlignment = TextAlignment.Center, Text = "Resend Code", TextColor = Color.Blue };
            resend.GestureRecognizers.Add(new TapGestureRecognizer(resendCodeClick));
            sl.Children.Add(l);sl.Children.Add(resend);
            myGrid.Children.Add(sl,0,4);
            

            Button Confirm = new Button();
            Confirm.Clicked += Confirm_Clicked;
            Confirm.Text = "Confirm";
            myGrid.Children.Add(Confirm,0,5);

            Content = myGrid;
        }

        private void PrevPage(Xamarin.Forms.View arg1, object arg2)
        {
            DoThis(new object(),EventArgs.Empty);
        }

        private async void resendCodeClick(Xamarin.Forms.View arg1, object arg2)
        {
            var resp = App._Api.GetDataResponseFromServer("user/regenerate_OTP", JsonConvert.SerializeObject(new RegenerateOTPJson { username = userName }));
            var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
            await DisplayAlert("", resu.message, "Ok");
            
        }

        private void OtpNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            int nextText = e.NewTextValue == null?0:e.NewTextValue.Length;
            int prevText = e.OldTextValue == null?0: e.OldTextValue.Length;
            int numb = (sender as Entry).Text.Length;
            if(nextText > prevText)
            {
                if (numb == 3)
                {
                    (sender as Entry).Text += '-';
                }
                else if (numb > 7)
                {
                    (sender as Entry).Text = (sender as Entry).Text.Substring(0, 7);
                }
            }
        }

        private async void Confirm_Clicked(object sender, EventArgs e)
        {
            int index = OtpNumber.Text.IndexOf("-");
            if (index < 0 || OtpNumber.Text.Length == 7)
            {
                String OTP = OtpNumber.Text.Substring(0, index) + OtpNumber.Text.Substring(index + 1);
                data.otp = OTP;
                try
                {
                    var resp = App._Api.GetDataResponseFromServer("user/validate_OTP", JsonConvert.SerializeObject(data));
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    await DisplayAlert("", resu.message, "ok");
                    if (resu.message.ToLower() == "otp expired")
                    {
                        resendCodeClick(null, new object());
                    }
                    else if (resu.status == "true")
                    {
                        UserLoginJson ur = new UserLoginJson() { password = data.password, phone_number = data.phone_number };
                        var respond_login = App._Api.GetDataResponseFromServer("user/log_in", JsonConvert.SerializeObject(ur));
                        var result_login = JsonConvert.DeserializeObject<UserResponse>(respond_login.Result);

                        UserData _userdata = new UserData()
                        {
                            TOKEN = result_login.token,
                            BALANCE = "IDR " + result_login.user[0].balance.ToString("n0") + "",
                            EMAIL = result_login.user[0].email,
                            PHOTO = String.IsNullOrEmpty(result_login.user[0].photo)?"user72.png":"http://www.urunan.id/splitbill/" + result_login.user[0].photo,
                            USERNAME = result_login.user[0].username,
                            ALAMAT = result_login.user[0].device,
                            PASSWORD = password,
                            TELEPHONE = userName,
                            NOTIFICATIONCOUNTER = result_login.notification,
                            NOTIFICATIONSHOW = Convert.ToInt32(result_login.notification) > 0 ? true : false,
                            BALANCEDOUBLEVALUE = result_login.user[0].balance
                        };
                        OneSignal.Current.SendTag("username", result_login.user[0].username);
                        App.GlobalDataUser = _userdata;
                        var home = new HomePage();
                        App.Current.MainPage = home;
                        App.MyHomePage = home;
                    }
                }
                catch (Exception ex)
                {
                    await DisplayAlert("","No Internet","ok");
                }
            }

        }
    }
}