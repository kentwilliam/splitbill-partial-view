﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Rg.Plugins.Popup.Services;
using Newtonsoft.Json;
using Splitbill.Api.Encode;
using Splitbill.Api.Core;

namespace Splitbill.Pages.ForgotPassword
{
    public class ForgotPage : ContentPage
    {
        ExtendedEntry eEmail;
        public ForgotPage()
        {
            Grid ContentGrid = new Grid();
            ContentGrid.BackgroundColor = Color.FromRgb(250, 250, 252);
            
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Image iLogo1 = new Image();
            iLogo1.Source = "LogoUrunan.png";
            iLogo1.Aspect = Aspect.AspectFit;
            iLogo1.HorizontalOptions = LayoutOptions.Center;
            iLogo1.VerticalOptions = LayoutOptions.Center;
            ContentGrid.Children.Add(iLogo1, 0, 1);
            Grid.SetRowSpan(iLogo1, 3);

            //forgot your password
            StackLayout ForgotStack = new StackLayout();
            ForgotStack.Orientation = StackOrientation.Vertical;
            ForgotStack.HorizontalOptions = LayoutOptions.Center;
            ForgotStack.VerticalOptions = LayoutOptions.Center;

            Label lForgot = new Label();
            lForgot.FontAttributes = FontAttributes.Bold;
            lForgot.Text = "Forgot your Password?";
            lForgot.TextColor = Color.Black;
            lForgot.HorizontalTextAlignment = TextAlignment.Center;
            lForgot.VerticalTextAlignment = TextAlignment.Center;
            ForgotStack.Children.Add(lForgot);

            //forgot your password
            Label lKet = new Label();
            lKet.FontAttributes = FontAttributes.None;
            lKet.Text = "Enter your New Password below";
            lKet.TextColor = Color.Black;
            lKet.HorizontalTextAlignment = TextAlignment.Center;
            lKet.VerticalTextAlignment = TextAlignment.Center;
            ForgotStack.Children.Add(lKet);
            ContentGrid.Children.Add(ForgotStack, 0, 4);

            //email
            Frame border = new Frame() {
                WidthRequest = 290,
                HorizontalOptions = LayoutOptions.Center,
                Margin = 0,
                Padding = 0,
                VerticalOptions = LayoutOptions.Center,
                IsClippedToBounds = true
            };

            StackLayout EmailStack = new StackLayout();
            EmailStack.Orientation = StackOrientation.Horizontal;
            //EmailStack.HorizontalOptions = LayoutOptions.Center;
            //EmailStack.VerticalOptions = LayoutOptions.Center;
            EmailStack.Spacing = 0;
            EmailStack.WidthRequest = 290;

            Image iEmail = new Image();
            iEmail.BackgroundColor = Color.FromRgb(57, 169, 251);
            iEmail.Source = "MailOpen.png";
            iEmail.WidthRequest = App.IconSize;
            iEmail.HeightRequest = App.IconSize;
            iEmail.Aspect = Aspect.AspectFit;
            iEmail.HorizontalOptions = LayoutOptions.Start;
            EmailStack.Children.Add(iEmail);

            eEmail = new ExtendedEntry();
            eEmail.Placeholder = "Email Address";
            eEmail.HorizontalOptions = LayoutOptions.FillAndExpand;
            eEmail.BackgroundColor = Color.White;
            EmailStack.Children.Add(eEmail);

            border.Content = EmailStack;
            ContentGrid.Children.Add(border, 0, 6);

            //create account
            StackLayout CommandStack = new StackLayout();
            CommandStack.Orientation = StackOrientation.Horizontal;
            CommandStack.HorizontalOptions = LayoutOptions.Center;
            CommandStack.VerticalOptions = LayoutOptions.Center;

            Button bSubmit = new Button();
            bSubmit.Text = "Submit";
            bSubmit.BackgroundColor = Color.FromRgb(57, 169, 251);
            bSubmit.TextColor = Color.White;
            bSubmit.WidthRequest = 150;
            bSubmit.Clicked += BSubmit_Clicked;
            CommandStack.Children.Add(bSubmit);

            Button bCancel = new Button();
            bCancel.Text = "Cancel";
            bCancel.BackgroundColor = Color.FromRgb(126, 141, 172);
            bCancel.TextColor = Color.White;
            bCancel.WidthRequest = 150;
            bCancel.Clicked += BCancel_Clicked;
            CommandStack.Children.Add(bCancel);
            ContentGrid.Children.Add(CommandStack, 0, 7);

            //footer
            Grid FooterGrid = new Grid();
            FooterGrid.BackgroundColor = Color.FromRgb(241, 242, 247);
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            //help
            StackLayout HelpStack = new StackLayout();
            HelpStack.Orientation = StackOrientation.Horizontal;
            HelpStack.HorizontalOptions = LayoutOptions.End;
            HelpStack.VerticalOptions = LayoutOptions.Center;
            HelpStack.Margin = new Thickness(0, 0, 10, 10);

            Image iHelp = new Image();
            iHelp.Source = "Help.png";
            iHelp.Aspect = Aspect.AspectFit;
            HelpStack.Children.Add(iHelp);

            ContentGrid.Children.Add(FooterGrid, 0, 11);

            Content = ContentGrid;
        }

        private void BSubmit_Clicked(object sender, EventArgs e)
        {
            var resp = App._Api.GetDataResponseFromServer("user/forget_password", JsonConvert.SerializeObject(new ForgotPasswordJson() { email = eEmail.Text}));
            var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
            if (resu.status == "true")
            {
                DisplayAlert("",resu.message,"ok");
            }
            else
            {
                DisplayAlert("", resu.message, "ok");
            }
        }

        private void BCancel_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async void iloginClicked(Xamarin.Forms.View arg1, object arg2)
        {
            try
            { await Navigation.PopModalAsync(); }
            catch (Exception ex)
            { Application.Current.MainPage = new Splitbill.Pages.Login.LoginPage(); }
        }
    }
}