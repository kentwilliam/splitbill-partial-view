﻿using Com.OneSignal;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Decode.Mandiri;
using Splitbill.Api.Encode;
using Splitbill.Model.User;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Pages.Login
{
    public class LoginPage:ContentPage
    {
        HomePage home;
        ExtendedEntry eUsername;
        ExtendedEntry ePassword;
        Grid loadingScreen;
        Grid ContentGrid;
        bool Login = false;
        Frame LoadingFrame;
        bool AutoSignIn;
        public void setAutoSignin()
        {
            AutoSignIn = true;
        }
        public LoginPage()
        {
            ContentGrid = new Grid() { Opacity = 0};
            ContentGrid.BackgroundColor = Color.FromRgb(250, 250, 252);
            
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            //loadingScreen = new Grid() { RowSpacing = 0 };
            //loadingScreen.BackgroundColor = Color.FromRgb(250, 250, 252);
            //loadingScreen.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            //loadingScreen.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            //Image splash = new Image() { Source = "splashscreen.png", VerticalOptions = LayoutOptions.Fill, HorizontalOptions = LayoutOptions.Fill };
            //loadingScreen.Children.Add(splash, 0, 0);
            //Grid.SetRowSpan(splash, 12);

            //SfBusyIndicator busyIndicator = new SfBusyIndicator()
            //{
            //    AnimationType = AnimationTypes.ZoomingTarget,
            //    BackgroundColor = Color.FromRgb(250, 250, 252),
            //    VerticalOptions = LayoutOptions.Start,
            //    HorizontalOptions = LayoutOptions.Fill
            //};

            //loadingScreen.Children.Add(busyIndicator, 0, 6);
            //Grid.SetRowSpan(busyIndicator, 6);

            Image iLogo1 = new Image();
            iLogo1.Source = "LogoUrunan.png";
            iLogo1.Aspect = Aspect.AspectFit;
            iLogo1.HorizontalOptions = LayoutOptions.Center;
            iLogo1.VerticalOptions = LayoutOptions.Center;
            ContentGrid.Children.Add(iLogo1, 0, 1);
            Grid.SetRowSpan(iLogo1, 3);


            //username
            //XFShapeView.ShapeView UserStackBorder = new XFShapeView.ShapeView();
            //UserStackBorder.ShapeType = XFShapeView.ShapeType.Box;
            //UserStackBorder.CornerRadius = 10;
            //UserStackBorder.Color = Color.Gray;
            //UserStackBorder.WidthRequest = 290;
            //UserStackBorder.HorizontalOptions = LayoutOptions.Center;

            Frame UserStackBorder = new Frame();
            UserStackBorder.CornerRadius = 3;
            UserStackBorder.BackgroundColor = Color.LightGray;
            UserStackBorder.Padding = 0;
            UserStackBorder.Margin = 0;
            UserStackBorder.HorizontalOptions = LayoutOptions.Center;
            UserStackBorder.WidthRequest = 290;
            UserStackBorder.IsClippedToBounds = true;

            StackLayout UserStack = new StackLayout();
            UserStack.IsClippedToBounds = true;
            UserStack.Orientation = StackOrientation.Horizontal;
            UserStack.HorizontalOptions = LayoutOptions.FillAndExpand;
            UserStack.BackgroundColor = Color.FromRgb(57, 169, 251);
            UserStack.Spacing = 0;
            
            //UserStack.Margin = new Thickness(2);

            Image iLogoUser = new Image();
            iLogoUser.BackgroundColor = Color.FromRgb(57, 169, 251);
            iLogoUser.Source = "Phone.png";
            iLogoUser.WidthRequest = App.DpiFontSize *15;
            iLogoUser.HeightRequest = App.DpiFontSize * 15;
            iLogoUser.Aspect = Aspect.AspectFit;
            iLogoUser.HorizontalOptions = LayoutOptions.Fill;
            UserStack.Children.Add(iLogoUser);

            eUsername = new ExtendedEntry();
            eUsername.Placeholder = "Telephone";
            eUsername.Keyboard = Keyboard.Telephone;
            eUsername.Text = "";
            eUsername.FontSize = App.DpiFontSize * 8;
            eUsername.BackgroundColor = Color.White;
            eUsername.HorizontalOptions = LayoutOptions.FillAndExpand;

            UserStack.Children.Add(eUsername);
            UserStackBorder.Content = UserStack;
            ContentGrid.Children.Add(UserStackBorder, 0, 5);

            //password
            Frame PassStackBorder = new Frame();
            PassStackBorder.CornerRadius = 3;
            PassStackBorder.WidthRequest = 290;
            PassStackBorder.Margin = 0;
            PassStackBorder.Padding = 0;
            PassStackBorder.BackgroundColor = Color.LightGray;
            PassStackBorder.HorizontalOptions = LayoutOptions.Center;
            PassStackBorder.IsClippedToBounds = true;

            StackLayout PassStack = new StackLayout();
            PassStack.Orientation = StackOrientation.Horizontal;
            PassStack.HorizontalOptions = LayoutOptions.FillAndExpand;
            PassStack.Spacing = 0;
            PassStack.BackgroundColor = Color.FromRgb(57, 169, 251);
            PassStack.IsClippedToBounds = true;

            Image iLogoPass = new Image();
            iLogoPass.BackgroundColor = Color.FromRgb(57, 169, 251);
            iLogoPass.Source = "Password.png";
            iLogoPass.Aspect = Aspect.AspectFit;
            iLogoPass.WidthRequest = App.DpiFontSize * 15;
            iLogoUser.HeightRequest = App.DpiFontSize * 15;
            iLogoPass.HorizontalOptions = LayoutOptions.Center;
            PassStack.Children.Add(iLogoPass);

            ePassword = new ExtendedEntry();
            ePassword.Text = "";
            ePassword.Placeholder = "Password";
            ePassword.FontSize = App.DpiFontSize * 8;
            ePassword.BackgroundColor = Color.White;
            ePassword.HorizontalOptions = LayoutOptions.FillAndExpand;
            ePassword.IsPassword = true;
            

            PassStack.Children.Add(ePassword);
            PassStackBorder.Content = PassStack;
            ContentGrid.Children.Add(PassStackBorder, 0, 6);

            //sign in
            Button bSignIn = new Button();
            bSignIn.Text = "Sign In";
            bSignIn.BackgroundColor = Color.FromRgb(57, 169, 251);
            bSignIn.TextColor = Color.White;
            bSignIn.HorizontalOptions = LayoutOptions.Center;
            bSignIn.VerticalOptions = LayoutOptions.Center;
            bSignIn.WidthRequest = 300;
            bSignIn.Clicked += BSignIn_Clicked1;
            bSignIn.FontSize = App.DpiFontSize * 8;

            ContentGrid.Children.Add(bSignIn, 0, 8);

            //forgot your password
            Label lForgot = new Label();
            lForgot.Text = "Forgot your password?";
            lForgot.FontSize = App.DpiFontSize * 8;
            lForgot.HorizontalOptions = LayoutOptions.Center;
            lForgot.VerticalOptions = LayoutOptions.Center;
            lForgot.GestureRecognizers.Add(new TapGestureRecognizer(OnForgotTap));
            ContentGrid.Children.Add(lForgot, 0,7);

            //social media
            StackLayout SocialStack = new StackLayout();
            SocialStack.Orientation = StackOrientation.Horizontal;
            SocialStack.HorizontalOptions = LayoutOptions.Center;
            SocialStack.VerticalOptions = LayoutOptions.Center;

            Image iFacebook = new Image();
            iFacebook.Source = "FB.png";
            iFacebook.Aspect = Aspect.AspectFit;
            SocialStack.Children.Add(iFacebook);

            Image iTwitter = new Image();
            iTwitter.Source = "Twit.png";
            iTwitter.Aspect = Aspect.AspectFit;
            SocialStack.Children.Add(iTwitter);

            Image iGoogle = new Image();
            iGoogle.Source = "Google.png";
            iGoogle.Aspect = Aspect.AspectFit;
            SocialStack.Children.Add(iGoogle);
            ContentGrid.Children.Add(SocialStack, 0, 9);

            //footer
            Grid FooterGrid = new Grid();
            FooterGrid.BackgroundColor = Color.FromRgb(250, 250, 252);
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            //signup
            StackLayout SignUpStack = new StackLayout();
            SignUpStack.Orientation = StackOrientation.Horizontal;
            SignUpStack.HorizontalOptions = LayoutOptions.Start;
            SignUpStack.VerticalOptions = LayoutOptions.Center;
            SignUpStack.Margin = new Thickness(10, 0, 0, 10);
            SignUpStack.GestureRecognizers.Add(new TapGestureRecognizer(OnRegisterTap));

            Image iSignUp = new Image();
            iSignUp.Source = "SignUp.png";
            iSignUp.WidthRequest = App.DpiFontSize * 15;
            iSignUp.HeightRequest = App.DpiFontSize * 15;
            iSignUp.Aspect = Aspect.AspectFit;
            SignUpStack.Children.Add(iSignUp);

            Label lSignUp = new Label();
            lSignUp.Text = "Sign up";
            lSignUp.FontSize = 7 * App.DpiFontSize;
            lSignUp.TextColor = Color.Black;
            lSignUp.VerticalOptions = LayoutOptions.Center;
            SignUpStack.Children.Add(lSignUp);
            FooterGrid.Children.Add(SignUpStack, 0, 0);

            ContentGrid.Children.Add(FooterGrid, 0, 11);

            Views.Loading.Loading load = new Views.Loading.Loading();
            LoadingFrame = new Frame() {
                Margin = 0,
                Padding = 0,
                CornerRadius = 0,
                BackgroundColor = Color.Transparent,
                OutlineColor = Color.Transparent,
                Content = load,
                TranslationX = 1000,
            };

            MessagingCenter.Subscribe<LoginPage>(this, "off", (v) =>
            {
                v.LoadingFrame.TranslationX = 1000;
            });

            ContentGrid.Children.Add(LoadingFrame, 0, 0);
            Grid.SetRowSpan(LoadingFrame, 12);
            Content = ContentGrid;

        }

        private  void BSignIn_Clicked1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(ePassword.Text))
            {  DisplayAlert("", "Password Empty", "Ok"); }
            else if (String.IsNullOrEmpty(eUsername.Text))
            {  DisplayAlert("", "Phone Empty", "Ok"); }
            else
            {
                LoadingFrame.TranslationX = 0;
                Task.Run(() =>
                {
                    try
                    {
                        //var res = App._Api.GetDataResponseFromServer("user/log_in", JsonConvert.SerializeObject(new UserLoginJson() { password = "kent12345", phone_number = "085103243131" }));
                        var res = App._Api.GetDataResponseFromServer("user/log_in", JsonConvert.SerializeObject(new UserLoginJson() { password = ePassword.Text, phone_number = eUsername.Text }));
                        UserResponse ur = JsonConvert.DeserializeObject<UserResponse>(res.Result);
                        if (ur.status == "false")
                        {
                            Device.BeginInvokeOnMainThread(async() => {
                                await DisplayAlert("", ur.message, "Ok");
                                MessagingCenter.Send<LoginPage>(this, "off");
                            });
                            
                        }
                        else
                        {
                            UserData ud = new UserData()
                            {
                                USERNAME = ur.user[0].username,
                                ALAMAT = ur.user[0].username,
                                EMAIL = ur.user[0].email,
                                BALANCE = "Rp. " + ur.user[0].balance.ToString("n0"),
                                BALANCEDOUBLEVALUE = ur.user[0].balance,
                                PASSWORD = ur.user[0].password,
                                PHOTO = String.IsNullOrEmpty(ur.user[0].photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + ur.user[0].photo,
                                TOKEN = ur.token,
                                TELEPHONE = ur.user[0].phone_number,
                                NOTIFICATIONCOUNTER = ur.notification,
                            };
                            ud.NOTIFICATIONSHOW = int.Parse(ur.notification) > 0 ? true : false;

                            var userdata = App.MyUserDatabase.GetUser();
                            bool notication = true;
                            if (userdata != null)
                            {
                                notication = App.MyUserDatabase.GetUser().Notification;
                                App.MyUserDatabase.DeleteUser(new Database.Models.User() { Id = 0 });
                                App.MyUserDatabase.SaveUser(new Database.Models.User() { Id = 0, Password = ePassword.Text, Username = eUsername.Text, Notification = notication });
                            }
                            else
                            {
                                App.MyUserDatabase.SaveUser(new Database.Models.User() { Id = 0, Password = ePassword.Text, Username = eUsername.Text, Notification = notication });
                            }
                            ud.NOTIFICATIONACTIVE = App.MyUserDatabase.GetUser().Notification;
                            App.GlobalDataUser = ud;
                            String username = ur.user[0].username;
                            OneSignal.Current.SendTag("username", username);
                            OneSignal.Current.SetSubscription(true);
                            Username_Token ut = new Username_Token()
                            {
                                token = ur.token,
                                username = ur.user[0].username
                            };
                            var home = new HomePage();
                            Device.BeginInvokeOnMainThread(() => {
                                MessagingCenter.Send<LoginPage>(this, "off");
                                App.MyHomePage = home;
                                App.Current.MainPage = home;
                            });
                            

                        }
                    }
                    catch (Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(async() =>
                        {
                            await DisplayAlert("","No Internet Connection","Ok");
                            MessagingCenter.Send<LoginPage>(this, "off");
                        });
                        
                    }
                });
            }
        }

        protected override async void OnAppearing()
        {   
            base.OnAppearing();
            await ContentGrid.FadeTo(1, 300);
            var data = App.MyUserDatabase.GetUser();
            if (data != null)
            {
                eUsername.Text = data.Username;
                ePassword.Text = data.Password;
            }
            if (AutoSignIn)
            {
                BSignIn_Clicked1(new object(), EventArgs.Empty);
            }
        }
        private async void OnRegisterTap(View arg1, object arg2)
        {
            (arg1 as StackLayout).IsEnabled = false;
            await Navigation.PushModalAsync(new RegisterPage());
            (arg1 as StackLayout).IsEnabled = true;
        }

        private async void OnForgotTap(View arg1, object arg2)
        {
            (arg1 as Label).IsEnabled = false;
            Splitbill.Pages.ForgotPassword.ForgotPage forgot = new ForgotPassword.ForgotPage();
            await Navigation.PushModalAsync(forgot);
            (arg1 as Label).IsEnabled = true;
        }
    }
}
