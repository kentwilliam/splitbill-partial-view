﻿using FFImageLoading.Forms;
using Rg.Plugins.Popup.Services;
using Splitbill.Model;
using Splitbill.Views.Notification;
using Splitbill.Pages.Request;
using Splitbill.Views.AddMoney;
using Splitbill.Views.HomePage;
using Splitbill.Views.MyTransaction;
using Splitbill.Views.Send;
using Syncfusion.SfNavigationDrawer.XForms;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using Com.OneSignal;
using System.Threading.Tasks;
using Splitbill.Views.EditProfile;
using Splitbill.Views.Loading;
using Splitbill.Views.SuggestFriend;
using Splitbill.Views.Friend;
using System.Collections.ObjectModel;
using Splitbill.Model.RequestModel;
using Splitbill.Views.TrackingBill;

namespace Splitbill.Pages
{
    public class HomePage : ContentPage
    {
        ContentView previousView;
        MyTransaction myTransaction = new MyTransaction() { Opacity = 1,TranslationX = 1000};
        List<MenuModel> lMenu;
        ListView DrawerContent;
        SfNavigationDrawer navigationDrawer = null;
        Grid ContentGrid;
        Grid ContentViewAnotherPage;
        Loading loading = new Loading() { TranslationX = 1500};

        MyNotification notificationPage;
        ContentViewHomePage homePage;
        EditProfile editProfilePage;
        SendSplitbillView sendPage;
        Splitbill.Views.Request.Request requestPage;
        AddMoneyView addMoneyPage;
        MyTransaction mytransactionPage;
        SuggestFriend suggestFriendPage;
        FriendView friendPage;
        public HomePage()
        {
            InitView();
        }
        public void InitView()
        {
            ContentViewAnotherPage = new Grid() { };
            ContentGrid = new Grid() { Opacity = 1};
            ContentGrid.ColumnSpacing = 0;
            ContentGrid.RowSpacing = 0;
            ContentGrid.BackgroundColor = Color.FromRgb(57, 169, 251);
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });

            CachedImage iMenu = new CachedImage() {
                Source = "Menu.png",
                Margin = new Thickness(10, 0, 10, 10),
                Aspect = Aspect.AspectFit,
                HeightRequest= App.DpiFontSize * 20,
                WidthRequest = App.DpiFontSize * 20,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
        };
            

            StackLayout stackMenu = new StackLayout() {
                WidthRequest = 50,
                HeightRequest = 50,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            stackMenu.Children.Add(iMenu);
            stackMenu.GestureRecognizers.Add(new TapGestureRecognizer(OnTap));

            CachedImage iNotif = new CachedImage()
            {
                Source = "NotificationIcon.png",
            Margin = new Thickness(10, 5, 10, 5),
            Aspect = Aspect.AspectFit,
            WidthRequest = App.DpiFontSize * 20,
            HeightRequest = App.DpiFontSize * 20,
            HorizontalOptions = LayoutOptions.FillAndExpand,
            VerticalOptions = LayoutOptions.FillAndExpand,
        };
            iNotif.GestureRecognizers.Add(new TapGestureRecognizer(NotifClick));

            Grid mynotif = new Grid()
            {
                HorizontalOptions = LayoutOptions.Center,
            HeightRequest = 50,
            WidthRequest = 50,
        };
            mynotif.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mynotif.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mynotif.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            mynotif.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            mynotif.Children.Add(iNotif,0,0);
            Grid.SetRowSpan(iNotif,2);
            Grid.SetColumnSpan(iNotif, 2);

            Label conter = new Label()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White,
                VerticalTextAlignment = TextAlignment.Center,
                FontSize = 5* App.DpiFontSize,
            };
            conter.SetBinding(Label.TextProperty,"NOTIFICATIONCOUNTER");
            XFShapeView.ShapeView bulet = new XFShapeView.ShapeView() {
                ShapeType = XFShapeView.ShapeType.Circle,
                Color = Color.Red,
                Content = conter,
                Margin = new Thickness(0,1,0,0)
            };
            bulet.BindingContext = App.GlobalDataUser;
            bulet.SetBinding(XFShapeView.ShapeView.IsVisibleProperty, "NOTIFICATIONSHOW");

            mynotif.Children.Add(bulet,1,0);
            ContentGrid.Children.Add(ContentViewAnotherPage,0,0);
            Grid.SetRowSpan(ContentViewAnotherPage,2);
            Grid.SetColumnSpan(ContentViewAnotherPage, 3);
            ContentGrid.Children.Add(stackMenu, 0, 0);
            ContentGrid.Children.Add(mynotif, 2, 0);

            ContentGrid.Children.Add(loading);
            Grid.SetRowSpan(loading,2);
            Grid.SetColumnSpan(loading, 3);

            navigationDrawer = new SfNavigationDrawer() {
                DrawerWidth = 250,
                Transition = Transition.SlideOnTop,
                DrawerHeaderHeight = 0,
                DrawerFooterHeight = 0,
                BackgroundColor = Color.FromRgb(250, 250, 252),
        };
            
            Content = navigationDrawer;
            CreateDrawerContent();
            navigationDrawer.ContentView = ContentGrid;
        }
        
        private async void NotifClick(View arg1, object arg2)
        {
            CreateDrawerContent();
            if (notificationPage == null) { notificationPage = new MyNotification(); }
            SlideHideView(notificationPage);
            notificationPage.InitData();
        }

        private void OnTap(View arg1, object arg2)
        {
            navigationDrawer.ToggleDrawer();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ContentGrid.FadeTo(1, 750);
            ContentViewAnotherPage.Children.Clear();
            ContentViewHomePage vie = new ContentViewHomePage();
            vie.InitData();
            ContentViewAnotherPage.Children.Add(vie);
            
            await HideLoadingPage();

        }
        DataTemplate MenuDataTemplate = new DataTemplate(() =>
        {
            CachedImage img = new CachedImage();
            Label title = new Label();
            Grid HorizontalGrid = new Grid();
            StackLayout WrapperStack = new StackLayout();

            HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });
            HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(8, GridUnitType.Star) });
            HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });
            HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });

            img.Margin = new Thickness(3, 3, 3, 3);
            img.WidthRequest = 35;
            img.HeightRequest = 35;
            img.Aspect = Aspect.AspectFit;
            title.HorizontalOptions = LayoutOptions.Center;
            title.VerticalOptions = LayoutOptions.Center;

            title.FontSize = 14;
            title.HorizontalOptions = LayoutOptions.Start;
            title.VerticalOptions = LayoutOptions.Center;

            img.SetBinding(CachedImage.SourceProperty, "ImageSource");
            title.SetBinding(Label.TextProperty, "TitleImage");
            title.SetBinding(Label.TextColorProperty, "LabelColor");

            //BoxView garisAtas = new BoxView();
            //garisAtas.BackgroundColor = Color.LightGray;

            //HorizontalGrid.Children.Add(garisAtas, 0, 0);
            HorizontalGrid.Children.Add(img, 0, 1);
            HorizontalGrid.Children.Add(title, 1, 1);
            //Grid.SetColumnSpan(garisAtas, 2);
            WrapperStack.Children.Add(HorizontalGrid);
            return new NavExtendedViewcell { View = WrapperStack };
        });
        private void CreateDrawerContent()
        {
            //drawer content
           
            
            MenuModel menu = null;
            lMenu = new List<MenuModel>();
            menu = new MenuModel();
            menu.ImageSource = "HomeIcon.png";
            menu.TitleImage = "Home";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "EditProfile.png";
            menu.TitleImage = "Edit Profile";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "AddMoney.png";
            menu.TitleImage = "Add Money";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "Send.png";
            menu.TitleImage = "Send";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "Request.png";
            menu.TitleImage = "Request";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "MyTransaction.png";
            menu.TitleImage = "My Transaction";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "Records.png";
            menu.TitleImage = "Transaction Book";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "Friends.png";
            menu.TitleImage = "Friends";
            menu.LabelColor = Color.FromRgb(0, 162, 232);
            lMenu.Add(menu);

            menu = new MenuModel();
            menu.ImageSource = "LogOut.png";
            menu.TitleImage = "Sign Out";
            lMenu.Add(menu);

            menu.LabelColor = Color.FromRgb(0, 162, 232);
            

            DrawerContent = new ListView();
            DrawerContent.ItemSelected += DrawerContent_ItemSelected;
            DrawerContent.ItemTemplate = MenuDataTemplate;
            DrawerContent.ItemsSource = lMenu;
            DrawerContent.BackgroundColor = Color.FromRgb(250, 250, 252);
            DrawerContent.SeparatorColor = Color.Transparent;
            navigationDrawer.DrawerContentView = DrawerContent;

            Grid Head = new Grid() { BackgroundColor = Color.FromRgb(250, 250, 252)};
            Image Logo = new Image() {
                Source = "LogoUrunan.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            Head.Children.Add(Logo);

            navigationDrawer.DrawerHeaderView = Head;
            navigationDrawer.DrawerHeaderHeight = 150;
            
        }

        private async void DrawerContent_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            CreateDrawerContent();
            var data = e.SelectedItem as MenuModel;
            if (data.TitleImage.ToLower() == "add money")
            {
                if (addMoneyPage == null) { addMoneyPage = new AddMoneyView(); }
                navigationDrawer.ToggleDrawer();
                await Task.Delay(300);
                await ShowLoadingPage();
                SlideHideView(addMoneyPage);
                await HideLoadingPage();
            }
            else if (data.TitleImage.ToLower() == "edit profile")
            {
                await ToEditProfile();
            }
            else if (data.TitleImage.ToLower() == "home")
            {
                if (homePage == null) { homePage = new ContentViewHomePage(); }
                navigationDrawer.ToggleDrawer();
                BackToHome();
            }
            else if (data.TitleImage.ToLower() == "request")
            {
                if (navigationDrawer.IsOpen)
                {
                    navigationDrawer.ToggleDrawer();
                }
                ToRequestView();
            }
            else if (data.TitleImage.ToLower() == "send")
            {
                if (sendPage == null) { sendPage = new SendSplitbillView(); }
                sendPage.ResetForm();
                navigationDrawer.ToggleDrawer();
                await Task.Delay(300);
                await ShowLoadingPage();
                SlideHideView(sendPage);
                await HideLoadingPage();
            }
            else if (data.TitleImage.ToLower() == "friends")
            {
                ToFriendsPage();

            }
            else if (data.TitleImage.ToLower() == "my transaction")
            {
                TomyTransaction();
            }
            else if (data.TitleImage.ToLower() == "transaction book")
            {
                navigationDrawer.ToggleDrawer();
                await Task.Delay(300);
                await ShowLoadingPage();
                SlideHideView(new TrackingBill());
                await HideLoadingPage();
            }
            else if (data.TitleImage.ToLower() == "sign out")
            {
                navigationDrawer.ToggleDrawer();
                Popup.Logout.PopupLogout logout = new Popup.Logout.PopupLogout();
                logout.Eve += LogoutClosed;
                await PopupNavigation.PushAsync(logout);
            }
            
        }

        public void TomyTransaction()
        {
            if (mytransactionPage == null) { mytransactionPage = new MyTransaction(); }
            navigationDrawer.ToggleDrawer();
            SlideHideView(mytransactionPage);
            mytransactionPage.InitData();
        }

        public void ToFriendsPage()
        {
            if (friendPage == null) { friendPage = new FriendView(); }
            if (navigationDrawer.IsOpen)
            {
                navigationDrawer.ToggleDrawer();
            }
            else
            {

            }
            SlideHideView(friendPage);
            friendPage.InitData();
        }

        public async Task ToEditProfile()
        {
            if (editProfilePage == null) { editProfilePage = new EditProfile(); }
            if (navigationDrawer.IsOpen)
            { navigationDrawer.ToggleDrawer(); }
            SlideHideView(editProfilePage);
        }

        private async void LogoutClosed(object sender, EventArgs e)
        {
            ShowLoadingPage();
            var resp = App._Api.GetDataResponseFromServer("user/log_out", JsonConvert.SerializeObject(new Username_Token()
            {
                token = App.GlobalDataUser.TOKEN,
                username = App.GlobalDataUser.USERNAME
            }));
            OneSignal.Current.SetSubscription(false);
            try
            {
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                if (App._Api.ProcessDataOrNot(resu.message, false))
                {
                    if (resu.status == "true")
                    {
                        DisplayAlert("", resu.message, "ok");
                        ContentGrid.Children.Clear();
                        App.GlobalDataUser = null;
                        App.MyHomePage = null;
                        App.Current.MainPage = new Login.LoginPage();
                    }
                    else
                    { DisplayAlert("", resu.message, "ok"); }
                }
                else
                {
                    Task.Delay(200);
                    await PopupNavigation.PopAllAsync();
                    DisplayAlert("", resu.message, "Ok");
                    App.Current.MainPage = new Pages.Login.LoginPage();
                }
            }
            catch (Exception ex)
            { DisplayAlert("","internet Connection Error ","ok");
                
            }
            HideLoadingPage();
        }
        private void LogoutQuit(object sender, EventArgs e)
        {
            OneSignal.Current.SetSubscription(false);
            base.OnBackButtonPressed();   
        }
        private void SlideHideView(ContentView target)
        {
            ContentViewAnotherPage.Children.Clear();
            ContentViewAnotherPage.Children.Add(target);
        }

        public async void BackToHome()
        {
            CreateDrawerContent();
            ContentViewAnotherPage.Children.Clear(); ContentViewHomePage vie = new ContentViewHomePage();
            ContentViewAnotherPage.Children.Add(vie);
            vie.InitData();
        }
        public async void ToSendView()
        {
            await ShowLoadingPage();
            SlideHideView(new Splitbill.Views.Send.SendSplitbillView());
            await HideLoadingPage();
        }
        public void ToRequestView()
        {
            if (requestPage == null) { requestPage = new Views.Request.Request(); }
            requestPage.ResetForm();
            SlideHideView(requestPage);   
        }
        int ctr = 0;
        public async void ShowAlert(String message)
        {
            await DisplayAlert("",message,"ok");
        }
        public async void ToNotificationOpenRequest(String Id,String Origin)
        {
            if (notificationPage == null) { notificationPage = new MyNotification(); }
            SlideHideView(notificationPage);
            notificationPage.InitData();
            notificationPage.OpenRequest(Id,Origin);
        }
        public async void ToNotificationOpenSend(String ID,String Origin)
        {
            if (notificationPage == null) { notificationPage = new MyNotification(); }
            SlideHideView(notificationPage);
            notificationPage.InitData();
            notificationPage.OpenSend(ID, Origin);
        }
        public async void ToNotificationOpenRequestPaid(String ID, String Origin)
        {
            if (notificationPage == null) { notificationPage = new MyNotification(); }
            SlideHideView(notificationPage);
            notificationPage.InitData();
            notificationPage.OpenRequestPaid(ID, Origin);
        }
        public async void ToNotificationPage()
        {
            if (PopupNavigation.PopupStack.Count > 0)
            {
                await PopupNavigation.PopAsync();
            }
            SlideHideView(notificationPage);
            notificationPage.InitData();
        }
        public async Task ShowLoadingPage()
        { loading.TranslationX = 0; await Task.Delay(100); }
        public async Task HideLoadingPage()
        { loading.TranslationX = 1000; await Task.Delay(100); }
        int ButtonCount=0;
        protected override bool OnBackButtonPressed()
        {
            if (navigationDrawer.IsOpen)
            {
                navigationDrawer.ToggleDrawer();
                return true;
            }
            else
            {
                if (ButtonCount < 1)
                {
                   
                }
                else if (ButtonCount >= 1)
                {
                    Device.BeginInvokeOnMainThread(async () => {
                        var result = await DisplayAlert("Warning", "Are you sure you want to exit the app?", "Yes", "No");
                        if (result)
                        {
                            MessagingCenter.Send<String>("Yap", "Shutdown");

                        }
                    });
                }
                ButtonCount++;
                return true;
            }
        }
        bool TestHandleFunc()
        {
            ButtonCount = 0;
            return true;
        }
        public void LogOut()
        {
            LogoutClosed(new object(), EventArgs.Empty);
        }
     
    }
}