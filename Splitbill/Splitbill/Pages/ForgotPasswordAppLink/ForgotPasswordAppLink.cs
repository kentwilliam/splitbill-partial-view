﻿using Newtonsoft.Json;
using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill.Pages.ForgotPasswordAppLink
{
    public class ForgotPassword : ContentPage
    {
        Entry NewPassword;
        Entry ConfirmPassword;
        String _username;
        String _password;
        String _token;
        public ForgotPassword(String Token,String Username)
        {
            _username = Username;
            _token = Token;
            Grid ContentGrid = new Grid();
            ContentGrid.BackgroundColor = Color.FromRgb(250, 250, 252);

            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Image iLogo1 = new Image();
            iLogo1.Source = "LogoUrunan.png";
            iLogo1.Aspect = Aspect.AspectFit;
            iLogo1.HorizontalOptions = LayoutOptions.Center;
            iLogo1.VerticalOptions = LayoutOptions.Center;
            ContentGrid.Children.Add(iLogo1, 0, 1);
            Grid.SetRowSpan(iLogo1, 3);

            //forgot your password
            StackLayout ForgotStack = new StackLayout();
            ForgotStack.Orientation = StackOrientation.Vertical;
            ForgotStack.HorizontalOptions = LayoutOptions.Center;
            ForgotStack.VerticalOptions = LayoutOptions.Center;

            Label lForgot = new Label();
            lForgot.FontSize = 18;
            lForgot.FontAttributes = FontAttributes.Bold;
            lForgot.Text = "Forgot your Password?";
            lForgot.TextColor = Color.Black;
            lForgot.HorizontalTextAlignment = TextAlignment.Center;
            lForgot.VerticalTextAlignment = TextAlignment.Center;
            ForgotStack.Children.Add(lForgot);

            //forgot your password
            Label lKet = new Label();
            lKet.FontSize = 12;
            lKet.FontAttributes = FontAttributes.None;
            lKet.Text = "Enter your e-mail below to reset your password";
            lKet.TextColor = Color.Black;
            lKet.HorizontalTextAlignment = TextAlignment.Center;
            lKet.VerticalTextAlignment = TextAlignment.Center;
            ForgotStack.Children.Add(lKet);
            ContentGrid.Children.Add(ForgotStack, 0, 4);

            //email
            StackLayout Border = new StackLayout();
            Border.Orientation = StackOrientation.Horizontal;
            Border.HorizontalOptions = LayoutOptions.Center;
            Border.VerticalOptions = LayoutOptions.Center;
            Border.BackgroundColor = Color.Gray;
            Border.Padding = 1;

            StackLayout EmailStack = new StackLayout();
            EmailStack.Orientation = StackOrientation.Horizontal;
            EmailStack.HorizontalOptions = LayoutOptions.Center;
            EmailStack.VerticalOptions = LayoutOptions.Center;
            EmailStack.BackgroundColor = Color.FromRgb(241, 242, 247);

            Image iEmail = new Image();
            iEmail.BackgroundColor = Color.FromRgb(57, 169, 251);
            iEmail.Source = "MailOpen.png";
            iEmail.Aspect = Aspect.AspectFit;
            EmailStack.Children.Add(iEmail);

            NewPassword = new Entry();
            NewPassword.Placeholder = "New Password";
            NewPassword.WidthRequest = 250;
            NewPassword.BackgroundColor = Color.FromRgb(250, 250, 252);
            EmailStack.Children.Add(NewPassword);
            Border.Children.Add(EmailStack);
            ContentGrid.Children.Add(Border, 0, 6);

            StackLayout Border1 = new StackLayout();
            Border1.Orientation = StackOrientation.Horizontal;
            Border1.HorizontalOptions = LayoutOptions.Center;
            Border1.VerticalOptions = LayoutOptions.Center;
            Border1.BackgroundColor = Color.Gray;
            Border1.Padding = 1;

            StackLayout EmailStack1 = new StackLayout();
            EmailStack1.Orientation = StackOrientation.Horizontal;
            EmailStack1.HorizontalOptions = LayoutOptions.Center;
            EmailStack1.VerticalOptions = LayoutOptions.Center;
            EmailStack1.BackgroundColor = Color.FromRgb(241, 242, 247);

            Image iEmail1 = new Image();
            iEmail1.BackgroundColor = Color.FromRgb(57, 169, 251);
            iEmail1.Source = "MailOpen.png";
            iEmail1.Aspect = Aspect.AspectFit;
            EmailStack1.Children.Add(iEmail1);

            ConfirmPassword = new Entry();
            ConfirmPassword.Placeholder = "ConfirmPassword";
            ConfirmPassword.WidthRequest = 250;
            ConfirmPassword.BackgroundColor = Color.FromRgb(250, 250, 252);
            EmailStack1.Children.Add(ConfirmPassword);
            Border1.Children.Add(EmailStack1);
            ContentGrid.Children.Add(Border1, 0,7);

            //create account
            StackLayout CommandStack = new StackLayout();
            CommandStack.Orientation = StackOrientation.Horizontal;
            CommandStack.HorizontalOptions = LayoutOptions.Center;
            CommandStack.VerticalOptions = LayoutOptions.Center;

            Button bSubmit = new Button();
            bSubmit.Text = "Submit";
            bSubmit.BackgroundColor = Color.FromRgb(57, 169, 251);
            bSubmit.TextColor = Color.White;
            bSubmit.WidthRequest = 175;
            bSubmit.Clicked += BSubmit_Clicked;
            CommandStack.Children.Add(bSubmit);
            ContentGrid.Children.Add(CommandStack,0,8);


            //footer
            Grid FooterGrid = new Grid();
            FooterGrid.BackgroundColor = Color.FromRgb(241, 242, 247);
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            //help
            StackLayout HelpStack = new StackLayout();
            HelpStack.Orientation = StackOrientation.Horizontal;
            HelpStack.HorizontalOptions = LayoutOptions.End;
            HelpStack.VerticalOptions = LayoutOptions.Center;
            HelpStack.Margin = new Thickness(0, 0, 10, 10);

            Image iHelp = new Image();
            iHelp.Source = "Help.png";
            iHelp.Aspect = Aspect.AspectFit;
            HelpStack.Children.Add(iHelp);
            ContentGrid.Children.Add(FooterGrid, 0, 11);

            Content = ContentGrid;

        }

        private void BSubmit_Clicked(object sender, EventArgs e)
        {
            if (NewPassword.Text == ConfirmPassword.Text)
            {
                Splitbill.Api.Encode.ForgetPasswordJsonApplink js = new Api.Encode.ForgetPasswordJsonApplink()
                {
                    password = NewPassword.Text,
                    token = _token,
                    username = _username
                };
                var resp = App._Api.GetDataResponseFromServer("user/reset_password", JsonConvert.SerializeObject(js));
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                DisplayAlert("", resu.message, "ok"); 
                
            }
            else
            {
                DisplayAlert("Incorrect Password","Password is not the same","Ok");
            }
        }
    }
}