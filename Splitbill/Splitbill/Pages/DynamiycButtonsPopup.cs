﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Pages
{
    public class DynamiycButtonsPopup:PopupPage
    {
        StackLayout Buttons;
        public DynamiycButtonsPopup()
        {
            Buttons = new StackLayout() {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 250,
                BackgroundColor = Color.White,
                IsClippedToBounds = true
            };
            Frame ButtonsFrame = new Frame() {
                Margin = 0,Padding = 0,
                OutlineColor = Color.CornflowerBlue,
                BackgroundColor = Color.Transparent,
                Content = Buttons,
                IsClippedToBounds = true
            };
            Content = ButtonsFrame;
        }
        public void AddButton(Button buttonToAdd)
        {
            Buttons.Children.Add(buttonToAdd);
        }
    }
}
