﻿using CustomLayouts;
using CustomLayouts.Controls;
using Splitbill.Pages.Login;
using Splitbill.Views;
using SplitBill.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Pages
{
    public class IntroPage:ContentPage
    {
        Button toHome;
        public IntroPage()
        {
            toHome = new Button() {
                IsVisible = true,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BorderRadius = 10,
                Text = "Get Started",
                TextColor = Color.White,
                BackgroundColor = Color.CornflowerBlue,
            };
            toHome.Clicked += ToHome_Clicked;
            Grid mygrid = new Grid();
            //mygrid.BackgroundColor = Color.FromRgb(73, 127, 165);
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            mygrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(75) });
            mygrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            mygrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(75) });

            RelativeLayout relativeLayout = null;
            Model.CarouselModel model = new Model.CarouselModel();
            CarouselLayout carousel = null;
            Xamarin.Forms.View dots = null;

            relativeLayout = new RelativeLayout();
            //relativeLayout.BackgroundColor = Color.FromRgb(57, 169, 251);
            BindingContext = model;

            carousel = CreatePagesCarousel();
            dots = CreatePagerIndicatorContainer();

            //relativeLayout.BackgroundColor = Color.FromRgb(73, 127, 165);
            relativeLayout.Children.Add(carousel,
                        Constraint.RelativeToParent((parent) => { return parent.X; }),
                        Constraint.RelativeToParent((parent) => { return parent.Y; }),
                        Constraint.RelativeToParent((parent) => { return parent.Width; }),
                        Constraint.RelativeToParent((parent) => { return parent.Height/1.2; })
                    );

            relativeLayout.Children.Add(dots,
                        Constraint.Constant(0),
                        Constraint.RelativeToView(carousel,
                            (parent, sibling) => { return sibling.Height - 40; }),
                        Constraint.RelativeToParent(parent => parent.Width),
                        Constraint.Constant(40)
            );

            Label LoginText = new Label() {
                Text = "Sign in",
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(0,0,10,0)
            };
            LoginText.GestureRecognizers.Add(new TapGestureRecognizer(LoginTab));

            Image background = new Image()
            {
                Source = "background.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Aspect = Aspect.AspectFill
            };
            mygrid.Children.Add(background,0,0);
            mygrid.Children.Add(relativeLayout, 0, 1);
            mygrid.Children.Add(LoginText, 2, 0);
            mygrid.Children.Add(toHome,1,10);
            Grid.SetRowSpan(relativeLayout,10);
            Grid.SetColumnSpan(relativeLayout, 3);
            Grid.SetRowSpan(background,12);
            Grid.SetColumnSpan(background,3);
            Content = mygrid;
        }

        private async void ToHome_Clicked(object sender, EventArgs e)
        {
            (sender as Button).IsEnabled = false;
            await Navigation.PushModalAsync(new Splitbill.Pages.RegisterPage());
            (sender as Button).IsEnabled = true;
        }

        private void LoginTab(View arg1, object arg2)
        {
            App.Current.MainPage = new LoginPage();
        }
        public CarouselLayout CreatePagesCarousel()
        {
            CarouselLayout carousel = new CarouselLayout();
            carousel.HorizontalOptions = LayoutOptions.FillAndExpand;
            carousel.VerticalOptions = LayoutOptions.FillAndExpand;
            carousel.ItemTemplate = new DataTemplate(typeof(ViewCarousel));
            carousel.SetBinding(CarouselLayout.ItemsSourceProperty, "Pages");
            carousel.SetBinding(CarouselLayout.SelectedItemProperty, "CurrentPage", BindingMode.TwoWay);

            return carousel;
        }

        public Xamarin.Forms.View CreatePagerIndicatorContainer()
        {
            return new StackLayout
            {
                Children = { CreatePagerIndicators() }
            };
        }

        public Xamarin.Forms.View CreatePagerIndicators()
        {
            var pagerIndicator = new PagerIndicatorDots() { DotSize = 10, DotColor = Color.White };
            pagerIndicator.SetBinding(PagerIndicatorDots.ItemsSourceProperty, "Pages");
            pagerIndicator.SetBinding(PagerIndicatorDots.SelectedItemProperty, "CurrentPage");
            return pagerIndicator;
        }


    }
}
