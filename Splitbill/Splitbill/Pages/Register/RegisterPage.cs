﻿using Newtonsoft.Json;
using Splitbill.Api.Core;
using Splitbill.Api.Encode;
using Splitbill.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill.Pages
{
    public class RegisterPage : ContentPage
    {
        ExtendedEntry ePhone;
        ExtendedEntry eUsername;
        ExtendedEntry ePassword;
        ExtendedEntry eEmail;
        public RegisterPage()
        {
            ePhone = new ExtendedEntry();eUsername = new ExtendedEntry();ePassword = new ExtendedEntry();eEmail = new ExtendedEntry();
            ePhone.Text = "";eUsername.Text = "";ePassword.Text = "";eEmail.Text = "";
            Grid ContentGrid = new Grid();
            ContentGrid.BackgroundColor = Color.FromRgb(250, 250, 252);

            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Image iLogo1 = new Image();
            iLogo1.Source = "LogoUrunan.png";
            iLogo1.Aspect = Aspect.AspectFit;
            iLogo1.HorizontalOptions = LayoutOptions.Center;
            iLogo1.VerticalOptions = LayoutOptions.Center;
            ContentGrid.Children.Add(iLogo1, 0, 1);
            Grid.SetRowSpan(iLogo1, 3);

            CreateUsename(ContentGrid);
            CreatePassword(ContentGrid);
            CreateEmail(ContentGrid);
            CreatePhone(ContentGrid);
            CreateAccount(ContentGrid);
            CreateFooter(ContentGrid);

            Content = ContentGrid;
        }

        private void CreateAccount(Grid ContentGrid)
        {
            //create account
            Button bCreateAccount = new Button();
            bCreateAccount.Text = "Create Account";
            bCreateAccount.FontSize = App.DpiFontSize * 6;
            bCreateAccount.BackgroundColor = Color.FromRgb(57, 169, 251);
            bCreateAccount.TextColor = Color.White;
            bCreateAccount.HorizontalOptions = LayoutOptions.Center;
            bCreateAccount.VerticalOptions = LayoutOptions.Center;
            bCreateAccount.WidthRequest = 300;
            bCreateAccount.Clicked += BCreateAccount_Clicked;
            ContentGrid.Children.Add(bCreateAccount, 0, 8);
        }
        private void CloseModal(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
        private async void BCreateAccount_Clicked(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(ePhone.Text))
            { await DisplayAlert("", "Phone Empty", "Ok"); }
            else if (String.IsNullOrEmpty(eUsername.Text))
            { await DisplayAlert("", "Username Empty", "Ok"); }
            else if (String.IsNullOrEmpty(eEmail.Text))
            { await DisplayAlert("", "Email Empty", "Ok"); }
            else if (String.IsNullOrEmpty(ePassword.Text))
            { await DisplayAlert("", "Password Empty", "Ok"); }
            else
            {
                SignUpJson data = new SignUpJson() { email = eEmail.Text, password = ePassword.Text, phone_number = ePhone.Text, username = eUsername.Text };
                try
                {
                    var resp = App._Api.GetDataResponseFromServer("user/sign_up", JsonConvert.SerializeObject(data));
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    if (resu.status == "true")
                    {
                        Splitbill.Pages.OTP.OTPPage otp = new OTP.OTPPage(data);
                        await Navigation.PushModalAsync(otp);
                    }
                    await DisplayAlert("", resu.message, "ok");
                }
                catch (Exception ex)
                {
                    await DisplayAlert("", "No Internet", "ok");
                }
            }
        }

        private void CreateFooter(Grid ContentGrid)
        {
            //footer
            Grid FooterGrid = new Grid();
            FooterGrid.BackgroundColor = Color.FromRgb(250, 250, 252);
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            FooterGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            

            StackLayout LoginStack = new StackLayout();
            LoginStack.Orientation = StackOrientation.Horizontal;
            LoginStack.HorizontalOptions = LayoutOptions.Start;
            LoginStack.VerticalOptions = LayoutOptions.Center;
            LoginStack.Margin = new Thickness(10, 0, 0, 10);
            LoginStack.IsClippedToBounds = true;

            Image iLogin = new Image();
            iLogin.Source = "Friends.png";
            iLogin.Aspect = Aspect.AspectFit;
            iLogin.WidthRequest = App.DpiFontSize * 15;
            iLogin.HeightRequest = App.DpiFontSize * 15;
            iLogin.HorizontalOptions = LayoutOptions.Center;
            LoginStack.Children.Add(iLogin);
            Label lLogin = new Label();
            lLogin.Text = "Sign In";
            lLogin.FontSize = App.DpiFontSize * 6;
            lLogin.TextColor = Color.Black;
            lLogin.VerticalOptions = LayoutOptions.Center;
            LoginStack.Children.Add(lLogin);
            FooterGrid.Children.Add(LoginStack, 0, 0);

            LoginStack.GestureRecognizers.Add(new TapGestureRecognizer(ClickLogin));

            //help
            //StackLayout HelpStack = new StackLayout();
            //HelpStack.Orientation = StackOrientation.Horizontal;
            //HelpStack.HorizontalOptions = LayoutOptions.End;
            //HelpStack.VerticalOptions = LayoutOptions.Center;
            //HelpStack.Margin = new Thickness(0, 0, 10, 10);

            //Image iHelp = new Image();
            //iHelp.Source = "Help.png";
            //iHelp.Aspect = Aspect.AspectFit;
            //HelpStack.Children.Add(iHelp);

            //Label lHelp = new Label();
            //lHelp.Text = "Help";
            //lHelp.TextColor = Color.Black;
            //lHelp.VerticalOptions = LayoutOptions.Center;
            //HelpStack.Children.Add(lHelp);
            //FooterGrid.Children.Add(HelpStack, 1, 0);

            ContentGrid.Children.Add(FooterGrid, 0, 11);
        }

        private void CreatePhone(Grid ContentGrid)
        {

            //Phone
            Frame Border = new Frame();
            Border.Margin = new Thickness(0);
            Border.Padding = new Thickness(0);
            Border.HorizontalOptions = LayoutOptions.Center;
            Border.VerticalOptions = LayoutOptions.Center;
            Border.IsClippedToBounds = true;

            StackLayout PhoneStack = new StackLayout();
            PhoneStack.Orientation = StackOrientation.Horizontal;
            PhoneStack.HorizontalOptions = LayoutOptions.Center;
            PhoneStack.VerticalOptions = LayoutOptions.Center;
            PhoneStack.BackgroundColor = Color.FromRgb(250, 250, 252);
            PhoneStack.IsClippedToBounds = true;

            Image iPhone = new Image();
            iPhone.BackgroundColor = Color.FromRgb(57, 169, 251);
            iPhone.Source = "Phone.png";
            iPhone.Aspect = Aspect.AspectFit;
            iPhone.WidthRequest = App.DpiFontSize * 15;
            iPhone.HeightRequest = App.DpiFontSize * 15;
            iPhone.HorizontalOptions = LayoutOptions.Center;
            PhoneStack.Children.Add(iPhone);

            ePhone.Placeholder = "Phone Number";
            ePhone.WidthRequest = 240;
            ePhone.FontSize = App.DpiFontSize * 6;
            ePhone.BackgroundColor = Color.FromRgb(250, 250, 252);
            PhoneStack.Children.Add(ePhone);
            Border.Content = PhoneStack;
            ContentGrid.Children.Add(Border, 0, 7);
        }

        private void CreateUsename(Grid ContentGrid)
        {
            //username
            Frame UserStackBorder = new Frame();
            UserStackBorder.Margin = new Thickness(0);
            UserStackBorder.Padding = new Thickness(0);
            UserStackBorder.HorizontalOptions = LayoutOptions.Center;
            UserStackBorder.VerticalOptions = LayoutOptions.Center;
            UserStackBorder.BackgroundColor = Color.Gray;
            UserStackBorder.IsClippedToBounds = true;

            StackLayout UserStack = new StackLayout();
            UserStack.Orientation = StackOrientation.Horizontal;
            UserStack.HorizontalOptions = LayoutOptions.Center;
            UserStack.VerticalOptions = LayoutOptions.Center;
            UserStack.BackgroundColor = Color.FromRgb(250, 250, 252);
            UserStack.IsClippedToBounds = true;

            Image iLogoUser = new Image();
            iLogoUser.BackgroundColor = Color.FromRgb(57, 169, 251);
            iLogoUser.Source = "Username.png";
            iLogoUser.Aspect = Aspect.AspectFit;
            iLogoUser.HeightRequest = App.DpiFontSize * 15;
            iLogoUser.WidthRequest = App.DpiFontSize * 15 ;
            iLogoUser.HorizontalOptions = LayoutOptions.Center;
            UserStack.Children.Add(iLogoUser);


            eUsername= new ExtendedEntry();
            eUsername.Placeholder = "Username";
            eUsername.WidthRequest = 240;
            eUsername.FontSize = App.DpiFontSize * 6;
            eUsername.BackgroundColor = Color.FromRgb(250, 250, 252);
            UserStack.Children.Add(eUsername);
            UserStackBorder.Content = UserStack;
            ContentGrid.Children.Add(UserStackBorder, 0, 4);
        }

        private void CreatePassword(Grid ContentGrid)
        {
            //password
            Frame Border = new Frame();
            Border.Margin = 0;
            Border.Padding = 0;
            Border.HorizontalOptions = LayoutOptions.Center;
            Border.VerticalOptions = LayoutOptions.Center;
            Border.IsClippedToBounds = true;
            
            StackLayout PassStack = new StackLayout();
            PassStack.Orientation = StackOrientation.Horizontal;
            PassStack.HorizontalOptions = LayoutOptions.Center;
            PassStack.VerticalOptions = LayoutOptions.Center;
            PassStack.BackgroundColor = Color.FromRgb(250, 250, 252);
            PassStack.IsClippedToBounds = true;

            Image iLogoPass = new Image();
            iLogoPass.BackgroundColor = Color.FromRgb(57, 169, 251);
            iLogoPass.Source = "Password.png";
            iLogoPass.Aspect = Aspect.AspectFit;
            iLogoPass.WidthRequest = App.DpiFontSize * 15;
            iLogoPass.HeightRequest = App.DpiFontSize * 15;
            iLogoPass.HorizontalOptions = LayoutOptions.Center;
            PassStack.Children.Add(iLogoPass);

            ePassword.Placeholder = "Password";
            ePassword.IsPassword = true;
            ePassword.WidthRequest = 240;
            ePassword.FontSize = App.DpiFontSize * 6;

            ePassword.BackgroundColor = Color.FromRgb(250, 250, 252);
            PassStack.Children.Add(ePassword);
            Border.Content = PassStack;
            ContentGrid.Children.Add(Border, 0, 5);
        }

        private void CreateEmail(Grid ContentGrid)
        {
            //email
            Frame Border = new Frame();
            Border.Margin = 0;
            Border.HorizontalOptions = LayoutOptions.Center;
            Border.VerticalOptions = LayoutOptions.Center;
            Border.Padding = 0;
            Border.IsClippedToBounds = true;

            StackLayout EmailStack = new StackLayout();
            EmailStack.Orientation = StackOrientation.Horizontal;
            EmailStack.HorizontalOptions = LayoutOptions.Center;
            EmailStack.VerticalOptions = LayoutOptions.Center;
            EmailStack.BackgroundColor = Color.FromRgb(250, 250, 252);
            EmailStack.IsClippedToBounds = true;

            Image iEmail = new Image();
            iEmail.BackgroundColor = Color.FromRgb(57, 169, 251);
            iEmail.Source = "Mail.png";
            iEmail.Aspect = Aspect.AspectFit;
            iEmail.WidthRequest = App.DpiFontSize * 15;
            iEmail.HeightRequest = App.DpiFontSize * 15;
            iEmail.HorizontalOptions = LayoutOptions.Center;
            EmailStack.Children.Add(iEmail);

            
            eEmail.Placeholder = "Email Address";
            eEmail.WidthRequest = 240;
            eEmail.HeightRequest = 50;
            eEmail.FontSize = App.DpiFontSize * 6;
            eEmail.BackgroundColor = Color.FromRgb(250, 250, 252);

            EmailStack.Children.Add(eEmail);
            Border.Content = EmailStack;
            ContentGrid.Children.Add(Border, 0, 6);
        }

        private async void ClickLogin(Xamarin.Forms.View arg1, object arg2)
        {
            await Navigation.PopModalAsync();
        }
    }
}