﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SplitBill.DependencyServiceMedia
{
    public interface IMediaService
    {
        Task OpenGallery();
        void ClearFiles(List<string> filePaths);
    }
}
