﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class TransactionDetailJsonResponse_request:ErrorMessage
    {
        public MoneyRequestJson_Request request { get; set; }
        public List<MoneyRequestJson_Menu_assign> menu_assign { get; set; }
        public String notification { get; set; }
    }

    public class TransactionDetailJsonResponse_send:ErrorMessage
    {
        public List<MoneyRequestJson_Send> send { get; set; }
        public String notification { get; set; }
    }

    public class MoneyRequestJson_Send
    {
        public String idMoney_send { get; set; }
        public String sender_id { get; set; }
        public String reciever_id { get; set; }
        public String amount { get; set; }
        public String description { get; set; }
        public String bill_photo { get; set; }
        public String lat { get; set; }
        public String lng { get; set; }
        public String place_id { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String before { get; set; }
        public String bold { get; set; }
        public String after { get; set; }
        public int posisi { get; set; }
    }
    public class MoneyRequestJson_Request
    {
        public String idMoney_request_detail { get; set; }
        public String money_request_id { get; set; }
        public String reciever_id { get; set; }
        public Double amount_after { get; set; }
        public String id_log_history_pay { get; set; }
        public String amount { get; set; }
        public String status { get; set; }
        public String paid_date { get; set; }
        public String reminder_counter { get; set; }
        public String id_log_history_request { get; set; }
        public String requester_id { get; set; }
        public String request_date { get; set; }
        public String description { get; set; }
        public String bill_photo { get; set; }
        public String lat { get; set; }
        public String lng { get; set; }
        public String place_id { get; set; }
        public int discount { get; set; }
        public int tax { get; set; }
        public int service { get; set; }
        public String scheduled_paid_date { get; set; }
        public String scheduled_desc { get; set; }
        public double total_bill { get; set; }
    }
    public class MoneyRequestJson_Menu_assign
    {
        public String reciever_id { get; set; }
        public String name { get; set; }
        public double amount { get; set; }
        public double amount_user { get; set; }
        public String photo_menu { get; set; }
    }
}
