﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class SuggestFriendJsonResponse:ErrorMessage
    {
        public List<String> contact { get; set; }
        public List<String> photo { get; set; }
    }
    
}
