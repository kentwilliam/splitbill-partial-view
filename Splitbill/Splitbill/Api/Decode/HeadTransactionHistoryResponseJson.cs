﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class TransactionHistoryResponseJson
    {
        public List<HeadTransactionHistoryResponseJson> owe;
        public List<HeadTransactionHistoryResponseJson> lend;
    }
    public class HeadTransactionHistoryResponseJson
    {
        public String username;
        public String photo;
        public int unpaid_amount;
        public int paid_amount;
        public int total_amount;
        public List<DetailTransactionHistoryResponseJson> data;
    }
    public class DetailTransactionHistoryResponseJson
    {
        public String idMoney_request_detail;
        public int status;
        public int amount_after;
        public String idMoney_request;
        public String date;
    }
}
