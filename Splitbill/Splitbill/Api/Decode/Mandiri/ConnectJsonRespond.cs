﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode.Mandiri
{
    public class ConnectJsonRespond:ErrorMessage
    {
        public String ticket { get; set; }
        public String mobileno { get; set; }
    }
}
