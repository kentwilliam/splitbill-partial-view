﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode.Mandiri
{
    public class MoneyMandiriJsonResponse:ErrorMessage
    {
        public double balance { get; set; }
        public String notification { get; set; }
    }
}
