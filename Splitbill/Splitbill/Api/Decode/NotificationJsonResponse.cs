﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class NotificationJsonResponse:ErrorMessage
    {
        public List<notificationJsonDetailResponse> request { get; set; }
        public String notification { get; set; }
    }
    public class notificationJsonDetailResponse
    {
        public String idNotification { get; set; }
        public String user_id { get; set; }
        public String content { get; set; }
        public String origin { get; set; }
        public String origin_id { get; set; }
        public String hashtag { get; set; }
        public String sender_id { get; set; }
        public Double? amount { get; set; }
        public int status { get; set; }
        public String read_flag { get; set; }
        public String photo { get; set; }
    }
}
