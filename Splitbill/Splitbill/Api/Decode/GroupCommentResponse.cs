﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class HeadGroupCommentResponse : ErrorMessage
    {
        public List<GroupCommentResponse> comment { get; set; }
        public List<String> like { get; set; }
    }

    public class GroupCommentResponse
    {
        public String id { get; set; }
        public String user_id { get; set; }
        public String photo_id { get; set; }
        public String comment { get; set; }
        public String time { get; set; }
        public String commenter_photo { get; set; }
    }
}
