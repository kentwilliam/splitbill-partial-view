﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class GetGroupsJsonRespond:ErrorMessage
    {
        public List<GetGroupsDetailJsonResonse> group { get; set; }
    }
    public class GetGroupsDetailJsonResonse
    {
        public String idGroup { get; set; }
        public String creator_id { get; set; }
        public String create_date { get; set; }
        public String name { get; set; }
        public String lat { get; set; }
        public String lng { get; set; }
        public String member_username { get; set; }
        public String member_photo { get; set; }
        public String cover_photo { get; set; }
    }
}
