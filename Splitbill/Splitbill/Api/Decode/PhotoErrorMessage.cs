﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class PhotoErrorMessage
    {
        public string status { get; set; }
        public string Token { get; set; }
        public string filename { get; set; }
        public string message { get; set; }
    }
}
