﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Core
{
    public class ErrorMessage
    {
        public String status { get; set; }
        public String message { get; set; }
    }
}
