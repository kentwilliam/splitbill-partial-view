﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class HeadViewJsonResponse:ErrorMessage {
        public ViewGroupJsonResponse group { get; set; }
    }
    public class ViewGroupJsonResponse
    {
        public String idGroup { get; set; }
        public String creator_id { get; set; }
        public String create_date { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String cover_photo { get; set; }
        public List<photoDetail> photo { get; set; }
        public String member_username { get; set; }
        public String date { get; set; }
    }
    public class photoDetail
    {        
        public String elapsed_time { get; set; }
        public String uploader { get; set; }
        public String uploader_photo { get; set; }
        public List<String> filename { get; set; }
        public List<String> photo_id { get; set; }
    }
}
