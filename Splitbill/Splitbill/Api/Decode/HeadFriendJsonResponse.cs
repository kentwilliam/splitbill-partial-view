﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class HeadFriendsJsonResponse : ErrorMessage
    {
        public List<FriendsJsonResponse> friends { get; set; }
    }
    public class FriendsJsonResponse
    {
        public String id { get; set; }
        public String id_teman { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String email { get; set; }
        public String phone_number { get; set; }
        public String photo { get; set; }
    }
}
