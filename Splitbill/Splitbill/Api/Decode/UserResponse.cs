﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class UserResponse : ErrorMessage
    {
        public List<UserDetailResponse> user { get; set; }
        public String token { get; set; }
        public String notification { get; set; }
    }
    public class UserDetailResponse
    {
        public String username { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String password { get; set; }
        public int balance { get; set; }
        public String email { get; set; }
        public String phone_number { get; set; }
        public String photo { get; set; }
        public String device { get; set; }
        public String platform { get; set; }
        public String device_id { get; set; }
    }
}
