﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class TransactionHistoryJsonResponse:ErrorMessage
    {
       public TransactionHistoryLog log { get; set; }
    }
    public class TransactionHistoryLog
    {
        public List<TransactionHistoryComplete> complete { get; set; }
        public List<TransactionHistoryPending> pending { get; set; }
        public List<TransactionHistorySend_Money> send_money { get; set; }
    }
    public class TransactionHistoryComplete
    {
        public string id { get; set; }
        public string time { get; set; }
        public string before { get; set; }
        public string bold { get; set; }
        public string after { get; set; }
        public string id_teman { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string place_id { get; set; }
        public int posisi { get; set; }
        public int amount { get; set; }
        public string photo { get; set; }
        public String description { get; set; }
        public String id_parent { get; set; }
    }
    public class TransactionHistoryPending
    {
        public string id { get; set; }
        public string time { get; set; }
        public string before { get; set; }
        public string bold { get; set; }
        public string after { get; set; }
        public string id_teman { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string place_id { get; set; }
        public double amount { get; set; }
        public int posisi { get; set; } 
        public string photo { get; set; }
        public String id_parent { get; set; }
    }
    public class TransactionHistorySend_Money
    {
        public string id { get; set; }
        public string time { get; set; }
        public string before { get; set; }
        public string bold { get; set; }
        public string after { get; set; }
        public string id_teman { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string place_id { get; set; }
        public int posisi { get; set; }
        public int amount { get; set; }
        public string title { get; set; }
        public string photo { get; set; }
        public String description { get; set; }
        public String id_parent { get; set; }
    }
}
