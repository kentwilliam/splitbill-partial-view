﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Decode
{
    public class FriendsActivityJsonResponse:ErrorMessage
    {
        public List<FriendsActivityDetailResponse> timeline { get; set; }
    }
    public class FriendsActivityDetailResponse
    {
        public String id { get; set; }
        public String id_teman { get; set; }
        public String time { get; set; }
        public String message { get; set; }
        public String id_stranger { get; set; }
        public String photo { get; set; }
        public String description { get; set; }
    }
}
