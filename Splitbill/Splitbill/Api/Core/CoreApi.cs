﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Splitbill.Api.Core
{
    public class CoreApi
    {
        String Base_url = "https://www.urunan.id/splitbill/";
        public async Task<String> GetDataResponseFromServer(String Action, String data_stream_out)
        {
            //Action Merupakan nama Controller/Fungsi Contoh: "user/log_in"
            String result_content = String.Empty;
            HttpResponseMessage result = null;

            try
            {
                Uri url = new Uri(Base_url);
                HttpClientHandler handler = new HttpClientHandler();
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = TimeSpan.FromSeconds(10);
                    client.BaseAddress = new Uri(Base_url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                    var content = new StringContent(data_stream_out, Encoding.UTF8, "application/json");
                    try
                    {
                        result = client.PostAsync(Action, content).Result;
                        var ss = result.StatusCode;
                        result_content = await result.Content.ReadAsStringAsync();

                    }
                    catch (Exception ex)
                    {
                        result_content = "No Internet";
                    }
                    

                    
                    return result_content;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public async Task<String> GetResponsePhotoApi(String Action, String data_stream_out)
        {
            //Action Merupakan nama Controller/Fungsi Contoh: "user/log_in"
            String result_content = String.Empty;
            HttpResponseMessage result = null;

            try
            {
                Uri url = new Uri(Base_url);
                HttpClientHandler handler = new HttpClientHandler();
                using (var client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(Base_url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                    var content = new StringContent(data_stream_out, Encoding.UTF8, "application/x-www-form-urlencoded");
                    result = client.PostAsync(Action, content).Result;

                    var ss = result.StatusCode;
                    result_content = await result.Content.ReadAsStringAsync();

                    return result_content;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        internal void GetDataResponseFromServer(string v, object jsonconvert)
        {
            throw new NotImplementedException();
        }

        public bool ProcessDataOrNot(String Message,bool DataFill)
        {
            bool process = false;
            if (!String.IsNullOrEmpty(Message))//jika ada message
            {
                if (Message.ToLower().Contains("expired"))
                {
                    process = false;
                }
                else
                {
                    process = true;
                }
            }
            else //jika tidak ada message
            {
                if (DataFill)//terdapat data
                { process = true; }
                else//tidak terdapat data
                { process = true; }
            }
            return process;
        }
    }
}
