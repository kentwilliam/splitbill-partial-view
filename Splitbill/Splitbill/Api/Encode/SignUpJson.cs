﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class SignUpJson
    {
        public String username { get; set; }
        public String phone_number { get; set; }
        public String email { get; set; }
        public String password { get; set; }
        public String otp { get; set; }
    }   
}
