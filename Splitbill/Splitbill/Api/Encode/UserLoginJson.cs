﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class UserLoginJson
    {
        public String phone_number { get; set; }
        public String password { get; set; }
    }
}
