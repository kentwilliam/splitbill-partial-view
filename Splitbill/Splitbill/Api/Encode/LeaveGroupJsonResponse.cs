﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class LeaveGroupJsonResponse:Username_Token
    {
        public String group_id { get; set; }
    }
}
