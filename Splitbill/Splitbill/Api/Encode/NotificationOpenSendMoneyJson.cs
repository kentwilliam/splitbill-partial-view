﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class NotificationOpenSendMoneyJson:Username_Token
    {
        public String id_detail { get; set; }
        public String OTP { get; set; }
        public Double total { get; set; }
    }
}
