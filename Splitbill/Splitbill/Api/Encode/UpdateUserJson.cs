﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class UpdateUserJson:Username_Token
    {
        public string email { get; set; }
        public string photo { get; set; }
        public string new_username { get; set; }
    }
}
