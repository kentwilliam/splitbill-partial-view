﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class SendNotificationDetailJson:Username_Token
    {
        public String origin { get; set; }
        public String origin_id { get; set; }
    }
}
