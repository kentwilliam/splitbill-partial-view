﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class UpdatePasswordJson:Username_Token
    {
        public string new_password { get; set; }
        public string old_password { get; set; }
    }
}
