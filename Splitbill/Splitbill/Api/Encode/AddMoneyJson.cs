﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class AddMoneyJson:Username_Token
    {
        public String amount { get; set; }
    }
}
