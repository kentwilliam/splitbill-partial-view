﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class SendPhotoGroupJson:Username_Token
    {
        public string string64 { get; set; }
        public string group_id { get; set; }
    }
}
