﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class NotificationOpenScheduleJson:Username_Token
    {
        public String scheduled_desc { get; set; }
        public String payment_date { get; set; }
        public String id_detail { get; set; }
        public String OTP { get; set; }
    }
}
