﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class CreateGroupJson:Username_Token
    {
        public String name { get; set; }
        public String lat { get; set; }
        public String lng { get; set; }
        public String place_id { get; set; }
        public List<String> friends { get; set; }
        public String photo { get; set; }
        public String group_id { get; set; }
        public String description { get; set; }
    }
}
