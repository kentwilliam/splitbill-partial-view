﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class ForgetPasswordJsonApplink:Username_Token
    {
        public String password { get; set; }
    }
}
