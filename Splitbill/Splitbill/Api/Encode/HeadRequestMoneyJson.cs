﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class HeadRequestMoneyJson
    {
        public HeadRequestMoneyJson()
        {
            MacamMenu = new List<DetailHeadRequestMoneyJson>();
        }
        public String photo { get; set; }
        public double total { get; set; }
        public String hashtag { get; set; }
        public double tax { get; set; }
        public double discount { get; set; }
        public double service { get; set; }
        public String username { get; set; }
        public String token { get; set; }
        public float lat { get; set; }
        public float lng { get; set; }
        public String place_id { get; set; }
        public String description { get; set; }
        public List<DetailHeadRequestMoneyJson> MacamMenu { get; set; }
        public String OTP { get; set; }
        public String totalAfterTax { get; set; }
    }
    public class DetailHeadRequestMoneyJson
    {
        public DetailHeadRequestMoneyJson()
        {
            Friends = new List<String>();
        }
        public List<String> Friends { get; set; }
        public String item { get; set; }
        public double harga { get; set; }
        public int qty { get; set; }
        public String photo { get; set; }
    }
}
