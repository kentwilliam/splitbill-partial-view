﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class GroupCommentPostJson : Username_Token
    {
        public String comment_id { get; set; }
        public String comment { get; set; }
        public String photo_id { get; set; }
    }
}
