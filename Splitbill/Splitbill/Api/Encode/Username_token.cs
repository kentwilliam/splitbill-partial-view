﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Core
{
    public class Username_Token
    {
        public String username { get; set; }
        public String token { get; set; }
    }
}
