﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class SendMoneyJson:Username_Token
    {
        public String friend { get; set; }
        public Double amount { get; set; }
        public String description { get; set; }
        public String lat { get; set; }
        public String lng { get; set; }
        public String place_id { get; set; }
        public String photo { get; set; }
        public String OTP { get; set; }
    }
}
