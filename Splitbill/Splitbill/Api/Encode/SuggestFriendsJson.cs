﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Api.Encode
{
    public class SuggestFriendsJson:Username_Token
    {
        public SuggestFriendsJson()
        {
            phone_number = new List<string>();
        }
        public List<String> phone_number { get; set; }
    }
}
