﻿using Splitbill.Model.TransactionHistory;
using Syncfusion.DataSource.Extensions;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Converter
{
    public class GroupSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            { return value; }

            GroupResult groupResult = value as GroupResult;
            SfListView sflist = parameter as SfListView;

            var items = new List<Splitbill.Model.RequestModel.GroupMember>(groupResult.Items.ToList<Splitbill.Model.RequestModel.GroupMember>());
            if (items.All(x => x.SELECTED == false))
            {
                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i];
                    (item as Splitbill.Model.RequestModel.GroupMember).SELECTED = false;
                    sflist.SelectedItems.Remove(item);
                }
                return ImageSource.FromFile("Empty.png");
            }
            else if (items.All(x => x.SELECTED == true))
            {
                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i];
                    (item as Splitbill.Model.RequestModel.GroupMember).SELECTED = true;
                    sflist.SelectedItems.Add(item);
                }
                return ImageSource.FromFile("Selected.png");
            }
            else
            {
                return ImageSource.FromFile("HalfEmpty.png");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class SelectionBoolToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return ImageSource.FromFile("Selected.png");
            }
            else
            {
                return ImageSource.FromFile("Empty.png");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class GroupHeaderPendingFormatedTextValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormattedString MessageReturns = new FormattedString();
            String idtransaction = value.ToString();
            var datasource = (parameter as SfListView).DataSource;
            GroupResult GR = new GroupResult();
            foreach (var x in datasource.Groups)
            {
                if (x.Key.ToString() == idtransaction)
                {
                    GR = x;
                    break;
                }
            }
            if (GR != null)
            {
                if (GR.Items != null)
                {
                    MessageReturns = new FormattedString();
                    var totalItems = GR.Items.ToList<Splitbill.Model.MyTransaction.Pending>();
                    var nama = totalItems.Select(x => x.NAMA).ToList();
                    String boldTempName = nama[0];//Kalau Kosong Error

                    int sisaTeman = nama.Where(x => x.ToLower() != boldTempName.ToLower()).Count();
                    MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.CornflowerBlue, Text = "You " });
                    MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, Text = " Hangout With " });
                    MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.CornflowerBlue, Text = boldTempName });
                    if (sisaTeman == 1)
                    {
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, Text = " and " });
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, ForegroundColor = Color.CornflowerBlue, Text = nama.Where(x => x.ToLower() != boldTempName.ToLower()).First().ToString() });
                    }
                    else if (sisaTeman > 1)
                    {
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, Text = " and " });
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.CornflowerBlue, Text = sisaTeman.ToString() + " Others" });
                    }
                }

            }
            return MessageReturns;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class GroupHeaderPendingTujuanValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormattedString MessageReturn = new FormattedString();
            String idtransaction = value.ToString();
            var datasource = (parameter as SfListView).DataSource;
            GroupResult GR = new GroupResult();
            foreach (var x in datasource.Groups)
            {
                if (x.Key.ToString() == idtransaction)
                {
                    GR = x;
                    break;
                }
            }
            if (GR != null)
            {
                try
                {
                    var destination = GR.Items.ToList<Model.MyTransaction.Pending>().FirstOrDefault();
                    if(destination !=null)
                    {
                        MessageReturn.Spans.Add(new Span() { Text =String.IsNullOrEmpty(destination.DESTINATION)?"":"At " });
                        MessageReturn.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, ForegroundColor = Color.CornflowerBlue, FontAttributes = FontAttributes.Bold, Text = String.IsNullOrEmpty(destination.DESTINATION) ? "-" : destination.DESTINATION });
                    }
                    
                }
                catch (Exception ex)
                { }
                
            }
            return MessageReturn;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GroupHeaderCompletedFormetedTextValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormattedString MessageReturns = new FormattedString();
            String idtransaction = value.ToString();
            var datasource = (parameter as SfListView).DataSource;
            GroupResult GR = new GroupResult();
            foreach (var x in datasource.Groups)
            {
                if (x.Key.ToString() == idtransaction)
                {
                    GR = x;
                    break;
                }
            }
            if (GR != null)
            {
                if (GR.Items != null)
                {
                    MessageReturns = new FormattedString();
                    var totalItems = GR.Items.ToList<Splitbill.Model.MyTransaction.Completed>();
                    var nama = totalItems.Select(x => x.NAMA).ToList();
                    String boldTempName = nama[0];//Kalau Kosong Error

                    int sisaTeman = nama.Where(x => x.ToLower() != boldTempName.ToLower()).Count();
                    MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.CornflowerBlue, Text = "You " });
                    MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, Text = " Hangout With " });
                    MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.CornflowerBlue, Text = boldTempName });
                    if (sisaTeman == 1)
                    {
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, Text = " and " });
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, ForegroundColor = Color.CornflowerBlue, Text = nama.Where(x => x.ToLower() != boldTempName.ToLower()).First().ToString() });
                    }
                    else if (sisaTeman > 1)
                    {
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, Text = " and " });
                        MessageReturns.Spans.Add(new Span() { FontSize = 7 * App.DpiFontSize, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.CornflowerBlue, Text = sisaTeman.ToString() + " Others" });
                    }
                }

            }
            return MessageReturns;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GroupHeaderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.Name == "Color")
            {
                if ((int)value == 1)
                    return Color.White;
                else
                    return Color.FromHex("#f1efef");
            }
            else
            {
                if ((int)value == 1)
                    return new Thickness(5, 5, 5, 0);
                else
                    return new Thickness((int)value * 15, 5, 5, 0);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LeftGroupHeaderTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormattedString fmt = new FormattedString();
            String[] data = (value + "").Split(',');
            if (targetType.Name == "Thickness")
            {
                if (data[0] == "TOPSTACK")
                {
                    return new Thickness(5, 0, 0, 0);
                }
                else
                {
                    return new Thickness(25, 0, 0, 0);
                }
            }
            else
            {
                if (data[0] == "TOPSTACK")
                {
                    fmt.Spans.Add(new Span()
                    {
                        Text = data[1],
                        FontAttributes = FontAttributes.Bold,
                        FontSize = 8 * App.DpiFontSize
                    });
                }
                else
                {
                    fmt.Spans.Add(new Span()
                    {
                        Text = "Total Money Spent",
                        FontSize = 7 * App.DpiFontSize
                    });
                    fmt.Spans.Add(new Span()
                    {
                        Text = "\nPaid",
                        FontSize = 7 * App.DpiFontSize
                    });
                    fmt.Spans.Add(new Span()
                    {
                        Text = "\nUnpaid",
                        FontSize = 7 * App.DpiFontSize,
                        
                    });
                }
            }
            return fmt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RightGroupHeaderTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormattedString fmt = new FormattedString();
            String[] data = (value + "").Split(',');
            if (data[0] == "TOPSTACK")
            {
                fmt.Spans.Add(new Span()
                {
                    Text = System.Convert.ToInt32(data[3]) == 0? "Completed": "Rp."+System.Convert.ToInt32(data[3]).ToString("n0"),
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 8 * App.DpiFontSize
                });
            }
            else
            {
                fmt.Spans.Add(new Span()
                {
                    Text = "Rp."+System.Convert.ToInt32(data[3]).ToString("n0"),
                    FontSize = 7 * App.DpiFontSize
                });
                fmt.Spans.Add(new Span()
                {
                    Text = "\nRp."+System.Convert.ToInt32(data[2]).ToString("n0"),
                    FontSize = 7 * App.DpiFontSize
                });
                fmt.Spans.Add(new Span()
                {
                    Text = "\nRp."+System.Convert.ToInt32(data[1]).ToString("n0"),
                    FontSize = 7 * App.DpiFontSize
                });
            }
            return fmt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class PhotoGrouHeaderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String[] hasil = (value + "").Split(',');
            if (targetType.Name == "Boolean")//ImageSource & Boolean
            {
                if (hasil[0].ToLower() == "bottomstack")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (hasil[hasil.Length - 1] == "https://www.urunan.id/splitbill/")
                {
                    return "user72.png";
                }
                else
                {
                    return hasil[hasil.Length - 1];
                }
                
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CommentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String Temp = value as string;
            String[] ArrayTemp = Temp.Split('#');

            FormattedString frm = new FormattedString();
            frm.Spans.Add(new Span() {Text = ArrayTemp[0],FontAttributes = FontAttributes.Bold,FontSize = 6 * App.DpiFontSize });
            frm.Spans.Add(new Span() { Text = " ", FontSize = 6 * App.DpiFontSize });
            frm.Spans.Add(new Span() { Text = ArrayTemp[1],FontSize = 6*App.DpiFontSize });
            return frm;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
