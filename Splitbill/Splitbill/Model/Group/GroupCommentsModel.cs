﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Model.Group
{
    public struct GroupCommentsModel
    {
        public String Id { get; set; }
        public String UserName { get; set; }
        public String UserPhoto { get; set; }
        public String Comment { get; set; }
        public String Time { get; set; }
    }
}
