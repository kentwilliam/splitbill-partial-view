﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Splitbill.Model.Group
{
    public class GroupDisplay: INotifyPropertyChanged
    {
        private String idGroup { get; set; }
        private String DisplayNamaGrup { get; set; }
        private String ActualNamaGrup { get; set; }
        private String Photo { get; set; }


        public String IDGROUP { get {return idGroup; } set {idGroup = value;OnPropertyChanged("IDGROUP"); } }
        public String DISPLAYNAMAGRUP { get { return DisplayNamaGrup; } set { DisplayNamaGrup = value; OnPropertyChanged("DISPLAYNAMAGRUP"); } }
        public String ACTUALNAMAGRUP { get { return ActualNamaGrup; } set { ActualNamaGrup = value; OnPropertyChanged("ACTUALNAMAGRUP"); } }
        public String PHOTO { get { return Photo; } set { Photo = value; OnPropertyChanged("PHOTO"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
