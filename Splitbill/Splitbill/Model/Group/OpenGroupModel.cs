﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Model.Group
{
    public class OpenGroupModel
    {
        public OpenGroupModel()
        {
            group = new  List<ListPhoto>();
        }
        public List<ListPhoto> group { get; set; }
    }
    public class ListPhoto
    {
        public ListPhoto()
        {
            NotActualFileName = new List<PhotoFile>();
            ActualFileName = new List<PhotoFile>();
        }
        private int counter { get; set; }
        public String UploaderName { get; set; }

        public String UserUploadPhoto { get; set; }
        public String Time { get; set; }
        public int CounterActual_NotActual { get { return counter; } set {
                counter = value; CounterActual_NotActualString = value + "+";
                ShowCounter = counter == 0 ? false : true;
            } }
        public String CounterActual_NotActualString { get; set; }
        public bool ShowCounter { get; set; }
        public List<PhotoFile> NotActualFileName { get; set; }
        public List<PhotoFile> ActualFileName { get; set; }
    }
    public class PhotoFile
    {
        public String PhotoId { get; set; }
        public String PhotoName { get; set; }
        public int index { get; set; }
    }
}
