﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Model.Notification
{
    public class NotificationOpenScheduleModel
    {
        public DateTime dateSetting { get; set; }
        public String description { get; set; }
        public String username { get; set; }
        public String token { get; set; }
        public String id_Detail { get; set; }
        public bool clicked { get; set; }
        public Color blueColor { get; set; }
        public bool ActuallySetSchedule { get; set; }
    }
}
