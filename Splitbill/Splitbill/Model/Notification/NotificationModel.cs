﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Splitbill.Model.Notification
{
    public class NotificationModel:INotifyPropertyChanged
    {
        private String IdNotification { get; set; }
        private String User_Id { get; set; }
        private bool Read_Flag { get; set; }
        private String Hashtag { get; set; }
        private String Content { get; set; }
        private String Sender_Id { get; set; }
        private String Origin { get; set; }
        private Double Amount { get; set; }
        private String Photo { get; set; }
        private String ShowNotification { get; set; }

        public String IDNOTIFICATION   { get{return IdNotification;}   set{IdNotification = value;OnPropertyChanged("IDNOTIFICATION");} }
        public String USER_ID          { get{return User_Id;}          set{User_Id=value;OnPropertyChanged("USER_ID");} }
        public bool   READ_FLAG        { get{return Read_Flag;}        set{Read_Flag=value;OnPropertyChanged("READ_FLAG");} }
        public String HASHTAG          { get{return Hashtag;}          set{Hashtag=value;OnPropertyChanged("HASHTAG");} }
        public String CONTENT          { get{return Content;}          set{Content=value;OnPropertyChanged("CONTENT");} }
        public String SENDER_ID        { get{return Sender_Id;}        set{Sender_Id=value;OnPropertyChanged("SENDER_ID");} }
        public String ORIGIN           { get{return Origin;}           set{Origin=value;OnPropertyChanged("ORIGIN");} }
        public Double AMOUNT           { get{return Amount;}           set{Amount=value;OnPropertyChanged("AMOUNT");} }
        public String PHOTO            { get{return Photo;}            set{Photo=value;OnPropertyChanged("PHOTO");} }
        public String SHOWNOTIFICATION { get{return ShowNotification;} set{ShowNotification=value;OnPropertyChanged("SHOWNOTIFICATION");}}

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
