﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Model.Notification
{
    public class NotificationOpenSfListModel
    {
        public String NAMAMAKANAN { get; set; }
        public String TOTALMENU { get; set; }
        public String photo { get; set; }
        public FormattedString FRIENDS { get; set; }
        public String MONEYTOPAY { get; set; }
    }
}
