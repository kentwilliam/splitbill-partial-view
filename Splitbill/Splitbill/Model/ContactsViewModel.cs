﻿using Splitbill.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Model
{
    public class ContactsViewModel : INotifyPropertyChanged
    {
        #region Properties

        public ObservableCollection<Contacts> contactsinfo { get; set; }

        #endregion

        #region Constructor

        public ContactsViewModel()
        {
            contactsinfo = new ObservableCollection<Contacts>();
            foreach (var cusName in CustomerNames)
            {
                var contact = new Contacts(cusName);
                if (cusName.IndexOf("BCA") > 0)
                { contact.GrupKey = "BCA.png";}
                if (cusName.IndexOf("Mandiri") > 0)
                { contact.GrupKey = "Mandiri.png"; }
                if (cusName.IndexOf("Permata") > 0)
                { contact.GrupKey = "Permata.png";}
                if (cusName.IndexOf("GBU") > 0)
                { contact.GrupKey = "GBU.png"; }
                contactsinfo.Add(contact);
            }
        }

        #endregion

        #region Fields

        string[] CustomerNames = new string[] {
            "ATM Mandiri"    ,
            "Klik Mandiri"    ,
            "M_Mandiri|Mandiri Mobile",
            "M_Mandiri (STK-SIM Tool Kit)",
        };

        #endregion

        #region Interface Member

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
