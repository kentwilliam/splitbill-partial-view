﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Model.MyTransaction
{
    public class Completed:INotifyPropertyChanged
    {
        private String IdTransaksi { get; set; }
        private String Nama { get; set; }
        private FormattedString Message { get; set; }
        private String Total { get; set; }
        private String Origin { get; set; }
        private String Photo { get; set; }
        private String time { get; set; }
        private String description { get; set; }
        private String GroupId { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        public String GROUPID { get { return GroupId; } set { GroupId = value;OnPropertyChanged("GROUPID"); } }
        public String DESCRIPTION { get { return description; } set { description = value;OnPropertyChanged("DESCRIPTION"); } }
        public String TIME { get { return time; } set { time = value; OnPropertyChanged("TIME"); } }
        public String PHOTO { get { return Photo; }set { Photo = value;OnPropertyChanged("PHOTO"); } }
        public String ORIGIN { get { return Origin; }set { Origin = value;OnPropertyChanged("ORIGIN"); } }
        public String NAMA { get { return Nama; } set { Nama = value; OnPropertyChanged("NAMA"); } }
        public String IDTRANSAKSI { get { return IdTransaksi; } set { IdTransaksi = value; OnPropertyChanged("IDTRANSAKSI"); } }
        public FormattedString MESSAGE { get { return Message; } set { Message = value; OnPropertyChanged("MESSAGE"); } }
        public String TOTAL { get { return Total; } set { Total = value; OnPropertyChanged("TOTAL"); } }

        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
