﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Model.MyTransaction
{
    public class FormattedStringModel
    {
        public string id_teman { get; set; }
        public string before { get; set; }
        public string after { get; set; }
        public string bold { get; set; }
        public int posisi { get; set; }
    }
}
