﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Model.MyTransaction
{
    public class Send_MoneyModel : INotifyPropertyChanged
    {
        private String photo { get; set; }
        private String nama { get; set; }
        private FormattedString message { get; set; }
        private String time { get; set; }
        private String idTransaksi { get; set; }
        private int total { get; set; }
        private String description { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public String PHOTO             { get { return photo; }         set {photo = value; OnPropertyChanged("PHOTO"); } }
        public String NAMA              { get { return nama; }          set {nama = value; OnPropertyChanged("NAMA"); } }
        public FormattedString MESSAGE  { get { return message; }       set {message = value; OnPropertyChanged("MESSAGE"); } }
        public String TIME              { get { return time; }          set {time = value; OnPropertyChanged("TIME"); } }
        public String IDTRANSAKSI       { get { return idTransaksi; }   set {idTransaksi = value;OnPropertyChanged("IDTRANSAKSI"); } }
        public int TOTAL                { get { return total; }         set { total = value;OnPropertyChanged("TOTAL"); } }
        public String DESCRIPTION       { get { return description; }   set { description = value;OnPropertyChanged("DESCRIPTION"); } }
    }
}
