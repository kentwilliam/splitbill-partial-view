﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Model.MyTransaction
{
    public class Pending:INotifyPropertyChanged
    {
        public Pending() {
            Destination = "";
        }
        private String GroupId { get; set; }
        private String RequestId { get; set; }
        private String Nama { get; set; }
        private FormattedString Message { get; set; }
        private String Total { get; set; }
        private String Origin { get; set; }
        private String Photo { get; set; }
        private String Destination { get; set; }


        public String DESTINATION { get { return Destination; } set { Destination = value; OnPropertyChanged("DESTINATION"); } }
        public String REQUESTID { get { return RequestId; }set { RequestId = value;OnPropertyChanged("REQUESTID"); } }
        public String ORIGIN { get { return Origin; }set { Origin = value;OnPropertyChanged("ORIGIN"); } }
        public String GROUPID { get { return GroupId; } set { GroupId = value;OnPropertyChanged("GROUPID"); } }
        public String NAMA { get { return Nama; } set { Nama = value;OnPropertyChanged("NAMA"); } }
        public FormattedString MESSAGE { get { return Message; } set { Message = value;OnPropertyChanged("MESSAGE"); } }
        public String TOTAL { get { return Total; } set { Total = value;OnPropertyChanged("TOTAL"); } }
        public String PHOTO { get { return Photo; }set { Photo = value;OnPropertyChanged("PHOTO"); } }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
