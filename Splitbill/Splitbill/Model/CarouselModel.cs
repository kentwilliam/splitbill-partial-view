﻿using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using CustomLayouts;
using CustomLayouts.ViewModels;

namespace Splitbill.Model
{
    public class CarouselModel : BaseViewModel
    {
        public CarouselModel()
        {
            Pages = new List<Carousel>() {
                new Carousel { ImageSource = "backgrounda.png" },
                new Carousel { ImageSource = "backgroundb.png" },
                new Carousel { ImageSource = "backgroundc.png" },
                new Carousel { ImageSource = "backgroundd.png" },
            };

            CurrentPage = Pages.First();
        }

        IEnumerable<Carousel> _pages;
        public IEnumerable<Carousel> Pages
        {
            get
            {
                return _pages;
            }
            set
            {
                SetObservableProperty(ref _pages, value);
                CurrentPage = Pages.FirstOrDefault();
            }
        }

        Carousel _currentPage;
        public Carousel CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                SetObservableProperty(ref _currentPage, value);
            }
        }
    }

    public class Carousel : BaseViewModel, ITabProvider
    {
        public Carousel() { }
        public string ImageSource { get; set; }
    }
}

