﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

enum State { }
namespace Splitbill.Model.MVVM
{
    public class GlobalLabel
    {
        public double XXLarge { get; }
        public double XLarge { get; }
        public double Large { get; }
        public double Medium { get;}
        public double Small { get; }
        public double XSmall { get; }
        public double XXSmall { get; }

        public GlobalLabel()
        {
            XXLarge = 1.75f;
            XLarge = 1.5f;
            Large = 1.25f;
            Medium = 1f;
            Small = 0.75f;
            XSmall = 0.5f;
            XXSmall = 0.25f;
        }
    }
}
