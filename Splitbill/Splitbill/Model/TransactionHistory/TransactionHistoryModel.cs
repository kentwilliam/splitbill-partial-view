﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Splitbill.Model.TransactionHistory
{
    public class TransactionHistoryModel
    {
        public Xamarin.Forms.Color Tulisan      { get; set; }
        public bool Coretan                     { get; set; }
        public String TOPSTACK                  { get; set; }
        public String BOTTOMSTACK               { get; set; }
        public String USERNAME                  { get; set; }
        public String PHOTO                     { get; set; }

        public int UNPAID_AMOUNT                { get; set; }
        public int PAID_AMOUNT                  { get; set; }
        public int TOTAL_AMOUNT                 { get; set; }

        public String IDMONEY_REQUEST_DETAIL    { get; set; }
        public int STATUS                       { get; set; }
        public String AMOUNT_AFTER              { get; set; }
        public String IDMONEY_REQUEST           { get; set; }
        public String DATE                      { get; set; }
    }
}
