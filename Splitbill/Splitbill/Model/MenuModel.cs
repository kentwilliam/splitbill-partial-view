﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Model
{
    public class MenuModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public String ImageSource { get; set; }
        public String TitleImage
        { get; set; }
        public Color LabelColor { get; set; }
        public void ChangeValue(String Target)
        {
            OnPropertyChanged(Target);
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
