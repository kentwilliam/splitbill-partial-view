﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splitbill.Model.User
{
    public class UserData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private String username { get; set; }
        private String token { get; set; }
        private String balance { get; set; }
        private Double balanceDoubleValue { get; set; }
        private String photo { get; set; }
        private String email { get; set; }
        private String alamat { get; set; }
        private String password { get; set; }
        private String telephone { get; set; }
        private String notificationCounter { get; set; }
        private bool notificationShow { get; set; }
        private bool notificationActive { get; set; }

        public bool NOTIFICATIONACTIVE { get { return notificationActive; } set { notificationActive = value; OnPropertyChanged("NOTIFICATIONACTIVE"); } }
        public String USERNAME { get { return username; } set { username = value; OnPropertyChanged("USERNAME"); } }
        public String TOKEN { get { return token; } set { token = value; OnPropertyChanged("TOKEN"); } }
        public String BALANCE { get { return balance; } set { balance = value; OnPropertyChanged("BALANCE"); } }
        public double BALANCEDOUBLEVALUE { get { return balanceDoubleValue; } set { balanceDoubleValue = value; OnPropertyChanged("BALANCEDOUBLEVALUE"); } }
        public String PHOTO { get { return photo; } set { photo = value; OnPropertyChanged("PHOTO"); } }
        public String EMAIL { get { return email; } set { email = value; OnPropertyChanged("EMAIL"); } }
        public String ALAMAT { get { return alamat; } set { alamat = value; OnPropertyChanged("ALAMAT"); } }
        public String PASSWORD { get { return password; } set { password = value; OnPropertyChanged("PASSWORD"); } }
        public String NOTIFICATIONCOUNTER { get { return notificationCounter; } set { notificationCounter = value; OnPropertyChanged("NOTIFICATIONCOUNTER"); } }
        public bool NOTIFICATIONSHOW { get { return notificationShow; } set { notificationShow = value; OnPropertyChanged("NOTIFICATIONSHOW"); } }
        public String TELEPHONE { get { return telephone; } set { telephone = value; } }
        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }


    }
}
