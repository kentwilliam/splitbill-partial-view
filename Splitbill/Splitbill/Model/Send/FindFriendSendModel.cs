﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Model.Send
{
    public class FindFriendSendModel:Username_Token
    {
        public String nama { get; set; }
        public String noHp { get; set; }
        public String photo { get; set;}
        public String friend { get; set; }
    }
}
