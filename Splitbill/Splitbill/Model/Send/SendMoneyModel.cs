﻿using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Model.Send
{
    public class SendMoneyModel:Username_Token
    {
        public String friend { get; set; }
        public Double amount { get; set; }
        public String description { get; set; }
        public String OTP { get; set; }
    }
}
