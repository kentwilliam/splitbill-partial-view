﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splitbill.Model.RequestModel
{
    public class RequestFriend
    {
        public String Name { get; set; }
        public String Friends { get; set; }
        public String Harga { get; set; }
        public String photo { get; set; }
    }
}
