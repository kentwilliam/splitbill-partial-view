﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splitbill.Model.RequestModel
{
    public class RequestSubRepository
    {
        ObservableCollection<RequestSubModel> request;

        public ObservableCollection<RequestSubModel> REQUEST {
            get { return request; }
            set
            {
               this.request = value;
            }
        }

        public RequestSubRepository()
        {
            request = new ObservableCollection<RequestSubModel>();
        }
        public void AddItem(RequestSubModel sj)
        {
            request.Add(sj);
        }
    }
}
