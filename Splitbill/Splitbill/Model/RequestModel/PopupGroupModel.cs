﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace Splitbill.Model.RequestModel
{
    public class PopupGroupModel : INotifyPropertyChanged
    {
        private ObservableCollection<GroupMember> member;
        private bool isSelected;

        public ObservableCollection<GroupMember> MEMBER { get { return member; } set { member = value; RaisePropertyChanged("MEMBER"); } }
        public bool ISSELECTED { get { return isSelected; } set { isSelected = value; RaisePropertyChanged("ISSELECTED"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(String name)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public PopupGroupModel()
        {
            member = new ObservableCollection<GroupMember>();
        }

    }
    public class GroupMember : INotifyPropertyChanged
    {
        private String membername { get; set; }
        private bool selected { get; set; }
        private String imgsrc { get; set; }
        private String groupname { get; set; }
        private String groupId { get; set; }

        public String GROUPID { get { return groupId; } set { groupId = value; RaisePropertyChanged("GROUPID"); } }
        public String GROUPNAME { get { return groupname; } set { groupname = value; RaisePropertyChanged("GROUPNAME"); } }
        public String IMGSRC { get { return imgsrc; } set { imgsrc = value; RaisePropertyChanged("IMGSRC"); } }
        public String MEMBERNAME { get { return membername; } set { membername = value; RaisePropertyChanged("MEMBERNAME"); } }
        public bool SELECTED { get { return selected; } set { selected = value; RaisePropertyChanged("SELECTED"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(String name)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
