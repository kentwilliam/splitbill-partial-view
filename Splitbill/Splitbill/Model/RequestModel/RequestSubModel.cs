﻿using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Model.RequestModel
{
    public class RequestSubModel : INotifyPropertyChanged
    {
        private String namaMakanan { get; set; }
        private String friends { get; set; }
        private String rupiah { get; set; }
        private String SetelahDiskon { get; set; }
        private  String Index { get; set; }
        private ImageSource imgsource { get; set; }
        private MediaFile _media { get; set; }
        private bool centang { get; set; }

        private bool usertraktir { get; set; }
        private String uangusertraktiran { get; set; }
        public String DiskonYangTraktir { get; set; }
        public bool SHOWSTACKLAYOUT { get; set; }
        public String PhotoName { get; set; }

        public bool CENTANG {
            get { return centang; }
            set { centang = value; OnPropertyChanged("CENTANG"); }
        }
        public MediaFile _MEDIA
        {
            get { return _media; }set { _media = value; }
        }
        public ImageSource IMGSOURCE { get {
                return imgsource;
            }
            set
            {
                imgsource = value;
                OnPropertyChanged("IMGSOURCE");
            }
        }
        public String INDEX
        {
            get { return Index; }
            set { Index = value;OnPropertyChanged("INDEX"); } }
        public bool USERTRAKTIR
        {
            get {
                return usertraktir;
            }
            set {
                usertraktir = value; SHOWSTACKLAYOUT = !value;
            }
        }
        public String UANGUSERTRAKTIRAN {
            get { return uangusertraktiran; }
            set { uangusertraktiran = value; }
        }
        public String NAMAMAKANAN
        {
            get
            {
                return namaMakanan;
            }
            set
            {
                namaMakanan = value;
                OnPropertyChanged("NAMAMAKANAN");
            }
        }

        public String FRIENDS
        {
            get { return friends; }
            set
            {
                friends = value;
                OnPropertyChanged("FRIENDS");
            }
        }
        public String RUPIAH
        {
            get {
                return rupiah;
            }
            set {
                rupiah = value;
                OnPropertyChanged("RUPIAH");
            }
        }
        public String SETELAHDISCOUNT
        {
            get
            { return SetelahDiskon; }
            set { SetelahDiskon = value;OnPropertyChanged("SETELAHDISCOUNT"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
