﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.Model.ContentViewHome
{
    public class FriendsActivity:INotifyPropertyChanged
    {
        private String id { get; set; }
        private String id_teman { get; set; }
        private String time { get; set; }
        private FormattedString message { get; set; }
        private String id_stranger { get; set; }
        private String photo { get; set; }
        private FormattedString description { get; set; }

        public String ID          { get { return id; } set { id = value; OnPropertyChanged("ID"); } }
        public String ID_TEMAN    { get { return id_teman; } set { id_teman = value; OnPropertyChanged("ID_TEMAN"); } }
        public String TIME        { get { return time; } set { time = value; OnPropertyChanged("TIME"); } }
        public FormattedString MESSAGE     { get { return message; } set { message = value; OnPropertyChanged("MESSAGE"); } }
        public String ID_STRANGER { get { return id_stranger; } set { id_stranger = value; OnPropertyChanged("ID_STRANGER"); } }
        public String PHOTO       { get { return photo; } set { photo = value; OnPropertyChanged("PHOTO"); } }
        public FormattedString DESCRIPTION { get { return description; } set { description = value; OnPropertyChanged("DESCRIPTION"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
