﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Database.Models
{
    public class User
    {
        [PrimaryKey]
        public int Id { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public bool Notification { get; set; }
        public User() { }
        public User(String Username, String Password,bool Notification)
        {
            this.Username = Username;
            this.Password = Password;
            this.Notification = Notification;
        }
        public bool CheckInformation()
        {
            if (!this.Username.Equals("") && !this.Password.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
