﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Splitbill.Database.Models;
using Xamarin.Forms;

namespace Splitbill.Database
{
    public class UserDatabase
    {
        static object locker = new object();

        SQLiteConnection database;
        public UserDatabase()
        {
            database = DependencyService.Get<ISQLite>().GetConnection();
            database.CreateTable<Models.User>();

        }
        public Models.User GetUser()
        {
            lock (locker)
            {
                if (database.Table<Models.User>().Count() == 0)
                {
                    return null;
                }
                else
                {
                    return database.Table<Models.User>().First();
                }
            }
        }

        public int SaveUser(Models.User ur)
        {
            lock (locker)
            {
                if (ur.Id != 0)
                {
                    database.Update(ur);
                    return ur.Id;
                }
                else
                {
                    return database.Insert(ur);
                }
            }
        }
        public int DeleteUser(Models.User ur)
        {
            lock (locker)
            {
                return database.Delete(ur);
            }
        }
    }
}
