﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Splitbill.Database
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
