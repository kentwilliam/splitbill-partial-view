﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Syncfusion.SfCarousel.XForms;
namespace Splitbill.DataTemplates.MyTransaction
{
    public class DataTemplateMyTransaction
    {
        public SfListView listPending;
        public SfListView listCompleted;
        public DataTemplate GetListCompletedDataTemplate()
        {
            return new DataTemplate(() => {
                Grid wrapper = new Grid() { RowSpacing = 0, BackgroundColor = Color.FromHex("#f1efef") };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5, 5, 5) };
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(ImageCircle.Forms.Plugin.Abstractions.CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand };
                Label nama = new Label()
                {
                    FontSize = 8 * App.DpiFontSize,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.CornflowerBlue
                };
                nama.SetBinding(Label.TextProperty, "NAMA");
                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 8 * App.DpiFontSize,
                };
                message.SetBinding(Label.FormattedTextProperty, "MESSAGE");

                Label time = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 6 * App.DpiFontSize,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Margin = new Thickness(0, 10, 5, 0)
                };
                time.SetBinding(Label.TextProperty, "TIME");

                detail.Children.Add(nama);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                myStack.Children.Add(time);
                //myStack.Children.Add();

                wrapper.Children.Add(myStack, 0, 0);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill }, 0, 1);
                return new ViewCell() { View = wrapper };
            });
        }
        public DataTemplate GetListCompletedGroupHeaderDataTemplate()
        {
            return new DataTemplate(() => {
                Grid wrapper = new Grid() { RowSpacing = 0, BackgroundColor = Color.White };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5, 5, 5) };
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(ImageCircle.Forms.Plugin.Abstractions.CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand };

                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 8 * App.DpiFontSize,
                };

                Label time = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 6 * App.DpiFontSize,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Margin = new Thickness(0, 10, 5, 0)
                };
                time.SetBinding(Label.TextProperty, "TIME");
                Binding bind = new Binding("Key");
                bind.ConverterParameter = listCompleted;
                bind.Converter = new Converter.GroupHeaderCompletedFormetedTextValueConverter();

                message.SetBinding(Label.FormattedTextProperty,bind);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                myStack.Children.Add(time);
                //myStack.Children.Add();

                wrapper.Children.Add(myStack, 0, 0);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill }, 0, 1);
                return new ViewCell() { View = wrapper };
            });
        }
        public DataTemplate GetListListSendReceiveMoneyDataTemplate()
        {
            return new DataTemplate(() =>
            {
                Grid wrapper = new Grid() { RowSpacing = 0 };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5, 5, 5) };
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(ImageCircle.Forms.Plugin.Abstractions.CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand };
                Label nama = new Label()
                {
                    FontSize = 8 * App.DpiFontSize,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.CornflowerBlue
                };
                nama.SetBinding(Label.TextProperty, "NAMA");
                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 8 * App.DpiFontSize,
                };
                message.SetBinding(Label.FormattedTextProperty, "MESSAGE");

                Label time = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 6 * App.DpiFontSize,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Margin = new Thickness(0, 10, 5, 0)
                };
                time.SetBinding(Label.TextProperty, "TIME");

                detail.Children.Add(nama);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                myStack.Children.Add(time);
                //myStack.Children.Add();

                wrapper.Children.Add(myStack, 0, 0);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill }, 0, 1);
                return new ViewCell() { View = wrapper };
            });
        }
        public DataTemplate GetListPendingDatatTemplate()
        {
            return new DataTemplate(() =>
            {
                Grid wrapper = new Grid() { RowSpacing = 0, BackgroundColor = Color.FromHex("#f1efef") };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5, 5, 5) };
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(ImageCircle.Forms.Plugin.Abstractions.CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand };
                Label nama = new Label()
                {
                    FontSize = App.DpiFontSize * 8,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.CornflowerBlue
                };
                nama.SetBinding(Label.TextProperty, "NAMA");
                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 8 * App.DpiFontSize,
                };
                message.SetBinding(Label.FormattedTextProperty, "MESSAGE");
                detail.Children.Add(nama);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                wrapper.Children.Add(myStack, 0, 0);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill }, 0, 1);
                return new ViewCell() { View = wrapper };
            });
        }
        public DataTemplate GetListPendingGroupHeaderDataTemplate()
        {
            return new DataTemplate(() =>
            {
                Grid wrapper = new Grid() { RowSpacing = 0 };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 69 });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1 });

                StackLayout sl = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    BackgroundColor = Color.White,
                    Margin = new Thickness(5, 0, 0, 0)
                };

                StackLayout stackTulisan = new StackLayout();

                Binding bind = new Binding("Key");
                bind.Converter = new Converter.GroupHeaderPendingFormatedTextValueConverter();
                bind.ConverterParameter = listPending;
                Label tulisan = new Label() { };
                tulisan.SetBinding(Label.FormattedTextProperty, bind);

                Binding tujuanBind = new Binding("Key");
                tujuanBind.Converter = new Splitbill.Converter.GroupHeaderPendingTujuanValueConverter();
                tujuanBind.ConverterParameter = listPending;
                Label tujuan = new Label();
                tujuan.SetBinding(Label.FormattedTextProperty, tujuanBind);

                CachedImage cimg = new CachedImage()
                {
                    WidthRequest = 20 * App.DpiFontSize,
                    HeightRequest = 20 * App.DpiFontSize,
                    VerticalOptions = LayoutOptions.Center,
                    Source = App.GlobalDataUser.PHOTO,
                    DownsampleToViewSize = true,
                    LoadingPlaceholder = "...",
                };
                cimg.Transformations.Add(new CircleTransformation());

                CachedImage Indicator = new CachedImage()
                {
                    WidthRequest = 7 * App.DpiFontSize,
                    HeightRequest = 7 * App.DpiFontSize,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 5, 0),
                    DownsampleToViewSize = true,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Source = "Down.png"
                };
                sl.GestureRecognizers.Add(new TapGestureRecognizer((v, e) => {
                    var xa = Indicator.Source.ToString();
                    if (xa.Contains("Up.png"))
                    {
                        Indicator.Source = "Down.png";
                    }
                    else
                    {
                        Indicator.Source = "Up.png";
                    }
                }));
                stackTulisan.VerticalOptions = LayoutOptions.CenterAndExpand;
                stackTulisan.Children.Add(tulisan);
                stackTulisan.Children.Add(tujuan);

                sl.Children.Add(cimg);
                sl.Children.Add(stackTulisan);
                sl.Children.Add(Indicator);

                wrapper.Children.Add(sl, 0, 0);
                wrapper.Children.Add(new BoxView() { HeightRequest = 1, BackgroundColor = Color.LightGray }, 0, 1);
                return wrapper;
            });
        }
        public DataTemplate GetListBalancesDataTemplate()
        {
            return new DataTemplate(() => {
                Grid wrapper = new Grid() { RowSpacing = 0 };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5,5, 5),HorizontalOptions = LayoutOptions.FillAndExpand};
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(ImageCircle.Forms.Plugin.Abstractions.CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.Fill,HorizontalOptions = LayoutOptions.FillAndExpand };
                StackLayout Header = new StackLayout() {Orientation = StackOrientation.Horizontal,HorizontalOptions = LayoutOptions.Fill};
                Label nama = new Label()
                {
                    FontSize = 8 * App.DpiFontSize,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.CornflowerBlue,
                    HorizontalOptions = LayoutOptions.Start
                };
                nama.SetBinding(Label.TextProperty, "NAMA");
                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = 8 * App.DpiFontSize,
                };
                Label tanggal = new Label() {
                    TextColor = Color.LightGray,
                    FontSize = 6 * App.DpiFontSize,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    Margin = new Thickness(0, 0, 5, 0),
                };
                tanggal.SetBinding(Label.TextProperty, "TIME");

                message.SetBinding(Label.FormattedTextProperty, "MESSAGE");
                Header.Children.Add(nama);
                Header.Children.Add(tanggal);
                detail.Children.Add(Header);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                wrapper.Children.Add(myStack, 0, 0);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill }, 0, 1);
                return new ViewCell() { View = wrapper };
            });
        }
    }
}
