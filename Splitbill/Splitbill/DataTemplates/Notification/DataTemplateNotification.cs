﻿using ImageCircle.Forms.Plugin.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.DataTemplates.Notification
{
    public class DataTemplateNotification
    {
        public DataTemplate GetNoNotificationDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() =>
            {
                Grid G = new Grid()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                Label lbl = new Label()
                {
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.LightGray,
                    Text = "No Transaction",
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center
                };

                G.Children.Add(lbl);
                return G;
            });
            return dt;
        }
        public DataTemplate GetHeaderNotificationDataTemplate()
        {
            DataTemplate HeadTemplate = new DataTemplate(() =>
            {
                var grid = new Grid() { RowSpacing = 0, BackgroundColor = Color.White };
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(49) });
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1) });

                var label = new Label
                {
                    FontAttributes = FontAttributes.Bold,
                    FontSize = App.DpiFontSize * 9,
                    TextColor = Color.CornflowerBlue,
                    Margin = new Thickness(0, 10, 0, 0)
                };
                label.SetBinding(Label.TextProperty, new Binding("Key"));
                grid.Children.Add(label, 0, 0);
                grid.Children.Add(new BoxView() { BackgroundColor = Color.LightGray }, 0, 1);
                return grid;
            });
            return HeadTemplate;
        }
        public DataTemplate GetGroupListViewDataTemplate()
        {
            return new DataTemplate(() =>
            {
                Grid wrapper = new Grid() { RowSpacing = 0, BackgroundColor = Color.FromHex("#f1efef") };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5, 5, 5) };
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand };
                Label nama = new Label()
                {
                    FontSize = App.DpiFontSize * 6,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.CornflowerBlue
                };
                nama.SetBinding(Label.TextProperty, "SENDER_ID");
                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = App.DpiFontSize * 6
                };
                message.SetBinding(Label.TextProperty, "CONTENT");
                detail.Children.Add(nama);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                wrapper.Children.Add(myStack, 0, 0);
                wrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill }, 0, 1);
                return new ViewCell() { View = wrapper };
            });
        }
        public DataTemplate GetAllTabItemDataTemplate()
        {
            return new DataTemplate(() =>
            {
                Grid wrapper = new Grid() { RowSpacing = 0, ColumnSpacing = 0 };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 74f });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = 1f });
                wrapper.ColumnDefinitions.Add(new ColumnDefinition { Width = 20 });

                StackLayout myStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(10, 5, 5, 5) };
                ImageCircle.Forms.Plugin.Abstractions.CircleImage ciimg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage()
                {
                    FillColor = Color.Gray,
                    BorderThickness = 1,
                    BorderColor = Color.Black,
                    WidthRequest = 45,
                };
                ciimg.SetBinding(CircleImage.SourceProperty, "PHOTO");
                StackLayout detail = new StackLayout() { VerticalOptions = LayoutOptions.CenterAndExpand };
                Label nama = new Label()
                {
                    FontSize = App.DpiFontSize * 6,
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.CornflowerBlue
                };
                nama.SetBinding(Label.TextProperty, "SENDER_ID");
                Label message = new Label()
                {
                    TextColor = Color.Gray,
                    FontSize = App.DpiFontSize * 6,
                };
                message.SetBinding(Label.TextProperty, "CONTENT");

                BoxView GarisBawah = new BoxView() { BackgroundColor = Color.LightGray, VerticalOptions = LayoutOptions.Fill };
                XFShapeView.ShapeView bulet = new XFShapeView.ShapeView()
                {
                    ShapeType = XFShapeView.ShapeType.Circle,
                    Color = Color.CornflowerBlue,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    WidthRequest = 5,
                    HeightRequest = 5,
                    Margin = new Thickness(5, 0, 0, 0)
                };
                bulet.SetBinding(CircleImage.IsVisibleProperty, "READ_FLAG");
                detail.Children.Add(nama);
                detail.Children.Add(message);

                myStack.Children.Add(ciimg);
                myStack.Children.Add(detail);
                wrapper.Children.Add(myStack, 1, 0);
                wrapper.Children.Add(GarisBawah, 0, 1);
                wrapper.Children.Add(bulet, 0, 0);
                Grid.SetColumnSpan(GarisBawah, 2);
                return new ViewCell() { View = wrapper };
            });
        }
    }
}
