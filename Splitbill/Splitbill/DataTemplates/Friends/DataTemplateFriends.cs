﻿using FFImageLoading.Forms;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Splitbill.DataTemplates.Friends
{
    public class DataTemplateFriends
    {
        public DataTemplate GetNoSuggestFriendDataTemplate()
        {
            return new DataTemplate(() => {
                Grid G = new Grid()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                Label lbl = new Label()
                {
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.LightGray,
                    Text = "No Suggest Friend",
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    FontSize = App.DpiFontSize * 8
                };

                G.Children.Add(lbl);
                return G;
            });
        }
        public DataTemplate GetSuggestFriendsDataTemplate()
        {
            return new DataTemplate(()=> {
                StackLayout OuterWrapper = new StackLayout() { Orientation = StackOrientation.Vertical, HeightRequest = 69 };
                StackLayout mainStack = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(0, 5, 0, 5) };
                CachedImage ciImg = new CachedImage()
                {
                    Margin = new Thickness(10, 0, 0, 0),
                    WidthRequest = 50,
                    HeightRequest = 50,
                    Aspect = Aspect.AspectFill,
                    Source = "user72.png",
                    DownsampleToViewSize = true
                };

                Label Nama = new Label() { TextColor = Color.Black, VerticalOptions = LayoutOptions.CenterAndExpand };
                Nama.SetBinding(Label.TextProperty, "nama");

                Image iconAdd = new Image()
                {
                    Source = "AddFriend.png",
                    HeightRequest = 15 * App.DpiFontSize,
                    WidthRequest = 15 * App.DpiFontSize,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                };

                iconAdd.GestureRecognizers.Add(new TapGestureRecognizer(async (v, e) => {
                    Popup.Friend.PopupAddFriend popup = new Popup.Friend.PopupAddFriend();
                    popup.Eve += ((vx, ex) =>
                    {
                        var resp = App._Api.GetDataResponseFromServer("user/add_friend", JsonConvert.SerializeObject(new Api.Encode.SuggestFriendAddFriendJson()
                        {
                            username = App.GlobalDataUser.USERNAME,
                            friend = Nama.Text,
                            token = App.GlobalDataUser.TOKEN
                        }));

                        var resu = resp.Result;
                        if (resu.ToLower() == "no internet")
                        {
                            App.MyHomePage.ShowAlert(JsonConvert.DeserializeObject<ErrorMessage>(resu).message);
                        }
                        else
                        {
                            App.MyHomePage.ShowAlert(JsonConvert.DeserializeObject<ErrorMessage>(resu).message);
                            App.MyHomePage.ToFriendsPage();
                        }
                    });
                    await PopupNavigation.PushAsync(popup);
                }));
                mainStack.Children.Add(ciImg);
                mainStack.Children.Add(Nama);
                mainStack.Children.Add(iconAdd);
                OuterWrapper.Children.Add(mainStack);
                OuterWrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 });
                return OuterWrapper;
            });
        }
        public DataTemplate Example1()
        {
            return new DataTemplate();
        }
        //public DataTemplate Example1()
        //{
        //    return new DataTemplate();
        //}
        //public DataTemplate Example1()
        //{
        //    return new DataTemplate();
        //}
    }
}
