﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using Splitbill.Model.RequestModel;
using Splitbill.Popup.Request;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using XLabs.Forms.Controls;


//Note: Data Template Request Adalah yang paling beda daripada yang lainnya
//Karena templatenya ada Fungsinya  dan Sudah di dungsikan
//Halaman Request Dan DataTemplate saling berhubungan
namespace Splitbill.DataTemplates.Request
{
    public class DataTemplateRequest
    {
        public Model.RequestModel.RequestSubRepository req;
        public SfListView deListFriend;
        public EventHandler CheckTotakDuit;
        public int selectedIndexDelistFriend;
        public int indexItem;
        public void SetReq(ref Model.RequestModel.RequestSubRepository _req )
        {
            req = _req;
        }
        public DataTemplate DeListFriendDataTemplate()
        {
            return new DataTemplate(() =>
            {
                CachedImage img = new CachedImage();
                Entry Nama = new Entry() { FontSize = 7 * App.DpiFontSize };

                Label Friends = new Label() { FontSize = App.DpiFontSize * 7 };
                Entry Inputan = new Entry() { FontSize = 7 * App.DpiFontSize };
                Inputan.Text = "";
                Inputan.Keyboard = Keyboard.Numeric;
                Inputan.Focused += (sender, ea) =>
                {
                    (sender as Entry).Text = "";
                };
                Inputan.TextChanged += (sender, ea) =>
                {
                    CheckTotakDuit(this,EventArgs.Empty);
                };
                Nama.Focused += (sender, ea) =>
                {
                    (sender as Entry).Text = "";
                };
                Nama.Completed += (sender, ea) =>
                {
                    if ((sender as Entry).Text.Count() > 0)
                    {
                        (sender as Entry).Text = (sender as Entry).Text.First().ToString().ToUpper() + (sender as Entry).Text.Substring(1);
                    }

                };

                //Inputan.BackgroundColor = Color.Gray;
                Inputan.Margin = 0;
                Inputan.VerticalOptions = LayoutOptions.Center;
                Inputan.HorizontalOptions = LayoutOptions.Fill;
                Inputan.FontSize = 12;

                Image IconUser = new Image()
                {
                    HorizontalOptions = LayoutOptions.End,
                    Source = "Friends.png",
                    Aspect = Aspect.AspectFit,
                    Margin = new Thickness(0, 0, 0, 0),
                    WidthRequest = 15 * App.DpiFontSize,
                    HeightRequest = 15 * App.DpiFontSize,
                };

                IconUser.SetBinding(Image.IsVisibleProperty, "SHOWSTACKLAYOUT");
                Grid HorizontalGrid = new Grid();
                StackLayout WrapperStack = new StackLayout() { Spacing = 0 };
                HorizontalGrid.ColumnSpacing = 0;
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.5f, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2.5, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0.6, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2.5, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.8, GridUnitType.Star) });

                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5f, GridUnitType.Star) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = 0.3 });

                BoxView bx = new BoxView()
                {
                    BackgroundColor = Color.LightGray

                };
                HorizontalGrid.Children.Add(bx, 0, 2);
                Grid.SetColumnSpan(bx, 5);

                XFShapeView.ShapeView circleBorder = new XFShapeView.ShapeView()
                {
                    ShapeType = XFShapeView.ShapeType.Circle,
                    BorderColor = Color.Black,
                    Color = Color.White,
                    HeightRequest = 50,
                    WidthRequest = 50,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                    BorderWidth = 1,
                };

                img.Source = "DC.png";
                img.Aspect = Aspect.AspectFit;
                img.DownsampleToViewSize = true;
                img.Transformations.Add(new CircleTransformation());

                Nama.HorizontalOptions = LayoutOptions.Fill;
                Nama.VerticalOptions = LayoutOptions.Fill;

                Nama.FontSize = 12;
                img.SetBinding(CachedImage.SourceProperty, "IMGSOURCE");
                Nama.SetBinding(Entry.TextProperty, "NAMAMAKANAN");
                Friends.SetBinding(Label.TextProperty, "FRIENDS");
                Inputan.SetBinding(Entry.TextProperty, "RUPIAH");


                circleBorder.Content = img;
                HorizontalGrid.Children.Add(circleBorder, 0, 0);
                Grid.SetRowSpan(circleBorder, 2);

                HorizontalGrid.Children.Add(Nama, 1, 0);

                StackLayout l = new StackLayout() { Orientation = StackOrientation.Vertical };
                l.Children.Add(Friends);

                HorizontalGrid.Children.Add(l, 1, 1);
                Grid.SetColumnSpan(l, 3);

                Label IDR = new Label()
                {
                    Text = "Rp",
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 0, 2),
                    FontSize = App.DpiFontSize * 8,
                };

                HorizontalGrid.Children.Add(IDR, 2, 0);
                HorizontalGrid.Children.Add(Inputan, 3, 0);
                HorizontalGrid.Children.Add(IconUser, 4, 0);

                StackLayout s = new StackLayout();
                s.SetBinding(StackLayout.IsVisibleProperty, "SHOWSTACKLAYOUT");
                s.Orientation = StackOrientation.Horizontal;
                s.WidthRequest = 75;

                Label heckboxLabel = new Label()
                {
                    Text = "Add Me",
                    FontSize = 4 * App.DpiFontSize,
                    VerticalTextAlignment = TextAlignment.Center,
                };


                Entry index = new Entry();
                index.SetBinding(Entry.TextProperty, "INDEX");
                index.IsVisible = false;
                index.WidthRequest = 1;
                s.Children.Add(index);

                IconUser.GestureRecognizers.Add(new TapGestureRecognizer()
                {
                    Command = new Command(() => {
                        selectedIndexDelistFriend = Convert.ToInt32(index.Text);
                        Popup.Request.PopupFindFriendsRequest friends = new Popup.Request.PopupFindFriendsRequest(Friends.Text);
                        PopupNavigation.PushAsync(friends);
                        friends.Eve += GetFriends;
                        friends.EveGroup += GetGroups;
                    })
                });

                CheckBox AddMe = new CheckBox()
                {
                    Margin = new Thickness(0, 0, 0, 0),
                    HorizontalOptions = LayoutOptions.Fill,
                };
                AddMe.SetBinding(CheckBox.CheckedProperty, "CENTANG");
                AddMe.CheckedChanged += (sender, ea) =>
                {
                    String currentString = req.REQUEST[Convert.ToInt32(index.Text)].FRIENDS;
                    if (ea.Value == true)
                    {
                        if (currentString.ToLower().IndexOf(App.GlobalDataUser.USERNAME.ToLower()) < 0)
                        {
                            String temp = App.GlobalDataUser.USERNAME + "," + req.REQUEST[Convert.ToInt32(index.Text)].FRIENDS;
                            req.REQUEST[Convert.ToInt32(index.Text)].FRIENDS = temp;
                        }
                    }
                    else
                    {
                        if (currentString.ToLower().IndexOf(App.GlobalDataUser.USERNAME.ToLower()) >= 0)
                        {
                            String[] friend = req.REQUEST[Convert.ToInt32(index.Text)].FRIENDS.Split(',');
                            String hasil = "";
                            foreach (var x in friend)
                            {
                                if (x.ToLower() != App.GlobalDataUser.USERNAME.ToLower() && x.ToLower() != "" && !hasil.Contains(x.ToLower()))
                                {
                                    hasil += x + ",";
                                }
                            }
                            req.REQUEST[Convert.ToInt32(index.Text)].FRIENDS = hasil;
                        }
                    }
                    deListFriend.SelectedItem = null;
                };


                MediaFile _Media;
#pragma warning disable CS0618 // Type or member is obsolete

                img.GestureRecognizers.Add(new TapGestureRecognizer(async (view, aa) =>
                {
                    //(view as CachedImage).IsEnabled = false;
                    if (img.Source == null)
                    {
                        await CrossMedia.Current.Initialize();
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {

                        }
                        _Media = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                        {
                            PhotoSize = PhotoSize.Medium,
                            CompressionQuality = 75
                        });
                        if (_Media == null)
                        {
                            return;
                        }
                        //img.Source = ImageSource.FromStream(() => file.GetStream());
                        req.REQUEST[Convert.ToInt32(index.Text)]._MEDIA = _Media;

                        Splitbill.Api.Encode.SendPhotoJson js = new Api.Encode.SendPhotoJson();
                        byte[] hasilFoto;

                        using (var memoryStream = new MemoryStream())
                        {
                            _Media.GetStream().CopyTo(memoryStream);
                            _Media.Dispose();
                            hasilFoto = memoryStream.ToArray();
                        }
                        String tempString64 = Convert.ToBase64String(hasilFoto);

                        js.username = App.GlobalDataUser.USERNAME;
                        js.token = App.GlobalDataUser.TOKEN;
                        js.string64 = tempString64;

                        var resp = App._Api.GetDataResponseFromServer("transaction/upload_menu_base64", JsonConvert.SerializeObject(js));
                        var resu = JsonConvert.DeserializeObject<Api.Decode.PhotoErrorMessage>(resp.Result);
                        if (App._Api.ProcessDataOrNot(resu.message, false) == false)
                        {
                            App.MyHomePage.LogOut();
                        }
                        req.REQUEST[Convert.ToInt32(index.Text)].IMGSOURCE = "http://www.urunan.id/splitbill/" + resu.filename;
                        req.REQUEST[Convert.ToInt32(index.Text)].PhotoName = "http://www.urunan.id/splitbill/" + resu.filename;
                    }
                    else
                    {
                        Splitbill.Popup.Request.PopupPreviewImage photo = new Popup.Request.PopupPreviewImage(img.Source);
                        photo.PreviewImage.Source = img.Source;
                        photo.DoThis += (obj, e) =>
                        {
                            PopupNavigation.PopAsync();
                        };
                        await PopupNavigation.PushAsync(photo, true);
                    }
                    //(view as CachedImage).IsEnabled = true;
                }));
#pragma warning restore CS0618 // Type or member is obsolete

                s.Children.Add(heckboxLabel);
                s.Children.Add(AddMe);
                HorizontalGrid.Children.Add(s, 4, 1);

                WrapperStack.Children.Add(HorizontalGrid);
                return HorizontalGrid;
            });
        }
        public DataTemplate DeListFriendRightSwipeDataTemplate()
        {
            return new DataTemplate(() => {
                var grid = new Grid();
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                var grid1 = new Grid()
                {
                    BackgroundColor = Color.FromRgb(216, 13, 13),
                    HorizontalOptions = LayoutOptions.Fill,
                    VerticalOptions = LayoutOptions.Fill
                };

                var gridEdit = new Grid()
                {
                    BackgroundColor = Color.FromRgb(0, 162, 232),
                    HorizontalOptions = LayoutOptions.Fill,
                    VerticalOptions = LayoutOptions.Fill
                };


                var favoriteGrid = new Grid() { HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center };
                var EditGrid = new Grid() { HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center };
                var TrashImage = new Image() { };
                var editFriend = new Image() { Source = "editFriends.png" };

                TrashImage.Source = "Delete.png";
                TrashImage.BindingContextChanged += RightImage_BindingContextChanged;
                editFriend.BindingContextChanged += Editfriend_bindingContextChanged;

                EditGrid.Children.Add(editFriend);
                favoriteGrid.Children.Add(TrashImage);

                grid1.Children.Add(favoriteGrid);
                gridEdit.Children.Add(EditGrid);

                grid.Children.Add(gridEdit, 0, 0);
                grid.Children.Add(grid1, 1, 0);
                grid.ColumnSpacing = 0;
                return grid;

            });
        }
        public DataTemplate DeListFriendFooterDataTemplate()
        {
            return new DataTemplate(() => {
                Frame Wrap = new Frame()
                {
                    Margin = new Thickness(3),
                    Padding = 0,
                    CornerRadius = 5,
                    HasShadow = true
                };

                var grid = new Grid() { BackgroundColor = Color.FromHex("#4CA1FE") };
                XFShapeView.ShapeView buttonAddItem = new XFShapeView.ShapeView()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Color = Color.FromRgb(215, 215, 215),
                };
                Label buttonText = new Label()
                {
                    Text = "+ Add Items",
                    FontSize = 8 * App.DpiFontSize,
                    FontAttributes = FontAttributes.Bold,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                };
                buttonAddItem.Content = buttonText;

                var headerLabel = buttonAddItem;

                headerLabel.GestureRecognizers.Add(new TapGestureRecognizer(Tambah_Item));
                grid.Children.Add(headerLabel);

                Wrap.Content = grid;
                return Wrap;
            });
        }

        Image friendEdit;
        private void Editfriend_bindingContextChanged(object sender, EventArgs e)
        {
            (sender as Image).IsEnabled = false;
            if (friendEdit == null)
            {
                var friendEdit = sender as Image;
                (friendEdit.Parent as Xamarin.Forms.View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(EditFriendList) });
            }
            (sender as Image).IsEnabled = true;
        }
        private void EditFriendList()
        {
            var d = req.REQUEST[indexItem].FRIENDS.Split(',').ToArray();
            d = d.Where(x => !String.IsNullOrWhiteSpace(x)).ToArray();
            Splitbill.Popup.Request.PopupListFriend pop = new Splitbill.Popup.Request.PopupListFriend();
            pop.InitView();
            pop.InitList(d);
            pop.DoPush += HapusAtauDeleteTeman;
            PopupNavigation.PushAsync(pop);
            deListFriend.ResetSwipe();
        }
        private async void HapusAtauDeleteTeman(object s, EventArgs e)
        {
            var page = (s as Splitbill.Popup.Request.PopupListFriend);
            String temp = "";
            foreach (var x in page.friend)
            {
                if (!string.IsNullOrWhiteSpace(x.NAMA))
                {
                    temp = temp + x.NAMA + ", ";
                }
            }
            req.REQUEST[indexItem].FRIENDS = temp;

            await PopupNavigation.PopAsync();
        }
        //Fungsi
        Image leftImage;
        private void RightImage_BindingContextChanged(object sender, EventArgs e)
        {
            if (leftImage == null)
            {
                leftImage = sender as Image;
                (leftImage.Parent as Xamarin.Forms.View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(DeleteGroup) });
            }
        }
        private void DeleteGroup()
        {
            req.REQUEST.RemoveAt(indexItem);
            //reindex
            for (int i = indexItem; i < req.REQUEST.Count; i++)
            {
                req.REQUEST[i].INDEX = Convert.ToInt32(req.REQUEST[i].INDEX) - 1 + "";
            }
            deListFriend.ResetSwipe();
            CheckTotakDuit(this,EventArgs.Empty);
        }
        private async void GetFriends(Object sender, EventArgs e)
        {
            var cont = sender as Popup.Request.PopupFindFriendsRequest;
            int counterFriends = 0;

            counterFriends = cont.sflistFriend.SelectedItems.Count;
            if (counterFriends > 0)
            {
                var friends = cont.sflistFriend.SelectedItems;
                String temp = req.REQUEST[selectedIndexDelistFriend].FRIENDS;
                if (req.REQUEST[selectedIndexDelistFriend].USERTRAKTIR)
                {
                    temp += App.GlobalDataUser.USERNAME + ",";
                }
                foreach (var i in friends)
                {
                    String teman = (i as Model.Send.FindFriendSendModel).nama;
                    if (!temp.Contains(teman))
                    {
                        temp = temp + teman + ", ";
                    }
                }
                temp.Remove(temp.LastIndexOf(','));
                Model.RequestModel.RequestSubModel t1 = req.REQUEST[selectedIndexDelistFriend];
                t1.FRIENDS = temp;
            }
            await PopupNavigation.PopAsync();
        }
        private async void GetGroups(Object sender, EventArgs e)
        {
            int counterGroup = (sender as PopupFindFriendsRequest).sflistGroup.SelectedItems.Count;
            if (counterGroup > 0)
            {
                var group = (sender as PopupFindFriendsRequest).sflistGroup.SelectedItems;
                String temp = req.REQUEST[selectedIndexDelistFriend].FRIENDS;
                foreach (var i in group)
                {
                    var item = (i as GroupMember);
                    if (!temp.ToLower().Contains(item.MEMBERNAME.ToLower()))
                    {
                        temp = temp + item.MEMBERNAME + ", ";
                    }
                }

                Model.RequestModel.RequestSubModel t1 = req.REQUEST[selectedIndexDelistFriend];
                t1.FRIENDS = temp;
                if (temp.Contains(App.GlobalDataUser.USERNAME))
                {
                    t1.CENTANG = true;
                }
            }
            await PopupNavigation.PopAsync();
        }
        private void Tambah_Item(Xamarin.Forms.View arg1, object arg2)
        {
            Model.RequestModel.RequestSubModel m = new RequestSubModel()
            {
                FRIENDS = "",
                NAMAMAKANAN = "Item " + (req.REQUEST.Count() + 1),
                RUPIAH = "0",
                SETELAHDISCOUNT = "0",
                SHOWSTACKLAYOUT = true,
                INDEX = req.REQUEST.Count + "",
            };
            req.AddItem(m);
            deListFriend.HeightRequest = req.REQUEST.Count * 85;
        }
    }
}
