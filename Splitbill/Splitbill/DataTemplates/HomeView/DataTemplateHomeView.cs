﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Splitbill.DataTemplates.HomeView
{
    public class DataTemplateHomeView
    {      
        public DataTemplate MeListOtherDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() => {
                Grid g = new Grid() { Margin = new Thickness(5, 0, 5, 0) };
                StackLayout detail = new StackLayout() { HorizontalOptions = LayoutOptions.FillAndExpand };
                Label l = new Label()
                {
                    Text = "- THERE IS NO ACTIVITY -",
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    FontSize = App.DpiFontSize * 5,
                    Margin = new Thickness(0, 10, 0, 0),
                    TextColor = Color.LightGray
                };
                BoxView separator = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f, Margin = new Thickness(15, 0, 15, 0) };

                detail.Children.Add(l);
                detail.Children.Add(separator);
                return new ViewCell() { View = detail };
            });
            return dt;
        }
        public DataTemplate FriendListOtherDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() =>
            {
                Grid MyGrid = new Grid();
                CachedImage backgroundImg = new CachedImage()
                {
                    Source = "noActivity.png",
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Aspect = Aspect.AspectFit,
                };
                MyGrid.Children.Add(backgroundImg);
                return new ViewCell() { View = MyGrid };
            });
            return dt;
        }
        public DataTemplate GroupListOtherDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() =>
            {
                Grid MyGrid = new Grid();
                Label backgroundImg = new Label()
                {
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    FontSize = App.DpiFontSize * 5,
                    TextColor = Color.LightGray,
                    Text = "You Don't Have Group",
                    FontAttributes = FontAttributes.Bold
                };
                MyGrid.Children.Add(backgroundImg);
                return new ViewCell() { View = MyGrid };
            });
            return dt;
        }
        public DataTemplate GroupListNormalDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() => {
                Grid G = new Grid()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                };
                CachedImage bimg = new CachedImage()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Aspect = Aspect.Fill,
                    Source = "Overlay.png",
                    DownsampleToViewSize = true
                };

                Image bimg2 = new Image()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Aspect = Aspect.AspectFill,
                };
                bimg2.SetBinding(Image.SourceProperty, "PHOTO");
                Label CLICKME = new Label()
                {
                    TextColor = Color.White,
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 6 * App.DpiFontSize,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center
                };
                CLICKME.SetBinding(Label.TextProperty, "DISPLAYNAMAGRUP");
                G.Children.Add(bimg2, 0, 0);
                G.Children.Add(bimg, 0, 0);
                G.Children.Add(CLICKME, 0, 0);
                Frame FBimg = new Frame()
                {
                    Padding = 0,
                    CornerRadius = 5,
                    IsClippedToBounds = true,
                    Content = G
                };
                return FBimg;
            });
            return dt;
        }
        public DataTemplate MeListNormalDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() =>
            {
                Grid templateGrid = new Grid() { BackgroundColor = Color.Transparent, IsClippedToBounds = true, VerticalOptions = LayoutOptions.CenterAndExpand };
                templateGrid.RowDefinitions.Add(new RowDefinition { Height = 50 });
                templateGrid.RowDefinitions.Add(new RowDefinition { Height = 0.5f });
                StackLayout template = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    Margin = new Thickness(10, 2, 2, 2),
                    VerticalOptions = LayoutOptions.Center
                };
                Label Title = new Label()
                {
                    FontSize = 6 * App.DpiFontSize,
                    VerticalOptions = LayoutOptions.Center,
                    VerticalTextAlignment = TextAlignment.Center
                };
                Title.SetBinding(Label.FormattedTextProperty, "MESSAGE");

                Label detailMessage = new Label()
                {
                    FontSize = 6 * App.DpiFontSize,
                    VerticalOptions = LayoutOptions.Center,
                    VerticalTextAlignment = TextAlignment.Center
                };
                detailMessage.SetBinding(Label.TextProperty, "DESCRIPTION");
                StackLayout TextJudul = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, };
                TextJudul.Children.Add(Title);
                TextJudul.Children.Add(detailMessage);
                CachedImage img1 = new CachedImage()
                {
                    HeightRequest = 50,
                    WidthRequest = 50,
                    CacheType = FFImageLoading.Cache.CacheType.Disk,
                    DownsampleToViewSize = true,
                };
                img1.Transformations.Add(new CircleTransformation());
                img1.SetBinding(CachedImage.SourceProperty, "PHOTO");
                Label jam = new Label()
                {
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    FontSize = 6 * App.DpiFontSize,
                    Margin = new Thickness(0, 0, 10, 0)
                };
                jam.SetBinding(Label.TextProperty, "TIME");
                template.Children.Add(img1);
                template.Children.Add(TextJudul);
                template.Children.Add(jam);
                templateGrid.Children.Add(template, 0, 0);
                templateGrid.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 0.5f, Margin = new Thickness(10, 0, 10, 0) }, 0, 1);
                return new ViewCell() { View = templateGrid };
            });
            return dt;
        }
        public DataTemplate FriendListNormalDataTemplate()
        {
            DataTemplate dt = new DataTemplate(() => {
                Grid templateGrid = new Grid() { BackgroundColor = Color.Transparent, IsClippedToBounds = true, VerticalOptions = LayoutOptions.CenterAndExpand };
                templateGrid.RowDefinitions.Add(new RowDefinition { Height = 50 });
                templateGrid.RowDefinitions.Add(new RowDefinition { Height = 0.5f });
                StackLayout template = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    Margin = new Thickness(10, 2, 2, 2),
                    VerticalOptions = LayoutOptions.Center
                };
                Label Title = new Label()
                {
                    FontSize = 6 * App.DpiFontSize,
                    VerticalOptions = LayoutOptions.Center,
                    VerticalTextAlignment = TextAlignment.Center
                };
                Title.SetBinding(Label.FormattedTextProperty, "MESSAGE");
                Label Description = new Label()
                {
                    FontSize = 6 * App.DpiFontSize,
                    VerticalOptions = LayoutOptions.Center,
                    VerticalTextAlignment = TextAlignment.Center
                };
                Description.SetBinding(Label.FormattedTextProperty, "DESCRIPTION");

                StackLayout TextJudul = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0 };
                StackLayout SubJudul1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
                SubJudul1.Children.Add(Title);
                TextJudul.Children.Add(SubJudul1);
                TextJudul.Children.Add(Description);

                CachedImage img1 = new CachedImage()
                {
                    HeightRequest = 50,
                    WidthRequest = 50,
                    DownsampleToViewSize = true
                };
                img1.Transformations.Add(new CircleTransformation());
                img1.SetBinding(CachedImage.SourceProperty, "PHOTO");
                Label jam = new Label()
                {
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    FontSize = 5 * App.DpiFontSize,
                    Margin = new Thickness(0, 0, 10, 0)
                };
                jam.SetBinding(Label.TextProperty, "TIME");
                template.Children.Add(img1);
                template.Children.Add(TextJudul);
                template.Children.Add(jam);
                templateGrid.Children.Add(template, 0, 0);
                templateGrid.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 0.5f, Margin = new Thickness(10, 0, 10, 0) }, 0, 1);
                return new ViewCell() { View = templateGrid };
            });
            return dt;
        }
        public DataTemplate GroupListGroupHeaderDataTemplate()
        {
            return new DataTemplate(() => {

             StackLayout iconWrapper = new StackLayout()
                {
                    Orientation = StackOrientation.Vertical,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    Margin = new Thickness(0,5,0,0),
                    Spacing = 0,
                };
                iconWrapper.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> { await PopupNavigation.PushAsync(new Splitbill.Popup.Group.CreateGroup()); }));
                XFShapeView.ShapeView shape = new XFShapeView.ShapeView() {
                    ShapeType = XFShapeView.ShapeType.Circle,
                    Color = Color.Transparent,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    BorderWidth = 2,
                    BorderColor = Color.Gray,
                    WidthRequest = 80*App.DpiFontSize,
                    HeightRequest = 80*App.DpiFontSize ,
                    Content = new Image() { Source = "Plus.png",
                    Margin = new Thickness(10),
                        Aspect = Aspect.AspectFit,
                    }
                };
            Label Title = new Label() {
                Text = "Create Group",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.End,
                Margin = new Thickness(0, 0, 0,10),
                FontSize = App.DpiFontSize* 4,
            };
                iconWrapper.Children.Add(shape);
                iconWrapper.Children.Add(Title);
                return iconWrapper;
            });
        }
    }
}
