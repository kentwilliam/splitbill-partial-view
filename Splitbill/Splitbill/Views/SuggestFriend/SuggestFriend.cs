﻿using FFImageLoading.Forms;
using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Model.Send;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Views.SuggestFriend
{
    public class SuggestFriend:ContentView
    {
        public EventHandler Eve;
        Splitbill.Model.SuggestFriend.ContacsModel contactHp;
        Grid backGrid = new Grid();
        SfListView sflist;
        Label Judul;
        List<FindFriendSendModel> ListModel = new List<FindFriendSendModel>();
        public FindFriendSendModel selectedModel;
        //dibuat untuk mengambil Data ketika modal close
        public String nama { get; set; }
        public String noHp { get; set; }


        public void initData()
        {
            Task.Run(()=> {
                Device.BeginInvokeOnMainThread(async() => {
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                });

                Splitbill.Api.Encode.SuggestFriendsJson js = new Api.Encode.SuggestFriendsJson()
                {
                    token = App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME
                };
                foreach (var contact in contactHp.Contacts)
                {
                    if (contact.Phones.Count > 0)
                    {
                        js.phone_number.Add(contact.Phones[0].Number);
                    }
                }
                try
                {
                    var resp = App._Api.GetDataResponseFromServer("user/suggest_friend", JsonConvert.SerializeObject(js));
                    var resu = JsonConvert.DeserializeObject<SuggestFriendJsonResponse>(resp.Result);
                    Device.BeginInvokeOnMainThread(()=> {
                        ListModel.Clear();
                        if (App._Api.ProcessDataOrNot(resu.message, resu.contact == null ? false : true))
                        {
                            foreach (var re in resu.contact)
                            {
                                ListModel.Add(new FindFriendSendModel()
                                {
                                    nama = re,
                                });
                            }
                            sflist.ItemsSource = null;
                            sflist.ItemsSource = ListModel;
                        }
                        else
                        {
                            App.MyHomePage.LogOut();
                        }
                    });
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(()=> {
                        App.MyHomePage.LogOut();
                    });
                    
                }

                Device.BeginInvokeOnMainThread(async () => {
                    await PopupNavigation.PopAllAsync();
                });
            });
        }
        public SuggestFriend()
        {
            contactHp = new Model.SuggestFriend.ContacsModel();
            Judul = new Label()
            {
                Text = "People \n You May know",
                FontSize =App.LargeText * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold
            };
            XFShapeView.ShapeView shape = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                CornerRadius = 15,
                Margin = new Thickness(15, 0, 15, 0),
                Color = Color.White
            };

            StackLayout search = new StackLayout() { Orientation = StackOrientation.Vertical};
            SearchBar bar = new SearchBar() { Margin = new Thickness(0, 0, 40, 0) };
            search.Children.Add(bar);

            sflist = new SfListView()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.Transparent,
                Margin = new Thickness(15, 0, 15, 0),
                ItemTemplate = new DataTemplate(()=> 
                {
                    StackLayout OuterWrapper = new StackLayout() { Orientation = StackOrientation.Vertical};
                    StackLayout mainStack = new StackLayout() { Orientation = StackOrientation.Horizontal};

                    CachedImage ciImg = new CachedImage()
                    {
                        Margin = new Thickness(10, 0, 0, 0),
                        WidthRequest = 50,
                        HeightRequest = 50,
                        Aspect = Aspect.AspectFill,
                        Source = "user72.png",
                        DownsampleToViewSize = true
                    };

                    Label Nama = new Label() {TextColor = Color.Black,VerticalOptions = LayoutOptions.CenterAndExpand};
                    Nama.SetBinding(Label.TextProperty,"nama");

                    Image iconAdd = new Image() {
                        Source = "AddFriend.png",
                        HeightRequest = 35,
                        WidthRequest = 35,
                        HorizontalOptions = LayoutOptions.EndAndExpand
                    };
                    mainStack.Children.Add(ciImg);
                    mainStack.Children.Add(Nama);
                    mainStack.Children.Add(iconAdd);
                    OuterWrapper.Children.Add(mainStack);
                    OuterWrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1});
                    return new ViewCell() { View = OuterWrapper };
                }),
                ItemSize = 65
            };

            Button SelectFriendButton = new Button()
            {
                VerticalOptions = LayoutOptions.End,
                BackgroundColor = Color.CornflowerBlue,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10),
                Text = "Add Friend",
            };
            SelectFriendButton.Clicked += SelectFriendButton_Clicked;
            search.Children.Add(sflist);
            search.Children.Add(SelectFriendButton);
            shape.Content = search;

            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            backGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Image backImage = new Image()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Source = "background.png",
                Aspect = Aspect.AspectFill
            };

            backGrid.Children.Add(backImage, 0, 0);
            Grid.SetRowSpan(backImage, 4);

            backGrid.Children.Add(Judul, 0, 0);
            backGrid.Children.Add(shape, 0, 1);
            Grid.SetRowSpan(shape, 3);
            Content = backGrid;
        }

        private async void SelectFriendButton_Clicked(object sender, EventArgs e)
        {
            //try
            //{
            //    selectedModel = sflist.SelectedItem as FindFriendSendModel;
            //    selectedModel.username = App.GlobalDataUser.USERNAME;
            //    selectedModel.token = App.GlobalDataUser.TOKEN;
            //    selectedModel.friend = selectedModel.nama;

            //    var resp = App._Api.GetDataResponseFromServer("user/add_friend",JsonConvert.SerializeObject(selectedModel));
            //    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);

            //    if (resu.message.ToLower().Contains("expired"))
            //    {
            //        await Task.Delay(200);
            //        App.MyHomePage.LogOut();
            //    }
            //    else
            //    {
            //        App.MyHomePage.showAlert(resu.message);
            //        initData();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    App.MyHomePage.showAlert("No Internet");
            //}
            
        }
    }
}
