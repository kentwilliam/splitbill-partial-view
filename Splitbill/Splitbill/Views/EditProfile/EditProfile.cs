﻿using Splitbill.Model;
using Splitbill.Model.MVVM;
using Splitbill.Model.RequestModel;
using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Splitbill.Pages;
using System.Collections.ObjectModel;
using SkiaSharp.Views.Forms;
using SkiaSharp;
using FFImageLoading.Forms;
using Splitbill.Popup.Send;
using Splitbill.Model.Send;
using Com.OneSignal;
using FFImageLoading.Transformations;

namespace Splitbill.Views.EditProfile
{
    public class EditProfile : ContentView
    {
        SKShader GradientShaderTest;
        MainPage MP;
        Editor Description;
        Entry Money;
        SfListView listItem;
        ExtendedEntry MyFriends;
        ExtendedEntry MyMoney;
        //public ObservableCollection<RequestFriend> ListTerdaftar;

        SendMoneyModel model = new SendMoneyModel();
        public void resetForm()
        {
            Money.Text = "0";
            Description.Text = "";
            //ListTerdaftar.Clear();
        }
        
        public EditProfile()
        {
            Grid mygrid = new Grid();
            mygrid.RowSpacing = 0;
            mygrid.BackgroundColor = Color.FromRgb(234,237,244);
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });


            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;
            mygrid.Children.Add(viewMelengkung,0,0);
            Grid.SetRowSpan(viewMelengkung,5);

            CachedImage ciImg1 = new CachedImage()
            {
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 90,
                HeightRequest = 90,
                Margin = new Thickness(0, 10, 0, 0),
                DownsampleToViewSize = true
            };
            ciImg1.Transformations.Add(new CircleTransformation());
            ciImg1.SetBinding(CachedImage.SourceProperty, "PHOTO");
            ciImg1.BindingContext = App.GlobalDataUser;

            mygrid.Children.Add(ciImg1, 0, 0);
            Grid.SetRowSpan(ciImg1, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize * 8, FontAttributes = FontAttributes.Bold ,TextColor = Color.White};
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;

            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize =  App.DpiFontSize * 8, VerticalTextAlignment = TextAlignment.Center ,TextColor = Color.White};
            jumlah.SetBinding(Label.TextProperty, "EMAIL");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);


            StackLayout DetailInfo = new StackLayout() {
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.White,
                Margin = new Thickness(10),
                Spacing = 5
            };
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };tab1.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(OpenEditProfile)});
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal}; tab3.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(OpenEditPassword) });

            #region tab1
            Label To = new Label()
            {
                Text = " Edit Profile",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center
            };
            CachedImage plusFriend = new CachedImage() {
                Aspect = Aspect.AspectFit,
                Source = "EditProfile.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest= App.DpiFontSize * 15,
                DownsampleToViewSize = true
            };
            tab1.Children.Add(plusFriend);
            tab1.Children.Add(To);
            DetailInfo.Children.Add(tab1);

            #endregion
            BoxView separator1 = new BoxView() { BackgroundColor = Color.LightGray ,HeightRequest = 1f};
            DetailInfo.Children.Add(separator1);
            #region tab2
            Label To1 = new Label()
            {
                Text = " Notification",
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            CachedImage notifIcon = new CachedImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "Notification.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                DownsampleToViewSize = true
            };
            Switch sw = new Switch() { HorizontalOptions = LayoutOptions.End};
            sw.Toggled += ((var, e) =>
            {
                if (e.Value)
                {
                    OneSignal.Current.SetSubscription(true);
                    var user = App.MyUserDatabase.GetUser();
                    App.MyUserDatabase.DeleteUser(user);
                    user.Notification = true;
                    App.MyUserDatabase.SaveUser(user);
                }
                else
                {
                    OneSignal.Current.SetSubscription(false);
                    var user = App.MyUserDatabase.GetUser();
                    App.MyUserDatabase.DeleteUser(user);
                    user.Notification = false;
                    App.MyUserDatabase.SaveUser(user);
                }
            });
            sw.SetBinding(Switch.IsToggledProperty, "NOTIFICATIONACTIVE");
            sw.BindingContext = App.GlobalDataUser;

            tab2.Children.Add(notifIcon);
            tab2.Children.Add(To1);
            tab2.Children.Add(sw);
            DetailInfo.Children.Add(tab2);

            #endregion
            BoxView separator2 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f };
            DetailInfo.Children.Add(separator2);
            #region tab3
            Label To2 = new Label()
            {
                Text = " Change Password",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center
            };
            CachedImage changePasswordIcon = new CachedImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "ChangePassword.png",
                WidthRequest = App.DpiFontSize*15,
                HeightRequest = App.DpiFontSize*15,
                DownsampleToViewSize = true
            };
            tab3.Children.Add(changePasswordIcon);
            tab3.Children.Add(To2);
            DetailInfo.Children.Add(tab3);

            #endregion
            BoxView separator3 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f,VerticalOptions = LayoutOptions.EndAndExpand};
            DetailInfo.Children.Add(separator3);

            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Color = Color.White,
                CornerRadius = 15,
                Margin = new Thickness(15,10,15,0),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            wrapper.Content = DetailInfo;
            mygrid.Children.Add(wrapper, 0, 5);
            Grid.SetRowSpan(wrapper, 11);
            Content = mygrid;
        }

        private async void OpenEditPassword(object obj)
        {
            Popup.EditProfile.PopupEditPassword pass = new Popup.EditProfile.PopupEditPassword();
            await PopupNavigation.PushAsync(pass);
        }

        private async void OpenEditProfile(object obj)
        {
            Popup.EditProfile.PopupEditProfile pep = new Popup.EditProfile.PopupEditProfile();
            await PopupNavigation.PushAsync(pep);
        }

        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            //path.LineTo(0, e.Info.Height-100);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
            //canvas.DrawPath(path, new SKPaint { Shader = GambarShaderTest });
        }
    }
}