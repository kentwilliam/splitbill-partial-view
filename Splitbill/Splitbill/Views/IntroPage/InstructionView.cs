﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill.Views.IntroPage
{
	public class InstructionView : ContentView
	{
        public String Source { get; set; }
        public InstructionView (String path,String buttonText,Color ButtonColor)
		{
            Grid mygrid = new Grid();
            Button Next = new Button() {
                Text = buttonText,
                BackgroundColor = ButtonColor,
                VerticalOptions = LayoutOptions.End,
                Margin = new Thickness(0,0,0,15)
            };


            Image background = new Image() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Source = path
            };


            mygrid.Children.Add(background);
            mygrid.Children.Add(Next);
			Content = mygrid;
		}
       
	}
}