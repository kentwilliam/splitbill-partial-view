﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill.Views.IntroPage
{
	public class IntroImageView : ContentView
	{
        public EventHandler Eve;
		public IntroImageView (String src,String ButtonText)
		{
            Grid MyGrid = new Grid();
            MyGrid.HorizontalOptions = LayoutOptions.FillAndExpand;
            MyGrid.VerticalOptions = LayoutOptions.FillAndExpand;
            Button NextButton = new Button()
            {
                BackgroundColor = Color.IndianRed,
                Text = ButtonText,
                VerticalOptions = LayoutOptions.End,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Margin = new Thickness(0,0,0,10)
            };

            Image Background = new Image() { Source = src,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            NextButton.Clicked += NextButton_Clicked;
            MyGrid.Children.Add(Background);
            MyGrid.Children.Add(NextButton);
            Content = MyGrid;
		}

        private void NextButton_Clicked(object sender, EventArgs e)
        {
            Eve(new object(),EventArgs.Empty);
        }
    }
}