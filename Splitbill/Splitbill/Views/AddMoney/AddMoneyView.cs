﻿using Splitbill.Model;
using Splitbill.Model.MVVM;
using ImageCircle.Forms.Plugin.Abstractions;
using Syncfusion.DataSource;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Rg.Plugins.Popup.Services;
using SkiaSharp.Views.Forms;
using SkiaSharp;
using Splitbill.Api.Core;
using Newtonsoft.Json;
using Splitbill.Api.Decode.Mandiri;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;

namespace Splitbill.Views.AddMoney
{
    public class AddMoneyView : ContentView
    {
        Grid mygrid;
        StackLayout ContentAddMoney;
        ContactsViewModel viewModel;
        SKShader GradientShaderTest;
        Button ConnectToMandiri;
        public async void InitData()
        {
            //await Task.Delay(400);
            //ConnectToMandiri_Clicked(new object(),EventArgs.Empty);
        }
        public AddMoneyView()
        {
            ContentAddMoney = new StackLayout() { Orientation = StackOrientation.Vertical };
            mygrid = new Grid();
            mygrid.RowSpacing = 0;
            mygrid.BackgroundColor = Color.FromRgb(234, 237, 244);
            for (int i = 0; i < 16; i++)
            {
                mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;

            mygrid.Children.Add(viewMelengkung, 0, 0);
            Grid.SetRowSpan(viewMelengkung, 5);

            CachedImage ciImg = new CachedImage()
            {
                HorizontalOptions = LayoutOptions.Center,
                Margin = new Thickness(0, 10, 0, 0),
                WidthRequest = 90,
                HeightRequest = 90,
                DownsampleToViewSize = true
            };
            ciImg.Transformations.Add(new CircleTransformation());
            ciImg.SetBinding(CachedImage.SourceProperty, "PHOTO");
            ciImg.BindingContext = App.GlobalDataUser;

            mygrid.Children.Add(ciImg, 0, 0);
            Grid.SetRowSpan(ciImg, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = 8*App.DpiFontSize , FontAttributes = FontAttributes.Bold ,TextColor = Color.White};
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;
            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = 8*App.DpiFontSize , VerticalTextAlignment = TextAlignment.Center ,TextColor = Color.White};
            jumlah.SetBinding(Label.TextProperty, "BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);

            XFShapeView.ShapeView box = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10,10,10,0),
                Color = Color.White,
                CornerRadius = 10,
            };

            //Transaction
            StackLayout tab3stack = new StackLayout();
            tab3stack.Orientation = StackOrientation.Horizontal;

            Label TransactionTitle = new Label();
            TransactionTitle.Text = "  Available Bank";
            TransactionTitle.FontAttributes = FontAttributes.Bold;
            TransactionTitle.FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium, typeof(Label));
            TransactionTitle.TextColor = Color.FromRgb(0, 162, 232);
            TransactionTitle.VerticalTextAlignment = TextAlignment.Center;
            TransactionTitle.HorizontalOptions = LayoutOptions.Start;
            TransactionTitle.Margin = new Thickness(15,10, 0, 0);
            TransactionTitle.VerticalOptions = LayoutOptions.Center;
            
            tab3stack.Children.Add(TransactionTitle);

            MandiriLayoutNotConnected();
            //MandiriLayoutConnected();

            box.Content = ContentAddMoney;
            mygrid.Children.Add(box,0,5);
            Grid.SetRowSpan(box,11);
            mygrid.BackgroundColor = Color.FromRgb(234, 237, 244);
            Content = mygrid;
            
        }
        private void MandiriLayoutNotConnected()
        {
            ContentAddMoney.Children.Clear();
            //MEMEMEMEMEMMEMEMEMEMMEEMMEMEMEMEMMEEMMEMEMEMMEMEMEEMMEEMEMEMEMEMEMMEMEMEMEMEMMEMEMEMEMMEEMMEMEMEMEME
            Image mandiriBG = new Image()
            {
                Source = "Mandiri.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Aspect = Aspect.AspectFit,
                HeightRequest = 50,
                Margin = new Thickness(5, 10, 5, 0),
            };
            ConnectToMandiri = new Button()
            {
                Text = "Connect To Mandiri",
                FontSize = App.NormalText * Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                BackgroundColor = Color.CornflowerBlue,
                HeightRequest = 45,
                Margin = new Thickness(30, 10, 30, 0),
                TextColor = Color.White
            };
            Label TextDetail1 = new Label()
            {
                Text = "Make Sure that your phone number \n already registered in Mandiri E-Cash",
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = (App.NormalText - 0.1f) * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                TextColor = Color.LightGray
            };
            BoxView grayBox1 = new BoxView()
            {
                BackgroundColor = Color.LightGray,
                HeightRequest = 1,
                Margin = new Thickness(10, 0, 10, 0)
            };
            ConnectToMandiri.Clicked += ConnectToMandiri_Clicked;
            ContentAddMoney.Children.Add(mandiriBG);
            ContentAddMoney.Children.Add(grayBox1);
            ContentAddMoney.Children.Add(ConnectToMandiri);
            ContentAddMoney.Children.Add(TextDetail1);
            //MEMEMEMEMEMEEMMEMEMEMEMEMEMEMEMEMMEMEMEMMEEMEEEMMEMEMEMEEMMMEMEMEEEMMEEMEMEMMEMEMEMEMEMEMEMEMEMEMEME
        }
        private void MandiriLayoutConnected()
        {
            ContentAddMoney.Children.Clear();
            Image mandiriBG = new Image()
            {
                Source = "Mandiri.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Aspect = Aspect.AspectFit,
                HeightRequest = 50,
                Margin = new Thickness(5, 10, 5, 0),
            };
            BoxView grayBox1 = new BoxView()
            {
                BackgroundColor = Color.LightGray,
                HeightRequest = 1,
                Margin = new Thickness(10, 0, 10, 0)
            };
            Label TextDetail1 = new Label()
            {
                Text = "You Are Connected With \n Mandiri E-Cash",
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = (App.NormalText) * Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                TextColor = Color.CornflowerBlue
            };
            StackLayout StackButton = new StackLayout() { Orientation = StackOrientation.Horizontal ,Margin = new Thickness(15,10,15,0)};
            Button Redeem = new Button() {
                Text = "REDEEM",
                FontSize = (App.NormalText-0.1f) * Device.GetNamedSize(NamedSize.Medium,typeof(Button)),
                TextColor = Color.White,
                BackgroundColor = Color.CornflowerBlue,
                HeightRequest = 50,
                WidthRequest = 150,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            Redeem.Clicked += (async(v,e) => {
                Splitbill.Popup.AddMoney.AddMoneyRedeem red = new Popup.AddMoney.AddMoneyRedeem();
                await PopupNavigation.PushAsync(red);
            });
            Button TopUp = new Button()
            {
                Text = "Top Up",
                FontSize = (App.NormalText-0.1f)* Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                TextColor = Color.White,
                BackgroundColor = Color.CornflowerBlue,
                HeightRequest = 50,
                WidthRequest = 150,
                HorizontalOptions = LayoutOptions.EndAndExpand
            };
            TopUp.Clicked += (async (v, e) => {
                Splitbill.Popup.AddMoney.AddMoneyTopup top = new Popup.AddMoney.AddMoneyTopup();
                await PopupNavigation.PushAsync(top);
            });
            StackButton.Children.Add(Redeem);
            StackButton.Children.Add(TopUp);
            ContentAddMoney.Children.Add(mandiriBG);
            ContentAddMoney.Children.Add(grayBox1);
            ContentAddMoney.Children.Add(TextDetail1);
            ContentAddMoney.Children.Add(StackButton);
        }
        private async void ConnectToMandiri_Clicked(object sender, EventArgs e)
        {
            try
            {
                Username_Token ut = new Username_Token()
                {
                    token = App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME
                };
                var resp = App._Api.GetDataResponseFromServer("Mandiri_API/mandiri_ecash_connect", JsonConvert.SerializeObject(ut));
                var resu = JsonConvert.DeserializeObject<ConnectJsonRespond>(resp.Result);
                if (!String.IsNullOrEmpty(resu.message))
                {
                    if (resu.message.ToLower().Contains("expired"))
                    {
                        App.MyHomePage.LogOut();
                    }
                    else if (resu.message.ToLower().Contains("is already connected"))
                    {
                        ActiveConnectMandiri();
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(resu.status) && resu.status == "PROCESSED")
                    {
                        String URL = "http://188.166.220.62/h2h-register/Main/register?ticket=" + resu.ticket;
                        Pagete MandiriPage = new Pagete(URL);
                        MandiriPage.Eve += CloseMandiriPage;
                        await Navigation.PushModalAsync(MandiriPage);
                    }
                    else
                    {
                        App.MyHomePage.ShowAlert(resu.message);
                    }
                }
                
            }
            catch (Exception ex)
            { App.MyHomePage.ShowAlert("No Internet"); }
           await App.MyHomePage.HideLoadingPage();
        }
        private async void CloseMandiriPage(object sender, EventArgs e)
        {
            if ((sender as Pagete).isConnected)
            {
                await App.MyHomePage.ShowLoadingPage();
                try
                {
                    Username_Token Ut = new Username_Token() { username = App.GlobalDataUser.USERNAME, token = App.GlobalDataUser.TOKEN };
                    var resp = App._Api.GetDataResponseFromServer("Mandiri_API/verify_mandiri_ecash_connect", JsonConvert.SerializeObject(Ut));
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    if (!String.IsNullOrEmpty(resu.message))
                    {
                        if (resu.message.ToLower() == "expired")
                        {
                            App.Current.MainPage = new Pages.Login.LoginPage();
                        }
                    }
                    else if (resu.status == "true")
                    {
                        ActiveConnectMandiri();
                    }
                    else
                    {
                        App.MyHomePage.ShowAlert(resu.message);
                    }
                }
                catch (Exception ex){}
                await App.MyHomePage.HideLoadingPage();
            }
            await Navigation.PopModalAsync();
        }
        private void ActiveConnectMandiri()
        {
            MandiriLayoutConnected();
        }
        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            //path.LineTo(0, e.Info.Height-100);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
            //canvas.DrawPath(path, new SKPaint { Shader = GambarShaderTest });
        }
    }
}