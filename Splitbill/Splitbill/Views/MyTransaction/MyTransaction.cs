﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Model.MyTransaction;
using Syncfusion.DataSource;
using Syncfusion.DataSource.Extensions;
using Syncfusion.ListView.XForms;
using Syncfusion.XForms.TabView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Views.MyTransaction
{
	public class MyTransaction : ContentView
	{
        DataTemplates.MyTransaction.DataTemplateMyTransaction DtMyTransaction = new DataTemplates.MyTransaction.DataTemplateMyTransaction();
        StackLayout Balances;
        ScrollView Sv;
        StackLayout MainWrapper;
        ScrollView MainWrapper4;
        SfTabView sftab;
        Button PrevButton;
        SfListView listCompleted;
        SfListView listPending;
        SfListView listBalances;
        SfListView listSend_ReceiveMoney;
        ObservableCollection<Model.MyTransaction.Completed> completeModel;
        ObservableCollection<Model.MyTransaction.Pending> pendingModel;
        ObservableCollection<Model.MyTransaction.Balance> balanceModel;
        ObservableCollection<Model.MyTransaction.Send_MoneyModel> sendMoneyModel;
        public void InitData()
        {
            Task.Run(() =>
            {
                List<Model.MyTransaction.Pending> TempListPendingModel = new List<Model.MyTransaction.Pending>();
                Device.BeginInvokeOnMainThread(async() =>
                {
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });

                var resp = App._Api.GetDataResponseFromServer("transaction/my_transaction_history", JsonConvert.SerializeObject(new Username_Token()
                {
                    token = App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME
                }));
                String result = resp.Result;
                if (result.ToLower() != "no internet")
                {
                    var resu = JsonConvert.DeserializeObject<TransactionHistoryJsonResponse>(result);
                    if (App._Api.ProcessDataOrNot(resu.message, resu.log == null ? false : true))
                    {
                        var completedData = resu.log.complete;
                        var pendingData = resu.log.pending;
                        var send_money = resu.log.send_money;
                        #region Masukan Data Completed
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            completeModel.Clear();
                            pendingModel.Clear();
                            balanceModel.Clear();
                            if (completedData.Count > 0)
                            {
                                foreach (var item in completedData)
                                {
                                    Splitbill.Model.MyTransaction.Completed data = new Model.MyTransaction.Completed()
                                    {
                                        IDTRANSAKSI = item.id,
                                        NAMA = item.id_teman,
                                    };
                                    FormattedStringModel fsm = new FormattedStringModel() {after = item.after,before = item.before,bold = item.bold,id_teman = item.id_teman,posisi = item.posisi };
                                    data.MESSAGE = FormattedStringHelper(fsm);
                                    data.TOTAL = item.amount.ToString();
                                    data.PHOTO = String.IsNullOrEmpty(item.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + item.photo;
                                    data.TIME = item.time;
                                    data.DESCRIPTION = item.description;
                                    data.GROUPID =String.IsNullOrEmpty(item.id_parent)?item.id:item.id_parent;
                                    if (item.bold.ToLower().Contains("send"))
                                    {
                                        data.ORIGIN = "snd";
                                    }
                                    else
                                    {
                                        data.ORIGIN = "req";
                                    }
                                    completeModel.Add(data);
                                }
                                listCompleted.HeightRequest = completeModel.Count * 75;
                                listCompleted.DataSource.GroupDescriptors.Clear();
                                listCompleted.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
                                {
                                    PropertyName = "namagroup",
                                    KeySelector = (object obj1) =>
                                    {
                                        var item = obj1 as Splitbill.Model.MyTransaction.Completed;
                                        return item.GROUPID;
                                    },
                                });
                                listCompleted.CollapseAll();
                            }
                            else
                            {
                                listCompleted.ItemTemplate = new DataTemplate(() =>
                                {
                                    Grid G = new Grid()
                                    {
                                        HorizontalOptions = LayoutOptions.FillAndExpand,
                                        VerticalOptions = LayoutOptions.FillAndExpand
                                    };
                                    Label lbl = new Label()
                                    {
                                        FontAttributes = FontAttributes.Bold,
                                        TextColor = Color.LightGray,
                                        Text = "No Transaction",
                                        HorizontalTextAlignment = TextAlignment.Center,
                                        VerticalTextAlignment = TextAlignment.Center
                                    };

                                    G.Children.Add(lbl);
                                    return G;
                                });
                                listCompleted.ItemsSource = "1";
                                listCompleted.ItemSize = 125;
                            }
                        });
                        #endregion
                        #region Masukan Data Pending
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (pendingData.Count > 0)
                            {
                                foreach (var item in pendingData)
                                {
                                    if (item.bold.ToLower().Trim() != "request")
                                    {
                                        Splitbill.Model.MyTransaction.Pending dataP = new Model.MyTransaction.Pending()
                                        {
                                            NAMA = item.id_teman,
                                            REQUESTID = item.id,
                                            GROUPID = item.id_parent,
                                            MESSAGE = FormattedStringHelper(new FormattedStringModel() {posisi = item.posisi,id_teman = item.id_teman,bold = item.bold,before = item.before,after = item.after }),
                                            PHOTO = String.IsNullOrEmpty(item.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + item.photo,
                                            DESTINATION = item.place_id,
                                        };
                                        pendingModel.Add(dataP);
                                    }
                                    else
                                    {
                                        Splitbill.Model.MyTransaction.Balance dataP = new Model.MyTransaction.Balance()
                                        {
                                            NAMA = item.id_teman,
                                            IDTRANSAKSI = item.id,
                                            TIME = item.time,
                                        };
                                        dataP.MESSAGE = FormattedStringHelper(new FormattedStringModel() {after = item.after,before = item.before,bold = item.bold,id_teman = item.id_teman,posisi = item.posisi });
                                        dataP.PHOTO = String.IsNullOrEmpty(item.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + item.photo;
                                        balanceModel.Add(dataP);
                                    }
                                }
                                Sv.Content = MainWrapper;
                            }
                            
                            if (balanceModel.Count < 1)
                            {
                                listBalances.ItemTemplate = new DataTemplate(() =>
                                {
                                    Grid G = new Grid()
                                    {
                                        HorizontalOptions = LayoutOptions.FillAndExpand,
                                        VerticalOptions = LayoutOptions.FillAndExpand
                                    };
                                    Label lbl = new Label()
                                    {
                                        FontAttributes = FontAttributes.Bold,
                                        TextColor = Color.LightGray,
                                        Text = "No Transaction",
                                        HorizontalTextAlignment = TextAlignment.Center,
                                        VerticalTextAlignment = TextAlignment.Center
                                    };

                                    G.Children.Add(lbl);
                                    return G;
                                });
                                listBalances.ItemsSource = null;
                                listBalances.ItemsSource = "1";
                                listBalances.ItemSize = 125;
                                listBalances.HeightRequest = 300;
                                listBalances.RefreshView();
                            }
                            else
                            {
                                listBalances.ItemsSource = null;
                                listBalances.ItemsSource = balanceModel;
                                listBalances.HeightRequest = balanceModel.Count() * 75;
                                listBalances.RefreshView();
                                listBalances.Refreshed();
                                listBalances.Focus();
                            }
                            if (pendingModel.Count < 1)
                            {
                                listPending.ItemTemplate = new DataTemplate(() =>
                                {
                                    Grid G = new Grid()
                                    {
                                        HorizontalOptions = LayoutOptions.FillAndExpand,
                                        VerticalOptions = LayoutOptions.FillAndExpand
                                    };
                                    Label lbl = new Label()
                                    {
                                        FontAttributes = FontAttributes.Bold,
                                        TextColor = Color.LightGray,
                                        Text = "No Transaction",
                                        HorizontalTextAlignment = TextAlignment.Center,
                                        VerticalTextAlignment = TextAlignment.Center
                                    };

                                    G.Children.Add(lbl);
                                    return G;
                                });
                                listPending.ItemsSource = "1";
                                listPending.ItemSize = 125;
                            }
                            else
                            {
                                listPending.ItemsSource = null;
                                listPending.ItemsSource = pendingModel;
                                listPending.HeightRequest = 75 * pendingModel.Count;
                                listPending.DataSource.GroupDescriptors.Clear();
                                listPending.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
                                {
                                    PropertyName = "namagroup",
                                    KeySelector = (object obj1) =>
                                    {
                                        var item = obj1 as Splitbill.Model.MyTransaction.Pending;
                                        return item.GROUPID;
                                    },
                                });
                                listPending.Refreshed();
                                listPending.RefreshView();
                                listPending.CollapseAll();
                            }

                        });
                        #endregion
                        #region Data SEnd_Money
                        List<Send_MoneyModel> tempSend_Money = new List<Send_MoneyModel>();
                        foreach (var item in send_money)
                        {
                            tempSend_Money.Add(new Send_MoneyModel() {
                                IDTRANSAKSI = item.id,
                                MESSAGE = FormattedStringHelper(new FormattedStringModel() { after = item.after,before = item.before,bold = item.bold,id_teman = item.id_teman,posisi = item.posisi}),
                                NAMA = item.id_teman,
                                PHOTO = String.IsNullOrEmpty(item.photo)?"User72.png":"http://www.urunan.id/splitbill/" + item.photo,
                                TIME = item.time,
                                DESCRIPTION = item.description,
                                TOTAL = item.amount,
                            });
                        }
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            sendMoneyModel = tempSend_Money.ToObservableCollection<Send_MoneyModel>();
                            listSend_ReceiveMoney.ItemsSource = sendMoneyModel;
                            listSend_ReceiveMoney.HeightRequest = sendMoneyModel.Count()> 4 ? 4*75:sendMoneyModel.Count() * 75;
                            listSend_ReceiveMoney.Refreshed();
                            listSend_ReceiveMoney.RefreshView();
                            listSend_ReceiveMoney.Focus();
                        });
                        #endregion
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            App.MyHomePage.LogOut();
                        });
                    }
                }
                else
                {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            App.MyHomePage.ShowAlert("No Internet Connection");
                        });

                }
                
                Device.BeginInvokeOnMainThread(async() =>
                {
                    await Task.Delay(300);
                    try
                    { await PopupNavigation.PopAsync(); }
                    catch (Exception ex) { }
                    
                });
            });
        }
        public MyTransaction()
		{
            completeModel = new ObservableCollection<Model.MyTransaction.Completed>();
            pendingModel = new ObservableCollection<Model.MyTransaction.Pending>();
            balanceModel = new ObservableCollection<Model.MyTransaction.Balance>();

            Grid mygrid = new Grid() { BackgroundColor = Color.FromRgb(234, 237, 244)};
            mygrid.RowSpacing = 0;
            for (int i = 0; i < 20; i++)
            {
                mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            #region Membuat Bagian Header
            Label judulPage = new Label()
            {
                VerticalOptions = LayoutOptions.Start,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = 10 * App.DpiFontSize,
                Text = "My Transaction"
            };
            Image background = new Image() {
                Source = "mytransactionBG.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Aspect = Aspect.AspectFill
            };
            Frame frameButtonLayout = new Frame()
            {
                HorizontalOptions = LayoutOptions.Center,
                IsClippedToBounds = true,
                CornerRadius = 5,
                Margin = new Thickness(0,0,0,20),
                VerticalOptions = LayoutOptions.Center,
                Padding = 0,
                BackgroundColor = Color.Transparent
            };
            StackLayout ButtonLayout = new StackLayout() {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                Spacing = 0,
                BackgroundColor = Color.FromRgba(255, 255, 255, 0.5),
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button bPending = new Button() {
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                FontSize = 6 * App.DpiFontSize,
                WidthRequest = 100,
                Text = "Pending",
            };
            bPending.Clicked += BPending_Clicked;

            Button bCompleted = new Button()
            {
                WidthRequest = 100,
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                FontSize = 6 * App.DpiFontSize,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Completed"
            };
            bCompleted.Clicked += BCompleted_Clicked;

            BoxView bx = new BoxView() {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.Fill,WidthRequest=0.5f
            ,Margin = new Thickness(0,2,0,2)};

            ButtonLayout.Children.Add(bPending);
            ButtonLayout.Children.Add(bx);
            ButtonLayout.Children.Add(bCompleted);
            #endregion

            #region Page 1
            Balances = new StackLayout() { Margin = new Thickness(15, 5, 15, 0) };
            #region buat bagian Balances

            Label Judul1 = new Label() {
                Text = "Balances",
                FontSize = 8 * App.DpiFontSize,
                TextColor = Color.CornflowerBlue,
                FontAttributes = FontAttributes.Bold
            };

            Frame F1 = new Frame() { Margin = 0,Padding = 0,CornerRadius = 5,IsClippedToBounds = true};
            listBalances = new SfListView() {
                SelectionBackgroundColor = Color.Transparent,
                ItemSize = 75,
                HeightRequest = 300,
                BackgroundColor = Color.White,
                ItemTemplate = DtMyTransaction.GetListBalancesDataTemplate(),
            };
            listBalances.ItemsSource = balanceModel;
            listBalances.ItemTapped += ListBalance_ItemTapped;
            
            F1.IsClippedToBounds = true;
            F1.Content = listBalances;

            Balances.Children.Add(Judul1);
            Balances.Children.Add(F1);
            
            #endregion

            StackLayout Pending = new StackLayout() { Margin = new Thickness(15, 0, 15, 0) };
            Label Judul2 = new Label()
            {
                Text = "Pending",
                FontSize = 8*App.DpiFontSize,
                TextColor = Color.CornflowerBlue,
                FontAttributes = FontAttributes.Bold
            };

            Frame F2 = new Frame() { Margin = 0, Padding = 0, CornerRadius = 5, IsClippedToBounds = true };
            listPending = new SfListView()
            {
                AllowGroupExpandCollapse = true,
                SelectionBackgroundColor = Color.Transparent,
                ItemSize = 75,
                HeightRequest = 300,
                BackgroundColor = Color.White,
                GroupHeaderSize = 71
            };

            listPending.ItemTemplate = DtMyTransaction.GetListPendingDatatTemplate() ;
            listPending.GroupHeaderTemplate = DtMyTransaction.GetListPendingGroupHeaderDataTemplate();

            listPending.ItemsSource = pendingModel;
            listPending.ItemTapped += ListPending_ItemTapped;
            F2.Content = listPending;

            Pending.Children.Add(Judul2);
            Pending.Children.Add(F2);

            MainWrapper = new StackLayout();
            MainWrapper.Children.Add(Balances);
            MainWrapper.Children.Add(Pending);

            Sv = new ScrollView() { Content = MainWrapper};
            #endregion

            #region Page 2
            StackLayout Balances3 = new StackLayout() { Margin = new Thickness(15, 5, 15, 0) };
            #region Buat Bagian Send_ReceiveMoney
            Label JudulSection1 = new Label()
            {
                Text = "Send/Request Money",
                FontSize = 8 * App.DpiFontSize,
                TextColor = Color.CornflowerBlue,
                FontAttributes = FontAttributes.Bold
            };
            Frame FrameListSend_ReceiveMoney = new Frame() { Margin = 0, Padding = 0, CornerRadius = 5, IsClippedToBounds = true };
            listSend_ReceiveMoney = new SfListView() {
                ItemSize = 75,
                HeightRequest = 300,
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                SelectionBackgroundColor = Color.Transparent,
                ItemTemplate =DtMyTransaction.GetListListSendReceiveMoneyDataTemplate() ,
                IsClippedToBounds = true
            };
            listSend_ReceiveMoney.ItemTapped += Listsend_receivemoneyItemTapped;
            FrameListSend_ReceiveMoney.Content = listSend_ReceiveMoney;
            Balances3.Children.Add(JudulSection1);
            Balances3.Children.Add(FrameListSend_ReceiveMoney);
            #endregion

            #region buat bagian Balances

            Label Judul3 = new Label()
            {
                Text = "Completed",
                FontSize = 8 * App.DpiFontSize,
                TextColor = Color.CornflowerBlue,FontAttributes = FontAttributes.Bold
            };

            Frame F3 = new Frame() {Margin = 0, Padding = 0, CornerRadius = 5, IsClippedToBounds = true };
            listCompleted = new SfListView()
            {
                AllowGroupExpandCollapse = true,
                ItemSize = 75,
                HeightRequest = 300,
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                SelectionBackgroundColor = Color.Transparent,
                GroupHeaderTemplate = DtMyTransaction.GetListCompletedGroupHeaderDataTemplate() ,
                ItemTemplate = DtMyTransaction.GetListCompletedDataTemplate(),
                GroupHeaderSize = 71
            };
            listCompleted.BindingContext = completeModel;
            listCompleted.ItemsSource = completeModel;
            listCompleted.ItemTapped += ListCompletedClick;
            F3.IsClippedToBounds = true;
            F3.Content = listCompleted;

            Balances3.Children.Add(Judul3);
            Balances3.Children.Add(F3);

            #endregion
            MainWrapper4 = new ScrollView() { TranslationX = 500 };
            MainWrapper4.Content = Balances3;

            #endregion
            mygrid.Children.Add(Sv, 0,7);
            mygrid.Children.Add(MainWrapper4, 0, 7);
            mygrid.Children.Add(background,0,0);
            mygrid.Children.Add(judulPage, 0, 1);

            frameButtonLayout.Content = ButtonLayout;
            mygrid.Children.Add(frameButtonLayout, 0,5);

            Grid.SetRowSpan(Sv, 14);
            Grid.SetRowSpan(MainWrapper4,14);
            Grid.SetRowSpan(background, 7);
            Grid.SetRowSpan(judulPage, 7);
            Grid.SetRowSpan(frameButtonLayout, 2);
            //Grid.SetRowSpan(Balances,6);
            Grid.SetRowSpan(Pending,10);

            Content = mygrid;
            DtMyTransaction.listPending = listPending;
            DtMyTransaction.listCompleted = listCompleted;
        }

        private async void ListPending_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (e.ItemType != ItemType.GroupHeader)
            {
                (sender as SfListView).IsEnabled = false;
                string idtransaksi = (e.ItemData as Model.MyTransaction.Pending).REQUESTID;
                Api.Encode.SendNotificationDetailJson js = new Api.Encode.SendNotificationDetailJson()
                {
                    username = App.GlobalDataUser.USERNAME,
                    token = App.GlobalDataUser.TOKEN,
                    origin_id = idtransaksi,
                    origin = "req"
                };
                Popup.MyTransaction.Pending.MyTransactionPendingOpen dialog = new Popup.MyTransaction.Pending.MyTransactionPendingOpen(js);
                await PopupNavigation.PushAsync(dialog);
                (sender as SfListView).IsEnabled = true;
            }
            else
            {
                
            }
        }

        private async void ListBalance_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (e.ItemType != ItemType.GroupHeader)
            {
                (sender as SfListView).IsEnabled = false;
                string idtransaksi = (e.ItemData as Model.MyTransaction.Balance).IDTRANSAKSI;
                Api.Encode.SendNotificationDetailJson js = new Api.Encode.SendNotificationDetailJson()
                {
                    username = App.GlobalDataUser.USERNAME,
                    token = App.GlobalDataUser.TOKEN,
                    origin_id = idtransaksi,
                    origin = "req"
                };
                Popup.MyTransaction.Balance.MyTransactionBalanceOpen dialog = new Popup.MyTransaction.Balance.MyTransactionBalanceOpen(js);
                await PopupNavigation.PushAsync(dialog);
                (sender as SfListView).IsEnabled = true;
            }
            
        }

        private async void BCompleted_Clicked(object sender, EventArgs e)
        {
            if (PrevButton == null)
            {
                PrevButton = sender as Button;
                (sender as Button).FontAttributes = FontAttributes.Bold;
            }
            else
            {
                PrevButton.FontAttributes = FontAttributes.None;
                (sender as Button).FontAttributes = FontAttributes.Bold;
                PrevButton = sender as Button;
            }

            Sv.TranslateTo(-500, 0, 250);
            MainWrapper4.TranslateTo(0, 0, 250);
        }
        private async void ListCompletedClick(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (e.ItemType != ItemType.GroupHeader)
            {
                var data = (e.ItemData as Splitbill.Model.MyTransaction.Completed);
                
                    Api.Encode.SendNotificationDetailJson js = new Api.Encode.SendNotificationDetailJson()
                    {
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME,
                        origin = "req",
                        origin_id = data.IDTRANSAKSI
                    };
                    Popup.MyTransaction.Completed.MyTransactionCompletedOpen pop = new Popup.MyTransaction.Completed.MyTransactionCompletedOpen(js);
                    await PopupNavigation.PushAsync(pop);
            }
        }


        private async void Listsend_receivemoneyItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            var data = e.ItemData as Send_MoneyModel;
            Popup.MyTransaction.Completed.MyTransactionCompleted pop = new Popup.MyTransaction.Completed.MyTransactionCompleted(data.MESSAGE + " Rp." + data.TOTAL, data.DESCRIPTION);
            await PopupNavigation.PushAsync(pop);
        }
        private async void BPending_Clicked(object sender, EventArgs e)
        {
            if (PrevButton == null)
            {
                PrevButton = sender as Button;
                (sender as Button).FontAttributes = FontAttributes.Bold;
            }
            else
            {
                PrevButton.FontAttributes = FontAttributes.None;
                (sender as Button).FontAttributes = FontAttributes.Bold;
                PrevButton = sender as Button;
            }

            MainWrapper4.TranslateTo(500, 0, 250);            
            Sv.TranslateTo(0, 0, 250);
        }
        private FormattedString FormattedStringHelper(FormattedStringModel item)
        {
            FormattedString form = new FormattedString();
            if (item.posisi == -1)
            {
                form.Spans.Add(new Span()
                {
                    Text = item.before + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.bold + " ",
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.after + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
            }
            if (item.posisi == 0)
            {
                form.Spans.Add(new Span()
                {
                    Text = item.id_teman + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.before + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.bold + " ",
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.after + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
            }
            if (item.posisi == 1)
            {
                form.Spans.Add(new Span()
                {
                    Text = item.before + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.id_teman + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.bold + " ",
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.after + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
            }
            if (item.posisi == 2)
            {
                form.Spans.Add(new Span()
                {
                    Text = item.before + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.bold + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.id_teman + " ",
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.after + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
            }
            if (item.posisi == 3)
            {
                form.Spans.Add(new Span()
                {
                    Text = item.before + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.bold + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.after + " ",
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 6 * App.DpiFontSize,
                });
                form.Spans.Add(new Span()
                {
                    Text = item.id_teman + " ",
                    FontSize = 6 * App.DpiFontSize,
                });
            }
            return form;
        }
    }
}