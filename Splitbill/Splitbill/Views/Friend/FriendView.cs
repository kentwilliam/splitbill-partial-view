﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Model.Send;
using Splitbill.Model.SuggestFriend;
using Syncfusion.DataSource.Extensions;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Views.Friend
{
    public class FriendView:ContentView
    {
        DataTemplates.Friends.DataTemplateFriends DtFriends = new DataTemplates.Friends.DataTemplateFriends();
        DataTemplates.HomeView.DataTemplateHomeView DtHome = new DataTemplates.HomeView.DataTemplateHomeView();
        ContacsModel contactHp;
        XFShapeView.ShapeView shapeFriend;
        XFShapeView.ShapeView shapeGroup;
        ObservableCollection<Model.Group.GroupDisplay> ListGroupModel = new ObservableCollection<Model.Group.GroupDisplay>();
        List<FindFriendSendModel> ListModelSuggestFriend = new List<FindFriendSendModel>();
        public EventHandler Eve;
        Grid mygrid = new Grid();
        SfListView sflist;
        SfListView suggestFriendsflist;
        Label Judul;
        List<FindFriendSendModel> ListModel = new List<FindFriendSendModel>();
        public FindFriendSendModel selectedModel;
        //dibuat untuk mengambil Data ketika modal close
        public String Nama { get; set; }
        public String NoHp { get; set; }
        public async void InitData()
        {
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async() =>
                {
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });

               
                ListGroupModel.Clear();
                Api.Encode.GetGrousJson js = new Api.Encode.GetGrousJson() {
                    username = App.GlobalDataUser.USERNAME,
                    token = App.GlobalDataUser.TOKEN,
                    id = null,
                };

                #region mendapatkan data Group
                var resp = App._Api.GetDataResponseFromServer("group/groups", JsonConvert.SerializeObject(js));
                String Result = resp.Result;
                if (Result.ToLower() != "no internet")
                {
                    var resu = JsonConvert.DeserializeObject<Api.Decode.GetGroupsJsonRespond>(Result);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (App._Api.ProcessDataOrNot(resu.message, resu.group == null ? false : true))
                        {
                            foreach (var r in resu.group)
                            {
                                Model.Group.GroupDisplay gd = new Model.Group.GroupDisplay() {
                                    IDGROUP = r.idGroup,
                                    DISPLAYNAMAGRUP = r.name.ToUpper(),
                                    ACTUALNAMAGRUP = r.name,
                                    PHOTO = String.IsNullOrEmpty(r.cover_photo) ? "GroupNull.png" : "https://www.urunan.id/splitbill/" + r.cover_photo,
                                };
                                ListGroupModel.Add(gd);
                            }
                        }
                        else
                        {
                            App.MyHomePage.LogOut();
                        }
                    });
                    #endregion
                    #region DataFriends
                    try
                    {
                        var resp1 = App._Api.GetDataResponseFromServer("user/friends_data", JsonConvert.SerializeObject(new Username_Token()
                        {
                            token = App.GlobalDataUser.TOKEN,
                            username = App.GlobalDataUser.USERNAME
                        }));
                        var resu1 = JsonConvert.DeserializeObject<HeadFriendsJsonResponse>(resp1.Result);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ListModel.Clear();
                            if (App._Api.ProcessDataOrNot(resu.message, false) == false)
                            {
                                App.MyHomePage.LogOut();
                            }
                            else
                            {
                                if (resu1.friends.Count > 0)
                                {
                                    foreach (var r in resu1.friends)
                                    {
                                        FindFriendSendModel model = new FindFriendSendModel() {
                                            nama = r.id_teman,
                                            noHp = r.phone_number,
                                            photo = String.IsNullOrEmpty(r.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + r.photo,
                                        };
                                        ListModel.Add(model);
                                    }
                                    sflist.ItemsSource = null;
                                    sflist.ItemsSource = ListModel;
                                    sflist.HeightRequest = ListModel.Count * 70;
                                }
                            }

                        });
                    }
                    catch (Exception ex)
                    {
                        App.MyHomePage.ShowAlert(ex.Message);
                    }
                    #endregion
                    #region Data SuggestFriend
                    Splitbill.Api.Encode.SuggestFriendsJson js1 = new Api.Encode.SuggestFriendsJson()
                    {
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME
                    };
                    foreach (var contact in contactHp.Contacts)
                    {
                        if (contact.Phones.Count > 0)
                        {
                            js1.phone_number.Add(contact.Phones[0].Number);
                        }
                    }
                    try
                    {
                        var resp2 = App._Api.GetDataResponseFromServer("user/suggest_friend", JsonConvert.SerializeObject(js1));
                        var resu2 = JsonConvert.DeserializeObject<SuggestFriendJsonResponse>(resp2.Result);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (App._Api.ProcessDataOrNot(resu2.message, resu2.contact == null ? false : true))
                            {
                                ListModelSuggestFriend.Clear();
                                if (resu2.contact.Count > 0)
                                {
                                    foreach (var re in resu2.contact)
                                    {
                                        ListModelSuggestFriend.Add(new FindFriendSendModel()
                                        {
                                            nama = re,
                                        });

                                    }
                                    suggestFriendsflist.ItemsSource = null;
                                    suggestFriendsflist.ItemsSource = ListModelSuggestFriend;
                                    suggestFriendsflist.RefreshView();
                                }
                                else
                                {
                                    suggestFriendsflist.ItemTemplate = DtFriends.GetNoSuggestFriendDataTemplate();
                                    suggestFriendsflist.ItemsSource = null;
                                    suggestFriendsflist.ItemsSource = "1";
                                    suggestFriendsflist.RefreshView();
                                }
                            }
                            else
                            {
                                App.MyHomePage.LogOut();
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            App.MyHomePage.ShowAlert(ex.Message);
                        });
                    }
                    #endregion
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        App.MyHomePage.ShowAlert("No Internet Connection");
                    });
                }
            });
            //Tempat 
        }
        public FriendView()
        {
            contactHp = new Model.SuggestFriend.ContacsModel();
            Judul = new Label()
            {
                Text = "My Friends",
                FontSize = 10 * App.DpiFontSize,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalTextAlignment = TextAlignment.Start,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold
            };
            ScrollView Friends = new ScrollView();
            shapeFriend = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Margin = new Thickness(15, 0, 15, 0),
                Color = Color.Transparent
            };
            shapeGroup = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Color = Color.Transparent,
                Margin = new Thickness(15, 15, 15, 0),
                TranslationX = 500,
            };
            
            #region Create Suggest Friend
            StackLayout SuggestFriendStack = new StackLayout() { Margin = new Thickness(0,5,0,0)};
            Label suggestLabel = new Label() {
                Text = "Friend recommendations",
                FontSize = 8 * App.DpiFontSize,
                TextColor = Color.CornflowerBlue,
                Margin = new Thickness(5,0,0,0),
                FontAttributes = FontAttributes.Bold
            };
            Frame suggestFrame = new Frame() {
                Margin = 0,
                Padding = 0,
                IsClippedToBounds = true,
                CornerRadius = 5
            };
            suggestFriendsflist = new SfListView()
            {
                HeightRequest = 300,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,
                Margin = new Thickness(5, 0, 5, 0),
                SelectionBackgroundColor = Color.Transparent,
                ItemTemplate = DtFriends.GetSuggestFriendsDataTemplate(),
                ItemSize = 70
            };
            SuggestFriendStack.Children.Add(suggestLabel);
            SuggestFriendStack.Children.Add(suggestFrame);
            suggestFrame.Content = suggestFriendsflist;
            suggestFriendsflist.IsClippedToBounds = true;
            
            #endregion

            #region create Friend
            Frame friendFrame = new Frame() {
                Margin = 0,
                Padding = 0,
                IsClippedToBounds = true,
                CornerRadius = 5
            };
            StackLayout search = new StackLayout() { Orientation = StackOrientation.Vertical };
            StackLayout Wrapper = new StackLayout();
            Label FriendsLabel = new Label() {
                Text = "Friends",
                TextColor = Color.CornflowerBlue,
                FontSize = 8 * App.DpiFontSize,
                Margin = new Thickness(5,0,0,0),
                FontAttributes = FontAttributes.Bold
            };
            sflist = new SfListView()
            {
                SelectionBackgroundColor = Color.Transparent,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,
                Margin = new Thickness(5, 0, 5, 0),
                ItemTemplate = new DataTemplate(()=> 
                {
                    StackLayout wrapper = new StackLayout() { Orientation = StackOrientation.Horizontal,Margin = new Thickness(0,5,0,5)};
                    CachedImage ciImg = new CachedImage()
                    {
                        Margin = new Thickness(10, 0, 0, 0),
                        WidthRequest = 50,
                        HeightRequest = 50,
                        Aspect = Aspect.AspectFill,
                        DownsampleToViewSize = true
                    };
                    ciImg.SetBinding(CachedImage.SourceProperty, "photo");
                    ciImg.Transformations.Add(new CircleTransformation());

                    StackLayout mainStack = new StackLayout() { Orientation = StackOrientation.Vertical };
                    Label Nama = new Label() { TextColor = Color.Black , FontSize = 8 * App.DpiFontSize, };
                    Nama.SetBinding(Label.TextProperty, "nama");

                    Label nomorTelpon = new Label() { TextColor = Color.Black , FontSize = 8 * App.DpiFontSize, };
                    nomorTelpon.SetBinding(Label.TextProperty, "noHp");

                    mainStack.Children.Add(Nama); mainStack.Children.Add(nomorTelpon);


                    wrapper.Children.Add(ciImg);
                    wrapper.Children.Add(mainStack);

                    StackLayout OuterWrapper = new StackLayout() { HeightRequest = 69 };
                    OuterWrapper.Children.Add(wrapper);
                    OuterWrapper.Children.Add(new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 });
                    return OuterWrapper;
                }),
                RightSwipeTemplate = new DataTemplate(()=> {
                    var grid = new Grid();
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                    var grid1 = new Grid()
                    {
                        BackgroundColor = Color.FromRgb(216, 13, 13),
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Fill
                    };

                    var gridEdit = new Grid()
                    {
                        BackgroundColor = Color.FromRgb(0, 162, 232),
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Fill
                    };


                    var favoriteGrid = new Grid() { HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center };
                    var TrashImage = new Image() {WidthRequest = 15 * App.DpiFontSize,HeightRequest = 15 * App.DpiFontSize };

                    TrashImage.Source = "Delete.png";
                    TrashImage.BindingContextChanged += RightImage_BindingContextChanged;

                    favoriteGrid.Children.Add(TrashImage);

                    grid1.Children.Add(favoriteGrid);
                    grid.Children.Add(grid1, 1, 0);
                    grid.ColumnSpacing = 0;
                    return grid;
                }),
                AllowSwiping = true,
                ItemSize = 70,
                IsClippedToBounds = true
            };
            
            sflist.SwipeStarted += DeListFriend_SwipeStarted;
            sflist.SwipeEnded += DeListFriend_SwipeEnded;
           
            friendFrame.Content = sflist;
            search.Children.Add(FriendsLabel);
            search.Children.Add(friendFrame);
            #endregion

            Wrapper.Children.Add(SuggestFriendStack);
            Wrapper.Children.Add(search);
            shapeFriend.Content = Wrapper;

            mygrid.RowSpacing = 0;
            for (int i = 0; i < 20; i++)
            {
                mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            mygrid.BackgroundColor = Color.FromRgb(234, 237, 244);
            Image backImage = new Image()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Source = "background.png",
                Aspect = Aspect.AspectFill,
            };
            #region buttonLayout
            Frame frameButtonLayout = new Frame()
            {
                HorizontalOptions = LayoutOptions.Center,
                IsClippedToBounds = true,
                CornerRadius = 5,
                Margin = new Thickness(0, 0, 0, 20),
                VerticalOptions = LayoutOptions.Center,
                Padding = 0,
                BackgroundColor = Color.Transparent
            };
            StackLayout ButtonLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                Spacing = 0,
                BackgroundColor = Color.FromRgba(255, 255, 255, 0.5),
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button bFriends = new Button()
            {
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                FontSize = 6 * App.DpiFontSize,
                WidthRequest = 100,
                Text = "Friends",
            };
            bFriends.Clicked += BFriends_Click;

            Button bGroup = new Button()
            {
                WidthRequest = 100,
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                FontSize = 6 * App.DpiFontSize,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Group"
            };
            bGroup.Clicked += BGroup_Clicked;

            BoxView bx = new BoxView()
            {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.Fill,
                WidthRequest = 0.5f
            ,
                Margin = new Thickness(0, 2, 0, 2)
            };

            ButtonLayout.Children.Add(bFriends);
            ButtonLayout.Children.Add(bx);
            ButtonLayout.Children.Add(bGroup);
            #endregion

            Frame ListGroupFrame = new Frame() {
                Margin = 0,
                Padding = 0,
                BackgroundColor = Color.White,
                CornerRadius = 5,
                IsClippedToBounds = true
            };
            SfListView ListGroup = new SfListView()
            {
                IsClippedToBounds = true,
                ItemSize = 125,
                ItemSpacing = 2,
                SelectionBackgroundColor = Color.LightGray,
                Margin = new Thickness(10, 25, 10, 0),
                Orientation = Orientation.Vertical,
            };
            ListGroup.ItemTemplate = DtHome.GroupListNormalDataTemplate() ;
            ListGroup.HeaderTemplate = DtHome.GroupListGroupHeaderDataTemplate() ;
            ListGroup.HeaderSize = 65;
            ListGroup.ItemTapped += ListGroup_ItemTapped;
            ListGroup.ItemsSource = ListGroupModel;

            ListGroupFrame.Content = ListGroup;
            shapeGroup.Content = ListGroupFrame;


            frameButtonLayout.Content = ButtonLayout;

            Friends.Content = shapeFriend;
            mygrid.Children.Add(backImage, 0, 0);
            mygrid.Children.Add(frameButtonLayout, 0, 5);
            mygrid.Children.Add(Judul, 0, 1);
            mygrid.Children.Add(Friends, 0,7);
            mygrid.Children.Add(shapeGroup, 0, 7);
            Grid.SetRowSpan(shapeGroup, 14);
            Grid.SetRowSpan(Friends, 14);
            Grid.SetRowSpan(Judul, 7);
            Grid.SetRowSpan(frameButtonLayout, 2);
            Grid.SetRowSpan(backImage, 7);
            Content = mygrid;
        }

        private async void BGroup_Clicked(object sender, EventArgs e)
        {
            (sender as Button).IsEnabled = false;
            shapeGroup.TranslateTo(0,0,250);
            shapeFriend.TranslateTo(-500, 0, 250);
            (sender as Button).IsEnabled = true;
        }

        private async void BFriends_Click(object sender, EventArgs e)
        {
            (sender as Button).IsEnabled = false;
            shapeFriend.TranslateTo(0, 0, 250);
            shapeGroup.TranslateTo(500, 0, 250);
            (sender as Button).IsEnabled = true;
        }

        private async void ListGroup_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (e.ItemType != ItemType.Header)
            {
                (sender as SfListView).IsEnabled = false;
                Splitbill.Popup.Group.OpenGroup opengroup = new Popup.Group.OpenGroup((e.ItemData as Model.Group.GroupDisplay).IDGROUP);
                await PopupNavigation.PushAsync(opengroup);
                (sender as SfListView).IsEnabled = true;
            }
        }

        private void SelectFriendButton_Clicked(object sender, EventArgs e)
        {
            selectedModel = sflist.SelectedItem as FindFriendSendModel;   
            Eve(this, EventArgs.Empty);
        }

        Image leftImage;
        private void RightImage_BindingContextChanged(object sender, EventArgs e)
        {
            if (leftImage == null)
            {
                sflist.ResetSwipe();
                leftImage = sender as Image;
                (leftImage.Parent as Xamarin.Forms.View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(DeleteFriend) });
            }
        }
        private async void DeleteFriend()
        {
            var pop = new Popup.Friend.PopupDeleteFriend();
            await PopupNavigation.PushAsync(pop);
            pop.Eve += ((v,e) =>
            {
                var g = leftImage.BindingContext as FindFriendSendModel;
                Api.Encode.SuggestFriendAddFriendJson js = new Api.Encode.SuggestFriendAddFriendJson()
                {
                    username = App.GlobalDataUser.USERNAME,
                    token = App.GlobalDataUser.TOKEN,
                    friend = g.nama
                };

                var resp = App._Api.GetDataResponseFromServer("user/delete_friends", JsonConvert.SerializeObject(js));
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                App.MyHomePage.ShowAlert(resu.message);
                if (resu.status == "true")
                {
                    App.MyHomePage.ToFriendsPage();
                }
                else
                {
                }
            });
        }
        private void DeListFriend_SwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            //indexItem = -1;
        }
        bool swiping;
        private void DeListFriend_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {

            //indexItem = e.ItemIndex;
            swiping = false;
            if (e.SwipeOffset > 50)
            {
                sflist.ResetSwipe();
            }
        }

    }
}
