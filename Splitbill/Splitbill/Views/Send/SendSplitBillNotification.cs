﻿using Splitbill.Model;
using Splitbill.Model.MVVM;
using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Splitbill.Pages;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Splitbill.Popup;

namespace Splitbill.Views.Send
{
    public class SendSplitBillNotification : ContentPage
    {
        public event EventHandler backtoMyTransaction;
        public event EventHandler closemodal;
        String Origin;
        String OriginID;
        //Api.ApiContr ap;
        //MoeneyRequestDetailJsonReceive er;
        double hargaTotal = 0;
        String bulan = "00";
        String tanggal = "00";
        String tahun = "0000";
        DateTime Next;
        String desc = "";
        Label Total;
        Label TotalValue;
        Button schedulePaymentButton;
        //SendSplitillNotificationModel modelForSchedule;
        //NotificationModel m;
        //ObservableCollection<NotificationModel> req;
        Image PreviewGambar = new Image();
        Button Send;
        public async Task InitData(String origin,String originId)
        {
            //Popup.NormalModal.NormalBusyIndicator busy = new Popup.NormalModal.NormalBusyIndicator();
            //await PopupNavigation.PushAsync(busy);
            //try
            //{
            //    await Task.Delay(200);
            //    Origin = origin;
            //    OriginID = originId;
            //    //Proses Data dari API
            //    Api.ApiContr.MoneyRequestDetailJson js = new MoneyRequestDetailJson();
            //    js.origin = Origin;
            //    js.origin_id = OriginID;
            //    js.token = App.GlobalDataUser.TOKEN;
            //    js.username = App.GlobalDataUser.USERNAME;

            //    er = ap.MoneyRequestDetail(js);
            //    if (String.IsNullOrEmpty(er.request.bill_photo) == true)
            //    {
            //        PreviewGambar.Source = "ssgray.png";
            //        PreviewGambar.IsEnabled = false;
            //    }
            //    else
            //    {
            //        PreviewGambar.GestureRecognizers.Add(new TapGestureRecognizer(async (v, e) => {
            //            PopupPreviewImage pop = new PopupPreviewImage("http://www.urunan.id/splitbill/" + er.request.bill_photo);
            //            await PopupNavigation.PushAsync(pop);
            //        }));
            //    }
            //    modelForSchedule = new SendSplitillNotificationModel();
            //    modelForSchedule.token = App.GlobalDataUser.TOKEN;
            //    modelForSchedule.username = App.GlobalDataUser.USERNAME;
            //    modelForSchedule.clicked = false;
            //    modelForSchedule.blueColor = Color.Gray;
            //    modelForSchedule.id_Detail = er.request.idMoney_request_detail;
            //    if (er.request.scheduled_paid_date == "" || er.request.scheduled_paid_date == "0000-00-00" || String.IsNullOrEmpty(er.request.scheduled_paid_date))
            //    {
            //        modelForSchedule.dateSetting = DateTime.Now;
            //    }
            //    else
            //    {
            //        modelForSchedule.dateSetting = DateTime.Parse(er.request.scheduled_paid_date);
            //        modelForSchedule.blueColor = Color.FromRgb(57, 169, 251);
            //        modelForSchedule.clicked = true;
            //    }
            //    modelForSchedule.description = er.request.scheduled_desc;

            //    var dataMenuAssign = er.menu_assign;
            //    var daftarMakanan = dataMenuAssign.Select(x => new { x.name,x.photo_menu}).Distinct();
            //    foreach (var i in daftarMakanan)
            //    {
            //        m = new NotificationModel();
            //        m.NAMAMAKANAN = i.name;
            //        m.TOTALMENU = "IDR "+(dataMenuAssign.Where(x=>x.name == i.name).FirstOrDefault().amount).ToString("n0");
            //        m.photo = i.photo_menu;
            //        FormattedString str = new FormattedString();
            //        var dataTeman = dataMenuAssign.Where(x=>x.name == i.name);
            //        foreach (var nama in dataTeman)
            //        {
            //            if (nama.reciever_id.ToLower() == App.GlobalDataUser.USERNAME.ToLower())
            //            {
            //                str.Spans.Add(new Span() { Text = nama.reciever_id +",", FontAttributes = FontAttributes.Bold,ForegroundColor = Color.Black });
            //            }
            //            else
            //            {
            //                str.Spans.Add(new Span() { Text = nama.reciever_id+ ",", ForegroundColor = Color.LightGray});
            //            }
            //        }
            //        m.FRIENDS = str;
            //        var amountUser = (dataMenuAssign.Where(x => x.name == i.name && x.reciever_id.ToLower() == App.GlobalDataUser.USERNAME.ToLower()).FirstOrDefault());
            //        m.MONEYTOPAY = amountUser == null?"":"IDR "+amountUser.amount_user.ToString("n0");
            //        req.Add(m);
            //    }

            //    Total.Text = "Total IDR: ";
            //    TotalValue.Text = er.request.amount_after.ToString("n0");
            //    //End
            //    schedulePaymentButton.BackgroundColor = modelForSchedule.blueColor;
            //}
            //catch (Exception ex)
            //{

            //}
            //await PopupNavigation.PopAsync();
        }
        public SendSplitBillNotification()
        {  
            //ap = new Api.ApiContr();
            Grid mygrid = new Grid();
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = 0.3 });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Grid ContentGrid = new Grid();
            ContentGrid.ColumnSpacing = 0;
            ContentGrid.RowSpacing = 0;
            ContentGrid.BackgroundColor = Color.FromRgb(57, 169, 251);
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            mygrid.Children.Add(ContentGrid, 0, 0);
            Image iLogo1 = new Image();
            iLogo1.Source = "LogoUrunan.png";
            iLogo1.Aspect = Aspect.AspectFit;
            iLogo1.HorizontalOptions = LayoutOptions.Center;
            iLogo1.VerticalOptions = LayoutOptions.Center;
            ContentGrid.Children.Add(iLogo1, 1, 0);

            Image backIcon = new Image();
            backIcon.Source = "BackIcon.png";
            backIcon.WidthRequest = 35;
            backIcon.HeightRequest = 35;
            backIcon.Margin = new Thickness(0, 5, 5, 5);
            backIcon.Aspect = Aspect.AspectFit;
            backIcon.VerticalOptions = LayoutOptions.Center;
            backIcon.GestureRecognizers.Add(new TapGestureRecognizer(backClick));
            ContentGrid.Children.Add(backIcon, 0, 0);


            Image backbutton = new Image();

            CreateUserInformation(mygrid);
            CreateTab2(mygrid);
            CreateTab3(mygrid);

            DataTemplate MenuDataTemplate = new DataTemplate(() =>
            {
                ImageCircle.Forms.Plugin.Abstractions.CircleImage img = new ImageCircle.Forms.Plugin.Abstractions.CircleImage();
                Label Nama = new Label();
                Label Friends = new Label();
                Label Inputan = new Label();
                Label moneyToPay = new Label();

                Inputan.VerticalOptions = LayoutOptions.FillAndExpand;
                Inputan.HorizontalOptions = LayoutOptions.Fill;
                Inputan.FontSize = 12;
                Inputan.TextColor = Color.LightGray;
                Inputan.Margin = new Thickness(0, 0, 5, 0);
                Grid HorizontalGrid = new Grid();
                StackLayout WrapperStack = new StackLayout();

                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.3, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.2, GridUnitType.Star) });
                HorizontalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.2, GridUnitType.Star) });

                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5, GridUnitType.Star) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.5, GridUnitType.Star) });
                HorizontalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.3) });

                BoxView bb = new BoxView() { BackgroundColor = Color.LightGray };
                HorizontalGrid.Children.Add(bb, 0, 0);
                Grid.SetColumnSpan(bb, 4);

                BoxView bb1 = new BoxView() { BackgroundColor = Color.LightGray };
                HorizontalGrid.Children.Add(bb1, 0, 3);
                Grid.SetColumnSpan(bb1, 4);

                img.Source = "DC.png";
                img.BorderColor = Color.Gray;
                img.BorderThickness = 4;
                img.HeightRequest = 50;
                img.WidthRequest = 50;
                img.Aspect = Aspect.AspectFill;
                img.HorizontalOptions = LayoutOptions.Center;
                img.VerticalOptions = LayoutOptions.Center;

                Nama.HorizontalOptions = LayoutOptions.Fill;
                Nama.VerticalOptions = LayoutOptions.FillAndExpand;
                Nama.FontSize = 14;
                Nama.HeightRequest = 100;
                Nama.TextColor = Color.Black;
                Nama.VerticalTextAlignment = TextAlignment.End;

                Nama.TextColor = Color.Gray;
                img.SetBinding(Image.SourceProperty, "photo");
                Nama.SetBinding(Label.TextProperty, "NAMAMAKANAN");
                Friends.SetBinding(Label.FormattedTextProperty, "FRIENDS");
                Inputan.SetBinding(Label.TextProperty, "TOTALMENU");
                moneyToPay.SetBinding(Label.TextProperty, "MONEYTOPAY");



                HorizontalGrid.Children.Add(img, 0, 1);
                Grid.SetRowSpan(img, 2);

                HorizontalGrid.Children.Add(Nama, 1, 1);

                StackLayout l = new StackLayout();
                l.Margin = new Thickness(0, 3, 0, 0);
                l.Orientation = StackOrientation.Vertical;
                Friends.TextColor = Color.LightGray;
                Friends.VerticalTextAlignment = TextAlignment.End;
                Friends.FontSize = 11;
                l.Children.Add(Friends);

                HorizontalGrid.Children.Add(l, 1, 2);
                Grid.SetColumnSpan(l, 2);

                StackLayout l1 = new StackLayout();
                l1.Margin = new Thickness(0, 5, 10, 0);
                moneyToPay.FontSize = 12;
                moneyToPay.FontAttributes = FontAttributes.Bold;
                moneyToPay.TextColor = Color.Black;
                moneyToPay.VerticalTextAlignment = TextAlignment.End;
                l1.Children.Add(moneyToPay);

                HorizontalGrid.Children.Add(l1, 3, 2);

                Inputan.VerticalTextAlignment = TextAlignment.End;
                HorizontalGrid.Children.Add(Inputan, 2, 1);

                WrapperStack.Children.Add(HorizontalGrid);
                return new ViewCell { View = WrapperStack };
            });

            SfListView sflist = new SfListView();
            sflist.ItemTemplate = MenuDataTemplate;
            //req = new ObservableCollection<NotificationModel>();
            //m = new NotificationModel();

            sflist.SelectionMode = SelectionMode.None;
            sflist.ItemSize = 85;
            //sflist.ItemsSource = req;
            mygrid.Children.Add(sflist,0,6);
            Grid.SetRowSpan(sflist,5);
            CreateLastTab(mygrid);

            StackLayout stak = new StackLayout();
            stak.Orientation = StackOrientation.Vertical;

            StackLayout isi3 = new StackLayout();
            isi3.Orientation = StackOrientation.Horizontal;

            Total = new Label();
            
            Total.HorizontalTextAlignment = TextAlignment.End;
            Total.Margin = new Thickness(0,0,10,0);
            TotalValue = new Label();
            Total.FontSize = 14;
            TotalValue.FontSize = 14;
            TotalValue.Margin = new Thickness(0, 0, 15, 0);
            Total.FontAttributes = FontAttributes.Bold;
            TotalValue.FontAttributes = FontAttributes.Bold;
            TotalValue.TextColor = Color.Black;
            isi3.Children.Add(Total);
            isi3.Children.Add(TotalValue);

            stak.Children.Add(isi3);
            stak.HorizontalOptions = LayoutOptions.EndAndExpand;
            mygrid.Children.Add(stak,0,11);

            mygrid.BackgroundColor = Color.FromHex("#fafafc");
            Content = mygrid;
        }

        private void backClick(Xamarin.Forms.View arg1, object arg2)
        {
            closemodal(new object(), EventArgs.Empty);
            base.OnBackButtonPressed();
        }
        protected override bool OnBackButtonPressed()
        {
            closemodal(new object(), EventArgs.Empty);
            return base.OnBackButtonPressed();
        }
        //CheckBox sw;
        private void CreateLastTab(Grid mygrid)
        {
            StackLayout s = new StackLayout();
            s.Orientation = StackOrientation.Horizontal;
            s.HorizontalOptions = LayoutOptions.Start;

            //sw = new CheckBox();
            //sw.CheckedChanged += Sw_CheckedChanged;
            Label l = new Label();
            l.Text = "I am Agree";
            l.VerticalTextAlignment = TextAlignment.Center;

            //s.Children.Add(sw);
            s.Children.Add(l);
            mygrid.Children.Add(s, 0, 12);

            StackLayout rightTab = new StackLayout();
            rightTab.Orientation = StackOrientation.Horizontal;
            rightTab.HorizontalOptions = LayoutOptions.End;
            schedulePaymentButton = new Button();
            schedulePaymentButton.FontSize = 9;
            schedulePaymentButton.Text = "Schedule Payment";
            schedulePaymentButton.TextColor = Color.White;
            schedulePaymentButton.Margin = new Thickness(0,0,15,0);
            schedulePaymentButton.BackgroundColor = Color.Gray;
            //schedulePaymentButton.HorizontalOptions = LayoutOptions.Fill;
            //schedulePaymentButton.VerticalOptions = LayoutOptions.Fill;
            schedulePaymentButton.Clicked += SchedulePaymentButton_Clicked;
            rightTab.Children.Add(schedulePaymentButton);
            mygrid.Children.Add(rightTab, 0, 12);

            Send = new Button();
            Send.TextColor = Color.White;
            Send.Text = "SEND";
            Send.FontSize = 11;
            Send.BorderRadius = 0;
            Send.TextColor = Color.White;
            Send.Clicked += KIRIMDUIT;
            Send.BackgroundColor = Color.LightGray;
            Send.IsEnabled = true;
            mygrid.Children.Add(Send, 0, 13);
        }

        //private void Sw_CheckedChanged(object sender, XLabs.EventArgs<bool> e)
        //{
        //    if (e.Value == true)
        //    {
                
        //        Send.BackgroundColor = Color.FromRgb(57, 162, 232);
        //    }
        //    else
        //    {  Send.BackgroundColor = Color.LightGray; }
        //}

        private void SchedulePaymentButton_Clicked(object sender, EventArgs e)
        {
            //Popup.PopupSchedulePayment pop = new Popup.PopupSchedulePayment(modelForSchedule);
            //pop.doThis += ScheduleClose;
            //pop.CloseThis += async (v, ex) => {
            //    modelForSchedule = new SendSplitillNotificationModel();
            //    modelForSchedule.blueColor = Color.LightGray;
            //    modelForSchedule.clicked = false;
            //    schedulePaymentButton.BackgroundColor = modelForSchedule.blueColor;
            //    await PopupNavigation.PopAsync();

            //    if (App.From != "notification")
            //    {
            //        var err = ap.CallDeactivateSchedule(new ApproveMoneyRequestPostJson() { id_detail = OriginID, token = App.GlobalDataUser.TOKEN, username = App.GlobalDataUser.USERNAME });
            //        await PopupNavigation.PushAsync(new SplitBill.Popup.NormalModal.NormalModalAlert(err.message));
            //        await Navigation.PopModalAsync();
            //    }
            //    App.From = "";
            //};
            //PopupNavigation.PushAsync(pop);
        }
        private async void ScheduleClose(object sender,EventArgs e)
        {
            //    var obj = (sender as Popup.PopupSchedulePayment);
            //    modelForSchedule = obj.model;
            //    schedulePaymentButton.BackgroundColor = modelForSchedule.blueColor;
            //    modelForSchedule.description = obj.shortDescription.Text;
            //    modelForSchedule.dateSetting = obj.cdp.Date;
            //    schedulePaymentButton.BackgroundColor = Color.FromRgb(57, 169, 251);
            //    await PopupNavigation.PopAsync();
        }
        private async void ScheduleEdited(object sender, EventArgs e)
        {
            //var obj = (sender as Popup.PopupSchedulePayment);
            //Next = obj.cdp.Date;
            //await PopupNavigation.PopAsync();
        }
        private void KIRIMDUIT(object sender, EventArgs e)
        {
            //if (sw.Checked == true && modelForSchedule.clicked == false)
            //{
            //    ApproveMoneyRequestPostJson js = new ApproveMoneyRequestPostJson();
            //    js.username = App.GlobalDataUser.USERNAME;
            //    js.token = App.GlobalDataUser.TOKEN;
            //    js.id_detail = er.request.idMoney_request_detail;

            //    ErrorMessage error = ap.CallAppoveMoney(js);
            //    Popup.PopupAlert al = new Popup.PopupAlert(error.message);
            //    if (error.status == "true")
            //    {
            //        al.DoPush += Berhasil;
            //        App.GlobalDataUser.BALANCEDOUBLEVALUE = App.GlobalDataUser.BALANCEDOUBLEVALUE - Convert.ToDouble(TotalValue.Text);
            //        App.GlobalDataUser.BALANCE = "IDR " + App.GlobalDataUser.BALANCEDOUBLEVALUE.ToString("n0");

            //    }
            //    else
            //    {
            //        al.DoPush += Gagal;
            //    }
            //    Navigation.PushPopupAsync(al);
            //}
            //else if (sw.Checked == true && modelForSchedule.clicked == true)
            //{
            //    ScheduleJson ur = new ScheduleJson();
            //    ur.token = App.GlobalDataUser.TOKEN;
            //    ur.username = App.GlobalDataUser.USERNAME;
            //    ur.scheduled_desc = modelForSchedule.description;
            //    ur.payment_date = modelForSchedule.dateSetting.Date.ToString();
            //    ur.id_detail = er.request.idMoney_request_detail;
            //    var x = ap.CallSetScheduledPayment(ur);

            //    SplitBill.Popup.PopupAlert ale = new Popup.PopupAlert(x.message);
            //    ale.DoPush += Berhasil;
            //    PopupNavigation.PushAsync(ale);
            //}
            //else
            //{
            //    DisplayAlert("I am Agree", "Press 'I am Agree' to Send Money", "ok");
            //}
        }

        private async void Berhasil(object sender, EventArgs e)
        {
            try
            { await Navigation.PopModalAsync(); await PopupNavigation.PopAsync(); }
            catch(Exception ex)
            { }
            closemodal(new object(),EventArgs.Empty);
            //App.globalMainPage.BackToHome();

        }

        private void Gagal(object sender, EventArgs e)
        {
            try
            { PopupNavigation.PopAsync(); }
            catch (Exception ex)
            { }
            closemodal(new object(),EventArgs.Empty);
        }

        private  void CreateTab3(Grid mygrid)
        {
            StackLayout sl = new StackLayout();
            sl.Orientation = StackOrientation.Horizontal;

            StackLayout kiri = new StackLayout();
            kiri.Orientation = StackOrientation.Horizontal;
            kiri.Margin = new Thickness(10, 0, 0, 0);
            Image i = new Image();
            i.Source = "MyTransaction.png";
            i.Aspect = Aspect.AspectFit;
            i.HeightRequest = 35;
            i.WidthRequest = 35;

            Label L = new Label();
            L.Text = "Description";
            L.VerticalTextAlignment = TextAlignment.Center;

            kiri.Children.Add(i);
            kiri.Children.Add(L);
            sl.Children.Add(kiri);


            StackLayout kanan = new StackLayout();
            kanan.Orientation = StackOrientation.Horizontal;
            kanan.HorizontalOptions = LayoutOptions.EndAndExpand;


            DatePicker dp = new DatePicker();
            dp.IsEnabled = false;
            

            PreviewGambar = new Image();
            PreviewGambar.Source = "ss.png";
            PreviewGambar.WidthRequest = 50;
            PreviewGambar.HeightRequest = 50;
            PreviewGambar.Margin = new Thickness(5);


            kanan.Children.Add(dp);
            kanan.Children.Add(PreviewGambar);

            sl.Children.Add(kanan);

            mygrid.Children.Add(sl, 0, 4);
        }

        private void CreateTab2(Grid mygrid)
        {
            StackLayout tab2 = new StackLayout();
            tab2.Orientation = StackOrientation.Horizontal;
            tab2.BackgroundColor = Color.FromHex("#313848");// Color.FromRgb(57, 162, 232);

            Label title1 = new Label();
            title1.Text = "Transaction";
            title1.HorizontalOptions = LayoutOptions.Start;
            title1.Margin = new Thickness(10, 0, 0, 0);
            title1.TextColor = Color.FromRgb(0, 162, 232);
            title1.VerticalTextAlignment = TextAlignment.Center;

            StackLayout userInfoMoneyStack = new StackLayout();
            userInfoMoneyStack.Orientation = StackOrientation.Horizontal;
            userInfoMoneyStack.HorizontalOptions = LayoutOptions.EndAndExpand;

            Image UserInfoMoney = new Image();
            UserInfoMoney.Source = "MoneyIDR.png";
            UserInfoMoney.Margin = new Thickness(3, 3, 3, 3);
            UserInfoMoney.WidthRequest = 35;
            UserInfoMoney.HeightRequest = 35;
            userInfoMoneyStack.Children.Add(UserInfoMoney);

            Label UserInfoMoneyValue = new Label();
            //UserInfoMoneyValue.Text = "  " + App.GlobalDataUser.BALANCE;
            //UserInfoMoneyValue.SetBinding(Label.TextProperty,"BALANCE");
            //UserInfoMoneyValue.BindingContext = App.GlobalDataUser;

            UserInfoMoneyValue.VerticalTextAlignment = TextAlignment.Center;
            UserInfoMoneyValue.TextColor = Color.FromRgb(0, 162, 232);
            userInfoMoneyStack.Children.Add(UserInfoMoneyValue);
            userInfoMoneyStack.Margin = new Thickness(0, 0, 15, 0);

            tab2.Children.Add(title1);
            tab2.Children.Add(userInfoMoneyStack);
            
            mygrid.Children.Add(tab2, 0, 3);
        }

        private void CreateUserInformation(Grid mygrid)
        {
            //Personal Information Detail
            Grid personalGrid = new Grid();
            personalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            personalGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            personalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            personalGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
            personalGrid.Margin = new Thickness(20,0, 0, 0);
            //Set Circle Image
            ImageCircle.Forms.Plugin.Abstractions.CircleImage ciImg = new ImageCircle.Forms.Plugin.Abstractions.CircleImage();
            //ciImg.Source = App.GlobalDataUser.PHOTO;
            ciImg.BorderColor = Color.Gray;
            ciImg.BorderThickness = 1;
            ciImg.HeightRequest = 90;
            ciImg.WidthRequest = 90;
            ciImg.HorizontalOptions = LayoutOptions.Center;
            personalGrid.Children.Add(ciImg, 0, 0);
            Grid.SetRowSpan(ciImg, 2);

            StackLayout userInfoStack = new StackLayout();
            userInfoStack.Orientation = StackOrientation.Horizontal;

            Image UserInforIcon = new Image();
            UserInforIcon.Source = "Friends.png";
            UserInforIcon.WidthRequest = 35;
            UserInforIcon.HeightRequest = 35;
            UserInforIcon.Margin = new Thickness(3, 3, 3, 3);
            userInfoStack.Children.Add(UserInforIcon);

            Label userInfoName = new Label();
            //userInfoName.Text = "  " + App.GlobalDataUser.USERNAME;

            userInfoName.VerticalTextAlignment = TextAlignment.Center;
            userInfoStack.Children.Add(userInfoName);
            userInfoStack.Margin = new Thickness(25, 0, 0, 0);
            personalGrid.Children.Add(userInfoStack, 1, 0);
            Grid.SetRowSpan(userInfoStack,2);
            mygrid.Children.Add(personalGrid, 0, 1);
            Grid.SetRowSpan(personalGrid, 2);
            //End User Information
        }
    }
}