﻿using Splitbill.Model;
using Splitbill.Model.MVVM;
using Splitbill.Model.RequestModel;
using ImageCircle.Forms.Plugin.Abstractions;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Splitbill.Pages;
using System.Collections.ObjectModel;
using SkiaSharp.Views.Forms;
using SkiaSharp;
using FFImageLoading.Forms;
using Splitbill.Popup.Send;
using Splitbill.Model.Send;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using FFImageLoading.Transformations;

namespace Splitbill.Views.Send
{
    public class SendSplitbillView : ContentView
    {
        SKShader GradientShaderTest;
        Editor description;
        ExtendedEntry MyFriends;
        ExtendedEntry MyMoney;
        //public ObservableCollection<RequestFriend> ListTerdaftar;

        SendMoneyModel model = new SendMoneyModel();
        public void ResetForm()
        {
            description.Text = "";
            model = new SendMoneyModel();
            MyFriends.Text = "";
            MyMoney.Text = "";

            //ListTerdaftar.Clear();
        }
        
        public SendSplitbillView()
        {
            Grid mygrid = new Grid() { RowSpacing = 0,BackgroundColor= Color.FromRgb(234, 237, 244) };
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });


            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;
            mygrid.Children.Add(viewMelengkung,0,0);
            Grid.SetRowSpan(viewMelengkung,5);

            CachedImage ciImg1 = new CachedImage()
            {
                HorizontalOptions = LayoutOptions.Center,
                Margin = new Thickness(0, 10, 0, 0),
                WidthRequest = 90,
                HeightRequest = 90,
                DownsampleToViewSize = true
            };
            ciImg1.Transformations.Add(new CircleTransformation());
            ciImg1.SetBinding(CachedImage.SourceProperty, "PHOTO");
            ciImg1.BindingContext = App.GlobalDataUser;

            mygrid.Children.Add(ciImg1, 0, 0);
            Grid.SetRowSpan(ciImg1, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = 8*App.DpiFontSize, FontAttributes = FontAttributes.Bold,TextColor = Color.White };
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;

            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = 8*App.DpiFontSize, VerticalTextAlignment = TextAlignment.Center,TextColor = Color.White };
            jumlah.SetBinding(Label.TextProperty, "BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);


            StackLayout DetailInfo = new StackLayout() { Orientation = StackOrientation.Vertical,BackgroundColor = Color.White,
            Spacing = 1,Margin = new Thickness(10)};
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.FillAndExpand };

            #region tab1
            Label To = new Label()
            {
                Text = " To:",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center
            };
             MyFriends= new ExtendedEntry() {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "Invite Friends",
                TextColor = Color.CornflowerBlue,
                FontSize = 8*App.DpiFontSize
            };
            MyFriends.Completed += (v, e) =>
            {
                (v as ExtendedEntry).Text = (v as ExtendedEntry).Text.First().ToString().ToUpper() + (v as ExtendedEntry).Text.Substring(1);
            };
            MyFriends.Focused += (v, e) => {
                OpenFriend(this, new object());
                MyFriends.Unfocus();
            };
            CachedImage plusFriend = new CachedImage() {
                Aspect = Aspect.AspectFit,
                Source = "Friends.png",
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15
            };
            plusFriend.GestureRecognizers.Add(new TapGestureRecognizer(OpenFriend));
            tab1.Children.Add(To);
            tab1.Children.Add(MyFriends);
            tab1.Children.Add(plusFriend);
            DetailInfo.Children.Add(tab1);

            #endregion
            BoxView separator1 = new BoxView() { BackgroundColor = Color.LightGray ,HeightRequest = 1f};
            DetailInfo.Children.Add(separator1);
            #region tab2
            Label To1 = new Label()
            {
                Text = " Rp:",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 8*App.DpiFontSize
            };
            MyMoney = new ExtendedEntry()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Placeholder = "0",
                TextColor = Color.CornflowerBlue,
                FontSize = 8*App.DpiFontSize
            };
            tab2.Children.Add(To1);
            tab2.Children.Add(MyMoney);
            DetailInfo.Children.Add(tab2);

            #endregion
            BoxView separator2 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f };
            DetailInfo.Children.Add(separator2);
            #region tab3
            Label To2 = new Label()
            {
                Text = " For:",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Start,
                Margin = new Thickness(0,10,0,0),
                FontSize = App.DpiFontSize * 8
            };
            description = new Editor()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 160,
                Text = "",
                TextColor = Color.CornflowerBlue,
                FontSize = App.DpiFontSize * 8
            };
            description.Completed += (v, e) =>
            {
                try
                { (v as Editor).Text = (v as Editor).Text.First().ToString().ToUpper() + (v as Editor).Text.Substring(1); }
                catch (Exception ex)
                { }
                
            };
            tab3.Children.Add(To2);
            tab3.Children.Add(description);
            DetailInfo.Children.Add(tab3);

            #endregion
            BoxView separator3 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1f, VerticalOptions = LayoutOptions.End,Margin = new Thickness(0,0,0,5) };
            DetailInfo.Children.Add(separator3);

            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Color = Color.White,
                CornerRadius = 15,
                Margin = new Thickness(15,10,15,0),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button buttonSendMoney = new Button()
            {
                BackgroundColor = Color.CornflowerBlue,
                Text = "Send",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin = new Thickness(10,5,10,0),
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.End,
                FontSize = 8 * App.DpiFontSize
            };
            buttonSendMoney.Clicked += B_Clicked;
            DetailInfo.Children.Add(buttonSendMoney);

            wrapper.Content = DetailInfo;
            mygrid.Children.Add(wrapper, 0, 5);
            Grid.SetRowSpan(wrapper, 11);
            Content = mygrid;
        }

        private async void B_Clicked(object sender, EventArgs e)
        {
            model.description = description.Text;
            try
            {
                model.amount = Double.Parse(MyMoney.Text);
            }
            catch
            {
                model.amount = 0;
            }
            model.username = App.GlobalDataUser.USERNAME;
            model.token = App.GlobalDataUser.TOKEN;
            if (MyMoney.Text != "0" && MyMoney.Text != "")
            {
                await App.MyHomePage.ShowLoadingPage();
                try
                {
                    //Panggil Api untuk mengirim sms ke Handphone dan langsung munculkan popup message
                    var resp = App._Api.GetDataResponseFromServer("transaction/get_money_send_OTP", JsonConvert.SerializeObject(new Username_Token()
                    {
                        token = App.GlobalDataUser.TOKEN,
                        username = App.GlobalDataUser.USERNAME
                    }));
                    var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                    if (App._Api.ProcessDataOrNot(resu.message, false) == false)
                    {
                        App.MyHomePage.LogOut();
                    }
                    else
                    {
                        if (resu.status == "true")
                        {
                            Popup.Send.PopupOtpSendMoney pop = new PopupOtpSendMoney(model);
                            await PopupNavigation.PushAsync(pop);
                        }
                    }
                }
                catch (Exception ex)
                {
                    App.MyHomePage.ShowAlert("No Internet");
                }
               await App.MyHomePage.HideLoadingPage();
            }
            else
            {
                App.MyHomePage.ShowAlert("Insert Money");
            }
        }
        private async void OpenFriend(Xamarin.Forms.View arg1, object arg2)
        {
            MyFriends.IsEnabled = false;
            Splitbill.Popup.Send.FindFriendsSendMoney friend = new Popup.Send.FindFriendsSendMoney();
            friend.Eve += ModalClose;
            await PopupNavigation.PushAsync(friend);
            MyFriends.IsEnabled = true;
        }
        private async void ModalClose(Object sender,  EventArgs e)
        {
            try
            {
                var mod = (sender as FindFriendsSendMoney).selectedModel;
                model.friend = mod.nama;
                MyFriends.Text = model.friend;
            }
            catch (Exception ex)
            { App.MyHomePage.ShowAlert("No Internet"); }
            await PopupNavigation.PopAsync();
        }
        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            //path.LineTo(0, e.Info.Height-100);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
            //canvas.DrawPath(path, new SKPaint { Shader = GambarShaderTest });
        }
    }
}