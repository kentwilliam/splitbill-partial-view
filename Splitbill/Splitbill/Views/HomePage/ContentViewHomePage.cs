﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Splitbill.Api.Core;
using Splitbill.CustomClass;
using Splitbill.Model.ContentViewHome;
using Splitbill.Popup;
using Syncfusion.ListView.XForms;
using Syncfusion.XForms.TabView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Syncfusion.SfPullToRefresh.XForms;
namespace Splitbill.Views.HomePage
{
   public class ContentViewHomePage:ContentView
    {
        SfPullToRefresh PullabelPage = new SfPullToRefresh();
        DataTemplates.HomeView.DataTemplateHomeView DTHome = new DataTemplates.HomeView.DataTemplateHomeView();//Data Template
        SfListView ListGroup;
        ObservableCollection<Model.Group.GroupDisplay> ListGroupModel = new ObservableCollection<Model.Group.GroupDisplay>();
        SKShader GradientShaderTest;
        SKShader GambarShaderTest;
        Grid myGrid;
        SfTabView tabv;
        Label prevText;

        SfListView FriendsList;
        SfListView MeList;
        Frame fImg;
        CircleImage cImg = new CircleImage();
        Button SuggestFriendBtn = new Button();
        ObservableCollection<Model.MyTransaction.Balance> balanceModel;
        Frame frame2;
        Frame frame1;
        Frame fSuggestFriend;
        ObservableCollection<Model.ContentViewHome.FriendsActivity> mod = new ObservableCollection<Model.ContentViewHome.FriendsActivity>();
        public void InitData()
        {
            GC.Collect();
            Task.Run(() => {
                Device.BeginInvokeOnMainThread(async() =>
                {
                    await PopupNavigation.PushAsync(new Views.Loading.LoadingPopup());
                    await Task.Delay(300);
                });

                #region mendapatkan data Group
                Api.Encode.GetGrousJson js = new Api.Encode.GetGrousJson() {
                    username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN,
                id = null,
            };
               
                var resp = App._Api.GetDataResponseFromServer("group/groups", JsonConvert.SerializeObject(js));
                String result = resp.Result;
                if (result.ToLower() != "no internet")
                {
                    if (!result.ToLower().Contains("expired"))
                    {
                        var resu = JsonConvert.DeserializeObject<Api.Decode.GetGroupsJsonRespond>(result);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (resu.group.Count > 0)
                            {
                                ListGroupModel.Clear();
                                ListGroup.ItemTemplate = DTHome.GroupListNormalDataTemplate();
                                ListGroup.ItemSize = 125;
                                ListGroup.ItemsSource = ListGroupModel;
                                if (App._Api.ProcessDataOrNot(resu.message, resu.group == null ? false : true))
                                {
                                    foreach (var r in resu.group.OrderByDescending(x => x.idGroup))
                                    {
                                        Model.Group.GroupDisplay gd = new Model.Group.GroupDisplay() {
                                            IDGROUP = r.idGroup,
                                        DISPLAYNAMAGRUP = r.name.ToUpper(),
                                        ACTUALNAMAGRUP = r.name,
                                        PHOTO = String.IsNullOrEmpty(r.cover_photo) ? "GroupNull.png" : "https://www.urunan.id/splitbill/" + r.cover_photo,
                                    };
                                        
                                        ListGroupModel.Add(gd);
                                    }
                                }
                            }
                            else
                            {
                                ListGroup.ItemTemplate = DTHome.GroupListOtherDataTemplate();
                                ListGroup.ItemsSource = "!";
                                ListGroup.ItemSize = 250;
                            }
                        });


                        try
                        {
                            #region mendapatkan data actifity friend

                            var resp1 = App._Api.GetDataResponseFromServer("transaction/friend_transaction_history", JsonConvert.SerializeObject(new Username_Token()
                            {
                                username = App.GlobalDataUser.USERNAME,
                                token = App.GlobalDataUser.TOKEN
                            }));
                            var resu1 = JsonConvert.DeserializeObject<Api.Decode.FriendsActivityJsonResponse>(resp1.Result);
                            if (App._Api.ProcessDataOrNot(resu1.message, resu1.timeline == null ? false : true))
                            {
                                for (int i = 0; i < resu1.timeline.Count; i++)
                                {
                                    resu1.timeline[i].photo = string.IsNullOrEmpty(resu1.timeline[i].photo) ? "user72.png" : "https://www.urunan.id/splitbill/" + resu1.timeline[i].photo;
                                }
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    //resu1.timeline.Clear();
                                    if (resu1.timeline.Count > 0)
                                    {
                                        mod.Clear();
                                        foreach (var r in resu1.timeline)
                                        {
                                            FriendsActivity fa = new FriendsActivity();
                                            FormattedString message = new FormattedString();
                                            message.Spans.Add(new Span() { FontSize = 6*App.DpiFontSize,ForegroundColor = Color.CornflowerBlue,FontAttributes = FontAttributes.Bold,Text = r.id_teman});
                                            message.Spans.Add(new Span() { FontSize = 6 * App.DpiFontSize, Text = r.message });
                                            message.Spans.Add(new Span() { FontSize = 6 * App.DpiFontSize, ForegroundColor = Color.CornflowerBlue, FontAttributes = FontAttributes.Bold, Text = r.id_stranger });

                                            FormattedString description = new FormattedString();
                                            description.Spans.Add(new Span() { FontSize = 6 * App.DpiFontSize, Text = String.IsNullOrEmpty(r.description)?"-":"At " });
                                            description.Spans.Add(new Span() { FontSize = 6 * App.DpiFontSize, ForegroundColor = Color.CornflowerBlue, FontAttributes = FontAttributes.Bold, Text = String.IsNullOrEmpty(r.description)?"":r.description });
                                            fa.DESCRIPTION = description;
                                            fa.ID = r.id;
                                            fa.PHOTO = r.photo;
                                            fa.TIME = r.time;
                                            fa.MESSAGE = message;
                                            fa.ID_STRANGER = r.id_stranger;
                                            mod.Add(fa);
                                        }
                                        FriendsList.ItemsSource = null;
                                        FriendsList.ItemsSource = mod;
                                        FriendsList.ItemSize = 70;
                                        FriendsList.ItemTemplate = DTHome.FriendListNormalDataTemplate();
                                        fImg.Content = cImg;
                                        fImg.Margin = 0;
                                        fSuggestFriend.Content = null;
                                    }
                                    else
                                    {
                                        FriendsList.ItemSize = 250;
                                        FriendsList.ItemTemplate = DTHome.FriendListOtherDataTemplate();
                                        FriendsList.ItemsSource = null;
                                        FriendsList.ItemsSource = "1";
                                        fSuggestFriend.Margin = new Thickness(0, 5, 0, 10);
                                        fSuggestFriend.Content = SuggestFriendBtn;
                                        fImg.Content = cImg;
                                        fImg.Margin = 0;
                                    }
                                });
                            }
                            resp1.Dispose();
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                App.MyHomePage.ShowAlert("Error Get Friends Activity");
                            });
                        }


                        try
                        {
                            #region Mendapatkan Data MyTransactionCompleted
                            var resp2 = App._Api.GetDataResponseFromServer("transaction/my_transaction_history", JsonConvert.SerializeObject(new Username_Token()
                            {
                                token = App.GlobalDataUser.TOKEN,
                                username = App.GlobalDataUser.USERNAME
                            }));
                            var resu2 = JsonConvert.DeserializeObject<Api.Decode.TransactionHistoryJsonResponse>(resp2.Result);
                            if (App._Api.ProcessDataOrNot(resu2.message, false))
                            {
                                var completedData = resu2.log.complete;
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    if (completedData.Count > 0)
                                    {
                                        balanceModel.Clear();
                                        foreach (var item in completedData)
                                        {
                                            FormattedString form = new FormattedString();
                                            if (item.posisi == 0)
                                            {
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.id_teman + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.before + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.bold + " ",
                                                    FontAttributes = FontAttributes.Bold,
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.after + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                            }
                                            if (item.posisi == 1)
                                            {
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.before + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.id_teman + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.bold + " ",
                                                    FontAttributes = FontAttributes.Bold,
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.after + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                            }
                                            if (item.posisi == 2)
                                            {
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.before + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.bold + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.id_teman + " ",
                                                    FontAttributes = FontAttributes.Bold,
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.after + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                            }
                                            if (item.posisi == 3)
                                            {
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.before + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.bold + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.after + " ",
                                                    FontAttributes = FontAttributes.Bold,
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                                form.Spans.Add(new Span()
                                                {
                                                    Text = item.id_teman + " ",
                                                    FontSize = 6 * App.DpiFontSize,
                                                });
                                            }
                                            Splitbill.Model.MyTransaction.Balance dataP = new Model.MyTransaction.Balance()
                                            {
                                                NAMA = item.id_teman,
                                                IDTRANSAKSI = item.id,
                                            };
                                            dataP.MESSAGE = form;
                                            dataP.TIME = item.time;
                                            dataP.PHOTO = String.IsNullOrEmpty(item.photo) ? "user72.png" : "https://www.urunan.id/splitbill/" + item.photo;
                                            dataP.DESCRIPTION = item.description;
                                            balanceModel.Add(dataP);
                                        }

                                        MeList.ItemSize = 75;
                                        MeList.ItemTemplate = DTHome.MeListNormalDataTemplate();
                                        MeList.Focus();

                                    }
                                    else
                                    {
                                        MeList.ItemSize = 250;
                                        MeList.ItemTemplate = DTHome.MeListOtherDataTemplate();
                                        MeList.ItemsSource = "!";
                                    }
                                });
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                App.MyHomePage.ShowAlert("Error Get Transaction");
                            });

                        }

                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            App.MyHomePage.LogOut();
                        });
                    }
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        App.MyHomePage.ShowAlert("No Internet Connection");
                    });
                }
                #endregion

                Device.BeginInvokeOnMainThread(async() =>
                {
                    await Task.Delay(300);
                    try
                    {
                        await PopupNavigation.PopAsync();
                    }
                    catch (Exception ex)
                    { }
                    
                });
            });
            PullabelPage.IsRefreshing = false;
        }
        public ContentViewHomePage()
        {
            balanceModel = new ObservableCollection<Model.MyTransaction.Balance>();
            cImg.Margin = new Thickness(5,10,5,5);
            cImg.BorderColor = Color.White;
            cImg.BorderThickness = 1;
            cImg.Source = "Send_Request.png";
            cImg.Aspect = Aspect.AspectFill;
            cImg.GestureRecognizers.Add(new TapGestureRecognizer(PopupModal));

            SuggestFriendBtn.BackgroundColor = Color.CornflowerBlue;
            SuggestFriendBtn.TextColor = Color.White;
            SuggestFriendBtn.Text = "SUGGEST FRIEND";
            SuggestFriendBtn.WidthRequest = 175;
            SuggestFriendBtn.FontSize = 7*App.DpiFontSize;
            SuggestFriendBtn.Clicked += ((v, e) => { App.MyHomePage.ToFriendsPage(); });
            myGrid = new Grid() {

            RowSpacing = 0,
            BackgroundColor = Color.FromRgb(244, 247, 255),
        };

            for (int i = 0; i < 16; i++)
            {
                myGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }

            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;

            myGrid.Children.Add(viewMelengkung, 0, 0);
            Grid.SetRowSpan(viewMelengkung, 6);

            CachedImage ciImg = new CachedImage()
            {
                HorizontalOptions = LayoutOptions.Center,
                Margin = new Thickness(0, 10, 0, 0),
                WidthRequest = 90,
                HeightRequest = 90,
                LoadingPlaceholder = "GetImage",
                DownsampleToViewSize = true,
            };
            ciImg.Transformations.Add(new CircleTransformation());
            ciImg.SetBinding(CachedImage.SourceProperty,"PHOTO");
            ciImg.BindingContext = App.GlobalDataUser;
            ciImg.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                await App.MyHomePage.ToEditProfile();
            }));
            myGrid.Children.Add(ciImg, 0, 0);
            Grid.SetRowSpan(ciImg, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize * 8, FontAttributes = FontAttributes.Bold, TextColor = Color.White };
            nama.SetBinding(Label.TextProperty,"USERNAME");
            nama.BindingContext = App.GlobalDataUser;
            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize = App.DpiFontSize * 8, VerticalTextAlignment = TextAlignment.Center,TextColor = Color.White };
            jumlah.SetBinding(Label.TextProperty,"BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            myGrid.Children.Add(info, 0, 3);


            ListGroup = new SfListView()
            {
                ItemSize = 90,
                ItemSpacing = 2,
                SelectionBackgroundColor = Color.Transparent,
                Margin = new Thickness(10, 25, 10, 0),
                Orientation = Orientation.Horizontal,
            };

            ListGroup.ItemTemplate = DTHome.GroupListNormalDataTemplate();
            ListGroup.HeaderTemplate = DTHome.GroupListGroupHeaderDataTemplate();
            ListGroup.HeaderSize = 65;
            ListGroup.ItemTapped += ListGroup_ItemTapped;
            ListGroup.Margin = new Thickness(10,20,10,20);
            ListGroup.ItemsSource = ListGroupModel;

            Label x = new Label() { Text = "Groups", VerticalOptions = LayoutOptions.Start, FontSize = 7 * App.DpiFontSize, Margin = new Thickness(10, 0, 0, 10),
            FontAttributes = FontAttributes.Bold};

            myGrid.Children.Add(ListGroup, 0, 6);
            myGrid.Children.Add(x, 0, 6);
            Grid.SetRowSpan(ListGroup, 3);

            StackLayout tabTulisan = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Margin = new Thickness(0,0,0,10)
            };
            Label text1 = new Label() { Text = "Friend", TextColor = Color.CornflowerBlue };
            text1.FontAttributes = FontAttributes.Bold;
            text1.FontSize = App.DpiFontSize * 8;
            text1.GestureRecognizers.Add(new TapGestureRecognizer(FriendsClick));
            Label text2 = new Label() { Text = "Me", Margin = new Thickness(15, 0, 0, 0) };
            text2.FontAttributes = FontAttributes.Bold;
            text2.TextColor = Color.LightGray;
            text2.FontSize = 8 * App.DpiFontSize;
            text2.GestureRecognizers.Add(new TapGestureRecognizer(MeClick));
            prevText = text1;

            tabTulisan.Children.Add(text1);
            tabTulisan.Children.Add(text2);

            myGrid.Children.Add(tabTulisan, 0, 8);
            Grid.SetRowSpan(tabTulisan, 2);
            FriendsList = new SfListView()
            {
               ItemSize = 60,
               BackgroundColor = Color.Transparent,
               SelectionBackgroundColor = Color.Transparent,
               IsClippedToBounds = true,
               Margin = new Thickness(5)
            };
            
            FriendsList.ItemTapped += MyList_ItemSelected;

            MeList = new SfListView()
            {
                ItemSize = 60,
                BackgroundColor = Color.Transparent,
                SelectionBackgroundColor= Color.Transparent,
                IsClippedToBounds = true,
                Margin = new Thickness(5)
            };
            MeList.ItemTapped += MeList_ItemTapped;
            tabv = new SfTabView() {
            HorizontalOptions = LayoutOptions.FillAndExpand,
            VerticalOptions = LayoutOptions.FillAndExpand,
            EnableSwiping = false,
            DisplayMode = TabDisplayMode.NoHeader,
        };
            var tabItems = new TabItemCollection()
            {
                new SfTabItem(){
                    Title = "Friends",
                    Content = FriendsList,
                },
                new SfTabItem(){
                    Title = "Me",
                    Content = MeList
                },
            };
            tabv.Items = tabItems;
            

            frame1 = new Frame()
            {
                Margin = new Thickness(10, 10, 10, 0),
                Padding = 0,
                BackgroundColor = Color.FromRgb(253,253,253),
                CornerRadius = 10,
                HasShadow = false,
            };
            frame2 = new Frame()
            {
                Margin = new Thickness(10, 10, 10, 0),
                Padding = 0,
                BackgroundColor = Color.FromRgb(253,253,253),
                CornerRadius = 10,
                TranslationX = 500,
                HasShadow = false,
            };
            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                ShapeType = XFShapeView.ShapeType.Box,
                Color = Color.White,
                Content = tabv,
            };

            frame1.Content = FriendsList;
            frame2.Content = MeList;
            myGrid.Children.Add(frame1, 0, 9);
            Grid.SetRowSpan(frame1, 7);

            myGrid.Children.Add(frame2, 0, 9);
            Grid.SetRowSpan(frame2, 7);
            fImg = new Frame() { HorizontalOptions = LayoutOptions.CenterAndExpand,Margin = 0,Padding = 0,BackgroundColor = Color.Transparent,HasShadow = false,OutlineColor = Color.Transparent};
            myGrid.Children.Add(fImg, 0, 4);
            Grid.SetRowSpan(fImg, 2);

            fSuggestFriend = new Frame() { HorizontalOptions = LayoutOptions.CenterAndExpand, Margin = 0, Padding = 0, BackgroundColor = Color.Transparent, HasShadow = false, OutlineColor = Color.Transparent };
            myGrid.Children.Add(fSuggestFriend, 0,14);
            Grid.SetRowSpan(fSuggestFriend, 2);

            FriendsList.ItemsSource = mod;
            ListGroup.ItemsSource = ListGroupModel;
            MeList.ItemsSource = balanceModel;


            PullabelPage.PullableContent = myGrid;
            PullabelPage.ProgressBackgroundColor = Color.White;
            PullabelPage.ProgressStrokeColor = Color.CornflowerBlue;
            //PullabelPage.BackgroundColor = Color.LightGray;
            PullabelPage.PullingThreshold = 120;
            PullabelPage.RefreshContentHeight = 50d;
            PullabelPage.RefreshContentThreshold = 50d;
            PullabelPage.RefreshContentWidth = 50d;
            PullabelPage.ProgressStrokeWidth = 5d;

            PullabelPage.Refreshing += PullabelPage_Refreshing;
            Content = PullabelPage;
        }

        private void PullabelPage_Refreshing(object sender, EventArgs e)
        {
            InitData();
        }

        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
          GradientShaderTest = SKShader.CreateLinearGradient(
          new SKPoint(0,0),new SKPoint(e.Info.Width - (e.Info.Width * 0.25f),e.Info.Height/2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) },null,SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
        }

        private async void ListGroup_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            
            if (e.ItemType != ItemType.Header)
            {
                (sender as SfListView).IsEnabled = false;
                Splitbill.Popup.Group.OpenGroup opengroup = new Popup.Group.OpenGroup((e.ItemData as Model.Group.GroupDisplay).IDGROUP);
                await PopupNavigation.PushAsync(opengroup);
                (sender as SfListView).IsEnabled = true;
            }
        }

        private void MeList_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            MeList.SelectedItem = null;
        }

        private void MyList_ItemSelected(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            FriendsList.SelectedItem = null;
        }

        private async void MeClick(View arg1, object arg2)
        {
            prevText.TextColor = Color.LightGray;
            prevText = arg1 as Label;
            prevText.TextColor = Color.CornflowerBlue;
            frame1.TranslateTo(-500, 0, 250);
            frame2.TranslateTo(0, 0, 250);
        }

        private async void FriendsClick(View arg1, object arg2)
        {
            prevText.TextColor = Color.LightGray;
            prevText = arg1 as Label;
            prevText.TextColor = Color.CornflowerBlue;
            frame2.TranslateTo(500, 0, 250);
            frame1.TranslateTo(0, 0, 250);

        }

        private async void PopupModal(View arg1, object arg2)
        {
            (arg1 as CircleImage).IsEnabled = false;
            PopupLogout pop = new PopupLogout();
            await PopupNavigation.PushAsync(pop);
            (arg1 as CircleImage).IsEnabled = true;
        }

    }
}
