﻿using Splitbill.Model;
using Splitbill.Model.RequestModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;


using Splitbill.Model.MVVM;
using Splitbill.Popup;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Net.Http;
using Splitbill.Pages;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;
using Splitbill.Model.RequestModel;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using XLabs.Forms.Controls;
using Newtonsoft.Json;
using Splitbill.Api.Core;
using Splitbill.Api.Encode;
using Com.OneSignal;
using Splitbill.CustomClass;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Splitbill.Popup.Request;

namespace Splitbill.Views.Request
{
    public class Request : ContentView
    {
        DataTemplates.Request.DataTemplateRequest DtRequest = new DataTemplates.Request.DataTemplateRequest();
        CachedImage iconPhoto;
        CachedImage iconCalendar;
        SKShader GradientShaderTest;
        MainPage MP;
        DatePicker da;
        Model.RequestModel.RequestSubRepository req;
        RequestSubModel delistfriendSelected;
        int tax;
        int discount;
        int service;
        String Hashtag;
        ImageSource SourceGambarNota;
        bool isProcessing;
        String PhotoName;
        int selectedIndexDelistFriend;
        Image Photo = new Image();
        TintableButton additionalButton;
        Popup.Request.PopupAdditional additional;
        int ctr = 0;
        Entry locationEntry = new Entry();
        Entry DescriptionEntry;
        public void InitData(ObservableCollection<RequestSubModel> req)
        {
            this.req.REQUEST = req;
            deListFriend.ItemsSource = this.req.REQUEST;
        }
        public void ResetForm()
        {
            TotalEntry.Text = "0";
            Hashtag = "";
            req.REQUEST.Clear();  
            previewImage = null;
            Photo.Source = "TakePhoto.png";
            additionalButton.BackgroundColor = Color.LightGray;
            tax =10;
            service = 5;
            discount = 0;
            iconCalendar.Source = "Calendar.png";
        }
        public Request()
        {
            req = new RequestSubRepository();
            DtRequest.CheckTotakDuit += CheckTotalDuit;
            DtRequest.SetReq(ref req);
            Grid mygrid = new Grid() {
                RowSpacing = 0,
            BackgroundColor = Color.FromRgb(234, 237, 244),
        };
            
            for (int i = 0; i < 16; i++ )
            {mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });}
            SKCanvasView viewMelengkung = new SKCanvasView();
            viewMelengkung.PaintSurface += ViewMelengkung_PaintSurface2;

            mygrid.Children.Add(viewMelengkung, 0, 0);
            Grid.SetRowSpan(viewMelengkung, 5);

            CachedImage ciImg = new CachedImage()
            {
                WidthRequest = 90,
                HeightRequest = 90,
                HorizontalOptions = LayoutOptions.Center,
                Margin = new Thickness(0, 10, 0, 0),
                DownsampleToViewSize = true
            };
            ciImg.Transformations.Add(new CircleTransformation());
            ciImg.SetBinding(CachedImage.SourceProperty, "PHOTO");
            ciImg.BindingContext = App.GlobalDataUser;

            mygrid.Children.Add(ciImg, 0, 0);
            Grid.SetRowSpan(ciImg, 3);

            StackLayout info = new StackLayout() { Orientation = StackOrientation.Vertical, Spacing = 0, HorizontalOptions = LayoutOptions.Center };
            Label nama = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize =  8*App.DpiFontSize, FontAttributes = FontAttributes.Bold,TextColor = Color.White };
            nama.SetBinding(Label.TextProperty, "USERNAME");
            nama.BindingContext = App.GlobalDataUser;
            Label jumlah = new Label() { HorizontalTextAlignment = TextAlignment.Center, FontSize =  8* App.DpiFontSize, VerticalTextAlignment = TextAlignment.Center ,TextColor = Color.White};
            jumlah.SetBinding(Label.TextProperty, "BALANCE");
            jumlah.BindingContext = App.GlobalDataUser;

            info.Children.Add(nama); info.Children.Add(jumlah);
            mygrid.Children.Add(info, 0, 3);

            StackLayout DetailInfo = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                Spacing = 1,
                Margin = new Thickness(10)
            };
            StackLayout tab1 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab2_5 = new StackLayout() { Orientation = StackOrientation.Horizontal };
            StackLayout tab3 = new StackLayout() { Orientation = StackOrientation.Horizontal, VerticalOptions = LayoutOptions.FillAndExpand };
            StackLayout tab4 = new StackLayout() { Orientation = StackOrientation.Horizontal, Margin = new Thickness(0,0, 0, 5) };
            #region tab1
            StackLayout leftTab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,HorizontalOptions = LayoutOptions.StartAndExpand};
            StackLayout rightTab1 = new StackLayout() { Orientation = StackOrientation.Horizontal,HorizontalOptions = LayoutOptions.EndAndExpand };
            Image logo = new Image() {
                WidthRequest = App.DpiFontSize * 15,
                HeightRequest = App.DpiFontSize * 15,
                Source = "HowMuch.png"
            };
            Label howmuch = new Label() {
                Text = "How Much",
                FontSize = 8*App.DpiFontSize,
                VerticalTextAlignment = TextAlignment.Center
            };
            
            Label To = new Label()
            {
                Text = " IDR:",
                HorizontalTextAlignment = TextAlignment.End,
                VerticalOptions = LayoutOptions.Center,
                FontSize = App.DpiFontSize * 8
            };
            TotalEntry = new Label() {Text = "0",
                FontSize = 8 * App.DpiFontSize,
                VerticalTextAlignment = TextAlignment.Center,WidthRequest = 85
            };
            leftTab1.Children.Add(logo);
            leftTab1.Children.Add(howmuch);

            rightTab1.Children.Add(To);
            rightTab1.Children.Add(TotalEntry);

            tab1.Children.Add(leftTab1);
            tab1.Children.Add(rightTab1);
            DetailInfo.Children.Add(tab1);

            #endregion
            BoxView separator1 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 0.5f };
            DetailInfo.Children.Add(separator1);
            #region tab2
            StackLayout LeftSide = new StackLayout() { Orientation = StackOrientation.Horizontal, Spacing = 1 ,HorizontalOptions = LayoutOptions.StartAndExpand};
            DescriptionEntry = new Entry()
            {
                Placeholder = "Description",

                HorizontalOptions = LayoutOptions.Start,
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 8*App.DpiFontSize
            };
            Image LeftLogo = new Image()
            {
                WidthRequest = 15 * App.DpiFontSize,
                HeightRequest = 15* App.DpiFontSize,
                Source = "MyTransaction.png",
            };
            LeftSide.Children.Add(LeftLogo);
            LeftSide.Children.Add(DescriptionEntry);
 

            DatePicker date = new DatePicker() { HorizontalOptions = LayoutOptions.FillAndExpand,IsVisible = false};
            CachedImage iconLocation = new CachedImage() {
                Source = "locationgray.png",
                HeightRequest = 15*App.DpiFontSize,
                WidthRequest = 15*App.DpiFontSize
            };
            iconLocation.GestureRecognizers.Add(new TapGestureRecognizer(async(v,e)=> {
                PopupLocation loc = new PopupLocation(locationEntry.Text);
                loc.Eve += (async(v1,e1) =>
                {
                    locationEntry.Text = v1 as string;
                    if (String.IsNullOrEmpty(v1 as string))
                    {
                        (v as CachedImage).Source = "locationgray.png";
                    }
                    else
                    {
                        (v as CachedImage).Source = "location.png";
                    }
                    await PopupNavigation.PopAsync();
                });

                await PopupNavigation.PushAsync(loc);
            }));
            iconCalendar = new CachedImage() {
                HeightRequest = 15 * App.DpiFontSize,
                WidthRequest = App.DpiFontSize * 15,
                Source = "Calendar.png",
                HorizontalOptions = LayoutOptions.EndAndExpand
            };
            iconCalendar.GestureRecognizers.Add(new TapGestureRecognizer((v,e)=> { date.Focus(); date.DateSelected += Date_DateSelected;  }));
            iconPhoto = new CachedImage()
            {
                WidthRequest = 15 * App.DpiFontSize,
                HeightRequest = 15 * App.DpiFontSize,
                Source = "TakePhoto.png",
                HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            iconPhoto.GestureRecognizers.Add(new TapGestureRecognizer(Open_Photo));
            StackLayout IMAGES = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.EndAndExpand };
            IMAGES.Children.Add(iconLocation);
            IMAGES.Children.Add(iconCalendar);
            IMAGES.Children.Add(iconPhoto);

            tab2.Children.Add(LeftSide);
            tab2.Children.Add(date);
            tab2.Children.Add(IMAGES);
            DetailInfo.Children.Add(tab2);
            BoxView separator2 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 0.5f };
            DetailInfo.Children.Add(separator2);
            #endregion

            #region tab3

            deListFriend = new SfListView();
            DtRequest.deListFriend = deListFriend;
            DataTemplate MenuDataTemplate = DtRequest.DeListFriendDataTemplate();
            deListFriend.RightSwipeTemplate = DtRequest.DeListFriendRightSwipeDataTemplate();
            deListFriend.SwipeEnded += DeListFriend_SwipeEnded;
            deListFriend.SwipeStarted += DeListFriend_SwipeStarted;
            deListFriend.ItemSize = 110;
            deListFriend.IsVisible = true;

            DelistTemplate = MenuDataTemplate;
            deListFriend.ItemSize = 100;
            deListFriend.SelectionBackgroundColor = Color.FromRgb(225, 225, 225);
            

            deListFriend.FooterTemplate = DtRequest.DeListFriendFooterDataTemplate();
            deListFriend.ItemTemplate = MenuDataTemplate;
            deListFriend.ItemsSource = req.REQUEST;
            deListFriend.AllowSwiping = true;
            tab3.Children.Add(deListFriend);

            XFShapeView.ShapeView wrapper = new XFShapeView.ShapeView()
            {
                BorderWidth = 1,
                CornerRadius = 15,
                ShapeType = XFShapeView.ShapeType.Box,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                BorderColor  = Color.Transparent,
                Color = Color.White,
                Margin = new Thickness(10,10,10,0)
            };
            DetailInfo.Children.Add(tab3);
            
            #endregion
            #region tab4
            additionalButton = new TintableButton() {
                HorizontalOptions = LayoutOptions.Start,
                Text = "Additional",
                FontSize = App.DpiFontSize * 6,
                TextColor = Color.Black,
                BorderRadius = 5,
                HeightRequest = 35,
                WidthRequest = 125,
                BackgroundColor = Color.LightGray
            };
            additionalButton.Clicked += (async(v, e) => {
                (v as Button).IsEnabled = false;
                PopupAdditional additional = new Popup.Request.PopupAdditional(tax,service,discount);
                additional.Eve += CloseTaxes;
                await PopupNavigation.PushAsync(additional,true);
                (v as Button).IsEnabled = true;
            });
            Button ManuallButton = new Button()
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Text = "Add Manual",
                FontSize = App.DpiFontSize *6,
                TextColor = Color.Black,
                BorderRadius = 5,
                HeightRequest = 35,
                WidthRequest = 125,
                BackgroundColor = Color.LightGray
            };
            ManuallButton.Clicked += AddManual_Clicked;

            tab4.Children.Add(additionalButton);
            tab4.Children.Add(ManuallButton);
            DetailInfo.Children.Add(tab4);
            #endregion

            Button Requestbtn = new Button()
            {
                BackgroundColor = Color.CornflowerBlue,
                FontSize = 8*App.DpiFontSize,
                Text = "Request",
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            Requestbtn.Clicked += ReqButton_Clicked;
            DetailInfo.Children.Add(Requestbtn);
            wrapper.Content = DetailInfo;
            mygrid.Children.Add(wrapper,0,5);
            Grid.SetRowSpan(wrapper, 11);

            Content = mygrid;
        }

        private void Date_DateSelected(object sender, DateChangedEventArgs e)
        {
            iconCalendar.Source = "CalendarBlue.png";
        }

        async void ProsesRequestAsync()
        {
            await App.MyHomePage.ShowLoadingPage();
            try
            {
                HeadRequestMoneyJson sj = new HeadRequestMoneyJson() {
                    photo = String.IsNullOrEmpty(PhotoName) ? "" : PhotoName,
                tax = tax,
                discount = discount,
                service = service,
                username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN,
                hashtag = Hashtag,
                lat = 0,
                lng = 0,
                place_id = locationEntry.Text,
                description = "",
            };
                double total = 0;
                double totalAfterTax = 0;
                int ctr = 0;
                foreach (var item in req.REQUEST)
                {
                    String Spasi = "";
                    for (int i = 0; i < ctr; i++)
                    {
                        Spasi = Spasi + " ";
                    }
                    DetailHeadRequestMoneyJson det = new DetailHeadRequestMoneyJson() {
                        photo = String.IsNullOrEmpty(item.PhotoName) ? "" : item.PhotoName,
                    item = item.NAMAMAKANAN + Spasi,
                    harga = Convert.ToInt32(item.RUPIAH),
                    qty = 1,
                };
                    
                    List<string> daftarTeman = item.FRIENDS.Split(',').Where(x => !String.IsNullOrWhiteSpace(x)).ToList();
                    for (int i = 0; i < daftarTeman.Count; i++)
                    {
                        daftarTeman[i] = daftarTeman[i].TrimStart().TrimEnd();
                    }
                    det.Friends = daftarTeman;

                    sj.MacamMenu.Add(det);
                    total += Convert.ToInt32(item.RUPIAH);

                    double Didiskon = Convert.ToDouble(item.RUPIAH) - (Convert.ToDouble(item.RUPIAH) * (Convert.ToDouble(discount) / 100));
                    double SetelahDiTax = Didiskon + (Didiskon * Convert.ToInt32(tax) / 100);
                    double SetelahDiServiceCharge = SetelahDiTax + (SetelahDiTax * Convert.ToInt32(service) / 100);
                    totalAfterTax = totalAfterTax + SetelahDiServiceCharge;
                    ctr += 1;
                }
                sj.total = total;
                sj.totalAfterTax = totalAfterTax.ToString("n0");
                var resp = App._Api.GetDataResponseFromServer("transaction/create_money_request_by_json", JsonConvert.SerializeObject(sj));
                var resu = JsonConvert.DeserializeObject<ErrorMessage>(resp.Result);
                if (resu.status == "true")
                {
                    await App.MyHomePage.HideLoadingPage();
                    App.MyHomePage.ShowAlert(resu.message);
                    App.MyHomePage.BackToHome();
                }
                else if (!String.IsNullOrEmpty(resu.message))
                {
                    if (resu.message.ToLower().Contains("expired"))
                    {
                        App.MyHomePage.LogOut();
                    }
                }
            }
            catch (Exception ex)
            {
                App.MyHomePage.ShowAlert("No Internet");
            }
            await App.MyHomePage.HideLoadingPage();
        }
        private async void CloseProcessRequest(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
            //MP.BackToHome();
        }

        private void ReqButton_Clicked(object arg1, EventArgs arg2)
        {
        if (req.REQUEST.Count > 0 && Double.Parse(TotalEntry.Text) > 0)
            {
                OneSignal.Current.SendTag("username", App.GlobalDataUser.USERNAME);
                ProsesRequestAsync();
            }
            else
            {
                App.MyHomePage.ShowAlert("Insert Amount & items");
            }
        }

        private async void AddManual_Clicked(object sender, EventArgs e)
        {
            Popup.Request.PopupAddmanual addmanualPopup = new Popup.Request.PopupAddmanual();
            addmanualPopup.Eve += CloseAddManual;
            (sender as Button).IsEnabled = false;
            await PopupNavigation.PushAsync(addmanualPopup);
            (sender as Button).IsEnabled = true;
        }

        private void CloseAddManual(object sender, EventArgs e)
        {
            String v1 = (sender as Popup.Request.PopupAddmanual).NamaMakanan;
            String v3 = (sender as Popup.Request.PopupAddmanual).Persentase;
            String v2 = (sender as Popup.Request.PopupAddmanual).HargaTotal;
            String v4 = (sender as Popup.Request.PopupAddmanual).Sisa;

            if (!String.IsNullOrEmpty(v1) && !String.IsNullOrEmpty(v2) && !String.IsNullOrEmpty(v3) && !String.IsNullOrEmpty(v4))
            {
                RequestSubModel SubMod = new RequestSubModel() {
                    FRIENDS = App.GlobalDataUser.USERNAME + ",",
                NAMAMAKANAN = v1,
                DiskonYangTraktir = v3,
                RUPIAH = (Convert.ToDouble(v2) * (Convert.ToDouble(v3) / 100)).ToString(),
                USERTRAKTIR = true,
                UANGUSERTRAKTIRAN = (Convert.ToDouble(v2) * (Convert.ToDouble(v3) / 100)).ToString(),
                INDEX = req.REQUEST.Count + "",
            };
                req.AddItem(SubMod);

                SubMod = new RequestSubModel() {
                    RUPIAH = (Convert.ToDouble(v2) - Convert.ToDouble(v4)) + "",
                FRIENDS = "",
                NAMAMAKANAN = v1,
                USERTRAKTIR = false,
                INDEX = req.REQUEST.Count + "",
            };
                req.AddItem(SubMod);
                CheckTotalDuit(this,EventArgs.Empty);
            }
            CheckTotalDuit(this,EventArgs.Empty);
            PopupNavigation.PopAsync();
        }
        void CheckTotalDuit(object sender, EventArgs e)
        {
            double tot = 0;
            foreach (var ex in req.REQUEST)
            {
                try
                {
                    double Didiskon = Convert.ToDouble(ex.RUPIAH) - (Convert.ToDouble(ex.RUPIAH) * (Convert.ToDouble(discount) / 100));
                    double SetelahDiTax = Didiskon + (Didiskon * Convert.ToInt32(tax) / 100);
                    double SetelahDiServiceCharge = SetelahDiTax + (SetelahDiTax * Convert.ToInt32(service) / 100);
                    tot += SetelahDiServiceCharge;
                }
                catch (Exception ) { }
                
            }
           TotalEntry.Text = tot.ToString("n0");
        }
        bool opencheckbox = true;

        private async void CloseTaxes(Object sender, EventArgs e)
        {
            try
            {
                tax = Convert.ToInt32((sender as Popup.Request.PopupAdditional).Taxes);
                discount = Convert.ToInt32((sender as Popup.Request.PopupAdditional).Discount);
                service = Convert.ToInt32((sender as Popup.Request.PopupAdditional).ServiceCharge);
                Hashtag = (sender as Popup.Request.PopupAdditional).Hashtag;
                if (tax == 0 && discount == 0 && service == 0)
                {
                    additionalButton.BackgroundColor = Color.LightGray;
                 
                }
                else
                {
                    additionalButton.BackgroundColor = Color.CornflowerBlue;
                 
                    for (int i = 0; i < req.REQUEST.Count(); i++)
                    {
                        Model.RequestModel.RequestSubModel r = new RequestSubModel();
                        r = req.REQUEST[i];
                        double hasilDiscount = Convert.ToDouble(r.RUPIAH) - (Convert.ToDouble(r.RUPIAH) * tax / 100);
                        double hasilTax = hasilDiscount + (hasilDiscount * tax / 100);
                        double hasilService = hasilTax + (hasilTax * service / 100);//Total Semua Habis diproses ada tax, discount, dan service
                        r.SETELAHDISCOUNT = hasilService + "";
                        req.REQUEST[i] = r;
                    }
                }
                opencheckbox = true;
                CheckTotalDuit(this,EventArgs.Empty);
                await PopupNavigation.PopAsync();
            }
            catch (Exception ex)
            { }
            
        }
        public List<RequestFriend> menukontak;
        SfListView deListFriend;
        DataTemplate DelistTemplate;
        Image previewImage;
        private async void Open_Photo(Xamarin.Forms.View arg1, object arg2)
        {
            (arg1 as CachedImage).IsEnabled = false;
            Splitbill.Popup.Request.PopupPhotoRequest photoDialog = new Popup.Request.PopupPhotoRequest(PhotoName);
            photoDialog.Eve += CloseOpenPhoto;
            await PopupNavigation.PushAsync(photoDialog);
            (arg1 as CachedImage).IsEnabled = true;
        }
        private async void CloseOpenPhoto(object sender,EventArgs e)
        {
            PhotoName = (sender as Splitbill.Popup.Request.PopupPhotoRequest).PhotoName;
            if (!String.IsNullOrEmpty(PhotoName))
            {
                iconPhoto.Source = "PhotoSign.png";
            }
            await PopupNavigation.PopAsync();
            await PopupNavigation.RemovePageAsync(sender as PopupPhotoRequest,true);
        }
        
        Label TotalEntry;
        int indexItem = -1;
        private void DeListFriend_SwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            indexItem = -1;
            DtRequest.indexItem = indexItem;
        }
        bool swiping;
        private void DeListFriend_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {

            indexItem = e.ItemIndex;
            DtRequest.indexItem = indexItem;
            swiping = false;
            if (e.SwipeOffset > 50)
            {
                deListFriend.ResetSwipe();
            }
        }
       
        private void HeaderLabel_Clicked(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ViewMelengkung_PaintSurface2(object sender, SKPaintSurfaceEventArgs e)
        {
            GradientShaderTest = SKShader.CreateLinearGradient(
            new SKPoint(0, 0), new SKPoint(e.Info.Width - (e.Info.Width * 0.25f), e.Info.Height / 2), new[] { new SKColor(133, 166, 226), new SKColor(127, 204, 220) }, null, SKShaderTileMode.Clamp);

            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Transparent);

            SKRect rec = new SKRect(0, e.Info.Height - 100, e.Info.Width, e.Info.Height);
            SKPath path = new SKPath();
            path.MoveTo(0, 0);
            path.LineTo(e.Info.Width, 0);
            path.LineTo(e.Info.Width, e.Info.Height - 100);
            path.ArcTo(rec, 0, 180, false);
            //path.LineTo(0, e.Info.Height-100);

            path.Close();
            canvas.DrawPath(path, new SKPaint { Shader = GradientShaderTest, Style = SKPaintStyle.StrokeAndFill, IsAntialias = true, StrokeWidth = 2 });
            //canvas.DrawPath(path, new SKPaint { Shader = GambarShaderTest });
        }
    }
}