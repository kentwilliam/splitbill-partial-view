﻿using Rg.Plugins.Popup.Pages;
using Syncfusion.SfBusyIndicator.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill.Views.Loading
{
	public class LoadingPopup : PopupPage
	{
		public LoadingPopup (String loadingName = "Get Data")
		{
            SfBusyIndicator busy = new SfBusyIndicator() {
                AnimationType = AnimationTypes.Ball,
                Title = loadingName,
                BackgroundColor = Color.FromRgba(0,0,0,0.25),
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                IsBusy =true
            };
            Content = busy;
		}
	}
}