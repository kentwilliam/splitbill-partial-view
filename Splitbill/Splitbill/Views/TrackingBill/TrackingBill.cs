﻿using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Converter;
using Splitbill.Model.TransactionHistory;
using Syncfusion.DataSource.Extensions;
using Syncfusion.ListView.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Splitbill.Views.TrackingBill
{
	public class TrackingBill : ContentView
	{
        SfListView sflist;
        SfListView sflistLend;
        Frame FrameSfListView;
        Frame FrameSfListViewLend;
        public void InitData()
        { 
            Username_Token js = new Username_Token()
            {
                username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN
            };
            var resp = App._Api.GetDataResponseFromServer("transaction/history",JsonConvert.SerializeObject(js));
            var resu = resp.Result;

            try
            {
                if (resu != "no internet")
                {
                    var hasilParsing = JsonConvert.DeserializeObject<TransactionHistoryResponseJson>(resu);
                    ObservableCollection<TransactionHistoryModel> SfModel = new ObservableCollection<TransactionHistoryModel>();
                    ObservableCollection<TransactionHistoryModel> SfModelLend = new ObservableCollection<TransactionHistoryModel>();
                    TransactionHistoryModel mod;
                    
                    foreach (var ow in hasilParsing.owe)
                    {
                        ow.data = ow.data.OrderBy(x => x.status == 1).ToList();
                        foreach (var owd in ow.data)
                        {
                            mod = new TransactionHistoryModel();
                            
                            mod.TOPSTACK = ow.username+"#"+ow.unpaid_amount;
                            mod.BOTTOMSTACK = ow.total_amount+"#"+ow.paid_amount+"#"+ow.unpaid_amount;
                            mod.USERNAME = ow.username;
                            mod.UNPAID_AMOUNT = ow.unpaid_amount;
                            mod.PHOTO = "https://www.urunan.id/splitbill/" + ow.photo;

                            mod.TOTAL_AMOUNT = ow.total_amount;
                            mod.UNPAID_AMOUNT = ow.unpaid_amount;
                            mod.PAID_AMOUNT = ow.paid_amount;

                            mod.IDMONEY_REQUEST = owd.idMoney_request;
                            mod.IDMONEY_REQUEST_DETAIL = owd.idMoney_request_detail;
                            mod.STATUS = owd.status;
                            mod.Tulisan = owd.status == 1 ? Color.FromHex("#b3b2b2") : Color.Black; 
                            mod.Coretan = owd.status == 1 ? true : false;
                            mod.DATE = owd.date;
                            mod.AMOUNT_AFTER = "Rp."+Convert.ToInt32(owd.amount_after).ToString("n0");
                            SfModel.Add(mod);
                        }
                    }
                    foreach (var ow in hasilParsing.lend)
                    {
                        ow.data = ow.data.OrderBy(x => x.status == 1).ToList();
                        foreach (var owd in ow.data)
                        {
                            mod = new TransactionHistoryModel();
                            mod.TOPSTACK = ow.username + "#" + ow.unpaid_amount;
                            mod.BOTTOMSTACK = ow.total_amount + "#" + ow.paid_amount + "#" + ow.unpaid_amount;
                            mod.USERNAME = ow.username;
                            mod.UNPAID_AMOUNT = ow.unpaid_amount;
                            mod.PHOTO = "https://www.urunan.id/splitbill/" + ow.photo;

                            mod.TOTAL_AMOUNT = ow.total_amount;
                            mod.UNPAID_AMOUNT = ow.unpaid_amount;
                            mod.PAID_AMOUNT = ow.paid_amount;

                            mod.IDMONEY_REQUEST = owd.idMoney_request;
                            mod.IDMONEY_REQUEST_DETAIL = owd.idMoney_request_detail;
                            mod.STATUS = owd.status;
                            mod.Tulisan = owd.status == 1 ? Color.FromHex("#b3b2b2") : Color.Black;
                            mod.Coretan = owd.status == 1 ? true : false;
                            mod.DATE = owd.date;
                            mod.AMOUNT_AFTER = "Rp." + Convert.ToInt32(owd.amount_after).ToString("n0");
                            SfModelLend.Add(mod);
                        }
                    }

                    sflistLend.ItemsSource = SfModelLend;
                    sflist.ItemsSource = SfModel;
                }
            }
            catch (Exception ex)
            {
                var debug = ex.Message;
            }
        }
		public TrackingBill ()
		{
            Grid mygrid = new Grid() { BackgroundColor = Color.FromRgb(234, 237, 244) };
            mygrid.RowSpacing = 0;
            for (int i = 0; i < 20; i++)
            {
                mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }


            #region Membuat Bagian Header
            Label judulPage = new Label()
            {
                VerticalOptions = LayoutOptions.Start,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = 10 * App.DpiFontSize,
                Text = "Transaction Book"
            };
            Image background = new Image()
            {
                Source = "mytransactionBG.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Aspect = Aspect.AspectFill
            };
            Frame frameButtonLayout = new Frame()
            {
                HorizontalOptions = LayoutOptions.Center,
                IsClippedToBounds = true,
                CornerRadius = 5,
                Margin = new Thickness(0, 0, 0, 20),
                VerticalOptions = LayoutOptions.Center,
                Padding = 0,
                BackgroundColor = Color.Transparent
            };
            StackLayout ButtonLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                Spacing = 0,
                BackgroundColor = Color.FromRgba(255, 255, 255, 0.5),
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button BOwe = new Button()
            {
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                FontSize = 6 * App.DpiFontSize,
                WidthRequest = 100,
                Text = "Owe",
            };
            BOwe.Clicked += BOwe_Clicked;

            Button BLend = new Button()
            {
                WidthRequest = 100,
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                FontSize = 6 * App.DpiFontSize,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Lend"
            };
            BLend.Clicked += BLend_Clicked;

            BoxView bx = new BoxView()
            {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.Fill,
                WidthRequest = 0.5f
            ,
                Margin = new Thickness(0, 2, 0, 2)
            };

            ButtonLayout.Children.Add(BOwe);
            ButtonLayout.Children.Add(bx);
            ButtonLayout.Children.Add(BLend);
            #endregion
            #region Bagian SflistView Owe
            sflist = new SfListView() { IsClippedToBounds = true};
            sflist.GroupHeaderSize = 55;
            sflist.GroupHeaderTemplate = new DataTemplate(() =>
            {
                StackLayout Gr = new StackLayout() { Spacing = 0,Orientation = StackOrientation.Horizontal,HeightRequest = 55};
                Binding binding = new Binding("Level");

                binding.Converter = new GroupHeaderConverter();
                Gr.SetBinding(Grid.BackgroundColorProperty, binding);

                Binding bindingText = new Binding("Key");
                bindingText.Converter = new LeftGroupHeaderTextConverter();
                bindingText.ConverterParameter = 0;

                var labelUL = new Label() {
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Start,
                    WidthRequest = 125
                };
                labelUL.SetBinding(Label.FormattedTextProperty, bindingText);
                labelUL.SetBinding(Label.MarginProperty,bindingText);

                Binding bindingRightText = new Binding("Key");
                bindingRightText.Converter = new RightGroupHeaderTextConverter();
                bindingRightText.ConverterParameter = 0;

                Binding PhotoBinding = new Binding("Key");
                PhotoBinding.Converter = new PhotoGrouHeaderConverter();

                CachedImage userPhoto = new CachedImage() {
                    DownsampleToViewSize = true,
                    HeightRequest = 25 * App.DpiFontSize,
                    WidthRequest = 25 * App.DpiFontSize,
                    Transformations = new List<FFImageLoading.Work.ITransformation>() { new CircleTransformation()},
                    Margin = new Thickness(15,0,0,0),
                    Source = "user72.png",
                };
                userPhoto.SetBinding(CachedImage.SourceProperty,PhotoBinding);
                userPhoto.SetBinding(CachedImage.IsVisibleProperty, PhotoBinding);

                var label_UR = new Label() {
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    WidthRequest = 100,
                    Margin = new Thickness(0,0,15,0)
                };
                label_UR.SetBinding(Label.FormattedTextProperty,bindingRightText);

                Gr.Children.Add(userPhoto);
                Gr.Children.Add(labelUL);
                Gr.Children.Add(label_UR);

                Grid wrapper = new Grid()
                {
                    RowSpacing = 0,
                    ColumnSpacing = 0
                };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1,GridUnitType.Star)});
                wrapper.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Absolute) });
                BoxView bx1 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 };
                wrapper.Children.Add(Gr,0,0);
                wrapper.Children.Add(bx1,0,1);
                return new ViewCell() { View = wrapper};
            });
            
            sflist.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor() {
                PropertyName= "TOPSTACK",
                KeySelector = (object obj) => {
                    var item = obj as TransactionHistoryModel;
                    return "TOPSTACK"+","+item.USERNAME + ","+item.PHOTO +","+item.UNPAID_AMOUNT+","+item.PHOTO;
                }
            });
            sflist.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
            {
                PropertyName = "BOTTOMSTACK",
                KeySelector = (object obj) => {
                    var item = obj as TransactionHistoryModel;
                    return "BOTTOMSTACK" + "," + item.UNPAID_AMOUNT +","+ item.PAID_AMOUNT+"," + item.TOTAL_AMOUNT+","+item.PHOTO;
                }
            });
            sflist.ItemTapped += Sflist_ItemTappedAsync;
            sflist.ItemTemplate = new DataTemplate(() =>
            {
                Grid MAGrid = new Grid() {
                    RowSpacing = 0,
                    ColumnSpacing = 0,
                    BackgroundColor = Color.FromHex("#f1efef")
                };
                MAGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2,GridUnitType.Star)});
                MAGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                MAGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                MAGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Absolute) });
                BoxView bx1 = new BoxView() { BackgroundColor = Color.LightGray,HeightRequest = 1 };
                Label left = new Label() { FontSize = 7*App.DpiFontSize,Margin = new Thickness(15,0,0,0),VerticalTextAlignment = TextAlignment.Center};
                left.SetBinding(Label.TextProperty, "DATE");

                Label right = new Label() { FontSize = 7* App.DpiFontSize,VerticalTextAlignment = TextAlignment.Center};
                right.SetBinding(Label.TextProperty, "AMOUNT_AFTER");
                right.SetBinding(Label.TextColorProperty,"Tulisan");
                Binding CoretanBinding = new Binding();
                CoretanBinding.Path = "Coretan";

                BoxView coret = new BoxView() {BackgroundColor = Color.LightGray,HeightRequest=1,VerticalOptions = LayoutOptions.Center ,
                Margin = new Thickness(0,0,25,0)};
                coret.SetBinding(BoxView.IsVisibleProperty,CoretanBinding);
                
                MAGrid.Children.Add(left,0,0);
                MAGrid.Children.Add(right,1, 0);
                MAGrid.Children.Add(coret,1,0);
                MAGrid.Children.Add(bx1,0,1);
                Grid.SetColumnSpan(bx1,2);
                return new ViewCell() { View = MAGrid };
            });
            sflist.GroupHeaderSize = 60;
            sflist.AllowGroupExpandCollapse = true;

            FrameSfListView = new Frame() { Margin = new Thickness(10,10,10,0), Padding = 0, CornerRadius = 5, IsClippedToBounds = true };
            FrameSfListView.Content = sflist;
            #endregion

            #region Bagian sflistview Lend
            sflistLend = new SfListView() { IsClippedToBounds = true };
            sflistLend.GroupHeaderSize = 55;
            sflistLend.GroupHeaderTemplate = new DataTemplate(() =>
            {
                StackLayout Gr = new StackLayout() { Spacing = 0, Orientation = StackOrientation.Horizontal, HeightRequest = 55 };
                Binding binding = new Binding("Level");

                binding.Converter = new GroupHeaderConverter();
                Gr.SetBinding(Grid.BackgroundColorProperty, binding);

                Binding bindingText = new Binding("Key");
                bindingText.Converter = new LeftGroupHeaderTextConverter();
                bindingText.ConverterParameter = 0;

                var labelUL = new Label()
                {
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Start,
                    WidthRequest = 125
                };
                labelUL.SetBinding(Label.FormattedTextProperty, bindingText);
                labelUL.SetBinding(Label.MarginProperty, bindingText);

                Binding bindingRightText = new Binding("Key");
                bindingRightText.Converter = new RightGroupHeaderTextConverter();
                bindingRightText.ConverterParameter = 0;

                Binding PhotoBinding = new Binding("Key");
                PhotoBinding.Converter = new PhotoGrouHeaderConverter();

                CachedImage userPhoto = new CachedImage()
                {
                    DownsampleToViewSize = true,
                    HeightRequest = 25 * App.DpiFontSize,
                    WidthRequest = 25 * App.DpiFontSize,
                    Transformations = new List<FFImageLoading.Work.ITransformation>() { new CircleTransformation() },
                    Margin = new Thickness(15, 0, 0, 0),
                    Source = "user72.png",
                };
                userPhoto.SetBinding(CachedImage.SourceProperty, PhotoBinding);
                userPhoto.SetBinding(CachedImage.IsVisibleProperty, PhotoBinding);

                var label_UR = new Label()
                {
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    WidthRequest = 100,
                    Margin = new Thickness(0, 0, 15, 0)
                };
                label_UR.SetBinding(Label.FormattedTextProperty, bindingRightText);

                Gr.Children.Add(userPhoto);
                Gr.Children.Add(labelUL);
                Gr.Children.Add(label_UR);

                Grid wrapper = new Grid()
                {
                    RowSpacing = 0,
                    ColumnSpacing = 0
                };
                wrapper.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                wrapper.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Absolute) });
                BoxView bx1 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 };
                wrapper.Children.Add(Gr, 0, 0);
                wrapper.Children.Add(bx1, 0, 1);
                return new ViewCell() { View = wrapper };
            });

            sflistLend.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
            {
                PropertyName = "TOPSTACK",
                KeySelector = (object obj) =>
                {
                    var item = obj as TransactionHistoryModel;
                    return "TOPSTACK" + "," + item.USERNAME + "," + item.PHOTO + "," + item.UNPAID_AMOUNT + "," + item.PHOTO;
                }
            });
            sflistLend.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
            {
                PropertyName = "BOTTOMSTACK",
                KeySelector = (object obj) =>
                {
                    var item = obj as TransactionHistoryModel;
                    return "BOTTOMSTACK" + "," + item.UNPAID_AMOUNT + "," + item.PAID_AMOUNT + "," + item.TOTAL_AMOUNT + "," + item.PHOTO;
                }
            });
            sflistLend.ItemTapped += Sflist_ItemTappedAsync;
            sflistLend.ItemTemplate = new DataTemplate(() =>
            {
                Grid MAGrid = new Grid()
                {
                    RowSpacing = 0,
                    ColumnSpacing = 0,
                    BackgroundColor = Color.FromHex("#f1efef")
                };
                MAGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });
                MAGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                MAGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                MAGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Absolute) });
                BoxView bx1 = new BoxView() { BackgroundColor = Color.LightGray, HeightRequest = 1 };
                Label left = new Label() { FontSize = 7 * App.DpiFontSize, Margin = new Thickness(15, 0, 0, 0), VerticalTextAlignment = TextAlignment.Center };
                left.SetBinding(Label.TextProperty, "DATE");

                Label right = new Label() { FontSize = 7 * App.DpiFontSize, VerticalTextAlignment = TextAlignment.Center };
                right.SetBinding(Label.TextProperty, "AMOUNT_AFTER");
                right.SetBinding(Label.TextColorProperty, "Tulisan");
                Binding CoretanBinding = new Binding();
                CoretanBinding.Path = "Coretan";

                BoxView coret = new BoxView()
                {
                    BackgroundColor = Color.LightGray,
                    HeightRequest = 1,
                    VerticalOptions = LayoutOptions.Center,
                    Margin = new Thickness(0, 0, 25, 0)
                };
                coret.SetBinding(BoxView.IsVisibleProperty, CoretanBinding);

                MAGrid.Children.Add(left, 0, 0);
                MAGrid.Children.Add(right, 1, 0);
                MAGrid.Children.Add(coret, 1, 0);
                MAGrid.Children.Add(bx1, 0, 1);
                Grid.SetColumnSpan(bx1, 2);
                return new ViewCell() { View = MAGrid };
            });
            sflistLend.GroupHeaderSize = 60;
            sflistLend.AllowGroupExpandCollapse = true;

            FrameSfListViewLend = new Frame() { Margin = new Thickness(10, 10, 10, 0), Padding = 0, CornerRadius = 5, IsClippedToBounds = true, TranslationX = 2000 };
            FrameSfListViewLend.Content = sflistLend;
            #endregion

            mygrid.Children.Add(background, 0, 0);
            mygrid.Children.Add(judulPage, 0, 1);

            frameButtonLayout.Content = ButtonLayout;
            mygrid.Children.Add(frameButtonLayout, 0, 5);
            mygrid.Children.Add(FrameSfListView, 0,7);
            mygrid.Children.Add(FrameSfListViewLend, 0, 7);

            Grid.SetRowSpan(FrameSfListViewLend, 14);
            Grid.SetRowSpan(FrameSfListView, 14);
            Grid.SetRowSpan(background, 7);
            Grid.SetRowSpan(judulPage, 7);
            Grid.SetRowSpan(frameButtonLayout, 2);
            Content = mygrid;
            InitData();
		}

        private void BOwe_Clicked(object sender, EventArgs e)
        {
            FrameSfListView.TranslateTo(0, 0, 1000);
            FrameSfListViewLend.TranslateTo(3500, 0, 1000);
        }

        private void BLend_Clicked(object sender, EventArgs e)
        {
            FrameSfListView.TranslateTo(-3500, 0, 1000);
            FrameSfListViewLend.TranslateTo(0, 0, 1000);
        }

        private async void Sflist_ItemTappedAsync(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (e.ItemType != ItemType.GroupHeader)
            {
                var debug = e.ItemData as TransactionHistoryModel;
                string idtransaksi = debug.IDMONEY_REQUEST_DETAIL;
                Api.Encode.SendNotificationDetailJson js = new Api.Encode.SendNotificationDetailJson()
                {
                    username = App.GlobalDataUser.USERNAME,
                    token = App.GlobalDataUser.TOKEN,
                    origin_id = idtransaksi,
                    origin = "req"
                };
                if (!debug.Coretan)
                {
                    Popup.MyTransaction.Balance.MyTransactionBalanceOpen dialog = new Popup.MyTransaction.Balance.MyTransactionBalanceOpen(js);
                    await PopupNavigation.PushAsync(dialog);
                }
                else
                {
                    Popup.TrackingBill.TrackingBillDone dialog = new Popup.TrackingBill.TrackingBillDone(js);
                    await PopupNavigation.PushAsync(dialog);
                }
            }
            

        }
    }
}