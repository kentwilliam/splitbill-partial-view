﻿using ImageCircle.Forms.Plugin.Abstractions;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using Splitbill.Api.Core;
using Splitbill.Api.Decode;
using Splitbill.Api.Encode;
using Splitbill.Model.Notification;
using Syncfusion.ListView.XForms;
using Syncfusion.XForms.TabView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splitbill.Views.Notification
{
	public class MyNotification : ContentView
	{
        DataTemplates.Notification.DataTemplateNotification DtNotification = new DataTemplates.Notification.DataTemplateNotification();
        SfTabView sftab;
        Button PrevButton;
        ScrollView CompletedView;
        ScrollView AllView;
        SfListView ALLTab;
        SfListView GroupListView;
        StackLayout AllStack;
        ObservableCollection<Splitbill.Model.Notification.NotificationModel> notificationModelAll;
        ObservableCollection<Splitbill.Model.Notification.NotificationModel> notificationModelHashTag;
        public  void InitData()
        {
             Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async() => {
                    ClearNotification();
                    await PopupNavigation.PushAsync(new Loading.LoadingPopup());
                    await Task.Delay(300);
                });

                var resp = App._Api.GetDataResponseFromServer("transaction/my_notif_hashtag", JsonConvert.SerializeObject(new Username_Token()
                {
                    token = App.GlobalDataUser.TOKEN,
                    username = App.GlobalDataUser.USERNAME
                }));
                String result = resp.Result;
                if (result.ToLower() == "no internet")
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        App.MyHomePage.ShowAlert("No Internet Connection");
                    });
                }
                else
                {
                    try
                    {
                        var resu = JsonConvert.DeserializeObject<NotificationJsonResponse>(result);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (resu.request.Count > 0)
                            {
                                if (App._Api.ProcessDataOrNot(resu.message, resu.request == null ? false : true))
                                {
                                    notificationModelHashTag.Clear();
                                    notificationModelAll.Clear();
                                    var request = resu.request.OrderBy(x => x.read_flag).ToList();
                                    foreach (var item in request)
                                    {
                                        Splitbill.Model.Notification.NotificationModel mod = new Model.Notification.NotificationModel()
                                        {
                                            CONTENT = item.content.Count() > 50 ? item.content.Substring(0, 50) + "..." : item.content,
                                            HASHTAG = String.IsNullOrEmpty(item.hashtag) ? " #No tag" : " #" + item.hashtag,
                                            IDNOTIFICATION = item.origin_id,
                                            ORIGIN = item.origin,
                                            READ_FLAG = item.read_flag == "0" ? true : false,
                                            SENDER_ID = item.sender_id,
                                            USER_ID = item.user_id,
                                            AMOUNT = item.amount == null ? 0 : item.amount.Value,
                                            PHOTO = String.IsNullOrEmpty(item.photo) ? "user72.png" : "http://www.urunan.id/splitbill/" + item.photo,
                                    };
                                        
                                        notificationModelAll.Add(mod);
                                        notificationModelHashTag.Add(mod);
                                    }
                                    ALLTab.HeightRequest = notificationModelAll.Count * 75;
                                    GroupListView.HeightRequest = 75 * notificationModelHashTag.Count + 75;
                                    SetGroupDescriptor();
                                }
                                else
                                {
                                    App.MyHomePage.LogOut();
                                }

                            }
                            else
                            {
                                ALLTab.ItemTemplate = DtNotification.GetNoNotificationDataTemplate();
                                ALLTab.ItemsSource = "1";
                                ALLTab.ItemSize = 125;
                                ALLTab.HeightRequest = 300;
                                GroupListView.ItemTemplate = DtNotification.GetNoNotificationDataTemplate();
                                GroupListView.ItemsSource = "1";
                                GroupListView.ItemSize = 125;
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            App.MyHomePage.ShowAlert(ex.Message);
                        });
                    }
                }


                Device.BeginInvokeOnMainThread(async() => {
                    await Task.Delay(300);
                    try
                    { await PopupNavigation.PopAsync(); }
                    catch (Exception ex)
                    { }
                });
            });
           
        }

        private void ALLTab_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            if (e.ItemType != ItemType.GroupHeader) {
                (e.ItemData as NotificationModel).READ_FLAG = false;
                if (e.ItemType == ItemType.Record && (e.ItemData as NotificationModel).ORIGIN == "req")
                {
                    OpenRequest((e.ItemData as NotificationModel).IDNOTIFICATION, (e.ItemData as NotificationModel).ORIGIN);
                }
                else if (e.ItemType == ItemType.Record && (e.ItemData as NotificationModel).ORIGIN == "snd")
                {
                    OpenSend((e.ItemData as NotificationModel).IDNOTIFICATION, (e.ItemData as NotificationModel).ORIGIN);
                }
            }
        }

        public MyNotification ()
		{
            ClearNotification();
            notificationModelAll = new ObservableCollection<NotificationModel>();
            notificationModelHashTag = new ObservableCollection<NotificationModel>();
            Grid mygrid = new Grid() { BackgroundColor = Color.FromRgb(234, 237, 244)};
            mygrid.RowSpacing = 0;
            for (int i = 0; i < 20; i++)
            {
                mygrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            #region Membuat Bagian Header
            Label judulPage = new Label()
            {
                VerticalOptions = LayoutOptions.Start,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = App.DpiFontSize * 10,
                Text = "My Notification"
            };
            Image background = new Image() {
                Source = "mynotificationBG.png",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Aspect = Aspect.AspectFill
            };
            Frame frameButtonLayout = new Frame()
            {
                HorizontalOptions = LayoutOptions.Center,
                IsClippedToBounds = true,
                CornerRadius = 5,
                Margin = new Thickness(0,0,0,20),
                VerticalOptions = LayoutOptions.Center,
                Padding = 0,
                BackgroundColor = Color.Transparent
            };
            StackLayout ButtonLayout = new StackLayout() {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Center,
                Spacing = 0,
                BackgroundColor = Color.FromRgba(255, 255, 255, 0.5),
                IsClippedToBounds = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Button bPending = new Button() {
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                FontSize = App.DpiFontSize * 6,
                WidthRequest = 100,
                Text = "ALL",
                FontAttributes = FontAttributes.Bold
            };
            bPending.Clicked += BAll_click;

            Button bCompleted = new Button()
            {
                WidthRequest = 100,
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                FontSize = App.DpiFontSize * 6,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Text = "Hashtag"
            };
            bCompleted.Clicked += Bhashtag_clicked;

            BoxView bx = new BoxView() {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.Fill,WidthRequest=0.5f
            ,Margin = new Thickness(0,2,0,2)};

            ButtonLayout.Children.Add(bPending);
            ButtonLayout.Children.Add(bx);
            ButtonLayout.Children.Add(bCompleted);
            #endregion

            #region Page 1
            AllStack = new StackLayout() { Margin = new Thickness(15, 0, 15, 0),TranslationX = 0 };
            #region buat bagian ALL

            Label Judul1 = new Label()
            {
                Text = "ALL",
                FontSize = App.DpiFontSize * 6,
                TextColor = Color.CornflowerBlue,
                FontAttributes = FontAttributes.Bold
            };

            Frame F1 = new Frame() { Margin = 0, Padding = 0, CornerRadius = 5, IsClippedToBounds = true };
            ALLTab = new SfListView()
            {
                ItemsSource = notificationModelAll,
                SelectionBackgroundColor = Color.LightGray,
                HeightRequest = 300,
                ItemSize = 75,
                BackgroundColor = Color.White,
                ItemTemplate = DtNotification.GetAllTabItemDataTemplate()
            };
            ALLTab.ItemTapped += ALLTab_ItemTapped;
            F1.IsClippedToBounds = true;
            F1.Content = ALLTab;

            AllStack.Children.Add(Judul1);
            AllStack.Children.Add(F1);

            #endregion

            AllView = new ScrollView() { VerticalOptions = LayoutOptions.FillAndExpand,BackgroundColor = Color.FromRgb(234, 237, 244) };

            StackLayout MainWrapper = new StackLayout();
            MainWrapper.Children.Add(AllStack);
            AllView.Content = AllStack;
            #endregion

            #region Page 3
            StackLayout CompletedStack = new StackLayout() { Margin = new Thickness(15, 0, 15, 0) };
            #region buat bagian Completed

            Label Judul3 = new Label()
            {
                Text = "HashTag",
                FontSize = App.DpiFontSize * 6,
                TextColor = Color.CornflowerBlue,
                FontAttributes = FontAttributes.Bold
            };

            Frame F3 = new Frame() { Margin = 0, Padding = 0, CornerRadius = 5, IsClippedToBounds = true };
            GroupListView = new SfListView()
            {
                ItemsSource = notificationModelHashTag,
                SelectionBackgroundColor = Color.LightGray,
                HeightRequest = 300,
                ItemSize = 75,
                BackgroundColor = Color.White,
                ItemTemplate = DtNotification.GetGroupListViewDataTemplate()
            };
            GroupListView.GroupHeaderTemplate = DtNotification.GetHeaderNotificationDataTemplate();
            GroupListView.AllowGroupExpandCollapse = true;
            GroupListView.GroupHeaderSize = 51;
            GroupListView.HeaderSize = 0;
            GroupListView.ItemTapped += ALLTab_ItemTapped;
           
            F3.IsClippedToBounds = true;
            F3.Content = GroupListView;

            CompletedStack.Children.Add(Judul3);
            CompletedStack.Children.Add(F3);

            #endregion

            CompletedView = new ScrollView() {TranslationX = 500 };
            CompletedView.VerticalOptions = LayoutOptions.FillAndExpand;
            CompletedView.BackgroundColor = Color.FromRgb(234, 237, 244);
            #endregion
            
            StackLayout MainWrapper4 = new StackLayout();
            MainWrapper4.Children.Add(CompletedStack);
            CompletedView.Content = MainWrapper4;

            mygrid.Children.Add(background,0,0);
            mygrid.Children.Add(judulPage, 0, 1);

            frameButtonLayout.Content = ButtonLayout;
            mygrid.Children.Add(frameButtonLayout, 0,5);
            mygrid.Children.Add(AllStack, 0,7);
            mygrid.Children.Add(CompletedView, 0, 7);
            Grid.SetRowSpan(AllStack, 14);
            Grid.SetRowSpan(CompletedView, 14);
            Grid.SetRowSpan(background, 7);
            Grid.SetRowSpan(judulPage, 7);
            Grid.SetRowSpan(frameButtonLayout, 2);
            Content = mygrid;
			
		}

        private async void BAll_click(object sender, EventArgs e)
        {
            if (PrevButton == null)
            {
                PrevButton = sender as Button;
                (sender as Button).FontAttributes = FontAttributes.Bold;
            }
            else
            {
                PrevButton.FontAttributes = FontAttributes.None;
                (sender as Button).FontAttributes = FontAttributes.Bold;
                PrevButton = sender as Button;
            }
            CompletedView.TranslateTo(500, 0, 350);
            AllStack.TranslateTo(0, 0, 350);
        }

        private async void Bhashtag_clicked(object sender, EventArgs e)
        {
            if (PrevButton == null)
            {
                PrevButton = sender as Button;
                (sender as Button).FontAttributes = FontAttributes.Bold;
            }
            else
            {
                PrevButton.FontAttributes = FontAttributes.None;
                (sender as Button).FontAttributes = FontAttributes.Bold;
                PrevButton = sender as Button;
            }
            AllStack.TranslateTo(-500, 0, 350);
            CompletedView.TranslateTo(0, 0, 350);
           
        }
        
        private void SetGroupDescriptor()
        {
            try
            {
                if (GroupListView.DataSource.GroupDescriptors.Count <= 0)
                {
                    GroupListView.DataSource.GroupDescriptors.Add(new Syncfusion.DataSource.GroupDescriptor()
                    {
                        PropertyName = "ContacName",
                        KeySelector = (object obj1) =>
                        {
                            var ITEM = (obj1 as Splitbill.Model.Notification.NotificationModel);
                            return ITEM.HASHTAG;
                        }
                    });
                }
            }
            catch (Exception ex)
            { }
        }
        public async void OpenRequest(String IDNotification,String Origin)
        {
            AllStack.IsEnabled = false;
            GroupListView.IsEnabled = false;
            SendNotificationDetailJson nota = new SendNotificationDetailJson()
            {
                username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN,
                origin_id = IDNotification,
                origin = Origin
            };
            Popup.Notification.NotificationOpen notif = new Popup.Notification.NotificationOpen(nota);
            await PopupNavigation.PushAsync(notif);
            AllStack.IsEnabled = true;
            GroupListView.IsEnabled = true;
        }
        public async void OpenSend(String IDNotification, String Origin)
        {
            AllStack.IsEnabled = false;
            GroupListView.IsEnabled = false;
            SendNotificationDetailJson nota = new SendNotificationDetailJson()
            {
                username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN,
                origin_id = IDNotification,
                origin = Origin
            };
            Splitbill.Popup.Notification.NotificationOpenSend snd = new Popup.Notification.NotificationOpenSend(nota);
            await PopupNavigation.PushAsync(snd);
            AllStack.IsEnabled = true;
            GroupListView.IsEnabled = true;
        }
        public async void OpenRequestPaid(String IDNotification, String Origin)
        {
            AllStack.IsEnabled = false;
            GroupListView.IsEnabled = false;
            SendNotificationDetailJson nota = new SendNotificationDetailJson()
            {
                username = App.GlobalDataUser.USERNAME,
                token = App.GlobalDataUser.TOKEN,
                origin_id = IDNotification,
                origin = Origin
            };
            Popup.MyTransaction.Pending.MyTransactionPendingOpen notif = new Popup.MyTransaction.Pending.MyTransactionPendingOpen(nota);
            await PopupNavigation.PushAsync(notif);
            AllStack.IsEnabled = true;
            GroupListView.IsEnabled = true;
        }
        public void ClearNotification()
        {
            try
            {
                Android.App.NotificationManager notifApp = (Android.App.NotificationManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.NotificationService);
                var notifList = notifApp.GetActiveNotifications();
                List<int> listdelete = new List<int>();
                for (int i = 0; i < notifList.Count(); i++)
                {
                    if (notifList[i].GroupKey.ToLower().Contains("com.uru.onesignal"))
                    {
                        listdelete.Add(i);
                    }

                }
                foreach (var item in listdelete)
                {
                }
                notifApp.CancelAll();
            }
            catch (Exception ex)
            {

            }
        }
    }
}